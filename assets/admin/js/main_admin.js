var base_url = window.location.origin+"/puya_muebles";
/*script general*/
	jQuery.extend(jQuery.validator.messages, {
	    required: "Este campo es requerido.",
	    remote: "Por favor Arregle este campo.",
	    email: "Ingrese una direccion de email valida.",
	    url: "Ingrese una URL valida.",
	    date: "Ingrese una fecha valida.",
	    dateISO: "Ingrese una fecha (ISO) valida.",
	    number: "Ingrese un numero valido.",
	    digits: "Ingrese solo digitos.",
	    creditcard: "Por favor ingrese un numero valido de tarjeta de credito.",
	    equalTo: "Por favor reingrese el mismo valor.",
	    accept: "Por favor ingrese un valor con extension valida.",
	    maxlength: jQuery.validator.format("Por no ingrese no mas de {0} caracteres."),
	    minlength: jQuery.validator.format("Por favor ingrese al menos {0} caracteres."),
	    rangelength: jQuery.validator.format("Por favor ingrese un valor entre {0} y {1} caracteres."),
	    range: jQuery.validator.format("Por favor ingrese un valor entre {0} y {1}."),
	    max: jQuery.validator.format("El valor ingresado debe ser menor a {0}."),
	    min: jQuery.validator.format("El valor ingresado debe ser mayor a {0}.")
	});
/*fin script general*/
function flashSimple(titulo, mensaje){ 
	$.gritter.add({
	    // (string | mandatory) the heading of the notification
	    title: titulo,
	    // (string | mandatory) the text inside the notification
	    text: mensaje
	});
};

