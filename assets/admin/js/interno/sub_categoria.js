$(document).ready(function(){
$('.jqte-test').jqte();
	//actualizar registros
	$("#btnUpdate").click(updateDataSubCate);

	function updateDataSubCate(){
			
		var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_actualizar")[0]);
			$.ajax({
				url:base_url+"/sub_categoria/editDataSubCategoria",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin actualizar registros

	//insertar registros
	$("#btnAdd").click(inseDataSubCate);

	function inseDataSubCate(){
		var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar")[0]);
			$.ajax({
				url:base_url+"/sub_categoria/guardarDataSubCategoria",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
			
	}
	//fin insertar registros

	//eliminar registro
	$(".btnDelete").click(function(){
		delDataUs($(this).attr("data-val"));	
	});

	function delDataUs(dato){
		$.ajax({
			url:base_url+"/sub_categoria/eliminaDataSubCategoria",
			type:"POST",
			data:{id_sub_categoria:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta['responseText']);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	};
	//fin eliminar registro

});