$(document).ready(function(){

	var fechaInicio = "";
	var fechaFin = "";
	var d = new Date();
	var strDate = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear();
	$( "#datepicker" ).daterangepicker({
        timePicker24Hour: true,
        format:  'DD-MM-YYYY',
        minDate: strDate
	}, function(start, end, label) {
		fechaInicio = start.format('YYYY-MM-DD HH:mm:ss');
		fechaFin = end.format('YYYY-MM-DD 23:59:59');
  		//console.log("New date range selected:" + start.format('YYYY-MM-DD') +" to " + end.format('YYYY-MM-DD') +" (predefined range:" + label +")");
	});

	//actualizar registros
	$("#btnUpdate").click(updateDataCodigoPromo);

	function updateDataCodigoPromo(){
		var form_data = new FormData($("#form_actualizar")[0]);
/*		
		$.ajax({
			url:base_url+"/codigo_promocion/editDataCodigoPromo",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 2000);
			}
		});*/
	}
	//fin actualizar registros

	//insertar registros
	$("#btnAdd").click(inseDataCodigoPromo);

	function inseDataCodigoPromo(){
		var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar")[0]);
			//alert(fechaInicio+" - "+fechaFin);
			form_data.append('fecha_inicio', fechaInicio);
			form_data.append('fecha_fin', fechaFin);
			$.ajax({
				url:base_url+"/codigo_promocion/guardarDataCodigoPromo",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				error:function(jqXHR,textStatus,errorThrown){
					console.log(jqXHR+"\n"+textStatus+"\n"+errorThrown);
					//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
		
	}
	//fin insertar registros

	//eliminar registro
	$(".btnDelete").click(function(){
		delDataCodigoPromo($(this).data("val"));	
	});

	function delDataCodigoPromo(dato){
		$.ajax({
			url:base_url+"/codigo_promocion/eliminaDataCodigoPromo",
			type:"POST",
			data:{id_codigo_promo:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	};
	//fin eliminar registro

});