
//actualizar registros
$("#btnUpdate").click(updateDataUs);

function updateDataUs(){
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/user/editDataUser",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
			
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataUs);

function inseDataUs(){
	var validator = $("#form_agregar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/user/guardarDataUser",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
		
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataUs($(this).attr("data-val"));	
});

function delDataUs(dato){
	$.ajax({
		url:base_url+"/user/eliminaDataUser",
		type:"POST",
		data:{id_usuario:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro