
//actualizar registros
$("#btnUpdate").click(updateDataComuna);

function updateDataComuna(){
	var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/comuna/editDataComuna",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
							location.reload();
						}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
	
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataComuna);

function inseDataComuna(){
	
	var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/comuna/guardarDataComuna",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataComuna($(this).attr("data-val"));	
});

function delDataComuna(dato){
	$.ajax({
		url:base_url+"/comuna/eliminaDataComuna",
		type:"POST",
		data:{id_comuna:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			alert(respuesta);
		},
		complete:function(respuesta){
			location.reload();
		}
	});
};
//fin eliminar registro