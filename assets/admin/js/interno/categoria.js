$('#fotoCategoria').hide();
$('#btnCortarCategorias').hide();
$('#resultFotoCategoria').hide();
$(document).ready(function(){
$('.jqte-test').jqte();

	var x_ax= 0,y_ax= 0,wc=0,hc=0,zx=1,zy=1,imgCategoria, cropper;
	
	//al momento de seleccionar la imagen le asigna el evento cropper para realizar el corte
	function readFile(input,cortecropper,contentResultado,btnCorte,contenedor) {
	  if (input.files) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
	          $('#'+cortecropper).show();
			  $('#'+contentResultado).hide();
	          $('#'+btnCorte).show();
	          $('#'+contenedor).show();
	          $('#'+cortecropper).attr('src', e.target.result);
	          var image = document.getElementById(cortecropper);
	          cropper = new Cropper(image, {
	          	minCropBoxWidth:1220,
	          	minCropBoxHeight:800,
	          	cropBoxResizable:false,
	            aspectRatio: 2.4 / 1
	          });
	        }
	                      
	        reader.readAsDataURL(input.files[0]);
	      }
	      else {
	        $('#'+cortecropper).hide();
	    }
	}

	function makeCropImg(cortecropper,contentResultado){
		cropper.getCroppedCanvas({
		  width: 1220,
		  height: 800,
		  fillColor: '#fff',
		  imageSmoothingEnabled: true,
		  imageSmoothingQuality: 'high',
		}).toBlob(function (blob) {
			var objectURL = URL.createObjectURL(blob);
			$('#'+contentResultado).attr("src",objectURL);
			$('#'+contentResultado).show();
			$('#contentFotoCategoria').hide();
			imgCategoria = blob;
		  	cropper.destroy();

		});
	}

	$('#uploadCategoria').on('change', function () { 
		readFile(this,
		"fotoCategoria",
		"resultFotoCategoria",
		"btnCortarCategorias",
		"contentFotoCategoria"); 
	});
	//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
	$('#btnCortarCategorias').click(function(){
		$(this).hide();
		makeCropImg("fotoCategoria","resultFotoCategoria");
	});
	//actualizar registros
	$("#btnUpdate").click(updateDataCate);

	function updateDataCate(){
		var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_actualizar")[0]);
			form_data.append('imagen_categoria',imgCategoria);
			$.ajax({
				url:base_url+"/categoria/editDataCategory",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin actualizar registros

	//insertar registros
	$("#btnAdd").click(inseDataCate);

	function inseDataCate(){
		var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar")[0]);
			form_data.append('imagen_categoria',imgCategoria);
			$.ajax({
				url:base_url+"/categoria/guardarDataCategory",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				error:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin insertar registros

	//eliminar registro
	$(".btnDelete").click(function(){
		delDataUs($(this).data("val"));	
	});

	function delDataUs(dato){
		$.ajax({
			url:base_url+"/categoria/eliminaDataCategory",
			type:"POST",
			data:{id_categoria:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}

		});
	};
	//fin eliminar registro

});