$('#fotoMarca').hide();
$('#btnCortarMarcas').hide();
$('#resultFotoMarca').hide();
$('.jqte-test').jqte();

	var x_ax= 0,y_ax= 0,wc=0,hc=0,zx=1,zy=1,imgMarca, cropper;
	
	//al momento de seleccionar la imagen le asigna el evento cropper para realizar el corte
	function readFile(input,cortecropper,contentResultado,btnCorte,contenedor) {
	  if (input.files) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
	          $('#'+cortecropper).show();
			  $('#'+contentResultado).hide();
	          $('#'+btnCorte).show();
	          $('#'+contenedor).show();
	          $('#'+cortecropper).attr('src', e.target.result);
	          var image = document.getElementById(cortecropper);
	          cropper = new Cropper(image, {
	          	minCropBoxWidth:400,
	          	minCropBoxHeight:600,
	          	cropBoxResizable:false,
	            aspectRatio: 1 / 1
	          });
	        }
	                      
	        reader.readAsDataURL(input.files[0]);
	      }
	      else {
	        $('#'+cortecropper).hide();
	    }
	}

	function makeCropImg(cortecropper,contentResultado){
		cropper.getCroppedCanvas({
		  width: 600,
		  height: 600,
		  fillColor: '#fff',
		  imageSmoothingEnabled: true,
		  imageSmoothingQuality: 'high',
		}).toBlob(function (blob) {
			var objectURL = URL.createObjectURL(blob);
			$('#'+contentResultado).attr("src",objectURL);
			$('#'+contentResultado).show();
			$('#contentFotoMarca').hide();
			imgMarca = blob;
		  	cropper.destroy();

		});
	}

	$('#uploadMarca').on('change', function () { 
		readFile(this,
		"fotoMarca",
		"resultFotoMarca",
		"btnCortarMarcas",
		"contentFotoMarca"); 
	});
	//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
	$('#btnCortarMarcas').click(function(){
		$(this).hide();
		makeCropImg("fotoMarca","resultFotoMarca");
	});
//actualizar registros
$("#btnUpdate").click(updateDataBrand);

function updateDataBrand(){
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		var form_data = new FormData($("#form_actualizar")[0]);
		form_data.append('imagen_marca',imgMarca);
		$.ajax({
			url:base_url+"/marca/editDataBrand",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
			},
			error:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
	
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataBrand);

function inseDataBrand(){
	var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar")[0]);
			form_data.append('imagen_marca',imgMarca);
			$.ajax({
				url:base_url+"/marca/guardarDataBrand",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
						//alert(respuesta);
				},
				error:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	
	
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataBrand($(this).attr("data-val"));	
});

function delDataBrand(dato){
	$.ajax({
		url:base_url+"/marca/eliminaDataBrand",
		type:"POST",
		data:{id_marca:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
		},
		error:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro