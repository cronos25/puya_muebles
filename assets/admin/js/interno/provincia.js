
//actualizar registros
$("#btnUpdate").click(updateDataProvincia);

function updateDataProvincia(){
		
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/provincia/editDataProvincia",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataProvincia);

function inseDataProvincia(){
	var validator = $("#form_agregar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/provincia/guardarDataProvincia",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
		
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataProvincia($(this).attr("data-val"));	
});

function delDataProvincia(dato){
	$.ajax({
		url:base_url+"/provincia/eliminaDataProvincia",
		type:"POST",
		data:{id_provincia:dato},
		success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro