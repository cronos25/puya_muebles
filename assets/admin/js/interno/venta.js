
$('.jqte-test').jqte();
$('.modal-pagado, .modal-finalizado, .modal-cancelado, .modal-pendiente,.modal-enviado').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('ven') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-footer .setEstado').attr('data-ven',recipient);
});

$('.modal-productos').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var recipient = button.data('ven') // Extract info from data-* attributes
	// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
  	$.ajax({
		url:base_url+"/venta/resumen_prod_venta",
		type:"POST",
		data:{id_ven:recipient},
		dataType : 'json',
		complete:function(respuesta){
  			modal.find('.modal-body').empty();

  			respuesta = $.parseJSON(respuesta['responseText']);
  			var resultadoEstado = "";
  			console.log(respuesta['venta_data']['estado']);
  			switch(respuesta['venta_data']['estado']) {
			    case "0":
					resultadoEstado = ' <span class="label label-warning">esperando pago</span>';
					break;
				case "1":
					resultadoEstado = '<span class="label label-danger">venta cancelada</span>';
					break;
				case "2":
					resultadoEstado = '<span class="label label-success">pagado</span>';
					break;
				case "3":
					resultadoEstado = '<span class="label label-info">enviado / listo para retiro</span>';
					break;
				case "4":
					resultadoEstado = '<span class="label label-primary">finalizado</span>';
					break;
				default:
					resultadoEstado = '<span class="label label-warning">esperando pago</span>';
					break;
			}

  			var maqueta = '<table class="table">'+
		       		'<tr>'+
		           		'<th colspan="2">Datos Cliente</th>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Nombre</td>'+
		       			'<td>'+respuesta['cliente_data']['nombre']+' '+respuesta['cliente_data']['apellido']+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Rut </td>'+
		       			'<td>'+respuesta['cliente_data']['rut']+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Telefono</td>'+
		       			'<td>'+respuesta['cliente_data']['telefono']+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Email</td>'+
		       			'<td>'+respuesta['cliente_data']['email']+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Region</td>'+
		       			'<td>'+respuesta['cliente_data']['region']+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Provincia</td>'+
		       			'<td>'+respuesta['cliente_data']['provincia']+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Comuna</td>'+
		       			'<td>'+respuesta['cliente_data']['comuna']+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Direccion</td>'+
		       			'<td>'+respuesta['cliente_data']['direccion']+'</td>'+
		       		'</tr>'+
		       	'</table>'+
		       	'<table class="table">'+
		       		'<tr>'+
		           		'<th colspan="2">Datos Venta</th>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Total Venta</td>'+
		       			'<td>$'+darformatomiles(respuesta['venta_data']['costo_productos'])+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Total Envio </td>'+
		       			'<td>$'+darformatomiles(respuesta['venta_data']['costo_envio'])+'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Estado Venta</td>'+
		       			'<td>'+
		       				resultadoEstado+
						'</td>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Mensaje</td>'+
		       			'<td>'+respuesta['carro_data']['mensaje']+'</td>'+
		       		'</tr>'+
		       	'</table>'+
		       	'<table class="table table-responsive text-center">'+
		       		'<tr>'+
		           		'<th colspan="3">Datos Productos</th>'+
		       		'</tr>'+
		       		'<tr>'+
		       			'<td>Imagen</td>'+
		       			'<td>Nombre</td>'+
		       			'<td>Cantidad</td>'+
		       			'<td>Color</td>'+
		       			'<td>SKU</td>'+
		       			'<td>Precio</td>'+
		       		'</tr>';
		       		$.each( respuesta['productos_data'], function( index, productoItem ){

		       			var precioFinal = 0;
		       			if (productoItem['asoc_val_desc']>0) {
		       				precioFinal = Math.round(productoItem['asoc_val']-((productoItem['asoc_val']*productoItem['asoc_val_desc'])/100));
		       			}else{
		       				precioFinal = productoItem['asoc_val'];
		       			}

		       			maqueta +='<tr>'+
				       			'<td class="col-md-2"><img src="'+productoItem['imagen_prod']+'" alt="imagen producto" class="img img-responsive"></td>'+
				       			'<td>'+productoItem['nombre']+'</td>'+
				       			'<td>'+productoItem['cantidad']+'</td>'+
				       			'<td>'+productoItem['color']+'</td>'+
				       			'<td>'+productoItem['codigo']+'</td>'+
				       			'<td>$ '+darformatomiles(precioFinal)+'</td>'+
				       		'</tr>';
		       		});
		       		

		       	maqueta +='</table>';


  			modal.find('.modal-body').append(maqueta);
		}
	});

  
});
//actualizar registros
$(".setEstado").click(function(){
	$(this).button('loading');

	if ($(this).data("val")===3) {
		var modal = $(".modal-enviado");
	  	
	  	var codigoseg = modal.find('.modal-body #input_codigo_seguimiento').val();
	  	if (codigoseg == "") {
	  		flashSimple("Atencion !", "debe ingresar un codigo de seguimiento para marcar el producto como enviado");	
	  		$(this).button('reset');
	  	}else{
			setEstadoData(codigoseg,$(this).attr("data-sel"),$(this).attr("data-ven"));	
	  	}
	}else{
		setEstadoData($(this).attr("data-val"),$(this).attr("data-sel"),$(this).attr("data-ven"));	
	}
	
});
function setEstadoData(dato,estado,item){
	$.ajax({
		url:base_url+"/venta/setSaleState",
		type:"POST",
		data:{estado_p:dato,estado_v:estado,ven:item},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);	
		},
		error:function(jqXHR,textStatus,errorThrown){
			console.log(jqXHR);
			alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
}

function darformatomiles(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
//fin actualizar registros

//insertar registros
/*$("#btnAdd").click(inseDataSc);

function inseDataSc(){
	$.ajax({
		url:base_url+"/carro_compra/guardarDatacarro_compra",
		type:"POST",
		data:$("#form_agregar").serialize(),
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			alert(respuesta);
		}
	});
}*/
//fin insertar registros

//eliminar registro
//fin eliminar registro