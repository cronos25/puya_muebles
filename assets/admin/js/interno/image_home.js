$(document).ready(function(){
	//actualizar registros
	$("#btnUpdate").click(updateDataImageHome);

	function updateDataImageHome(){
		var form_data = new FormData($("#form_actualizar")[0]);
		$.ajax({
			url:base_url+"/imagenes_home/editDataImageHome",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				location.reload();
			}
		});
	}
	//fin actualizar registros

	//insertar registros
	$("#btnAdd").click(inseDataImageHome);

	function inseDataImageHome(){
		var form_data = new FormData($("#form_agregar")[0]);
		$.ajax({
			url:base_url+"/imagenes_home/guardarDataImageHome",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			error:function(respuesta){
				flashSimple("Estado Registro", respuesta);

				//alert(respuesta);
			}
		});
	}
	//fin insertar registros

	//eliminar registro
	$(".btnDeleteHome").click(function(){
		delDataImageHome($(this).attr("data-val"));	
	});

	function delDataImageHome(dato){
		$.ajax({
			url:base_url+"/imagenes_home/eliminaDataImageHome",
			type:"POST",
			data:{id_imagen:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
			complete:function(respuesta){
				location.reload();
			}
		});
	};
	//fin eliminar registro

});