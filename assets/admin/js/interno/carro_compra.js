
//actualizar registros
$("#btnUpdate").click(function(){
	var btn = $(this);
	updateDataSc(btn);	
});

function updateDataSc(btn){
	btn.button('loading');
	$.ajax({
		url:base_url+"/carro_compra/editDataShoppingCart",
		type:"POST",
		data:$("#form_actualizar").serialize(),
		success:function(respuesta){
			btn.button('reset');
			alert(respuesta);
		},
		complete:function(respuesta){
			btn.button('reset');
			location.reload();
		}
	});
}
//fin actualizar registros

//insertar registros
/*$("#btnAdd").click(inseDataSc);

function inseDataSc(){
	$.ajax({
		url:base_url+"/carro_compra/guardarDatacarro_compra",
		type:"POST",
		data:$("#form_agregar").serialize(),
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			alert(respuesta);
		}
	});
}*/
//fin insertar registros
/*reenviar correo actual*/
$(".btnResend").click(function(){
	var btn = $(this);
	reSendDataSc($(this).attr("data-val"),btn);	
});
function reSendDataSc(dato,btn){
	btn.button('loading');
	$.ajax({
		url:base_url+"/carro_compra/ReenviaMailDataShoppingCart",
		type:"POST",
		data:{carro:dato},
		success:function(respuesta){
			flashSimple("estado reenvio", "reenviado con exito");
			btn.button('reset');
			alert(respuesta);
		},
		error:function(){
			flashSimple("estado reenvio", "error enviando el correo");
			btn.button('reset');
			alert(respuesta);
		},
		complete:function(respuesta){
			btn.button('reset');
			location.reload();
		}
	});
};

$(".btnReopen").click(function(){
	var btn = $(this);
	reActiveDataSc($(this).attr("data-val"),btn);	
});
function reActiveDataSc(dato,btn){
	btn.button('loading');
	$.ajax({
		url:base_url+"/carro_compra/reActiveDataSc",
		type:"POST",
		data:{carro:dato},
		success:function(respuesta){
			flashSimple("estado reenvio", "reactivado con exito");
			btn.button('reset');
			alert(respuesta);
		},
		error:function(respuesta){
			flashSimple("estado reenvio", "error reactivando el carro");
			btn.button('reset');
			alert(respuesta);
		},
		complete:function(respuesta){
			btn.button('reset');
			location.reload();
		}
	});
};
/*fin reenviar correo actual*/

//eliminar registro
$(".btnDelete").click(function(){
	var btn = $(this);
	delDataSc($(this).attr("data-val"),btn);	
});

function delDataSc(dato,btn){
	btn.button('loading');
	
	$.ajax({
		url:base_url+"/carro_compra/eliminaDataShoppingCart",
		type:"POST",
		data:{carro:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", "carro eliminado");
			alert(respuesta);
		},
		complete:function(respuesta){
				btn.button('reset');
			location.reload();
		}
	});
};
//fin eliminar registro