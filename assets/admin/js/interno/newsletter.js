
//actualizar registros
$("#btnUpdate").click(function(){
	updateDataNewsletter();
});

function updateDataNewsletter(){
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/newsletter/editDataNewsletter",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}

}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(function(){
    	inseDataNewsletter();
});

function inseDataNewsletter(){
		var validator = $("#form_agregar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/newsletter/suscribirNewsletter",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			error:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}

		
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataNewsletter($(this).attr("data-val"));	
});

function delDataNewsletter(dato){
	$.ajax({
		url:base_url+"/newsletter/eliminaDataNewsletter",
		type:"POST",
		data:{email:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		error:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro