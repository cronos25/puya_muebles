$(document).ready(function(){
	var fechaInicio = "";
	var fechaFin = "";
	var d = new Date();
	var strDate = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear();
	var fechaSeleccionada = false;
	$('.select2').select2();
	$("#datepicker").daterangepicker({
        timePicker24Hour: true,
        format:  'DD-MM-YYYY',
        minDate: strDate,
        locale: {
            applyLabel: 'Fijar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'al',
            weekLabel: 'S',
            customRangeLabel: 'Rango Personalizado'
        }
	}, function(start, end, label) {
		fechaInicio = start.format('YYYY-MM-DD HH:mm:ss');
		fechaFin = end.format('YYYY-MM-DD 23:59:59');
		fechaSeleccionada = true;
  		//console.log("New date range selected:" + start.format('YYYY-MM-DD') +" to " + end.format('YYYY-MM-DD') +" (predefined range:" + label +")");
	});
	$('.select2').change(function(){
		//alert($(this).val()+' '+$('.select2').val());
		if($(this).val() != ''|| $('.select2').val() != ''){
			$('.select2').removeAttr('required');
		}else{
			$('.select2').attr('required','required');
		}
	});

	//actualizar registros
	$("#btnUpdate").click(updateDataPromoTemporal);

	function updateDataPromoTemporal(){
		var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_actualizar")[0]);
			//alert(fechaInicio+" - "+fechaFin);
			if (fechaSeleccionada) {
				form_data.append('fecha_inicio', fechaInicio);
				form_data.append('fecha_fin', fechaFin);
			}else{
				form_data.append('fecha_inicio', $("#previoFechaInicio").val());
				form_data.append('fecha_fin', $("#previoFechaFin").val());
			}
			$.ajax({
				url:base_url+"/promocion_temporal/editarDataPromoTemporal",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				error:function(jqXHR,textStatus,errorThrown){
					console.log(jqXHR+"\n"+textStatus+"\n"+errorThrown);
					//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin actualizar registros

	//insertar registros
	$("#btnAdd").click(inseDataPromoTemporal);

	function inseDataPromoTemporal(){
		var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar")[0]);
			//alert(fechaInicio+" - "+fechaFin);
			form_data.append('fecha_inicio', fechaInicio);
			form_data.append('fecha_fin', fechaFin);
			$.ajax({
				url:base_url+"/promocion_temporal/guardarDataPromoTemporal",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				error:function(jqXHR,textStatus,errorThrown){
					console.log(jqXHR+"\n"+textStatus+"\n"+errorThrown);
					//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
		
	}
	//fin insertar registros

	//eliminar registro
	$(".btnDelete").click(function(){
		delDataPromoTemporal($(this).data("val"));	
	});

	function delDataPromoTemporal(dato){
		$.ajax({
			url:base_url+"/promocion_temporal/eliminaDataPromoTemporal",
			type:"POST",
			data:{id_promo_temporal:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	};
	//fin eliminar registro

});