$('#fotoBannerHome').hide();
$('#btnCortarBannerHome').hide();
$('#resultFotoBannerHome').hide();

function actualizaEstado(valorEstado,banner){
	$.ajax({
		url:base_url+"/banner_home/estadoBanner",
		type:"POST",
		data:{id_banner:banner,banner_estado:valorEstado},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		}
	});
}

$('.estadoBanner').bootstrapSwitch().on('switch-change', function(event, state) {
  if (state.value) {
  	//console.log("activo"); // true | false
  	actualizaEstado(0, $(this).data("item"));
  }else{
  	//console.log("inactivo"); // true | false
  	actualizaEstado(1, $(this).data("item"));
  }
});

var imgBannerHome, cropper;

//al momento de seleccionar la imagen le asigna el evento cropper para realizar el corte
function readFile(input,cortecropper,contentResultado,btnCorte,contenedor) {
  if (input.files) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
          $('#'+cortecropper).show();
		  $('#'+contentResultado).hide();
          $('#'+btnCorte).show();
          $('#'+contenedor).show();
          $('#'+cortecropper).attr('src', e.target.result);
          var image = document.getElementById(cortecropper);
          cropper = new Cropper(image, {
          	minCropBoxWidth:820,
          	minCropBoxHeight:360,
          	cropBoxResizable:false,
            aspectRatio: 8.2 / 3.6
          });
        }
                      
        reader.readAsDataURL(input.files[0]);
      }
      else {
        $('#'+cortecropper).hide();
    }
}

function makeCropImg(cortecropper,contentResultado){
	cropper.getCroppedCanvas({
	  width: 1366,
	  height: 600,
	  fillColor: '#fff',
	  imageSmoothingEnabled: false,
	  imageSmoothingQuality: 'medium',
	}).toBlob(function (blob) {
		var objectURL = URL.createObjectURL(blob);
		$('#'+contentResultado).attr("src",objectURL);
		$('#'+contentResultado).show();
		$('#contentFotoBannerHome').hide();
		imgBannerHome = blob;
	  	cropper.destroy();

	});
}

$('#uploadBannerHome').on('change', function () { 
	
    readFile(this,
	"fotoBannerHome",
	"resultFotoBannerHome",
	"btnCortarBannerHome",
	"contentFotoBannerHome"); 
});
//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
$('#btnCortarBannerHome').click(function(){
	$(this).hide();
	makeCropImg("fotoBannerHome","resultFotoBannerHome");
});

$('#tipo_banner').on('change',function(){

	if($("#tipo_banner").val()==0){
		$("#content_producto").show().addClass('show').removeClass('hide');
		$("#content_texto_boton").show().addClass('show').removeClass('hide');
		
		$('#producto_banner').addAttr('required','required');
	}else{
		$("#content_producto").hide().addClass('hide').removeClass('show');
		$("#content_texto_boton").hide().addClass('hide').removeClass('show');

		$("#texto_boton_banner_home").val(null).removeAttr('required');
		$('#producto_banner').prop('selectedIndex',0).removeAttr('required');
	}
});


//actualizar registros
$("#btnUpdate").click(updateDataBannerHome);

function updateDataBannerHome(){
	var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		var form_data = new FormData($("#form_actualizar")[0]);
		form_data.append('imagen_banner_home',imgBannerHome);
		$.ajax({
			url:base_url+"/banner_home/editDataBannerHome",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
	
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataBannerHome);

function inseDataBannerHome(){
	var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		var form_data = new FormData($("#form_agregar")[0]);
		form_data.append('imagen_banner_home',imgBannerHome);
		$.ajax({
			url:base_url+"/banner_home/guardarDataBannerHome",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR+"\n"+textStatus+"\n"+errorThrown);
				//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
	
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataBannerHome($(this).attr("data-val"));	
});

function delDataBannerHome(dato){
	$.ajax({
		url:base_url+"/banner_home/eliminaDataBannerHome",
		type:"POST",
		data:{id_banner:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro