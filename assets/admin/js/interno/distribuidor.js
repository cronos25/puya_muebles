$('#fotoDistribuidor').hide();
$('#btnCortarDistribuidor').hide();
$('#resultFotoDistribuidor').hide();
$('.jqte-test').jqte();
function actualizaEstado(valorEstado,distribuye){
	$.ajax({
		url:base_url+"/distribuidor/estadoDistribuidor",
		type:"POST",
		data:{id_distribuidor:distribuye,autoriza_estado:valorEstado},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		}
	});
}

$('.estadoAutorizado').bootstrapSwitch().on('switch-change', function(event, state) {
  if (state.value) {
  	//console.log("activo"); // true | false
  	actualizaEstado(0, $(this).data("item"));
  }else{
  	//console.log("inactivo"); // true | false
  	actualizaEstado(1, $(this).data("item"));
  }
});

var imgDistribuidor, cropper;

//al momento de seleccionar la imagen le asigna el evento cropper para realizar el corte
function readFile(input,cortecropper,contentResultado,btnCorte,contenedor) {
  if (input.files) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
          $('#'+cortecropper).show();
		  $('#'+contentResultado).hide();
          $('#'+btnCorte).show();
          $('#'+contenedor).show();
          $('#'+cortecropper).attr('src', e.target.result);
          var image = document.getElementById(cortecropper);
          cropper = new Cropper(image, {
          	minCropBoxWidth:400,
          	minCropBoxHeight:400,
          	cropBoxResizable:false,
            aspectRatio: 1 / 1
          });
        }
                      
        reader.readAsDataURL(input.files[0]);
      }
      else {
        $('#'+cortecropper).hide();
    }
}

function makeCropImg(cortecropper,contentResultado){
	cropper.getCroppedCanvas({
	  width: 400,
	  height: 400,
	  fillColor: '#fff',
	  imageSmoothingEnabled: true,
	  imageSmoothingQuality: 'high',
	}).toBlob(function (blob) {
		var objectURL = URL.createObjectURL(blob);
		$('#'+contentResultado).attr("src",objectURL);
		$('#'+contentResultado).show();
		$('#contentFotoDistribuidor').hide();
		imgDistribuidor = blob;
	  	cropper.destroy();
	});
}

$('#uploadDistribuidor').on('change', function () { 
	readFile(this,
	"fotoDistribuidor",
	"resultFotoDistribuidor",
	"btnCortarDistribuidor",
	"contentFotoDistribuidor"); 
});
//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
$('#btnCortarDistribuidor').click(function(){
	$(this).hide();
	makeCropImg("fotoDistribuidor","resultFotoDistribuidor");
});


//actualizar registros
$("#btnUpdate").click(updateDataDistribuidor);

function updateDataDistribuidor(){
	var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		var form_data = new FormData($("#form_actualizar")[0]);
		form_data.append('imagen_distribuidor',imgDistribuidor);
		$.ajax({
			url:base_url+"/distribuidor/editDataDistribuidor",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
	
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataDistribuidor);

function inseDataDistribuidor(){
	var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		var form_data = new FormData($("#form_agregar")[0]);
		form_data.append('imagen_distribuidor',imgDistribuidor);
		$.ajax({
			url:base_url+"/distribuidor/guardarDataDistribuidor",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}

	
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataDistribuidor($(this).attr("data-val"));	
});

function delDataDistribuidor(dato){
	$.ajax({
		url:base_url+"/distribuidor/eliminaDataDistribuidor",
		type:"POST",
		data:{id_distribuidor:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro