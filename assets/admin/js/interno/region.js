
//actualizar registros
$("#btnUpdate").click(updateDataRegion);

function updateDataRegion(){
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/region/editDataRegion",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
		
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataRegion);

function inseDataRegion(){
	var validator = $("#form_agregar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/region/guardarDataRegion",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
		
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataRegion($(this).attr("data-val"));	
});

function delDataRegion(dato){
	$.ajax({
		url:base_url+"/region/eliminaDataRegion",
		type:"POST",
		data:{id_region:dato},
		success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro