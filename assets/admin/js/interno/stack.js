$('.jqte-test').jqte();
$('#fotoStack').hide();
$('#btnCortarStacks').hide();
$('#resultFotoStack').hide();
$("#categoria_stack").change(creacionCodigoProd);
$("#marca_stack").change(creacionCodigoProd);
$('.select2').select2();
$("#categoria_stack").change(function(){
	var categoria = $(this).val();
	$.ajax({
		url:base_url+'/sub_categoria/obtenerSubCategoriasCategorias',
		type:"POST",
		data:{id_categoria:categoria},
		dataType : 'json',
		success:function(respuesta){
			//flashSimple("Estado Registro", respuesta);
			//window.location = site_url+"/home/muestra/"+respuesta;
			console.log("obtenidos");
		},
		complete:function(respuesta){
			//location.reload();
			respuesta = $.parseJSON(respuesta['responseText']);
			var selectorSubCategoria = $("#sub_categoria_stack");
			var baseSubtotal = 0;
			selectorSubCategoria.empty();

			console.log(respuesta);

			selectorSubCategoria.append("<option value=''>Seleccione Sub Categoria</option>");
			$.each( respuesta, function( index, element ){
				selectorSubCategoria.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
			});
		}
	});
});

function creacionCodigoProd(){
	var categoria = $("#categoria_stack").val();
	var marca = $("#marca_stack").val();
	if(categoria == ""){
		categoria = 0;
	}

	if(marca == ""){
		marca = 0;
	}
	$("#codigo_stack").val($("#codigo_stack").attr("data-val")+categoria+marca);
}
//insertar registros
$("#btnEdit").click(editDataStack);
var x_ax= 0,y_ax= 0,wc=0,hc=0,zx=1,zy=1,imgStack, cropper;
	
//al momento de seleccionar la imagen le asigna el evento cropper para realizar el corte
function readFile(input,cortecropper,contentResultado,btnCorte,contenedor) {
  if (input.files) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
          $('#'+cortecropper).show();
		  $('#'+contentResultado).hide();
          $('#'+btnCorte).show();
          $('#'+contenedor).show();
          $('#'+cortecropper).attr('src', e.target.result);
          var image = document.getElementById(cortecropper);
          cropper = new Cropper(image, {
          	minCropBoxWidth:400,
          	minCropBoxHeight:400,
          	cropBoxResizable:false,
            aspectRatio: 1 / 1
          });
        }
                      
        reader.readAsDataURL(input.files[0]);
      }
      else {
        $('#'+cortecropper).hide();
    }
}

function makeCropImg(cortecropper,contentResultado){
	cropper.getCroppedCanvas({
	  width: 400,
	  height: 400,
	  fillColor: '#fff',
	  imageSmoothingEnabled: true,
	  imageSmoothingQuality: 'high',
	}).toBlob(function (blob) {
		var objectURL = URL.createObjectURL(blob);
		$('#'+contentResultado).attr("src",objectURL);
		$('#'+contentResultado).show();
		$('#contentFotoStack').hide();
		imgStack = blob;
	  	cropper.destroy();

	});
}

$('#uploadStack').on('change', function () { 
	readFile(this,
	"fotoStack",
	"resultFotoStack",
	"btnCortarStacks",
	"contentFotoStack"); 
});
//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
$('#btnCortarStacks').click(function(){
	$(this).hide();
	makeCropImg("fotoStack","resultFotoStack");
});


function editDataStack(){
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		var form_data = new FormData($("#form_actualizar")[0]);
		
		form_data.append('img_cortada',imgStack);
		$.ajax({
			url:base_url+"/stack/editDataStack",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}		
}
//fin insertar registros
//creacionCodigoProd();
$("#btnAdd").click(inseDataStack);

function inseDataStack(){
		
	var validator = $("#form_agregar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		var form_data = new FormData($("#form_agregar")[0]);
		form_data.append('img_cortada',imgStack);
		$.ajax({
			url:base_url+"/stack/guardarDataStack",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				//console.log(respuesta);
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
}
//fin insertar registros
//eliminar registro
$(".btnDelete").click(function(){
	delDataStack($(this).attr("data-val"));	
});

function delDataStack(dato){
	$.ajax({
		url:base_url+"/stack/eliminaDataStack",
		type:"POST",
		data:{id_stack:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);	
		}
	});
};
	
$("#btnAddProdStack").click(inseDataProdStack);

function inseDataProdStack(){
	var validator = $("#form_edit_producto_stack").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  	});
	if (validator.form()) {
		$.ajax({
			url:base_url+"/producto_stack/guardarDataProdStack",
			type:"POST",
			data:$("#form_edit_producto_stack").serialize(),
			success:function(respuesta){
				console.log(respuesta);
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
		
}

$(".btnDeleteProdStack").click(function(){
	delDataProdStack($(this).attr("data-val"));	
});

function delDataProdStack(dato){
	$.ajax({
		url:base_url+"/producto_stack/eliminaDataProdStack",
		type:"POST",
		data:{id_prod_asoc:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);	
		}
	});
};
