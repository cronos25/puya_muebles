$(document).ready(function(){

	$('.select2').select2();

	//actualizar registros
	$("#btnUpdate").click(updateDataOfertaEspecial);

	function updateDataOfertaEspecial(){
		var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_actualizar")[0]);
			$.ajax({
				url:base_url+"/oferta_especial/editarDataOfertaEspecial",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin actualizar registros

	//insertar registros
	$("#btnAdd").click(inseDataOfertaEspecial);

	function inseDataOfertaEspecial(){
		var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar")[0]);
			$.ajax({
				url:base_url+"/oferta_especial/guardarDataOfertaEspecial",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
		
	}
	//fin insertar registros

	//eliminar registro
	$(".btnDelete").click(function(){
		delDataOfertaEspecial($(this).data("val"));	
	});

	function delDataOfertaEspecial(dato){
		$.ajax({
			url:base_url+"/oferta_especial/eliminaDataOfertaEspecial",
			type:"POST",
			data:{id_oferta_especial:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	};
	//fin eliminar registro

	function actualizaEstado(valorEstado,oferta){
		$.ajax({
			url:base_url+"/oferta_especial/estadoOferta",
			type:"POST",
			data:{id_oferta:oferta,oferta_estado:valorEstado},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			}
		});
	}

	$('.estadoOferta').bootstrapSwitch().on('switch-change', function(event, state) {
	  if (state.value) {
	  	//console.log("activo"); // true | false
	  	actualizaEstado(0, $(this).data("item"));
	  }else{
	  	//console.log("inactivo"); // true | false
	  	actualizaEstado(1, $(this).data("item"));
	  }
	});


});