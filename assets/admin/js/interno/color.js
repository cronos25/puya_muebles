$('.jqte-test').jqte();

//actualizar registros
$("#btnUpdate").click(updateDataColor);

function updateDataColor(){
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/color/editDataColor",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
		
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataColor);

function inseDataColor(){
	var validator = $("#form_agregar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/color/guardarDataColor",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
		
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataColor($(this).attr("data-val"));	
});

function delDataColor(dato){
	$.ajax({
		url:base_url+"/color/eliminaDataColor",
		type:"POST",
		data:{id_color:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro