
//actualizar registros
$("#btnUpdate").click(updateDataPrecioEnvio);

function updateDataPrecioEnvio(){
	var validator = $("#form_actualizar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/precio_envio/editDataPrecioEnvio",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
	
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataPrecioEnvio);

function inseDataPrecioEnvio(){
	var validator = $("#form_agregar").validate({
					  success: function(label) {
					    	label.addClass("valid");
						}
				  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/precio_envio/guardarDataPrecioEnvio",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	delDataPrecioEnvio($(this).attr("data-val"));	
});

function delDataPrecioEnvio(dato){
	$.ajax({
		url:base_url+"/precio_envio/eliminaDataPrecioEnvio",
		type:"POST",
		data:{id_precio_envio:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);
		}
	});
};
//fin eliminar registro

//fin eliminar registro
$(document).ready(function(){
	$("#id_region").change(function(){
		var region = $(this).val();
		$.ajax({
			url:base_url+'/provincia/obtenerProvinciasRegion',
			type:"POST",
			data:{id_region:region},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorProvincias = $("#id_provincia");
				var baseSubtotal = 0;
				selectorProvincias.empty();

				selectorProvincias.append("<option value=''>Seleccione Provincia</option>");
				$.each( respuesta, function( index, element ){
					selectorProvincias.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});

	$("#id_provincia").change(function(){
		var provincia = $(this).val();
		$.ajax({
			url:base_url+'/comuna/obtenerComunasProvincia',
			type:"POST",
			data:{id_provincia:provincia},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorProvincias = $("#id_comuna");
				var baseSubtotal = 0;
				selectorProvincias.empty();

				selectorProvincias.append("<option value=''>Seleccione Comuna</option>");
				$.each( respuesta, function( index, element ){
					selectorProvincias.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});

});