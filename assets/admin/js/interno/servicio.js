$('#fotoParrafo').hide();
$('#btnCortarParrafos').hide();
$('#resultFotoParrafo').hide();
$(document).ready(function(){
$('.jqte-test').jqte();

	var x_ax= 0,y_ax= 0,wc=0,hc=0,zx=1,zy=1,imgParrafo, cropper;
	
	//al momento de seleccionar la imagen le asigna el evento cropper para realizar el corte
	function readFile(input,cortecropper,contentResultado,btnCorte,contenedor) {
	  if (input.files) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
	          $('#'+cortecropper).show();
			  $('#'+contentResultado).hide();
	          $('#'+btnCorte).show();
	          $('#'+contenedor).show();
	          $('#'+cortecropper).attr('src', e.target.result);
	          var image = document.getElementById(cortecropper);
	          cropper = new Cropper(image, {
	          	minCropBoxWidth:600,
	          	minCropBoxHeight:300,
	          	cropBoxResizable:false,
	            aspectRatio: 2 / 1
	          });
	        }
	                      
	        reader.readAsDataURL(input.files[0]);
	      }
	      else {
	        $('#'+cortecropper).hide();
	    }
	}

	function makeCropImg(cortecropper,contentResultado){
		cropper.getCroppedCanvas({
		  width: 600,
		  height: 300,
		  fillColor: '#fff',
		  imageSmoothingEnabled: true,
		  imageSmoothingQuality: 'high',
		}).toBlob(function (blob) {
			var objectURL = URL.createObjectURL(blob);
			$('#'+contentResultado).attr("src",objectURL);
			$('#'+contentResultado).show();
			$('#contentFotoParrafo').hide();
			imgParrafo = blob;
		  	cropper.destroy();

		});
	}

	$('#uploadParrafo').on('change', function () { 
		readFile(this,
		"fotoParrafo",
		"resultFotoParrafo",
		"btnCortarParrafos",
		"contentFotoParrafo"); 
	});
	//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
	$('#btnCortarParrafos').click(function(){
		$(this).hide();
		makeCropImg("fotoParrafo","resultFotoParrafo");
	});


	//insertar registros
	$("#btnFijaDatoServ").click(inseDataServicio);

	function inseDataServicio(){
		var validator = $("#form_agregar_datos_servicio").validate({
							rules: {
							    // compound rule
							    telefono_servicio_categoria: {
							      digits: true
							    },
							    email_servicio_categoria: {
							      email: true
							    },
							    mail2_servicio_categoria: {
							      email: true
							    },
							    mail3_servicio_categoria: {
							      email: true
							    }							    
							  },
						  	success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			$.ajax({
				url:base_url+"/servicio/editDataServicio",
				type:"POST",
				data:$("#form_agregar_datos_servicio").serialize(),
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				error:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin insertar registros


//insertar registros de parrafo
	$("#btnAddParrafo").click(inseDataCate);

	function inseDataCate(){
		var validator = $("#form_agregar_parrafo_servicio").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar_parrafo_servicio")[0]);
			form_data.append('imagen_parrafo',imgParrafo);
			$.ajax({
				url:base_url+"/Servicio/guardarParrafoServicio",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				error:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin insertar registros de parrafo

	//eliminar registro
	$(".btnDelete").click(function(){
		delDataServ($(this).data("val"));	
	});

	function delDataServ(dato){
		$.ajax({
			url:base_url+"/Servicio/eliminaParrafoServicio",
			type:"POST",
			data:{id_parrafo:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}

		});
	};
	//fin eliminar registro


	//actualizar registros
	/*$("#btnUpdate").click(updateDataCate);

	function updateDataCate(){
		var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_actualizar")[0]);
			form_data.append('imagen_parrafo',imgParrafo);
			$.ajax({
				url:base_url+"/Parrafo/editDataCategory",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin actualizar registros

//insertar registros de parrafo
	$("#btnFijaDatoServ").click(inseDataCate);

	function inseDataCate(){
		var validator = $("#form_agregar_datos_servicio").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
		if (validator.form()) {
			var form_data = new FormData($("#form_agregar_datos_servicio")[0]);
			form_data.append('imagen_parrafo',imgParrafo);
			$.ajax({
				url:base_url+"/Parrafo/guardarDataCategory",
				type:"POST",
		        processData: false,
		        contentType: false,
				data:form_data,
				success:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				error:function(respuesta){
					flashSimple("Estado Registro", respuesta);
					//alert(respuesta);
				},
				complete:function(respuesta){
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			});
		}else{
			flashSimple("Estado Registro", 'Hay Errores en el formulario');
		}
	}
	//fin insertar registros


	//eliminar registro
	$(".btnDelete").click(function(){
		delDataUs($(this).data("val"));	
	});

	function delDataUs(dato){
		$.ajax({
			url:base_url+"/Parrafo/eliminaDataCategory",
			type:"POST",
			data:{id_Parrafo:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}

		});
	};*/
	//fin eliminar registro

});