$(document).ready(function(){
	$("#btnNewEspecForm").click(function(){
		$("#nuevasEspecList").append(
			'<div class="form-group">'+
              '<label for="nombre_especificacion" class="col-sm-3 control-label">Especificacion</label>'+
              '<div class="col-sm-9">'+
                '<div class="input-group">'+
                  '<input type="text" name="nombre_especificacion[]" class="form-control" placeholder="Nombre Especificacion">'+
                  '<span class="input-group-addon"><i class="fa fa-cog"></i></span>'+
                '</div>'+
              '</div>'+
            '</div>');
		
	});
	//actualizar registros


	$(".btn_update_espec").click(function(){
		updateEspecCate($(this).attr("data-val-esp"),$(this).attr("data-val-tar"));	
	});

	//$("#btnUpdate").click(updateEspecCate);

	/*function updateEspecCate(){
		var form_data = new FormData($("#form_actualizar")[0]);
		$.ajax({
			url:base_url+"/especificacion/editDataEspecificacion",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				location.reload();
			}
		});
	}*/
	function updateEspecCate(espec,item){
		var valor = $("#"+item).val();
		$.ajax({
			url:base_url+"/especificacion/editDataEspecificacion",
			type:"POST",
			data:{espec:espec,valor:valor},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				location.reload();
			}
		});
	}
	//fin actualizar registros

	//insertar registros
	$("#btnAdd").click(inseDataCate);

	function inseDataCate(){
		var form_data = new FormData($("#form_agregar")[0]);
		$.ajax({
			url:base_url+"/especificacion/guardarDataEspecificacion",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
			error:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
				
				//alert(respuesta);
			}
		});
	}
	//fin insertar registros

	//eliminar registro

	$(".btn_del_espec").click(function(){
		delDataEs($(this).attr("data-val-esp"));	
	});

	function delDataEs(dato){
		$.ajax({
			url:base_url+"/especificacion/eliminaDataEspecificacion",
			type:"POST",
			data:{id_especificacion:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
			complete:function(respuesta){
				location.reload();
			}
		});
	};


	function delDataEsTodas(dato){
		$.ajax({
			url:base_url+"/especificacion/eliminaDataEspecificacionTodas",
			type:"POST",
			data:{id_categorias:dato},
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
			complete:function(respuesta){
				location.reload();
			}
		});
	};
	
	$(".btnDeleteTodas").click(function(){
		delDataEsTodas($(this).attr("data-val"));	
	});

	//fin eliminar registro
	function getEspectByProd(prod){
		$.ajax({
			url:base_url+"/especificacion/obtenerEspecProd",
			type:"POST",
			data:{dataProd:prod},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				//alert(respuesta['mensaje']);
				$("#content_field_prod_add_espec").empty();
				$.each( respuesta, function( index, element ){
					$("#content_field_prod_add_espec").append( '<div class="form-group">'+
                      '<div class="col-sm-12">'+
                        '<div class="input-group">'+
                          '<span class="input-group-addon">'+element['nombre']+'</span>'+
                          '<input type="hidden" name="elementos_producto_add[espec][]" value="'+element['id']+'">'+
                          '<input type="text" name="elementos_producto_add[val][]" class="form-control" id="espec'+element['id']+'" aria-describedby="'+element['nombre']+'">'+
                        '</div>'+
                      '</div>'+
                    '</div>');
				});
			}
		});
	}

	$("#producto_especificacion").change(function(e){
		if ($( "#producto_especificacion" ).val()) {
			getEspectByProd($("#producto_especificacion").val());
		}
	});

	//insertar registros
	$("#btnAddProduct").click(inseDataEspect_prod);

	function inseDataEspect_prod(){
		var form_data = new FormData($("#form_agregar_espec_product")[0]);
		$.ajax({
			url:base_url+"/especificacion/guardarDataEspecificacionProducto",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				alert(respuesta);
			},
			error:function(respuesta){
				flashSimple("Estado Registro", respuesta);

				alert(respuesta);
			}
		});
	}
	//fin insertar registros

	//actualizar registros del valor de la especificacion
	$("#btnUpdateProd").click(updateEspec);

	function updateEspec(){
		var form_data = new FormData($("#form_actualizar_espec_valor")[0]);
		$.ajax({
			url:base_url+"/especificacion/editDataEspecificacionProducto",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
			},
			complete:function(respuesta){
				location.reload();
			}
		});
	}
	//fin actualizar registros
});