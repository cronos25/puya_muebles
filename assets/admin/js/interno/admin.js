$(document).ready(function() {
	//actualizar registros
	/*$("#form_login").on('submit',loginData);

	function loginData(){
		$.ajax({
			url:base_url+"/admin/abrirIniciarSesion",
			type:"POST",
			dataType : 'json',
			data:$("#form_login").serialize(),
			success:function(respuesta){
				//alert(respuesta['mensaje']);
				//flashSimple("Estado Login",respuesta["mensaje"]);
				//window.location = respuesta["link"]; 
				location.reload();
			}
		});
	}
	//fin actualizar registros
*/

	$("#btn_end_session").click(loginEndData);

	function loginEndData(){
		$.ajax({
			url:base_url+"/admin/cerrarSession",
			type:"POST",
			data:{cleardata:1},
			success:function(respuesta){
				//alert(respuesta["mensaje"]);
				//flashSimple("Estado Login",respuesta["mensaje"]);
				window.location = respuesta; 
			}
		});
	}

	$(".datareverse").dataTable({
		sDom:"<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
		sPaginationType:"bootstrap",
        aaSorting:[[0,"desc"]],
		oLanguage:{
			sLengthMenu:"_MENU_ records per page"}
		});
	$(".datatablecotizados").dataTable({
		sDom:"<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
		sPaginationType:"bootstrap",
        aaSorting: [],
		oLanguage:{
			sLengthMenu:"_MENU_ records per page"}
		});

/*
	var options = {
			canvas: true,
			lines: {
				show: true
			},
			points: {
				show: true
			},
			xaxis: {
				mode: "time",
                timezone: "browser",
                minTickSize: [3, "day"],
                timeformat: "%a",
                font: {
                    color: "#555"
                },
                tickColor: "#fafafa",
                autoscaleMargin: .2
			}
		};


	var data = [{
    "label": "Europe (EU27)",
    "data": [[2, 3.0], [2, 3.9], [3, 2.0], [3, 1.2], [2, 1.3], [5, 2.5], [2, 2.0], [7, 3.1], [6, 2.9], [6, 0.9]]
	}];
	$.plot($("#demodatacarro"), data,options);
*/

//data para primer contador de pagina de dashboard
	var C={
		type:"bar",
		canvas:true,
		barWidth:5,
		height:25,
	};
	function llamarDataContadorSC(){
		$.ajax({
			url:base_url+"/carro_compra/callShoppingCartActive10Days",
			type:"POST",
			success:function(respuesta){

				var m=[];
				var items = 0;
				//console.log(respuesta);
				$.each( JSON.parse(respuesta), function(index, element ){
				  	m.push(element['carrofecha']);
				  	items++;
				});
				$("#cant_cpd_home").html("<i class='fa fa-plus-circle'></i> "+items);

				C.barColor="#CE7B11";
				//contador carros de compra
				$("#mini-bar-scd").sparkline(m,C);
			}
		});
	}

	llamarDataContadorSC();

	var D={
		type:"bar",
		canvas:true,
		barWidth:5,
		height:25,
	};
	function llamarDataContadorSC(){
		$.ajax({
			url:base_url+"/venta/callSellsActive10Days",
			type:"POST",
			success:function(respuesta){

				var m=[];
				var items = 0;
				//console.log(respuesta);
				$.each( JSON.parse(respuesta), function(index, element ){
				  	m.push(element['pagofecha']);
				  	items++;
				});
				$("#cant_vpd_home").html("<i class='fa fa-plus-circle'></i> "+items);

				D.barColor="#1d92af";
				//contador carros de compra
				$("#mini-bar-scd").sparkline(m,C);
			}
		});
	}

	llamarDataContadorSC();
});
