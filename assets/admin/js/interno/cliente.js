
//actualizar registros
$("#btnUpdate").click(updateDataUs);

function updateDataUs(){
	var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/cliente/editDataCliente",
			type:"POST",
			data:$("#form_actualizar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
				//console.log(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 2000);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
}
//fin actualizar registros

//insertar registros
$("#btnAdd").click(inseDataUs);

function inseDataUs(){
	var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/cliente/guardarDataCliente",
			type:"POST",
			data:$("#form_agregar").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
				//console.log(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 2000);	
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	$(this).button('loading');
	delDataUs($(this).attr("data-val"));	
});

function delDataUs(dato){
	$.ajax({
		url:base_url+"/cliente/eliminaDataCliente",
		type:"POST",
		data:{id_cliente:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
			//console.log(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1500);	
		}
	});
};
//fin eliminar registro
$(document).ready(function(){
	$("#region_cliente").change(function(){
		var region = $(this).val();
		$.ajax({
			url:base_url+'/provincia/obtenerProvinciasRegion',
			type:"POST",
			data:{id_region:region},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorProvincias = $("#provincia_cliente");
				var baseSubtotal = 0;
				selectorProvincias.empty();

				selectorProvincias.append("<option value=''>Seleccione Provincia</option>");
				$.each( respuesta, function( index, element ){
					selectorProvincias.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});

	$("#provincia_cliente").change(function(){
		var provincia = $(this).val();
		$.ajax({
			url:base_url+'/comuna/obtenerComunasProvincia',
			type:"POST",
			data:{id_provincia:provincia},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorProvincias = $("#comuna_cliente");
				var baseSubtotal = 0;
				selectorProvincias.empty();

				selectorProvincias.append("<option value=''>Seleccione Comuna</option>");
				$.each( respuesta, function( index, element ){
					selectorProvincias.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});
	$('#rut_cliente').tooltip({
		title:"Rut Erroneo",
		trigger: 'manual'
	});
	$('#rut_cliente').Rut({
	  on_error: function(){ 
	  	$('#rut_cliente').parent().addClass("has-error");
	  	$('#rut_cliente').tooltip('show');
	  	$('#rut_cliente').focus();
	  },
	  on_success:function(){
	  	$('#rut_cliente').parent().removeClass("has-error");
	  	$('#rut_cliente').tooltip('hide');
	  },
	  format_on: 'keyup'
	});
});