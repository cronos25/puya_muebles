$('.jqte-test').jqte();
$('.select2').select2();
$('#color_producto').multiselect();
//creacionCodigoProd();
$("#categoria_producto").change(creacionCodigoProd);
$("#marca_producto").change(creacionCodigoProd);
/*sector para corte de imagenes */
//carga las variables a utilizar y las inicializa
var x_ax= 0,y_ax= 0,wc=0,hc=0,zx=1,zy=1,imgProductos,imgEtiqueta,imgHome, cropper;
	$('#fotoProducto').hide();
	$('#btnCortarProductos').hide();
	$('#resultFotoProducto').hide();

	$('#fotoEtiquetas').hide();
	$('#btnCortarEtiquetas').hide();
	$('#resultFotoEtiqueta').hide();

	$('#fotoHome').hide();
	$('#btnCortarHome').hide();
	$('#resultFotoHome').hide();
	//al momento de seleccionar la imagen le asigna el evento cropper para realizar el corte
	function readFile(input,cortecropper,contentResultado,btnCorte,contenedor) {
	  if (input.files) {
	  		//if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
        
		        reader.onload = function (e) {
		          $('#'+cortecropper).show();
				  $('#'+contentResultado).hide();
		          $('#'+btnCorte).show();
		          $('#'+contenedor).show();
		          $('#'+cortecropper).attr('src', e.target.result);
		          var image = document.getElementById(cortecropper);
		          cropper = new Cropper(image, {
		          	minCropBoxWidth:700,
		          	minCropBoxHeight:700,
		          	cropBoxResizable:false,
		            aspectRatio: 1 / 1
		          });
		        }
			                      
			     reader.readAsDataURL(input.files[0]);
                /*} else {
                    alert("This browser does not support FileReader.");
                    file = jQuery('#image').prop("files")[0];
		            blobURL = window.URL.createObjectURL(file);
		            jQuery('#image_preview img').attr('src', blobURL);
                }*/	        
	      }
	      else {
	        $('#'+cortecropper).hide();
	    }
	}

	function makeCropImg(cortecropper,contentResultado,valorResult){
		cropper.getCroppedCanvas({
		  width: 700,
		  height: 700,
		  fillColor: '#fff',
		  imageSmoothingEnabled: true,
		  imageSmoothingQuality: 'high',
		}).toBlob(function (blob) {
			var objectURL = URL.createObjectURL(blob);
			$('#'+contentResultado).attr("src",objectURL);
			$('#'+contentResultado).show();
			switch(valorResult){
				case 0:
					$('#contentFotoProducto').hide();
					imgProductos = blob;
					break;
				case 1:
					$('#contentFotoEtiquetas').hide();
		  			imgEtiqueta = blob;
					break;
				case 2:
					$('#contentFotoHome').hide();
		  			imgHome = blob;
					break;
			}
		  	cropper.destroy()

		});
	}

	$('#uploadProducto').on('change', function () { 
		readFile(this,
		"fotoProducto",
		"resultFotoProducto",
		"btnCortarProductos",
		"contentFotoProducto"); 
	});
	//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
	$('#btnCortarProductos').click(function(){
		$(this).hide();
		makeCropImg("fotoProducto","resultFotoProducto",0);
	});

	$('#uploadEtiqueta').on('change', function () { 
		readFile(this,
		"fotoEtiquetas",
		"resultFotoEtiqueta",
		"btnCortarEtiquetas",
		"contentFotoEtiquetas"); 
	});
	//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
	$('#btnCortarEtiquetas').click(function(){
		$(this).hide();
		makeCropImg("fotoEtiquetas","resultFotoEtiqueta",1);
	});

	$('#uploadHome').on('change', function () { 
		readFile(this,
		"fotoHome",
		"resultFotoHome",
		"btnCortarHome",
		"contentFotoHome"); 
	});
	//al presionar el boton cortar, este transforma la imagen cortada y muestra el resultado
	$('#btnCortarHome').click(function(){
		$(this).hide();
		makeCropImg("fotoHome","resultFotoHome",2);
	});
/*fin corte de imagenes*/

$("#categoria_producto").change(function(){
	var categoria = $(this).val();
	$.ajax({
		url:base_url+'/sub_categoria/obtenerSubCategoriasCategorias',
		type:"POST",
		data:{id_categoria:categoria},
		dataType : 'json',
		success:function(respuesta){
			//flashSimple("Estado Registro", respuesta);
			//window.location = site_url+"/home/muestra/"+respuesta;
			console.log("obtenidos");
		},
		error:function(jqXHR,textStatus,errorThrown){
			console.log(jqXHR);
			//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
		},
		complete:function(respuesta){
			//location.reload();
			respuesta = $.parseJSON(respuesta['responseText']);
			var selectorSubCategoria = $("#sub_categoria_producto");
			var baseSubtotal = 0;
			selectorSubCategoria.empty();

			selectorSubCategoria.append("<option value=''>Seleccione Sub Categoria</option>");
			$.each( respuesta, function( index, element ){
				selectorSubCategoria.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
			});
		}
	});
});

function creacionCodigoProd(){
	var categoria = $("#categoria_producto").val();
	var marca = $("#marca_producto").val();
	if(categoria == ""){
		categoria = 0;
	}

	if(marca == ""){
		marca = 0;
	}
	$("#codigo_producto").val($("#codigo_producto").attr("data-val")+categoria+marca);
}

$("#btnAdd").click(inseDataProd);

function inseDataProd(){
	var validator = $("#form_agregar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		var form_data = new FormData($("#form_agregar")[0]);
		form_data.append('img_productos',imgProductos);
		form_data.append('img_etiquetas',imgEtiqueta);
		form_data.append('img_home',imgHome);
		
		$.ajax({
			url:base_url+"/productos/guardarDataProduct",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
				//console.log(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}	
}
//fin insertar registros

//insertar registros
$("#btnEdit").click(editDataProd);

function editDataProd(){
	var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		var form_data = new FormData($("#form_actualizar")[0]);
		form_data.append('img_productos',imgProductos);
		form_data.append('img_etiquetas',imgEtiqueta);
		form_data.append('img_home',imgHome);
		$.ajax({
			url:base_url+"/productos/editDataProduct",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
				//console.log(respuesta);
			},
			complete:function(respuesta){
				console.log(respuesta);
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}	
}

$("#btnEditGo").click(editGoDataProd);

function editGoDataProd(){
	var validator = $("#form_actualizar").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		var form_data = new FormData($("#form_actualizar")[0]);
		form_data.append('img_productos',imgProductos);
		form_data.append('img_etiquetas',imgEtiqueta);
		form_data.append('img_home',imgHome);
		$.ajax({
			url:base_url+"/productos/editDataProduct",
			type:"POST",
	        processData: false,
	        contentType: false,
			data:form_data,
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
				//console.log(respuesta);
			},
			complete:function(respuesta){
				console.log(respuesta);
				setTimeout(function () {
					location.replace(base_url+"/admin/productos/add");
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}	
}
//fin insertar registros

//eliminar registro
$(".btnDelete").click(function(){
	$(this).button('loading');
	delDataProd($(this).attr("data-val"));	
});

function delDataProd(dato){
	$.ajax({
		url:base_url+"/productos/eliminaDataProduct",
		type:"POST",
		data:{id_producto:dato},
		success:function(respuesta){
			flashSimple("Estado Registro", respuesta);
			//alert(respuesta);
			//console.log(respuesta);
		},
		complete:function(respuesta){
			setTimeout(function () {
				location.reload();
			}, 1000);	
		}
	});
};

//asignar un productos destacado
$("#btnAddDestacado").click(asignaDestacado);

function asignaDestacado(){
	var validator = $("#asigna_destacado_prod").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/productos/setProductoDestacado",
			type:"POST",
			data:$("#asigna_destacado_prod").serialize(),
			success:function(respuesta){
				flashSimple("Estado Registro", respuesta);
				//alert(respuesta);
				//console.log(respuesta);
			},
			complete:function(respuesta){
				setTimeout(function () {
					location.reload();
				}, 1500);
			}
		});
	}else{
		flashSimple("Estado Registro", 'Hay Errores en el formulario');
	}	
}


	