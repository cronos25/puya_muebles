$(document).ready(function() {
/*venta script*/

function validarDatosDireccion(){
  var datosContactoValidos = false;
  datosContactoValidos = $("#form_direccion_carro").valid();

  return datosContactoValidos;
}

function validarDatosDireccionServer(){
  var serverDireccionValida = false;
  $.ajax({
    url:site_url+"/carro_compra/direccion_valida",
    data:$("#form_direccion_carro").serialize(),
    type:"POST",
    async: false,
    success:function(respuesta){
      //flashSimple("Estado Registro", respuesta);
      //window.location = site_url+"/home/muestra/"+respuesta;
      if(parseInt(respuesta) === 0){
        //serverDireccionValidaResp = true;
        serverDireccionValida =true;
      }
      //alert(respuesta);
    }
  });

  return serverDireccionValida;
}
// Step show event 
  $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
     //alert("You are on step "+stepNumber+" now");
     
     if(stepPosition === 'first'){
         $("#prev-btn").addClass('disabled');
     }else if(stepPosition === 'final'){
         $("#next-btn").addClass('disabled');
     }else{
         $("#prev-btn").removeClass('disabled');
         $("#next-btn").removeClass('disabled');
     }

      switch(stepNumber){
	      case 1:
	      	 $.ajax({
				url:base_url+'/cliente/llamar_productos_resumen',
				type:"POST",
				dataType : 'json',
				success:function(respuesta){
				},
				error:function(jqXHR,textStatus,errorThrown){
					//console.log(jqXHR);
					//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
				},
				complete:function(respuesta){
					//location.reload();
					respuesta = $.parseJSON(respuesta['responseText']);
					var contenedor = $("#content_resumen_envio");
					contenedor.empty();
					var maqueta = "<table>"+
                                  "<thead>"+
                                    "<tr>"+
                                      "<th><strong>Producto</strong></th>"+
                                      "<th class='text-right'><strong>Total</strong></th>"+
                                    "</tr>"+
                                  "</thead>"+
                                  "<tbody>";
                    var valorTotal = 0;
					$.each( respuesta['productos_data'], function( index, productoItem ){
						var totalProd = productoItem['asoc_val']-( productoItem['asoc_val']*(productoItem['asoc_val_desc']/100));
                        totalProd *= productoItem['cantidad'];
						valorTotal += totalProd;

						maqueta +="<tr>"+
	                      "<td>"+productoItem['nombre']+" x "+productoItem['cantidad']+"</td>"+
	                      "<td class='text-right'>$"+darformatomiles(totalProd)+"</td>"+
	                    "</tr>";
					});

					maqueta +="<tr>"+
                      "<td>Orden Total</td>"+
                      "<td class='text-right'>$"+darformatomiles(valorTotal)+"</td>"+
                    "</tr>";

					maqueta += "</tbody>"+
                                "</table>";

                    contenedor.append(maqueta);
				}
			});
	      	break;
	      case 2:
	      	$.ajax({
				url:base_url+'/cliente/llamar_productos_resumen',
				type:"POST",
				dataType : 'json',
				success:function(respuesta){
				},
				error:function(jqXHR,textStatus,errorThrown){
					//console.log(jqXHR);
					//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
				},
				complete:function(respuesta){
					//location.reload();
					respuesta = $.parseJSON(respuesta['responseText']);
					var contenedor = $("#content_resumen_pago");
					contenedor.empty();
					var maqueta = "<table>"+
                                  "<thead>"+
                                    "<tr>"+
                                      "<th><strong>Producto</strong></th>"+
                                      "<th class='text-right'><strong>Total</strong></th>"+
                                    "</tr>"+
                                  "</thead>"+
                                  "<tbody>";
                    var valorTotal = 0;
					$.each( respuesta['productos_data'], function( index, productoItem ){
						var totalProd = productoItem['asoc_val']-( productoItem['asoc_val']*(productoItem['asoc_val_desc']/100));
                        totalProd *= productoItem['cantidad'];
						valorTotal += totalProd;

						maqueta +="<tr>"+
	                      "<td>"+productoItem['nombre']+" x "+productoItem['cantidad']+"</td>"+
	                      "<td class='text-right'>$"+darformatomiles(totalProd)+"</td>"+
	                    "</tr>";
					});

					if (respuesta['carro_data']['retiro_tienda']==0) {
	                    var precioEnvio = parseInt(respuesta['carro_data']['costo_envio'], 10);
						maqueta +="<tr>"+
	                      "<td>Valor envio</td>"+
	                      "<td class='text-right'>$"+darformatomiles(precioEnvio)+"</td>"+
	                    "</tr>";

	                    valorTotal += precioEnvio;
	                    maqueta +="<tr>"+
	                      "<td>Orden Total</td>"+
	                      "<td class='text-right'>$"+darformatomiles(valorTotal)+"</td>"+
	                    "</tr>";
					}else{
						maqueta +="<tr>"+
	                      "<td>Orden Total</td>"+
	                      "<td class='text-right'>$"+darformatomiles(valorTotal)+"</td>"+
	                    "</tr>";
					}

					

					maqueta += "</tbody>"+
                                "</table>";

                    contenedor.append(maqueta);
				}
			});

	      	$.ajax({
				url:base_url+'/cliente/getMetodosPago',
				type:"POST",
				dataType : 'json',
				success:function(respuesta){
				},
				error:function(jqXHR,textStatus,errorThrown){
					//console.log(jqXHR);
					//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
				},
				complete:function(respuesta){
					//location.reload();
					respuesta = $.parseJSON(respuesta['responseText']);
					var contenedor = $("#contenedor_medios_pago");
					contenedor.empty();
					var maqueta = respuesta;

                    contenedor.append(maqueta);
				}
			});

			
	      	break;
      };
  });
   // Initialize the leaveStep event
  $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
     switch(stepNumber){
      case 1:

      	if (stepDirection == "backward") {
      		return true
      	}else{
      		var validoLocal = validarDatosDireccion();
	        var validoServer = validarDatosDireccionServer();  
	        
	        if (validoServer) {
	        	return	true
	        }else{
	        	if(validoLocal && validoServer){
	          return  true;
	         }else{
	          if(validoLocal && !validoServer){
	            new Noty({    
		            type: 'info',    
		            closeWith: ['click', 'button'],
		            timeout: 1500,
		            text: "Debe presionar el boton de guardar para fijar los datos de contacto"
		          }).show();
	          }

	          if(!validoLocal){
	            new Noty({    
		            type: 'info',    
		            closeWith: ['click', 'button'],
		            timeout: 1500,
		            text: "Existen errores en los datos de contacto"
		          }).show();
	          }
	          return  false;
	        }
	        
	        }        
      	}
        break;
      case 2:
          /*if ($("#termycond").is(":checked")) {
            return true;
          }else{
            new Noty({    
              type: 'info',    
              closeWith: ['click', 'button'],
              timeout: 1500,
              text: "debe aceptar los terminos y condiciones para continuar"
            }).show();
            return false;
          }*/
        break;
     };
     
  });
  // Toolbar extra buttons
  var btnFinish = $('<button type="submit" data-text="Finalizar"></button>').text('Finalizar')
                   .addClass('button-one submit-button pull-right')
                   .on('click', function(){ window.location = site_url; });
  var btnCancel = $('<button data-text="Reiniciar"></button>').text('Reiniciar')
                   .addClass('button-one submit-button pull-right')
                   .on('click', function(){ $('#smartwizard').smartWizard("reset"); });                         
	
	$("#btn_guardar_direccion").click(function(){
		

		$("#btn_guardar_direccion").button('loading');
		var validator = $("#form_direccion_carro").validate({
							  success: function(label) {
							    	label.addClass("valid");
								}
						  });
		if (validator.form()) {
			$.ajax({
			url:base_url+"/carro_compra/actualizarDireccionCarro",
			type:"POST",
			data: $("#form_direccion_carro").serialize()
			}).done(function(respuesta){
				new Noty({		
					type: 'info',    
					closeWith: ['click', 'button'],
					timeout: 2000,
					text: respuesta
				}).show();	
				$("#btn_guardar_direccion").button('reset');
			});
		}else{
			new Noty({		
				type: 'error',    
				closeWith: ['click', 'button'],
				timeout: 2000,
				text: 'Hay Errores en el formulario de direccion'
			}).show();

			$("#btn_guardar_direccion").button('reset');
		}
	});   

  $('#smartwizard').smartWizard({ 
        selected: 0, 
        transitionEffect:'fade',
        keyNavigation: false,
        showStepURLhash: false,
        lang: {  // Language variables for button
            next: 'Siguiente',
            previous: 'Anterior'
        },
        anchorSettings: {
          anchorClickable: false,
          removeDoneStepOnNavigateBack: true
        },
        toolbarSettings: {
          toolbarExtraButtons: [btnFinish, btnCancel]
        }
  });


$("#btn-muestra-panel-pago").click(function(){
	//alert($("#comentario-pedidos").val());
	$(this).button('loading');
	llamarPanelPago($("#comentario-pedidos").val(),$("#cod_descuento_previa").val());
});

$(".removeItemcart").on('click',function() {
		var itemCarro = $(this);

		var n = new Noty({
		  text: '¿Seguro desea eliminar estre producto del carro?',
		  type: 'info',
		  layout: 'topcenter',
		  buttons: [
		    Noty.button('Eliminar', 'btn btn-success', function () {
		        removeItemProd(itemCarro.attr('data-asoc'),itemCarro.attr('shopping-cart'),itemCarro.attr('data-val'));
		    }, {id: 'button1', 'data-status': 'ok'}),

		    Noty.button('Cancelar', 'btn btn-default', function () {
		        n.close();
		    })
		  ]
		}).show();

		/*$.notify({
		  title: 'Atención !',
		  text: 'Seguro desea eliminar estre producto del carro?',
		  button: 'Confirmar'
		}, { 
		  style: 'metro',
		  className:'Gray',
		  autoHide: false,
		  clickToHide: false,
		  position: "top center"
		});

		//listen for click events from this style
		$(document).on('click', '.notifyjs-metro-base .no', function() {
		  //programmatically trigger propogating hide event
		  $(this).trigger('notify-hide');
		});
		$(document).on('click', '.notifyjs-metro-base .yes', function() {
		  //show button text
		  removeItemProd(itemCarro.attr('data-asoc'),itemCarro.attr('shopping-cart'),itemCarro.attr('data-val'));
		  //hide notification
		  $(this).trigger('notify-hide');
		});*/
		//
	});
	
//function agregaProd(data){
function removeItemProd(asoc,cart,val){
	$.ajax({
		url:base_url+"/carro_compra/removeItemShoppingCart",
		type:"POST",
		data: {asoc_data:asoc,cart:cart,val_data:val},
		success:function(respuesta){
			new Noty({		
				type: 'info',    
				closeWith: ['click', 'button'],
				timeout: 1500,
				text: respuesta,
				callbacks: {
		        onClose: function() {location.reload();}
			}
			}).show();
		},
		complete:function(respuesta){
			
		}
	});
};

function llamarPanelPago(mensajeventa,cod_previo){

		$.ajax({
			url:base_url+'/cliente/getMetodosPago',
			type:"POST",
			data:{mensaje_venta:mensajeventa,cod_previo:cod_previo},
			dataType : 'json',
			success:function(respuesta){
				$("#btn-muestra-panel-pago").button('reset');
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			error:function(jqXHR,textStatus,errorThrown){
				$("#btn-muestra-panel-pago").button('reset');
				console.log(jqXHR);
				//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var contenedorPanel = $("#panel_venta");
				contenedorPanel.empty();

				contenedorPanel.append(respuesta);
				$("#btn-muestra-panel-pago").button('reset');
			}
		});
}

$("#consulta_desc_codigo_carro").click(function(){
	if($("#cod_descuento_previa").val().trim()){
		$.ajax({
			url:base_url+"/codigo_promocion/previaCodigoDescuento",
			type:"POST",
			data:{codigo_previo:$("#cod_descuento_previa").val()},
			success:function(respuesta){
				//alert(respuesta);
			},
			complete:function(respuesta){
				var obj = jQuery.parseJSON(respuesta['responseText']);
				if (obj['estado']==0) {
					$("#valor_cupon").text("$"+obj['descuento']);
					$("#total_actual_carro").text("$"+obj['total']);
					$("#descuentopromocarro").text("$"+obj['promo']);
					$("#costoenviocarro").text("$"+obj['envio']);
					new Noty({		
						type: 'info',    
						closeWith: ['click', 'button'],
						timeout: 2000,
						text: obj['mensaje']
					}).show();	
				}else{
					$("#valor_cupon").text("$"+obj['descuento']);
					
					new Noty({		
						type: 'error',    
						closeWith: ['click', 'button'],
						timeout: 2000,
						text: obj['mensaje']
					}).show();
				}
			}
		});
	}
});

//falta modificar los datos del controlador para procesar la venta
$("#contenedor_medios_pago").on('click',"#realiza_venta",procesarVenta);

function procesarVenta(){
	$("#realiza_venta").button('loading');
	var validator = $("#form_venta_efectivo").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
		url:base_url+"/venta/realizaVenta",
		type:"POST",
		data: $("#form_venta_efectivo").serialize()
		}).done(function(respuesta){
			new Noty({		
				type: 'info',    
				closeWith: ['click', 'button'],
				timeout: 2000,
				text: respuesta,
				callbacks: {
			        onClose: function() {location.reload();}
				}
			}).show();	
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 2000,
			text: 'Hay Errores en el formulario de registro'
		}).show();

		$("#realiza_venta").button('reset');
	}
};

$("#set_retiro_tienda").click(function() {
    if($(this).is(':checked')) {    	
    	$.ajax({
		url:base_url+"/carro_compra/fijaRetiroTienda",
		type:"POST",
		data:{cart: $(this).data('val'),retiro_tienda:1}
		}).done(function(respuesta){
			new Noty({		
				type: 'info',    
				closeWith: ['click', 'button'],
				timeout: 2000,
				text: respuesta
			}).show();	
		});
    }else{
    	$.ajax({
		url:base_url+"/carro_compra/fijaRetiroTienda",
		type:"POST",
		data:{cart: $(this).data('val'),retiro_tienda:0}
		}).done(function(respuesta){
			new Noty({		
				type: 'info',    
				closeWith: ['click', 'button'],
				timeout: 2000,
				text: respuesta
			}).show();
		});
    }
});

$("#carro_check_direccion_cliente").click(function() {
    if($(this).is(':checked')) {
    	$("#form_direccion_carro input[name=carro_nombre]").val($("#form_direccion_carro input[name=carro_nombre]").data("dir-user"));
    	$("#form_direccion_carro input[name=carro_email]").val($("#form_direccion_carro input[name=carro_email]").data("dir-user"));
    	$("#form_direccion_carro input[name=carro_telefono]").val($("#form_direccion_carro input[name=carro_telefono]").data("dir-user"));
    	$("#form_direccion_carro textarea[name=carro_direccion]").text($("#form_direccion_carro textarea[name=carro_direccion]").data("dir-user"));
    	
    	$("#form_direccion_carro select[name=carro_region]").val($("#form_direccion_carro select[name=carro_region]").data("dir-user"));
    	/*$("#form_direccion_carro select[name=carro_provincia]").val($("#form_direccion_carro select[name=carro_provincia]").data("dir-user"));
    	$("#form_direccion_carro select[name=carro_comuna]").val($("#form_direccion_carro select[name=carro_comuna]").data("dir-user"));
    	*/       //alert("usar datos usuario");
    	$.ajax({
			url:base_url+'/provincia/obtenerProvinciasRegion',
			type:"POST",
			data:{id_region:$("#form_direccion_carro select[name=carro_region]").data("dir-user")},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorProvincias = $("#form_direccion_carro select[name=carro_provincia]");
				var baseSubtotal = 0;
				selectorProvincias.empty();

				selectorProvincias.append("<option value=''>Seleccione Provincia</option>");
				$.each( respuesta, function( index, element ){
					if ($("#form_direccion_carro select[name=carro_provincia]").data("dir-user") == element['id']) {
						selectorProvincias.append("<option value='"+element['id']+"' selected >"+element['nombre']+"</option>");
					}else{
						selectorProvincias.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
					}
				});
			}
		});

		$.ajax({
			url:base_url+'/comuna/obtenerComunasProvincia',
			type:"POST",
			data:{id_provincia:$("#form_direccion_carro select[name=carro_provincia]").data("dir-user")},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorComunas = $("#form_direccion_carro select[name=carro_comuna]");
				var baseSubtotal = 0;
				selectorComunas.empty();

				selectorComunas.append("<option value=''>Seleccione Comuna</option>");
				$.each( respuesta, function( index, element ){
					if ($("#form_direccion_carro select[name=carro_comuna]").data("dir-user") == element['id']) {
						selectorComunas.append("<option value='"+element['id']+"' selected >"+element['nombre']+"</option>");
					}else{
						selectorComunas.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
					}
				});
			}
		});
    } else {
    	$("#form_direccion_carro input[name=carro_nombre]").val("");
    	$("#form_direccion_carro input[name=carro_email]").val("");
    	$("#form_direccion_carro input[name=carro_telefono]").val("");
    	$("#form_direccion_carro textarea[name=carro_direccion]").text("");
        //alert("no usar datos usuario");
        $('#form_direccion_carro select[name=carro_region]').prop('selectedIndex',0);
		var selectorProvincias = $("#form_direccion_carro select[name=carro_provincia]");
		var selectorComunas = $("#form_direccion_carro select[name=carro_comuna]");
		selectorProvincias.empty();
		selectorComunas.empty();
		selectorComunas.append("<option value=''>Seleccione Region</option>");
		selectorProvincias.append("<option value=''>Seleccione Region</option>");
    }
});

	$("#form_direccion_carro select[name=carro_region]").change(function(){
		var region = $(this).val();
		$.ajax({
			url:base_url+'/provincia/obtenerProvinciasRegion',
			type:"POST",
			data:{id_region:region},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorProvincias = $("#form_direccion_carro select[name=carro_provincia]");
				var selectorComunas = $("#form_direccion_carro select[name=carro_comuna]");
				var baseSubtotal = 0;
				selectorProvincias.empty();
				selectorComunas.empty();
				selectorComunas.append("<option value=''>Seleccione Provincia</option>");

				selectorProvincias.append("<option value=''>Seleccione Provincia</option>");
				$.each( respuesta, function( index, element ){
					selectorProvincias.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});

	$("#form_direccion_carro select[name=carro_provincia]").change(function(){
		var provincia = $(this).val();
		$.ajax({
			url:base_url+'/comuna/obtenerComunasProvincia',
			type:"POST",
			data:{id_provincia:provincia},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorComunas = $("#form_direccion_carro select[name=carro_comuna]");
				var baseSubtotal = 0;
				selectorComunas.empty();

				selectorComunas.append("<option value=''>Seleccione Comuna</option>");
				$.each( respuesta, function( index, element ){
					selectorComunas.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});

/*venta script*/

$(".qtybutton").click(function(){
	var parent = $(this).parents('.form_cantidad_prod');
	var validator = parent.validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	var actualCantidad = parent.find('.input_cart_quantity').val()
	var precioActual = parent.find('.input_cart_quantity').data('valor-item');
	parent.closest('tr').find('.product-subtotal').text("$"+darformatomiles(actualCantidad*precioActual));
	
	if (validator.form()) {
		$.ajax({
			url:base_url+"/carro_compra/modificaCantidadCarro",
			type:"POST",
			data:parent.serialize(),
			success:function(respuesta){
				//alert(respuesta);
			},
			complete:function(respuesta){
				new Noty({		
					type: 'success',    
					closeWith: ['click', 'button'],
					timeout: 1500,
					text: respuesta['responseText']
				}).show();
			}
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 1500,
			text: 'Error Actualizando la cantidad'
		}).show();
	}
});

/*

$(".btn-update-quantity-cart").click(function(){
	var parent = $(this).parents('.form_cantidad_prod');
	var validator = parent.validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/carro_compra/modificaCantidadCarro",
			type:"POST",
			data:parent.serialize(),
			success:function(respuesta){
				//alert(respuesta);
			},
			complete:function(respuesta){
				new Noty({		
					type: 'success',    
					closeWith: ['click', 'button'],
					timeout: 1500,
					text: respuesta['responseText'],
					callbacks: {
				        onClose: function() {location.reload();}
    				}
				}).show();
			}
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 1500,
			text: 'Error Actualizando la cantidad'
		}).show();
	}
});	

$(".btn-update-flavour-cart").click(function(){
	var parent = $(this).parents('.form_sabor_prod');
	var validator = parent.validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/carro_compra/modificaSaborCarro",
			type:"POST",
			data:parent.serialize(),
			success:function(respuesta){
				//alert(respuesta);
			},
			complete:function(respuesta){
				new Noty({		
					type: 'success',    
					closeWith: ['click', 'button'],
					timeout: 1500,
					text: respuesta['responseText'],
					callbacks: {
				        onClose: function() {location.reload();}
    				}
				}).show();
			}
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 2000,
			text: 'Hay Errores en el formulario de registro'
		}).show();
	}
	
});	
*/
//actualizar registros
$("#btnUpdatePerfil").click(updateCliente);

function updateCliente(){
	$("#btnUpdatePerfil").button('loading');
	var validator = $("#form_update_data_perfil" ).validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/cliente/editDataPerfil",
			type:"POST",
			data:$("#form_update_data_perfil").serialize(),
			success:function(respuesta){
				
			},
			complete:function(respuesta){
				var obj = jQuery.parseJSON( respuesta['responseText'] );
				if (obj.estado) {
					new Noty({		
						type: 'success',    
						closeWith: ['click', 'button'],
						timeout: 1500,
						text: obj.respuesta,
						callbacks: {
					        onClose: function() {
					        	location.reload();
								$("#btnUpdatePerfil").button('reset');
					        }
	    				}
					}).show();
				}else{
					new Noty({		
						type: 'info',    
						closeWith: ['click', 'button'],
						timeout: 1500,
						text: obj.respuesta,
					}).show();
					$("#btnUpdatePerfil").button('reset');
				}
			}
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 2000,
			text: 'Hay Errores en el formulario de registro'
		}).show();
		$("#btnUpdatePerfil").button('reset');
	}
	
}

$("#btnUpdateFacturacion").click(updateFacturacionCliente);

function updateFacturacionCliente(){
	$("#btnUpdateFacturacion").button('loading');

	var validator = $("#form_update_data_facturacion_perfil" ).validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/cliente/editDataPerfilFacturacion",
			type:"POST",
			data:$("#form_update_data_facturacion_perfil").serialize(),
			success:function(respuesta){
			},
			complete:function(respuesta){
				var obj = jQuery.parseJSON( respuesta['responseText'] );
				if (obj.estado) {
					new Noty({		
						type: 'success',    
						closeWith: ['click', 'button'],
						timeout: 1500,
						text: obj.respuesta,
						callbacks: {
					        onClose: function() {
					        	location.reload();
								$("#btnUpdateFacturacion").button('reset');
					        }
	    				}
					}).show();
				}else{
					new Noty({		
						type: 'info',    
						closeWith: ['click', 'button'],
						timeout: 1500,
						text: obj.respuesta,
					}).show();
					$("#btnUpdateFacturacion").button('reset');
				}
				
			}
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 2000,
			text: 'Hay Errores en el formulario de registro'
		}).show();
		$("#btnUpdateFacturacion").button('reset');
	}
	
}

$("#btnUpdatePassPerfil").click(updatePassCliente);

function updatePassCliente(){
	$("#btnUpdatePassPerfil").button('loading');
	var validator = $("#form_update_pass_perfil" ).validate({
						  success: function(label) {
						    	label.addClass("valid");
							},
						  rules: {
						    nueva_pass_cliente: "required",
						    nueva_pass_repeat_cliente: {
						      equalTo: "#nueva_pass_cliente"
						    }
						  }
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/cliente/editDataPerfilPassword",
			type:"POST",
			data:$("#form_update_pass_perfil").serialize(),
			success:function(respuesta){
			},
			complete:function(respuesta){
				var obj = jQuery.parseJSON( respuesta['responseText'] );
				if (obj.estado) {
					new Noty({		
						type: 'success',    
						closeWith: ['click', 'button'],
						timeout: 1500,
						text: obj.respuesta,
						callbacks: {
					        onClose: function() {
								$("#btnUpdatePassPerfil").button('reset');
					        
					        	location.reload();
					        }
	    				}
					}).show();
				}else{
					new Noty({		
						type: 'info',    
						closeWith: ['click', 'button'],
						timeout: 1500,
						text: obj.respuesta,
					}).show();
					$("#btnUpdatePassPerfil").button('reset');

				}
				
			}
		});
	}else{
		$("#btnUpdatePassPerfil").button('reset');
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 2000,
			text: 'Hay Errores en el formulario de registro'
		}).show();
	}
	
}
//fin actualizar registros

//insertar registros

function registroCliente(){
	var validator = $("#form_registro" ).validate({
							rules: {
							    // compound rule
							    telefono_cliente: {
							      required: true,
							      digits: true
							    },
							    // compound rule
							    edad_cliente: {
							      required: true,
							      digits: true
							    }
							  },
							success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
			url:base_url+"/cliente/guardarDataRegistro",
			type:"POST",
			data:$("#form_registro").serialize(),
			success:function(respuesta){
				console.log("fin");
				console.log(respuesta);
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				//window.location = base_url+"/home/perfil";
				//respuesta = $.parseJSON(respuesta);
				console.log(respuesta);
				new Noty({		
					type: 'success',    
					closeWith: ['click', 'button'],
					timeout: 2000,
					text: respuesta['responseText']
				}).show();
			}
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 2000,
			text: 'Hay Errores en el formulario de registro'
		}).show();
	}
}
	$("#btnregistrar").click(registroCliente);
//fin insertar registros

/*cliente script*/
	$("#region_cliente").change(function(){
		var region = $(this).val();
		$.ajax({
			url:base_url+'/provincia/obtenerProvinciasRegion',
			type:"POST",
			data:{id_region:region},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorProvincias = $("#provincia_cliente");
				var selectorComunas = $("#comuna_cliente");
				var baseSubtotal = 0;
				selectorProvincias.empty();
				selectorComunas.empty();
				selectorComunas.append("<option value=''>Seleccione Provincia</option>");

				selectorProvincias.append("<option value=''>Seleccione Provincia</option>");
				$.each( respuesta, function( index, element ){
					selectorProvincias.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});

	$("#provincia_cliente").change(function(){
		var provincia = $(this).val();
		$.ajax({
			url:base_url+'/comuna/obtenerComunasProvincia',
			type:"POST",
			data:{id_provincia:provincia},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				console.log("obtenidos");
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				//alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
				respuesta = $.parseJSON(respuesta['responseText']);
				var selectorComunas = $("#comuna_cliente");
				var baseSubtotal = 0;
				selectorComunas.empty();

				selectorComunas.append("<option value=''>Seleccione Comuna</option>");
				$.each( respuesta, function( index, element ){
					selectorComunas.append("<option value='"+element['id']+"'>"+element['nombre']+"</option>");
				});
			}
		});
	});
	$('#rut_cliente').tooltip({
		title:"Rut Erroneo",
		trigger: 'manual'
	});
	$('#rut_cliente').Rut({
	  on_error: function(){ 
	  	$('#rut_cliente').parent().addClass("has-error");
	  	$('#rut_cliente').tooltip('show');
	  	$('#rut_cliente').focus();
	  },
	  on_success:function(){
	  	$('#rut_cliente').parent().removeClass("has-error");
	  	$('#rut_cliente').tooltip('hide');
	  },
	  format_on: 'keyup'
	});
	$('#rut_facturacion').tooltip({
		title:"Rut Erroneo",
		trigger: 'manual'
	});
	$('#rut_facturacion').Rut({
	  on_error: function(){ 
	  	$('#rut_facturacion').parent().addClass("has-error");
	  	$('#rut_facturacion').tooltip('show');
	  	$('#rut_facturacion').focus();
	  },
	  on_success:function(){
	  	$('#rut_facturacion').parent().removeClass("has-error");
	  	$('#rut_facturacion').tooltip('hide');
	  },
	  format_on: 'keyup'
	});
/*fin cliente script*/

});
