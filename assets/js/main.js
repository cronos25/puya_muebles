var site_url = window.location.origin+"/puya_muebles";
var base_url = window.location.origin+"/puya_muebles";
var estado_login = false;
$(document).ready(function() {
/*script general*/
	jQuery.extend(jQuery.validator.messages, {
	    required: "Este campo es requerido.",
	    remote: "Por favor Arregle este campo.",
	    email: "Ingrese una direccion de email valida.",
	    url: "Ingrese una URL valida.",
	    date: "Ingrese una fecha valida.",
	    dateISO: "Ingrese una fecha (ISO) valida.",
	    number: "Ingrese un numero valido.",
	    digits: "Ingrese solo digitos.",
	    creditcard: "Por favor ingrese un numero valido de tarjeta de credito.",
	    equalTo: "Por favor reingrese el mismo valor.",
	    accept: "Por favor ingrese un valor con extension valida.",
	    maxlength: jQuery.validator.format("Por no ingrese no mas de {0} caracteres."),
	    minlength: jQuery.validator.format("Por favor ingrese al menos {0} caracteres."),
	    rangelength: jQuery.validator.format("Por favor ingrese un valor entre {0} y {1} caracteres."),
	    range: jQuery.validator.format("Por favor ingrese un valor entre {0} y {1}."),
	    max: jQuery.validator.format("El valor ingresado debe ser menor a {0}."),
	    min: jQuery.validator.format("El valor ingresado debe ser mayor a {0}.")
	});
/*fin script general*/	

/*producto script*/
(function ($) {
  
  $('.spinner .btn:first-of-type').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
  });
  $('.spinner .btn:last-of-type').on('click', function() {
  	if($('.spinner input').val()>1){
	    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
	}
  });
  $('.spinner input').on('blur',function(){
  	if($('.spinner input').val() == ""){
  		$('.spinner input').val(1)
  	}
  });

  $('.spinner input').keypress(function(e){
	onlynumbers(e);
	});

  $('.prod_spinner_cantidad_up').on('click', function() {
  	var parent = $(this).parents('.prod_spinner_cantidad');
  	if(parseInt(parent.find('input').attr("data-max-value"), 10) >	 
  		parseInt(parent.find('input').val(), 10)){
    	parent.find('input').val( parseInt(parent.find('input').val(), 10) + 1);
    }
  });
  $('.prod_spinner_cantidad_down').on('click', function() {
  	var parent = $(this).parents('.prod_spinner_cantidad');
  	if(parent.find('input').val()>1){
	    parent.find('input').val( parseInt(parent.find('input').val(), 10) - 1);
	}
  });
  $('.prod_spinner_cantidad input').on('blur',function(){
  	var parent = $(this).parents('.prod_spinner_cantidad');
  	if(parent.find('input').val() == ""){
  		parent.find('input').val(1)
  	}else if(parseInt(parent.find('input').attr("data-max-value"), 10) <	 
  		parseInt(parent.find('input').val(), 10)){
  		parent.find('input').val(parseInt(parent.find('input').attr("data-max-value"), 10));
  	}
  });

  $('.prod_spinner_cantidad input').keypress(function(e){
	onlynumbers(e);
	});
})(jQuery);

	$(".agregaProdCarro").click(agregaProd);
	//function agregaProd(data){
	function agregaProd(){
		if($('.prod_spinner_cantidad').val() > 0){
			$.ajax({
				url:base_url+"/carro_compra/addItemShoppingCart",
				type:"POST",
				data: $("#form_add_item_shoppingcart").serialize()
			}).done(function(respuesta){
					
				new Noty({		
					type: 'success',    
					closeWith: ['click', 'button'],
					timeout: 1500,
					text: respuesta,
					callbacks: {
			        onClose: function() {location.reload();}
				}
				}).show();
			});
		}else{
			new Noty({		
				type: 'warning',    
				closeWith: ['click', 'button'],
				timeout: 1500,
				text: "la cantidad debe ser mayor a 0"
			}).show();
		}
	};

	//addItemPage
	/*$(".agregaProdCarroPage").click(function(){
		agregaProdPage($(this).data('prod'));
	});*/
	
	$(".selector_color").click(function(){
		var parent = $(this).parents('li');
		var superparent = parent.parents('ul');
		superparent.find("li").find('input').attr('checked', false);
		superparent.find("li").removeClass("active");
		
		parent.addClass('active');
		parent.find("input").attr('checked', true);
	});
	
/*producto script*/

/*categoria script*/
$("input.check_filter_brand, input.check_filter_precio_cate").on("click",function(){
		var favorite = [];
		var filtromoney1 = false;//<10
		var filtromoney2 = false;//>11-<=200
		var filtromoney3 = false;//>21-<=35
		var filtromoney4 = false;//>36-<=50
		var filtromoney5 = false;//>50

	    $.each($("input[name='chk_filtro_marcas']:checked"), function(){            
	        favorite.push($(this).val());
	    });
	    $.each($("input[name='chk_filtro_precio_cate']:checked"), function(){            
	        if($(this).val()==1){
	        	filtromoney1=true;
	        }else if($(this).val()==2){
	        	filtromoney2=true;
	        }else if($(this).val()==3){
	        	filtromoney3=true;
	        }else if($(this).val()==4){
	        	filtromoney4=true;
	        }else if($(this).val()==5){
	        	filtromoney5=true;
	        }
	    });
	    if (favorite.length == 0){
	    	if($("#content_main_categorias").attr("data-view") != null){
				$.ajax({
					url:base_url+"/categoria/getDataListCategorias",
					type:"POST",
					data:{catevalue:$("#content_main_categorias").attr("data-view")},
					dataType : 'json',
					success:function(respuesta){
						//flashSimple("Estado Registro", respuesta);
						//window.location = site_url+"/home/muestra/"+respuesta;
						var contenedor = $("#content_main_categorias");
						var contenedorpagination = $("#pagination_categorias");
						contenedor.empty();
						contenedorpagination.empty();
						var i = 0;
						var paneles=0;
						var contentmaqueta = "";
						$.each( respuesta['categorias'], function( index, productoItem ){
							if(i%12==0){
				            	paneles++;
				                contentmaqueta+= '<div class="tab-pane col-md-12 fade';
				                if(paneles==1){contentmaqueta+= ' in active';}
				                contentmaqueta+='" id="tab'+paneles+'" role="tabpanel">';
				            }  
				            if(!filtromoney1&&
				            	!filtromoney2&&
				            	!filtromoney3&&
				            	!filtromoney4&&
				            	!filtromoney5){
				            	contentmaqueta+='<div class="col-md-4">'+
									'<div class="item-thumbnail">'+
									'<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
									'</div>'+
									'<div class="item-details">'+
									'<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
									'<div class="item-info">'+productoItem['descripcion']+'</div>'+
									'<div class="row grid-pre">'+
									  '<div class="col-md-6">';
								if(productoItem['envio_gratis']==0){
									contentmaqueta+='<div class="item-envio">'+
								    '<span>'+
								     ' <svg class="icon" viewBox="0 0 46 32">'+
								      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
								      '</svg></span>'+
								      'Envio gratis'+
								  '</div>';
								}
								contentmaqueta+='</div>'+
								  '<div class="col-md-6">';
								if(Boolean(productoItem['regalos'])){
									contentmaqueta+='<div class="item-regalo">'+
								        '<span><svg class="icon" viewBox="0 0 29 32">'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
								        '</svg></span>'+
								        'Regalo'+
								      '</div>';
								}
				          		contentmaqueta+='</div>'+
							        '</div>'+
							        '<div class="row">'+
							          '<div class="col-md-6">'+
							            '<div class="item-price">$'+
											darformatomiles(productoItem['precio'])+
							            '</div>'+
							          '</div>'+
							          '<div class="col-md-6 text-right">';
							    if(estado_login){
						            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							       }else{
							        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
						            
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							        }
							    contentmaqueta+='</div>'+
							        '</div>'+
							      '</div>'+
							    '</div>';
				            }else{
				            	if( (filtromoney1 && productoItem['precio']<=10000) ||
				            		(filtromoney2 && productoItem['precio']>10000 && productoItem['precio']<=20000) ||
				            		(filtromoney3 && productoItem['precio']>20000 && productoItem['precio']<=35000) ||
				            		(filtromoney4 && productoItem['precio']>35000 && productoItem['precio']<=50000) ||
				            		(filtromoney5 && productoItem['precio']>50000)){
					            	contentmaqueta+='<div class="col-md-4">'+
								      '<div class="item-thumbnail">'+
								        '<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
								        '</div>'+
								      '<div class="item-details">'+
								        '<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
										'<div class="item-info">'+productoItem['descripcion']+'</div>'+
								        '<div class="row grid-pre">'+
								          '<div class="col-md-6">';
									if(productoItem['envio_gratis']==0){
										contentmaqueta+='<div class="item-envio">'+
									    '<span>'+
									     ' <svg class="icon" viewBox="0 0 46 32">'+
									      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
									      '</svg></span>'+
									      'Envio gratis'+
									  '</div>';
									}
									contentmaqueta+='</div>'+
									  '<div class="col-md-6">';
									if(Boolean(productoItem['regalos'])){
										contentmaqueta+='<div class="item-regalo">'+
									        '<span><svg class="icon" viewBox="0 0 29 32">'+
									        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
									        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
									        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
									        '</svg></span>'+
									        'Regalo'+
									      '</div>';
									}
					          		contentmaqueta+='</div>'+
								        '</div>'+
								        '<div class="row">'+
								          '<div class="col-md-6">'+
								            '<div class="item-price">$'+
												darformatomiles(productoItem['precio'])+
								            '</div>'+
								          '</div>'+
								          '<div class="col-md-6 text-right">';
						          if(estado_login){
						            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							       }else{
							        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
						            
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							        }
							    contentmaqueta+='</div>'+
							        '</div>'+
							      '</div>'+
							    '</div>';
						            }
				            
				            }
							i++;
			        		if(i%12==0) contentmaqueta+="</div>";
						});
						
						contenedor.append(contentmaqueta);

						var contentmaquetapag = "";
						contentmaquetapag = '<li class="previous" onclick="goTo(1);">'+
		                            '<a data-target="" aria-label="Previous">'+
		                            '<span aria-hidden="true">&laquo;</span>'+
		                            '</a>'+
		                        '</li>';
		                         for(var j=1;j<=paneles;j++){
		                        contentmaquetapag+='<li class="pagination_sel';
		                        if(j==1) contentmaquetapag+=' active ';
		                        contentmaquetapag+='"'; 
		                        if(j==1){contentmaquetapag+=' id="first"';} 
		                        contentmaquetapag+='>'+
		                            '<a aria-controls="tab'+j+'" data-toggle="tab" data-target="#tab'+j+'" role="tab">'+j+'</a>'+
		                        '</li>';
		                        }
		                        
		                    	contentmaquetapag+='<li class="next" onclick="goTo(2);">'+
		                        '<a data-target="" aria-label="Next">'+
		                        '<span aria-hidden="true">&raquo;</span>'+
		                        '</a>'+
		                        '</li>';

		                contenedorpagination.append(contentmaquetapag);
		                $(".agregaProdCarroPage").click(function(){
							agregaProdPage($(this).attr('data-prod'));
						});
					},
					complete:function(respuesta){
						//location.reload();
					}
				});
	    	}
		}else{
	    	if($("#content_main_categorias").attr("data-view") != null){
				$.ajax({
					url:base_url+"/categoria/getDataListCategorias",
					type:"POST",
					data:{catevalue:$("#content_main_categorias").attr("data-view"),dfb:favorite},
					dataType : 'json',
					success:function(respuesta){
						//flashSimple("Estado Registro", respuesta);
						//window.location = site_url+"/home/muestra/"+respuesta;
						var contenedor = $("#content_main_categorias");
						var contenedorpagination = $("#pagination_categorias");
						contenedor.empty();
						contenedorpagination.empty();
						var i = 0;
						var paneles=0;
						var contentmaqueta = "";
						$.each( respuesta['categorias'], function( index, productoItem ){
							if(i%12==0){
				            	paneles++;
				                contentmaqueta+= '<div class="tab-pane col-md-12 fade';
				                if(paneles==1){contentmaqueta+= ' in active';}
				                contentmaqueta+='" id="tab'+paneles+'" role="tabpanel">';
				            }  
				            if(!filtromoney1&&
				            	!filtromoney2&&
				            	!filtromoney3&&
				            	!filtromoney4&&
				            	!filtromoney5){
				            	contentmaqueta+='<div class="col-md-4">'+
									'<div class="item-thumbnail">'+
									'<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
									'</div>'+
									'<div class="item-details">'+
									'<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
									'<div class="item-info">'+productoItem['descripcion']+'</div>'+
									'<div class="row grid-pre">'+
									  '<div class="col-md-6">';
								if(productoItem['envio_gratis']==0){
									contentmaqueta+='<div class="item-envio">'+
								    '<span>'+
								     ' <svg class="icon" viewBox="0 0 46 32">'+
								      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
								      '</svg></span>'+
								      'Envio gratis'+
								  '</div>';
								}
								contentmaqueta+='</div>'+
								  '<div class="col-md-6">';
								if(Boolean(productoItem['regalos'])){
									contentmaqueta+='<div class="item-regalo">'+
								        '<span><svg class="icon" viewBox="0 0 29 32">'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
								        '</svg></span>'+
								        'Regalo'+
								      '</div>';
								}
				          		contentmaqueta+='</div>'+
							        '</div>'+
							        '<div class="row">'+
							          '<div class="col-md-6">'+
							            '<div class="item-price">$'+
											darformatomiles(productoItem['precio'])+
							            '</div>'+
							          '</div>'+
							          '<div class="col-md-6 text-right">';
							    if(estado_login){
						            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							       }else{
							        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
						            
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							        }
							    contentmaqueta+='</div>'+
							        '</div>'+
							      '</div>'+
							    '</div>';
				            }else{
				            	if( (filtromoney1 && productoItem['precio']<=10000) ||
				            		(filtromoney2 && productoItem['precio']>10000 && productoItem['precio']<=20000) ||
				            		(filtromoney3 && productoItem['precio']>20000 && productoItem['precio']<=35000) ||
				            		(filtromoney4 && productoItem['precio']>35000 && productoItem['precio']<=50000) ||
				            		(filtromoney5 && productoItem['precio']>50000)){
				            		
						            	contentmaqueta+='<div class="col-md-4">'+
									      '<div class="item-thumbnail">'+
									        '<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
									        '</div>'+
									      '<div class="item-details">'+
									        '<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
											'<div class="item-info">'+productoItem['descripcion']+'</div>'+
									        '<div class="row grid-pre">'+
									          '<div class="col-md-6">';
										if(productoItem['envio_gratis']==0){
											contentmaqueta+='<div class="item-envio">'+
										    '<span>'+
										     ' <svg class="icon" viewBox="0 0 46 32">'+
										      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
										      '</svg></span>'+
										      'Envio gratis'+
										  '</div>';
										}
										contentmaqueta+='</div>'+
										  '<div class="col-md-6">';
										if(Boolean(productoItem['regalos'])){
											contentmaqueta+='<div class="item-regalo">'+
										        '<span><svg class="icon" viewBox="0 0 29 32">'+
										        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
										        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
										        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
										        '</svg></span>'+
										        'Regalo'+
										      '</div>';
										}
						          		contentmaqueta+='</div>'+
									        '</div>'+
									        '<div class="row">'+
									          '<div class="col-md-6">'+
									            '<div class="item-price">$'+
													darformatomiles(productoItem['precio'])+
									            '</div>'+
									          '</div>'+
									          '<div class="col-md-6 text-right">';
									    if(estado_login){
								            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'" href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
									              '<svg class="icon" viewBox="0 0 36 32">'+
									                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
									                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
									                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
									                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
									                    '</svg>'+
										            '</a>';
									       }else{
									        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
								            
								              '<svg class="icon" viewBox="0 0 36 32">'+
								                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
								                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
								                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
								                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
								                    '</svg>'+
									            '</a>';
									        }
									    contentmaqueta+='</div>'+
									        '</div>'+
									      '</div>'+
									    '</div>';
								            }
				            
				            }
							i++;
			        		if(i%12==0) contentmaqueta+="</div>";
						});
						
						contenedor.append(contentmaqueta);

						var contentmaquetapag = "";
						contentmaquetapag = '<li class="previous" onclick="goTo(1);">'+
		                            '<a data-target="" aria-label="Previous">'+
		                            '<span aria-hidden="true">&laquo;</span>'+
		                            '</a>'+
		                        '</li>';
		                         for(var j=1;j<=paneles;j++){
		                        contentmaquetapag+='<li class="pagination_sel';
		                        if(j==1) contentmaquetapag+=' active ';
		                        contentmaquetapag+='"'; 
		                        if(j==1){contentmaquetapag+=' id="first"';} 
		                        contentmaquetapag+='>'+
		                            '<a aria-controls="tab'+j+'" data-toggle="tab" data-target="#tab'+j+'" role="tab">'+j+'</a>'+
		                        '</li>';
		                        }
		                        
		                    	contentmaquetapag+='<li class="next" onclick="goTo(2);">'+
		                        '<a data-target="" aria-label="Next">'+
		                        '<span aria-hidden="true">&raquo;</span>'+
		                        '</a>'+
		                        '</li>';

		                contenedorpagination.append(contentmaquetapag);
		                $(".agregaProdCarroPage").click(function(){
							agregaProdPage($(this).attr('data-prod'));
						});
					},
					complete:function(respuesta){
						//location.reload();
					}
				});
			};
	    };
	});
/*fin categoria script*/
/*buscador script*/

/*----------------------------
      price-slider active
------------------------------ */  
  $( "#slider-range" ).on('slidestop',function( event, ui ){
        filtroEnBuscador();
    });
/*----------------------------
  price-slider active
------------------------------ */

$(".selector_color_filtro").click(function(){
	var parent = $(this).parents('li');
	var superparent = parent.parents('ul');
	if (parent.hasClass('active')) {
		parent.removeClass("active");
		parent.find('input').prop( "checked", false );
	}else{
		parent.addClass('active');
		parent.find("input").prop('checked', true);
	}

	filtroEnBuscador();
});

$("#btnBuscadorSite, input.check_filter_brand_search, input.check_filter_cate_search").click(function(){
	filtroEnBuscador();
});


//$("input.check_filter_brand_search, input.check_filter_cate_search").click(filtroEnBuscador());

function daydiff(first, second) {
    return Math.round((second-first)/(1000*60*60*24));
}

function filtroEnBuscador(){
	var favCategorias = [];
	var favColores = [];
	var favMarcas = [];
	var buscarStr="";

    $.each($("input[name='chk_filtro_marcas_buscar']:checked"), function(){            
        favMarcas.push($(this).val());
    });

    $.each($("input[name='chk_filtro_categorias_buscar']:checked"), function(){            
        favCategorias.push($(this).val());
    });

    $.each($("input[name='color_data_filtro']"), function(){            
        if ($(this).is( ":checked" )) {
        	favColores.push($(this).val());
        }
    });

    //rango a precios a filtrar
	var valuesPrecio = $( "#slider-range" ).slider( "option", "values" );
	/*
	alert("precios menor:"+valuesPrecio[0]+" mayor: "+valuesPrecio[1]);

	//id de colores a filtrar
	alert("colores :"+ favColores.length);
	alert(textoBuscadorSite);*/

	textoBuscadorSite = $("#textoBuscadorSite").val();


	$.ajax({
		url:base_url+"/productos/getDataListFiltrados",
		type:"POST",
		data:{tdf:'b',
				fm:favMarcas,
				fc:favCategorias,
				fco:favColores,
				buscar:textoBuscadorSite},
		dataType : 'json',
		success:function(respuesta){
			//flashSimple("Estado Registro", respuesta);
			//window.location = site_url+"/home/muestra/"+respuesta;
			var contenedor = $("#content_productos_buscar");
			var contenedorpagination = $("#pagination_productos");
			contenedor.empty();
			contenedorpagination.empty();
			var i = 0;
			var paneles=0;
			var contentmaqueta = "";

			console.log(respuesta);

			$.each( respuesta['productos'], function( index, productoItem ){
				var muestra=false;
				
	            if (productoItem['descuento']>0 ) {
	            	var preciodesc = Math.round(productoItem['precio']-((productoItem['precio']*productoItem['descuento'])/100));
	            	if (preciodesc>= valuesPrecio[0] && preciodesc<= valuesPrecio[1]) {
	            		muestra = true;
	            	}
	            }else{
	            	if (productoItem['precio']>= valuesPrecio[0] && productoItem['precio']<= valuesPrecio[1]) {
	            		muestra = true;
	            	}
	            }

            	if (muestra) {
	            	if(i%9==0){
		            	paneles++;
		                contentmaqueta+= '<div class="tab-pane fade';
		                if(paneles==1){contentmaqueta+= ' in active';}
		                contentmaqueta+='" id="tab'+paneles+'" role="tabpanel">';
		            }  
	            		contentmaqueta+='<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
		                '<div class="single-product">'+
		                  '<div class="product-img">';

	                  var fecha1 = new Date();
	                  var fecha2 = new Date(productoItem['fecha_creacion']);
	                  var diff = daydiff(fecha1, fecha2);
	                  if (diff < 21) {
	                   contentmaqueta+='<span class="pro-label new-label">Nuevo</span>';
	                  }
	                contentmaqueta+='<span class="pro-price-2">';
	                if (productoItem['descuento']>0){

	                    contentmaqueta+= "$"+darformatomiles(Math.round(productoItem['precio']-((productoItem['precio']*productoItem['descuento'])/100)))+"<s><small>$"+darformatomiles(productoItem['precio'])+"</small></s>";
	                 }else{
	                 	contentmaqueta+="$"+darformatomiles(productoItem['precio']);
	                 }
	                contentmaqueta+='</span><a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt="imagen_producto" /></a>'+
	                  	'</div>'+
	                  	'<div class="product-info clearfix text-center">'+
	                    '<div class="fix">'+
	                      '<h4 class="post-title"><a href="#">'+productoItem['nombre'] +'</a></h4>'+
	                    '</div>'+
	                    '<div class="product-action product-action-busca clearfix">'+
	                  	'<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'" data-toggle="tooltip" data-placement="top" title="Ver Producto"><i class="zmdi zmdi-zoom-in"></i></a>';
	                if(estado_login){  
	                  contentmaqueta+='<a href="'+site_url+'/home/registro?next=productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'" data-toggle="tooltip" data-placement="top" title="Agregar al carrito"><i class="zmdi zmdi-shopping-cart-plus"></i></a>';
	                }else{
	                  contentmaqueta+='<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'" data-toggle="tooltip" data-placement="top" title="Agregar al carrito"><i class="zmdi zmdi-shopping-cart-plus"></i></a>';
	                }
	                contentmaqueta+='</div>'+
	                      '</div>'+
	                    '</div>'+
	                  '</div>';

					i++;
	        		if(i%9==0) contentmaqueta+="</div>";
            	}
			});
			contentmaqueta+=( (respuesta['productos'].lenght )%2==0)?'':'</div>';

             if (i%9!= 0 && respuesta['productos'].lenght%2==0 ){
               contentmaqueta+='</div>'+
               '</div>';
			}
			contenedor.append(contentmaqueta);

			var contentmaquetapag = "";
			contentmaquetapag = '<li class="previous" onclick="goTo(1);">'+
                          '<a href="#previous" aria-label="Previous">'+
                            '<i class="zmdi zmdi-long-arrow-left"></i>'+
                          '</a>'+
                        '</li>';
                     for(var j=1;j<=paneles;j++){
                    contentmaquetapag+='<li class="pagination_sel';
                    if(j==1) contentmaquetapag+=' active ';
                    contentmaquetapag+='"'; 
                    if(j==1){contentmaquetapag+=' id="first"';} 
                    contentmaquetapag+='>'+
                        '<a aria-controls="tab'+j+'" data-toggle="tab" data-target="#tab'+j+'" role="tab">'+j+'</a>'+
                    '</li>';
                    }
                	contentmaquetapag+='<li class="next" onclick="goTo('+((paneles>1)?2:1)+');">'+
                    '<a data-target="" aria-label="Next">'+
                    '<i class="zmdi zmdi-long-arrow-right"></i>'+
                    '</a>'+
                    '</li>';

            contenedorpagination.append(contentmaquetapag);
            $(".agregaProdCarroPage").click(function(){
				agregaProdPage($(this).attr('data-prod'));
			});
		},
		complete:function(respuesta){

			//location.reload();
		}
	});
}
/*fin buscador script*/
/*sub categoria script*/
$("input.check_filter_brand_subcate, input.check_filter_precio_subcate").on("click",function(){
		var favorite = [];
		var filtromoney1 = false;//<10
		var filtromoney2 = false;//>11-<=200
		var filtromoney3 = false;//>21-<=35
		var filtromoney4 = false;//>36-<=50
		var filtromoney5 = false;//>50

	    $.each($("input[name='chk_filtro_marcas_subcate']:checked"), function(){            
	        favorite.push($(this).val());
	    });
	    $.each($("input[name='check_filter_precio_subcate']:checked"), function(){            
	        if($(this).val()==1){
	        	filtromoney1=true;
	        }else if($(this).val()==2){
	        	filtromoney2=true;
	        }else if($(this).val()==3){
	        	filtromoney3=true;
	        }else if($(this).val()==4){
	        	filtromoney4=true;
	        }else if($(this).val()==5){
	        	filtromoney5=true;
	        }
	    });
	    if (favorite.length == 0){
	    	if($("#content_main_sub_categorias").attr("data-view") != null){
				$.ajax({
					url:base_url+"/sub_categoria/getDataListSubCategorias",
					type:"POST",
					data:{catevalue:$("#content_main_sub_categorias").attr("data-view")},
					dataType : 'json',
					success:function(respuesta){
						//flashSimple("Estado Registro", respuesta);
						//window.location = site_url+"/home/muestra/"+respuesta;
						var contenedor = $("#content_main_sub_categorias");
						var contenedorpagination = $("#pagination_sub_categorias");
						contenedor.empty();
						contenedorpagination.empty();
						var i = 0;
						var paneles=0;
						var contentmaqueta = "";
						$.each( respuesta['sub_categorias'], function( index, productoItem ){
							if(i%12==0){
				            	paneles++;
				                contentmaqueta+= '<div class="tab-pane col-md-12 fade';
				                if(paneles==1){contentmaqueta+= ' in active';}
				                contentmaqueta+='" id="tab'+paneles+'" role="tabpanel">';
				            }  
				            if(!filtromoney1&&
				            	!filtromoney2&&
				            	!filtromoney3&&
				            	!filtromoney4&&
				            	!filtromoney5){
				            	contentmaqueta+='<div class="col-md-4">'+
									'<div class="item-thumbnail">'+
									'<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
									'</div>'+
									'<div class="item-details">'+
									'<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
									'<div class="item-info">'+productoItem['descripcion']+'</div>'+
									'<div class="row grid-pre">'+
									  '<div class="col-md-6">';
								if(productoItem['envio_gratis']==0){
									contentmaqueta+='<div class="item-envio">'+
								    '<span>'+
								     ' <svg class="icon" viewBox="0 0 46 32">'+
								      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
								      '</svg></span>'+
								      'Envio gratis'+
								  '</div>';
								}
								contentmaqueta+='</div>'+
								  '<div class="col-md-6">';
								if(Boolean(productoItem['regalos'])){
									contentmaqueta+='<div class="item-regalo">'+
								        '<span><svg class="icon" viewBox="0 0 29 32">'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
								        '</svg></span>'+
								        'Regalo'+
								      '</div>';
								}
				          		contentmaqueta+='</div>'+
							        '</div>'+
							        '<div class="row">'+
							          '<div class="col-md-6">'+
							            '<div class="item-price">$'+
											darformatomiles(productoItem['precio'])+
							            '</div>'+
							          '</div>'+
							          '<div class="col-md-6 text-right">';
							    if(estado_login){
						            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							       }else{
							        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
						            
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							        }
							    contentmaqueta+='</div>'+
							        '</div>'+
							      '</div>'+
							    '</div>';
				            }else{
				            	if( (filtromoney1 && productoItem['precio']<=10000) ||
				            		(filtromoney2 && productoItem['precio']>10000 && productoItem['precio']<=20000) ||
				            		(filtromoney3 && productoItem['precio']>20000 && productoItem['precio']<=35000) ||
				            		(filtromoney4 && productoItem['precio']>35000 && productoItem['precio']<=50000) ||
				            		(filtromoney5 && productoItem['precio']>50000)){
					            	contentmaqueta+='<div class="col-md-4">'+
								      '<div class="item-thumbnail">'+
								        '<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
								        '</div>'+
								      '<div class="item-details">'+
								        '<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
										'<div class="item-info">'+productoItem['descripcion']+'</div>'+
								        '<div class="row grid-pre">'+
								          '<div class="col-md-6">';
									if(productoItem['envio_gratis']==0){
										contentmaqueta+='<div class="item-envio">'+
									    '<span>'+
									     ' <svg class="icon" viewBox="0 0 46 32">'+
									      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
									      '</svg></span>'+
									      'Envio gratis'+
									  '</div>';
									}
									contentmaqueta+='</div>'+
									  '<div class="col-md-6">';
									if(Boolean(productoItem['regalos'])){
										contentmaqueta+='<div class="item-regalo">'+
									        '<span><svg class="icon" viewBox="0 0 29 32">'+
									        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
									        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
									        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
									        '</svg></span>'+
									        'Regalo'+
									      '</div>';
									}
					          		contentmaqueta+='</div>'+
								        '</div>'+
								        '<div class="row">'+
								          '<div class="col-md-6">'+
								            '<div class="item-price">$'+
												darformatomiles(productoItem['precio'])+
								            '</div>'+
								          '</div>'+
								          '<div class="col-md-6 text-right">';
						          if(estado_login){
						            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							       }else{
							        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
						            
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							        }
							    contentmaqueta+='</div>'+
							        '</div>'+
							      '</div>'+
							    '</div>';
						            }
				            
				            }
							i++;
			        		if(i%12==0) contentmaqueta+="</div>";
						});
						
						contenedor.append(contentmaqueta);

						var contentmaquetapag = "";
						contentmaquetapag = '<li class="previous" onclick="goTo(1);">'+
		                            '<a data-target="" aria-label="Previous">'+
		                            '<span aria-hidden="true">&laquo;</span>'+
		                            '</a>'+
		                        '</li>';
		                         for(var j=1;j<=paneles;j++){
		                        contentmaquetapag+='<li class="pagination_sel';
		                        if(j==1) contentmaquetapag+=' active ';
		                        contentmaquetapag+='"'; 
		                        if(j==1){contentmaquetapag+=' id="first"';} 
		                        contentmaquetapag+='>'+
		                            '<a aria-controls="tab'+j+'" data-toggle="tab" data-target="#tab'+j+'" role="tab">'+j+'</a>'+
		                        '</li>';
		                        }
		                        
		                    	contentmaquetapag+='<li class="next" onclick="goTo(2);">'+
		                        '<a data-target="" aria-label="Next">'+
		                        '<span aria-hidden="true">&raquo;</span>'+
		                        '</a>'+
		                        '</li>';

		                contenedorpagination.append(contentmaquetapag);
		                $(".agregaProdCarroPage").click(function(){
							agregaProdPage($(this).attr('data-prod'));
						});
					},
					error:function(jqXHR,textStatus,errorThrown){
						console.log(jqXHR);
						alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
					},
					complete:function(respuesta){
						//location.reload();
					}
				});
	    	}
		}else{
	    	if($("#content_main_sub_categorias").attr("data-view") != null){
				$.ajax({
					url:base_url+"/sub_categoria/getDataListSubCategorias",
					type:"POST",
					data:{catevalue:$("#content_main_sub_categorias").attr("data-view"),dfb:favorite},
					dataType : 'json',
					success:function(respuesta){
						//flashSimple("Estado Registro", respuesta);
						//window.location = site_url+"/home/muestra/"+respuesta;
						var contenedor = $("#content_main_sub_categorias");
						var contenedorpagination = $("#pagination_sub_categorias");
						contenedor.empty();
						contenedorpagination.empty();
						var i = 0;
						var paneles=0;
						var contentmaqueta = "";
						$.each( respuesta['sub_categorias'], function( index, productoItem ){
							if(i%12==0){
				            	paneles++;
				                contentmaqueta+= '<div class="tab-pane col-md-12 fade';
				                if(paneles==1){contentmaqueta+= ' in active';}
				                contentmaqueta+='" id="tab'+paneles+'" role="tabpanel">';
				            }  
				            if(!filtromoney1&&
				            	!filtromoney2&&
				            	!filtromoney3&&
				            	!filtromoney4&&
				            	!filtromoney5){
				            	contentmaqueta+='<div class="col-md-4">'+
									'<div class="item-thumbnail">'+
									'<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
									'</div>'+
									'<div class="item-details">'+
									'<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
									'<div class="item-info">'+productoItem['descripcion']+'</div>'+
									'<div class="row grid-pre">'+
									  '<div class="col-md-6">';
								if(productoItem['envio_gratis']==0){
									contentmaqueta+='<div class="item-envio">'+
								    '<span>'+
								     ' <svg class="icon" viewBox="0 0 46 32">'+
								      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
								      '</svg></span>'+
								      'Envio gratis'+
								  '</div>';
								}
								contentmaqueta+='</div>'+
								  '<div class="col-md-6">';
								if(Boolean(productoItem['regalos'])){
									contentmaqueta+='<div class="item-regalo">'+
								        '<span><svg class="icon" viewBox="0 0 29 32">'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
								        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
								        '</svg></span>'+
								        'Regalo'+
								      '</div>';
								}
				          		contentmaqueta+='</div>'+
							        '</div>'+
							        '<div class="row">'+
							          '<div class="col-md-6">'+
							            '<div class="item-price">$'+
											darformatomiles(productoItem['precio'])+
							            '</div>'+
							          '</div>'+
							          '<div class="col-md-6 text-right">';
							    if(estado_login){
						            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							       }else{
							        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
						            
						              '<svg class="icon" viewBox="0 0 36 32">'+
						                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
						                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
						                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
						                    '</svg>'+
							            '</a>';
							        }
							    contentmaqueta+='</div>'+
							        '</div>'+
							      '</div>'+
							    '</div>';
				            }else{
				            	if( (filtromoney1 && productoItem['precio']<=10000) ||
				            		(filtromoney2 && productoItem['precio']>10000 && productoItem['precio']<=20000) ||
				            		(filtromoney3 && productoItem['precio']>20000 && productoItem['precio']<=35000) ||
				            		(filtromoney4 && productoItem['precio']>35000 && productoItem['precio']<=50000) ||
				            		(filtromoney5 && productoItem['precio']>50000)){
				            		
						            	contentmaqueta+='<div class="col-md-4">'+
									      '<div class="item-thumbnail">'+
									        '<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
									        '</div>'+
									      '<div class="item-details">'+
									        '<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
											'<div class="item-info">'+productoItem['descripcion']+'</div>'+
									        '<div class="row grid-pre">'+
									          '<div class="col-md-6">';
										if(productoItem['envio_gratis']==0){
											contentmaqueta+='<div class="item-envio">'+
										    '<span>'+
										     ' <svg class="icon" viewBox="0 0 46 32">'+
										      '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
										      '</svg></span>'+
										      'Envio gratis'+
										  '</div>';
										}
										contentmaqueta+='</div>'+
										  '<div class="col-md-6">';
										if(Boolean(productoItem['regalos'])){
											contentmaqueta+='<div class="item-regalo">'+
										        '<span><svg class="icon" viewBox="0 0 29 32">'+
										        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
										        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
										        '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
										        '</svg></span>'+
										        'Regalo'+
										      '</div>';
										}
						          		contentmaqueta+='</div>'+
									        '</div>'+
									        '<div class="row">'+
									          '<div class="col-md-6">'+
									            '<div class="item-price">$'+
													darformatomiles(productoItem['precio'])+
									            '</div>'+
									          '</div>'+
									          '<div class="col-md-6 text-right">';
									    if(estado_login){
								            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'" href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
									              '<svg class="icon" viewBox="0 0 36 32">'+
									                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
									                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
									                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
									                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
									                    '</svg>'+
										            '</a>';
									       }else{
									        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
								            
								              '<svg class="icon" viewBox="0 0 36 32">'+
								                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
								                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
								                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
								                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
								                    '</svg>'+
									            '</a>';
									        }
									    contentmaqueta+='</div>'+
									        '</div>'+
									      '</div>'+
									    '</div>';
								            }
				            
				            }
							i++;
			        		if(i%12==0) contentmaqueta+="</div>";
						});
						
						contenedor.append(contentmaqueta);

						var contentmaquetapag = "";
						contentmaquetapag = '<li class="previous" onclick="goTo(1);">'+
		                            '<a data-target="" aria-label="Previous">'+
		                            '<span aria-hidden="true">&laquo;</span>'+
		                            '</a>'+
		                        '</li>';
		                         for(var j=1;j<=paneles;j++){
		                        contentmaquetapag+='<li class="pagination_sel';
		                        if(j==1) contentmaquetapag+=' active ';
		                        contentmaquetapag+='"'; 
		                        if(j==1){contentmaquetapag+=' id="first"';} 
		                        contentmaquetapag+='>'+
		                            '<a aria-controls="tab'+j+'" data-toggle="tab" data-target="#tab'+j+'" role="tab">'+j+'</a>'+
		                        '</li>';
		                        }
		                        
		                    	contentmaquetapag+='<li class="next" onclick="goTo(2);">'+
		                        '<a data-target="" aria-label="Next">'+
		                        '<span aria-hidden="true">&raquo;</span>'+
		                        '</a>'+
		                        '</li>';

		                contenedorpagination.append(contentmaquetapag);
		                $(".agregaProdCarroPage").click(function(){
							agregaProdPage($(this).attr('data-prod'));
						});
					},
					error:function(jqXHR,textStatus,errorThrown){
						console.log(jqXHR);
						alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
					},
					complete:function(respuesta){
						//location.reload();
					}
				});
			};
	    };
	});
/*fin sub categoria script*/
/*newsletter script*/
//
$("#btn_newssletter").click(inscribeNewsletter);
function inscribeNewsletter(){
	var validator = $("#newssletter_footer").validate({
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
		url:base_url+"/newsletter/suscribirNewsletter",
		type:"POST",
		data: $("#newssletter_footer").serialize()
		}).done(function(respuesta){
			new Noty({		
				type: 'info',    
				closeWith: ['click', 'button'],
				timeout: 1500,
				text: respuesta
			}).show();	
		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 1500,
			text: 'Por favor ingrese un mail'
		}).show();
	}
	
};
/*fin newsletter*/

/*servicio script*/


$("#submitContactoServicio").click(contactoServicioCall);
function contactoServicioCall(){
	$("#submitContactoServicio").data('text',"Enviando Mensaje");
	var $btn = $("#submitContactoServicio").button('loading');

	var validator = $("#form_contacto_servicio").validate({
						rules:{
							email:{
								email:true
							}
						},
						  success: function(label) {
						    	label.addClass("valid");
							}
					  });
	if (validator.form()) {
		$.ajax({
		url:base_url+"/servicio/contactarServicio",
		type:"POST",
		data: $("#form_contacto_servicio").serialize()
		}).done(function(respuesta){
			new Noty({		
				type: 'info',    
				closeWith: ['click', 'button'],
				timeout: 1500,
				text: respuesta
			}).show();	
			$btn.button('reset');
			$("#submitContactoServicio").data('text',"Enviar Mensaje");

		});
	}else{
		new Noty({		
			type: 'error',    
			closeWith: ['click', 'button'],
			timeout: 1500,
			text: 'Por favor complete todos los campos'
		}).show();
		$btn.button('reset');
		$("#submitContactoServicio").data('text',"Enviar Mensaje");
	}
}

/*fin servicio script*/

});

function mostrarCategoriasAll(){
	if($("#content_main_categorias").attr("data-view") != null){
		$.ajax({
			url:base_url+"/categoria/getDataListCategorias",
			type:"POST",
			data:{catevalue:$("#content_main_categorias").attr("data-view")},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				var contenedor = $("#content_main_categorias");
				var contenedorpagination = $("#pagination_categorias");
				contenedor.empty();
				contenedorpagination.empty();
				var i = 0;
				var paneles=0;
				var contentmaqueta = "";
				$.each( respuesta['categorias'], function( index, productoItem ){
					if(i%12==0){
		            	paneles++;
		                contentmaqueta+= '<div class="tab-pane col-md-12 fade';
		                if(paneles==1){contentmaqueta+= ' in active';}
		                contentmaqueta+='" id="tab'+paneles+'" role="tabpanel">';
		            }  
		            contentmaqueta+='<div class="col-md-4">'+
			      '<div class="item-thumbnail">'+
			        '<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
			        '</div>'+
			      '<div class="item-details">'+
			        '<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
					'<div class="item-info">'+productoItem['descripcion']+'</div>'+
			        '<div class="row grid-pre">'+
			          '<div class="col-md-6">';
			          if(productoItem['envio_gratis']==0){
							contentmaqueta+='<div class="item-envio">'+
			                '<span>'+
			                 ' <svg class="icon" viewBox="0 0 46 32">'+
			                  '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
			                  '</svg></span>'+
			                  'Envio gratis'+
			              '</div>';
						}
              			contentmaqueta+='</div>'+
				          '<div class="col-md-6">';
				        if(Boolean(productoItem['regalos'])){
							contentmaqueta+='<div class="item-regalo">'+
				                '<span><svg class="icon" viewBox="0 0 29 32">'+
				                '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
				                '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
				                '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
				                '</svg></span>'+
				                'Regalo'+
				              '</div>';
						}
          		contentmaqueta+='</div>'+
					        '</div>'+
					        '<div class="row">'+
					          '<div class="col-md-6">'+
					            '<div class="item-price">$'+
									darformatomiles(productoItem['precio'])+
					            '</div>'+
					          '</div>'+
					          '<div class="col-md-6 text-right">';
				if(estado_login){
		            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
		              '<svg class="icon" viewBox="0 0 36 32">'+
		                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
		                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
		                    '</svg>'+
			            '</a>';
			       }else{
			        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
		            
		              '<svg class="icon" viewBox="0 0 36 32">'+
		                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
		                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
		                    '</svg>'+
			            '</a>';
			        }
			    contentmaqueta+='</div>'+
			        '</div>'+
			      '</div>'+
			    '</div>';

					i++;
			        if(i%12==0) contentmaqueta+="</div>";
				});
				
				contenedor.append(contentmaqueta);
				var contentmaquetapag = "";
				contentmaquetapag = '<li class="previous" onclick="goTo(1);">'+
                            '<a data-target="" aria-label="Previous">'+
                            '<span aria-hidden="true">&laquo;</span>'+
                            '</a>'+
                        '</li>';
                         for(var j=1;j<=paneles;j++){
                        contentmaquetapag+='<li class="pagination_sel';
                        if(j==1) contentmaquetapag+=' active ';
                        contentmaquetapag+='"'; 
                        if(j==1){contentmaquetapag+=' id="first"';} 
                        contentmaquetapag+='>'+
                            '<a aria-controls="tab'+j+'" data-toggle="tab" data-target="#tab'+j+'" role="tab">'+j+'</a>'+
                        '</li>';
                        }
                        
                    	contentmaquetapag+='<li class="next" onclick="goTo(2);">'+
                        '<a data-target="" aria-label="Next">'+
                        '<span aria-hidden="true">&raquo;</span>'+
                        '</a>'+
                        '</li>';

                contenedorpagination.append(contentmaquetapag);
                $(".agregaProdCarroPage").click(function(){
						agregaProdPage($(this).attr('data-prod'));
					});
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
			}
		});
	}
}	
function mostrarSubCategoriasAll(){
	if($("#content_main_sub_categorias").attr("data-view") != null){
		$.ajax({
			url:base_url+"/sub_categoria/getDataListSubCategorias",
			type:"POST",
			data:{catevalue:$("#content_main_sub_categorias").attr("data-view")},
			dataType : 'json',
			success:function(respuesta){
				//flashSimple("Estado Registro", respuesta);
				//window.location = site_url+"/home/muestra/"+respuesta;
				var contenedor = $("#content_main_sub_categorias");
				var contenedorpagination = $("#pagination_sub_categorias");
				contenedor.empty();
				contenedorpagination.empty();
				var i = 0;
				var paneles=0;
				var contentmaqueta = "";
				$.each( respuesta['sub_categorias'], function( index, productoItem ){
					if(i%12==0){
		            	paneles++;
		                contentmaqueta+= '<div class="tab-pane col-md-12 fade';
		                if(paneles==1){contentmaqueta+= ' in active';}
		                contentmaqueta+='" id="tab'+paneles+'" role="tabpanel">';
		            }  
		            contentmaqueta+='<div class="col-md-4">'+
			      '<div class="item-thumbnail">'+
			        '<a href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'"><img src="'+productoItem['foto_producto']+'" alt=""></a>'+
			        '</div>'+
			      '<div class="item-details">'+
			        '<h1 class="item-title"><b>'+productoItem['marca']+'</b> '+productoItem['nombre']+' </h1>'+
					'<div class="item-info">'+productoItem['descripcion']+'</div>'+
			        '<div class="row grid-pre">'+
			          '<div class="col-md-6">';
			          if(productoItem['envio_gratis']==0){
							contentmaqueta+='<div class="item-envio">'+
			                '<span>'+
			                 ' <svg class="icon" viewBox="0 0 46 32">'+
			                  '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>'+
			                  '</svg></span>'+
			                  'Envio gratis'+
			              '</div>';
						}
              			contentmaqueta+='</div>'+
				          '<div class="col-md-6">';
				        if(Boolean(productoItem['regalos'])){
							contentmaqueta+='<div class="item-regalo">'+
				                '<span><svg class="icon" viewBox="0 0 29 32">'+
				                '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
				                '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>'+
				                '<path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>'+
				                '</svg></span>'+
				                'Regalo'+
				              '</div>';
						}
          		contentmaqueta+='</div>'+
					        '</div>'+
					        '<div class="row">'+
					          '<div class="col-md-6">'+
					            '<div class="item-price">$'+
									darformatomiles(productoItem['precio'])+
					            '</div>'+
					          '</div>'+
					          '<div class="col-md-6 text-right">';
				if(estado_login){
		            contentmaqueta+='<a class="btn-item-car" data-prod="'+productoItem['id']+'"	href="'+site_url+'/productos/view/'+productoItem['id']+'/'+productoItem['nombre'].replace(/[()]/g, '').replace('%','')+'">'+
		              '<svg class="icon" viewBox="0 0 36 32">'+
		                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
		                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
		                    '</svg>'+
			            '</a>';
			       }else{
			        contentmaqueta+='<a href="'+base_url+'/cliente/" class="btn-item-car">'+
		            
		              '<svg class="icon" viewBox="0 0 36 32">'+
		                    '<path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>'+
		                    '<path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>'+
		                    '<path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>'+
		                    '</svg>'+
			            '</a>';
			        }
			    contentmaqueta+='</div>'+
			        '</div>'+
			      '</div>'+
			    '</div>';

					i++;
			        if(i%12==0) contentmaqueta+="</div>";
				});
				
				contenedor.append(contentmaqueta);
				var contentmaquetapag = "";
				contentmaquetapag = '<li class="previous" onclick="goTo(1);">'+
                            '<a data-target="" aria-label="Previous">'+
                            '<span aria-hidden="true">&laquo;</span>'+
                            '</a>'+
                        '</li>';
                         for(var j=1;j<=paneles;j++){
                        contentmaquetapag+='<li class="pagination_sel';
                        if(j==1) contentmaquetapag+=' active ';
                        contentmaquetapag+='"'; 
                        if(j==1){contentmaquetapag+=' id="first"';} 
                        contentmaquetapag+='>'+
                            '<a aria-controls="tab'+j+'" data-toggle="tab" data-target="#tab'+j+'" role="tab">'+j+'</a>'+
                        '</li>';
                        }
                        
                    	contentmaquetapag+='<li class="next" onclick="goTo(2);">'+
                        '<a data-target="" aria-label="Next">'+
                        '<span aria-hidden="true">&raquo;</span>'+
                        '</a>'+
                        '</li>';

                contenedorpagination.append(contentmaquetapag);
                $(".agregaProdCarroPage").click(function(){
						agregaProdPage($(this).attr('data-prod'));
					});
			},
			error:function(jqXHR,textStatus,errorThrown){
				console.log(jqXHR);
				alert( jqXHR+"\n"+textStatus+"\n"+errorThrown);
			},
			complete:function(respuesta){
				//location.reload();
			}
		});
	}
}
//function agregaProd(data){
	/*function agregaProdPage(data,sabor){
		$.ajax({
			url:base_url+"/carro_compra/addItemShoppingCart",
			type:"POST",
			data:{producto_data:data,cantidad_data:0,sabor_data:sabor}
		}).done(function(respuesta){
			new Noty({		
				type: 'success',    
				closeWith: ['click', 'button'],
				timeout: 1500,
				text: respuesta,
				callbacks: {
			        onClose: function() {location.reload();}
				}
			}).show();
		});
	};*/

function goTo(number){
   $('ul.pagination_pag li:eq('+number+') a').tab('show');
   upgradePreNext(number);
}
function upgradePreNext(number){
	if (number>1){
       $('ul.pagination_pag li:eq(0)').attr("onclick","goTo("+(number-1)+")");
       $('ul.pagination_pag li:eq(0)').attr("class", "previous");
   } else {
       $('ul.pagination_pag li:eq(0)').attr("class", "disabled");
   }
    if (number < $('ul.pagination_pag li.pagination_sel').length){
       $('ul.pagination_pag li:eq('+($('ul.pagination_pag li.pagination_sel').length+1)+')').attr("onclick","goTo("+(number+1)+")");
       $('ul.pagination_pag li:eq('+($('ul.pagination_pag li.pagination_sel').length+1)+')').attr("class", "next");
   } else {
       $('ul.pagination_pag li:eq('+($('ul.pagination_pag li.pagination_sel').length+1)+')').attr("class", "disabled");
   }
}

function darformatomiles(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}