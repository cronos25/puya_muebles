-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 24-11-2017 a las 16:06:07
-- Versión del servidor: 5.7.20-0ubuntu0.17.10.1
-- Versión de PHP: 7.0.22-0ubuntu0.17.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `puyamuebles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner_home`
--

CREATE TABLE `banner_home` (
  `id` int(11) NOT NULL,
  `imagen` text NOT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '0' COMMENT '0.-activo, 1.-inactivo',
  `tipo_banner` int(11) NOT NULL COMMENT '0.-prducto/ 1.- informacion',
  `texto_btn_banner` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carro_compra`
--

CREATE TABLE `carro_compra` (
  `id` int(11) NOT NULL,
  `codigo` varchar(200) NOT NULL,
  `uuid` varchar(200) DEFAULT NULL COMMENT 'este campo es para un uso futuro',
  `estado` int(11) NOT NULL DEFAULT '0' COMMENT '0.- abierto / 1.- cancelado / 2.- procesado a venta/3.-finalizado',
  `id_cliente` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mensaje` text NOT NULL,
  `nombre_carro` varchar(200) DEFAULT NULL,
  `email_carro` varchar(200) DEFAULT NULL,
  `telefono_carro` varchar(50) DEFAULT NULL,
  `region_carro` int(11) DEFAULT NULL,
  `provincia_carro` int(11) DEFAULT NULL,
  `comuna_carro` int(11) DEFAULT NULL,
  `direccion_carro` varchar(240) DEFAULT NULL,
  `retiro_tienda` int(11) DEFAULT '0' COMMENT '0.- sin retiro en tienda / 1.- retiro en tienda'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `carro_compra`
--

INSERT INTO `carro_compra` (`id`, `codigo`, `uuid`, `estado`, `id_cliente`, `fecha_creacion`, `mensaje`, `nombre_carro`, `email_carro`, `telefono_carro`, `region_carro`, `provincia_carro`, `comuna_carro`, `direccion_carro`, `retiro_tienda`) VALUES
(7, '11.111.111-13', '170908174551', 3, 3, '2017-09-08 17:45:51', '', 'prueba puya', 'prueba@puya.cl', '9123123', 8, 29, 146, 'direccion falsa 123', 1),
(8, '11.111.111-13', '170923174954', 3, 3, '2017-09-23 17:49:54', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(9, '11.111.111-13', '170923181605', 1, 3, '2017-09-23 18:16:05', '', 'prueba puya', 'prueba@puya.cl', '9123123', 8, 29, 146, 'direccion falsa 123', 0),
(10, '11.111.111-13', '170925132904', 0, 3, '2017-09-25 13:29:04', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `tipo_categoria` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0.- producto / 1.- servicio',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `descripcion`, `imagen`, `tipo_categoria`, `fecha_creacion`) VALUES
(8, 'Puya Muebles', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque asperiores in minima provident reiciendis harum repellendus temporibus pariatur, amet ut, mollitia reprehenderit autem ipsam quidem velit fugiat alias quasi incidunt!', 'http://localhost/puya_muebles/assets/uploads/category/ebb6e875d4b0d5d1bdb7634d3c537b06.jpg', 0, '2017-09-13 16:48:13'),
(9, 'Puya Diseño', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque asperiores in minima provident reiciendis harum repellendus temporibus pariatur, amet ut, mollitia reprehenderit autem ipsam quidem velit fugiat alias quasi incidunt!', 'http://localhost/puya_muebles/assets/uploads/category/c22c56581cd1b9e3549a215a7e400b4d.jpg', 1, '2017-09-13 16:48:13'),
(10, 'Puya Maker', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque asperiores in minima provident reiciendis harum repellendus temporibus pariatur, amet ut, mollitia reprehenderit autem ipsam quidem velit fugiat alias quasi incidunt!', 'http://localhost/puya_muebles/assets/uploads/category/9fffb12c31f097b3f19a0b0e1e51c81e.jpg', 1, '2017-09-13 16:48:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_comuna` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `edad` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `rut` varchar(12) NOT NULL,
  `telefono` int(11) NOT NULL,
  `medio_pago_preferido` int(11) NOT NULL DEFAULT '0' COMMENT '0.- efectivo, 1.- transferencia, 2.-pago online',
  `sexo` tinyint(4) DEFAULT NULL,
  `rut_fac` varchar(13) DEFAULT NULL,
  `nombre_fac` varchar(200) DEFAULT NULL,
  `giro_fac` varchar(200) DEFAULT NULL,
  `telefono_fac` int(11) DEFAULT NULL,
  `direccion_fac` varchar(200) DEFAULT NULL,
  `region_id_fac` int(11) DEFAULT NULL,
  `provincia_id_fac` int(11) DEFAULT NULL,
  `comuna_id_fac` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `username`, `password`, `email`, `nombre`, `apellido`, `direccion`, `id_region`, `id_comuna`, `id_provincia`, `edad`, `puntos`, `rut`, `telefono`, `medio_pago_preferido`, `sexo`, `rut_fac`, `nombre_fac`, `giro_fac`, `telefono_fac`, `direccion_fac`, `region_id_fac`, `provincia_id_fac`, `comuna_id_fac`) VALUES
(3, '', 'bf450df2fa4976a6d305202e05a3c7c4', 'prueba@puya.cl', 'prueba', 'puya', 'direccion falsa 123', 8, 146, 29, 26, 19600, '11.111.111-1', 9123123, 0, 0, '', '', '', 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_codigo_usado`
--

CREATE TABLE `cliente_codigo_usado` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_codigo_promocion` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `fecha_uso` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo_promocion`
--

CREATE TABLE `codigo_promocion` (
  `id` int(11) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `codigo` varchar(50) NOT NULL COMMENT 'codigo creado por usuario',
  `descuento` int(11) NOT NULL COMMENT 'procentaje en entero max 100',
  `categoria_id` int(11) DEFAULT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `sub_categoria_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE `color` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` text,
  `hexa` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `color`
--

INSERT INTO `color` (`id`, `nombre`, `descripcion`, `hexa`) VALUES
(7, 'barniz transparente', '', 'transparent'),
(8, 'rojo', 'acabado rojo para muebles', '#ff0000'),
(9, 'azul', 'lkasdmflaksdmf lkasmdflakm&nbsp;', '#0000ff');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color_producto`
--

CREATE TABLE `color_producto` (
  `id` int(11) NOT NULL,
  `id_color` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `color_producto`
--

INSERT INTO `color_producto` (`id`, `id_color`, `id_producto`) VALUES
(76, 8, 17),
(75, 7, 17),
(85, 8, 18),
(78, 7, 19),
(79, 8, 20),
(80, 7, 21),
(81, 8, 21),
(84, 7, 18),
(86, 9, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

CREATE TABLE `comuna` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_provincia` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`id`, `nombre`, `id_provincia`) VALUES
(1, 'Iquique', 3),
(2, 'Alto Hospicio', 3),
(3, 'Pozo Almonte', 4),
(4, 'Camiña', 4),
(5, 'Colchane', 4),
(6, 'Huara', 4),
(7, 'Pica', 4),
(8, 'Antofagasta', 5),
(9, 'Mejillones', 5),
(10, 'Sierra Gorda', 5),
(11, 'Taltal', 5),
(12, 'Calama', 6),
(13, 'Ollagüe', 6),
(14, 'San Pedro de Atacama', 6),
(15, 'Tocopilla', 7),
(16, 'María Elena', 7),
(17, 'Copiapó', 8),
(18, 'Caldera', 8),
(19, 'Tierra Amarilla', 8),
(20, 'Chañaral', 9),
(21, 'Diego de Almagro', 9),
(22, 'Vallenar', 10),
(23, 'Alto del Carmen', 10),
(24, 'Freirina', 10),
(25, 'Huasco', 10),
(26, 'La Serena', 11),
(27, 'Coquimbo', 11),
(28, 'Andacollo', 11),
(29, 'La Higuera', 11),
(30, 'Paihuano', 11),
(31, 'Vicuña', 11),
(32, 'Illapel', 12),
(33, 'Canela', 12),
(34, 'Los Vilos', 12),
(35, 'Salamanca', 12),
(36, 'Ovalle', 13),
(37, 'Combarbalá', 13),
(38, 'Monte Patria', 13),
(39, 'Punitaqui', 13),
(40, 'Río Hurtado', 13),
(41, 'Valparaíso', 14),
(42, 'Casablanca', 14),
(43, 'Concón', 14),
(44, 'Juan Fernández', 14),
(45, 'Puchuncaví', 14),
(46, 'Quintero', 14),
(47, 'Viña del Mar', 14),
(48, 'Isla de Pascua', 15),
(49, 'Los Andes', 16),
(50, 'Calle Larga', 16),
(51, 'Rinconada', 16),
(52, 'San Esteban', 16),
(53, 'La Ligua', 17),
(54, 'Cabildo', 17),
(55, 'Papudo', 17),
(56, 'Petorca', 17),
(57, 'Zapallar', 17),
(58, 'Quillota', 18),
(59, 'La Calera', 18),
(60, 'Hijuelas', 18),
(61, 'La Cruz', 18),
(62, 'Nogales', 18),
(63, 'San Antonio', 19),
(64, 'Algarrobo', 19),
(65, 'Cartagena', 19),
(66, 'El Quisco', 19),
(67, 'El Tabo', 19),
(68, 'Santo Domingo', 19),
(69, 'San Felipe', 20),
(70, 'Catemu', 20),
(71, 'Llay Llay', 20),
(72, 'Panquehue', 20),
(73, 'Putaendo', 20),
(74, 'Santa María', 20),
(75, 'Quilpué', 21),
(76, 'Limache', 21),
(77, 'Olmué', 21),
(78, 'Villa Alemana', 21),
(79, 'Rancagua', 22),
(80, 'Codegua', 22),
(81, 'Coinco', 22),
(82, 'Coltauco', 22),
(83, 'Doñihue', 22),
(84, 'Graneros', 22),
(85, 'Las Cabras', 22),
(86, 'Machalí', 22),
(87, 'Malloa', 22),
(88, 'Mostazal', 22),
(89, 'Olivar', 22),
(90, 'Peumo', 22),
(91, 'Pichidegua', 22),
(92, 'Quinta de Tilcoco', 22),
(93, 'Rengo', 22),
(94, 'Requínoa', 22),
(95, 'San Vicente', 22),
(96, 'Pichilemu', 23),
(97, 'La Estrella', 23),
(98, 'Litueche', 23),
(99, 'Marchihue', 23),
(100, 'Navidad', 23),
(101, 'Paredones', 23),
(102, 'San Fernando', 24),
(103, 'Chépica', 24),
(104, 'Chimbarongo', 24),
(105, 'Lolol', 24),
(106, 'Nancagua', 24),
(107, 'Palmilla', 24),
(108, 'Peralillo', 24),
(109, 'Placilla', 24),
(110, 'Pumanque', 24),
(111, 'Santa Cruz', 24),
(112, 'Talca', 25),
(113, 'Constitución', 25),
(114, 'Curepto', 25),
(115, 'Empedrado', 25),
(116, 'Maule', 25),
(117, 'Pelarco', 25),
(118, 'Pencahue', 25),
(119, 'Río Claro', 25),
(120, 'San Clemente', 25),
(121, 'San Rafael', 25),
(122, 'Cauquenes', 26),
(123, 'Chanco', 26),
(124, 'Pelluhue', 26),
(125, 'Curicó', 27),
(126, 'Hualañé', 27),
(127, 'Licantén', 27),
(128, 'Molina', 27),
(129, 'Rauco', 27),
(130, 'Romeral', 27),
(131, 'Sagrada Familia', 27),
(132, 'Teno', 27),
(133, 'Vichuquén', 27),
(134, 'Linares', 28),
(135, 'Colbún', 28),
(136, 'Longaví', 28),
(137, 'Parral', 28),
(138, 'Retiro', 28),
(139, 'San Javier', 28),
(140, 'Villa Alegre', 28),
(141, 'Yerbas Buenas', 28),
(142, 'Concepción', 29),
(143, 'Coronel', 29),
(144, 'Chiguayante', 29),
(145, 'Florida', 29),
(146, 'Hualqui', 29),
(147, 'Lota', 29),
(148, 'Penco', 29),
(149, 'San Pedro de la Paz', 29),
(150, 'Santa Juana', 29),
(151, 'Talcahuano', 29),
(152, 'Tomé', 29),
(153, 'Hualpén', 29),
(154, 'Lebu', 30),
(155, 'Arauco', 30),
(156, 'Cañete', 30),
(157, 'Contulmo', 30),
(158, 'Curanilahue', 30),
(159, 'Los Álamos', 30),
(160, 'Tirúa', 30),
(161, 'Los Ángeles', 31),
(162, 'Antuco', 31),
(163, 'Cabrero', 31),
(164, 'Laja', 31),
(165, 'Mulchén', 31),
(166, 'Nacimiento', 31),
(167, 'Negrete', 31),
(168, 'Quilaco', 31),
(169, 'Quilleco', 31),
(170, 'San Rosendo', 31),
(171, 'Santa Bárbara', 31),
(172, 'Tucapel', 31),
(173, 'Yumbel', 31),
(174, 'Alto Biobío', 31),
(175, 'Chillán', 32),
(176, 'Bulnes', 32),
(177, 'Cobquecura', 32),
(178, 'Coelemu', 32),
(179, 'Coihueco', 32),
(180, 'Chillán Viejo', 32),
(181, 'El Carmen', 32),
(182, 'Ninhue', 32),
(183, 'Ñiquén', 32),
(184, 'Pemuco', 32),
(185, 'Pinto', 32),
(186, 'Portezuelo', 32),
(187, 'Quillón', 32),
(188, 'Quirihue', 32),
(189, 'Ránquil', 32),
(190, 'San Carlos', 32),
(191, 'San Fabián', 32),
(192, 'San Ignacio', 32),
(193, 'San Nicolás', 32),
(194, 'Treguaco', 32),
(195, 'Yungay', 32),
(196, 'Temuco', 33),
(197, 'Carahue', 33),
(198, 'Cunco', 33),
(199, 'Curarrehue', 33),
(200, 'Freire', 33),
(201, 'Galvarino', 33),
(202, 'Gorbea', 33),
(203, 'Lautaro', 33),
(204, 'Loncoche', 33),
(205, 'Melipeuco', 33),
(206, 'Nueva Imperial', 33),
(207, 'Padre las Casas', 33),
(208, 'Perquenco', 33),
(209, 'Pitrufquén', 33),
(210, 'Pucón', 33),
(211, 'Saavedra', 33),
(212, 'Teodoro Schmidt', 33),
(213, 'Toltén', 33),
(214, 'Vilcún', 33),
(215, 'Villarrica', 33),
(216, 'Cholchol', 33),
(217, 'Angol', 34),
(218, 'Collipulli', 34),
(219, 'Curacautín', 34),
(220, 'Ercilla', 34),
(221, 'Lonquimay', 34),
(222, 'Los Sauces', 34),
(223, 'Lumaco', 34),
(224, 'Purén', 34),
(225, 'Renaico', 34),
(226, 'Traiguén', 34),
(227, 'Victoria', 34),
(228, 'Puerto Montt', 37),
(229, 'Calbuco', 37),
(230, 'Cochamó', 37),
(231, 'Fresia', 37),
(232, 'Frutillar', 37),
(233, 'Los Muermos', 37),
(234, 'Llanquihue', 37),
(235, 'Maullín', 37),
(236, 'Puerto Varas', 37),
(237, 'Castro', 38),
(238, 'Ancud', 38),
(239, 'Chonchi', 38),
(240, 'Curaco de Vélez', 38),
(241, 'Dalcahue', 38),
(242, 'Puqueldón', 38),
(243, 'Queilén', 38),
(244, 'Quellón', 38),
(245, 'Quemchi', 38),
(246, 'Quinchao', 38),
(247, 'Osorno', 39),
(248, 'Puerto Octay', 39),
(249, 'Purranque', 39),
(250, 'Puyehue', 39),
(251, 'Río Negro', 39),
(252, 'San Juan de la Costa', 39),
(253, 'San Pablo', 39),
(254, 'Chaitén', 40),
(255, 'Futaleufú', 40),
(256, 'Hualaihué', 40),
(257, 'Palena', 40),
(258, 'Coyhaique', 41),
(259, 'Lago Verde', 41),
(260, 'Aysén', 42),
(261, 'Cisnes', 42),
(262, 'Guaitecas', 42),
(263, 'Cochrane', 43),
(264, 'O\'Higgins', 43),
(265, 'Tortel', 43),
(266, 'Chile Chico', 44),
(267, 'Río Ibáñez', 44),
(268, 'Punta Arenas', 45),
(269, 'Laguna Blanca', 45),
(270, 'Río Verde', 45),
(271, 'San Gregorio', 45),
(272, 'Cabo de Hornos', 46),
(273, 'Antártica', 46),
(274, 'Porvenir', 47),
(275, 'Primavera', 47),
(276, 'Timaukel', 47),
(277, 'Natales', 48),
(278, 'Torres del Paine', 48),
(279, 'Santiago', 49),
(280, 'Cerrillos', 49),
(281, 'Cerro Navia', 49),
(282, 'Conchalí', 49),
(283, 'El Bosque', 49),
(284, 'Estación Central', 49),
(285, 'Huechuraba', 49),
(286, 'Independencia', 49),
(287, 'La Cisterna', 49),
(288, 'La Florida', 49),
(289, 'La Granja', 49),
(290, 'La Pintana', 49),
(291, 'La Reina', 49),
(292, 'Las Condes', 49),
(293, 'Lo Barnechea', 49),
(294, 'Lo Espejo', 49),
(295, 'Lo Prado', 49),
(296, 'Macul', 49),
(297, 'Maipú', 49),
(298, 'Ñuñoa', 49),
(299, 'Pedro Aguirre Cerda', 49),
(300, 'Peñalolén', 49),
(301, 'Providencia', 49),
(302, 'Pudahuel', 49),
(303, 'Quilicura', 49),
(304, 'Quinta Normal', 49),
(305, 'Recoleta', 49),
(306, 'Renca', 49),
(307, 'San Joaquín', 49),
(308, 'San Miguel', 49),
(309, 'San Ramón', 49),
(310, 'Vitacura', 49),
(311, 'Puente Alto', 50),
(312, 'Pirque', 50),
(313, 'San José de Maipo', 50),
(314, 'Colina', 51),
(315, 'Lampa', 51),
(316, 'Tiltil', 51),
(317, 'San Bernardo', 52),
(318, 'Buin', 52),
(319, 'Calera de Tango', 52),
(320, 'Paine', 52),
(321, 'Melipilla', 53),
(322, 'Alhué', 53),
(323, 'Curacaví', 53),
(324, 'María Pinto', 53),
(325, 'San Pedro', 53),
(326, 'Talagante', 54),
(327, 'El Monte', 54),
(328, 'Isla de Maipo', 54),
(329, 'Padre Hurtado', 54),
(330, 'Peñaflor', 54),
(331, 'Valdivia', 35),
(332, 'Corral', 35),
(333, 'Lanco', 35),
(334, 'Los Lagos', 35),
(335, 'Máfil', 35),
(336, 'Mariquina', 35),
(337, 'Paillaco', 35),
(338, 'Panguipulli', 35),
(339, 'La Unión', 36),
(340, 'Futrono', 36),
(341, 'Lago Ranco', 36),
(342, 'Río Bueno', 36),
(343, 'Arica', 1),
(344, 'Camarones', 1),
(345, 'Putre', 2),
(346, 'General Lagos', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_servicio`
--

CREATE TABLE `datos_servicio` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL COMMENT 'email del servicio / sin el no se muestra el formulario de contacto',
  `telefono` varchar(60) DEFAULT NULL COMMENT 'telefono del servicio',
  `mail2` varchar(250) DEFAULT NULL COMMENT 'mail opcional 2',
  `mail3` varchar(250) DEFAULT NULL COMMENT 'mail opcional 3',
  `mensaje` text,
  `imagen` text COMMENT 'se puede usar despues / no hablitado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datos_servicio`
--

INSERT INTO `datos_servicio` (`id`, `id_categoria`, `email`, `telefono`, `mail2`, `mail3`, `mensaje`, `imagen`) VALUES
(1, 9, 'asd@asd.asd', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distribuidor`
--

CREATE TABLE `distribuidor` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `link` varchar(200) DEFAULT NULL,
  `autorizado` tinyint(4) NOT NULL COMMENT '0.- autorizado / 1.- no autorizado',
  `imagen` text NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `nombre` varchar(250) NOT NULL,
  `slogan` varchar(250) NOT NULL,
  `facebook_page` text NOT NULL,
  `tweeter_page` text NOT NULL,
  `instagram_page` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` text NOT NULL,
  `logo` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id`, `nombre`, `descripcion`, `logo`) VALUES
(19, 'Puya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa iure nostrum sit atque sunt, accusantium, dignissimos autem rerum consequatur iste officia animi quae officiis, repellendus nihil. Aliquam facilis sint odio!', 'http://localhost/puya_muebles/assets/uploads/brands/8248cdf335970bdfea0387c53d9db695.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `fecha_inscripcion` date NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '0' COMMENT '0.-activo, 1.-inactivo'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta_especial`
--

CREATE TABLE `oferta_especial` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `descuento` int(11) NOT NULL DEFAULT '0',
  `estado` int(11) NOT NULL DEFAULT '0' COMMENT '0.- activo / 1.- inactivo'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oferta_especial`
--

INSERT INTO `oferta_especial` (`id`, `id_producto`, `descuento`, `estado`) VALUES
(1, 17, 50, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_webpay_flow`
--

CREATE TABLE `pagos_webpay_flow` (
  `id` int(11) NOT NULL,
  `orden_comercio` int(11) NOT NULL,
  `orden_flow` int(11) NOT NULL,
  `monto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `mail_pagador` varchar(200) NOT NULL,
  `estado_pago` int(11) NOT NULL COMMENT '1 exito / 0 fracaso'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pagos_webpay_flow`
--

INSERT INTO `pagos_webpay_flow` (`id`, `orden_comercio`, `orden_flow`, `monto`, `fecha`, `mail_pagador`, `estado_pago`) VALUES
(1, 1506199995, 22669, 90000, '2017-09-23', 'prueba@puya.cl', 1),
(2, 1506199995, 22669, 90000, '2017-09-23', 'prueba@puya.cl', 1),
(3, 1506356570, 22711, 95000, '2017-09-25', 'prueba@puya.cl', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parrafo_servicio`
--

CREATE TABLE `parrafo_servicio` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `mensaje_parrafo` text NOT NULL,
  `imagen_parrafo` text NOT NULL,
  `tipo_parrafo` tinyint(4) NOT NULL COMMENT '0.-texto derecha / 1.- texto izquierda / 2.- sin imagen / 3.- sin texto',
  `numero_parrafo` int(11) NOT NULL COMMENT 'posicion del parrafo a mostrar, define el orden de la muestra del servicio'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parrafo_servicio`
--

INSERT INTO `parrafo_servicio` (`id`, `id_categoria`, `mensaje_parrafo`, `imagen_parrafo`, `tipo_parrafo`, `numero_parrafo`) VALUES
(4, 9, 'asdfasdf asdfas fasdfas fasd asfasdf asdf asd fasd f', 'http://localhost/puya_muebles/assets/uploads/parrafos/149c2c84717fbb723b2d0bed47d5e28e.jpg', 0, 1),
(10, 9, '', 'http://localhost/puya_muebles/assets/uploads/parrafos/86dfbda6af86febe4072e32001ccf2fb.jpg', 0, 1),
(11, 9, '', 'http://localhost/puya_muebles/assets/uploads/parrafos/14f373bd64150d8ff8b7648ead9befc7.jpg', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio_envio`
--

CREATE TABLE `precio_envio` (
  `id` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  `comuna_id` int(11) NOT NULL,
  `desde` float NOT NULL DEFAULT '0' COMMENT 'en cm',
  `hasta` float NOT NULL DEFAULT '0' COMMENT 'en cm',
  `tipo_medida` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0.- S / 1.- M / 2.-L / 3.- XL / 4.-XXL / 5.- XXS'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `precio_envio`
--

INSERT INTO `precio_envio` (`id`, `precio`, `region_id`, `provincia_id`, `comuna_id`, `desde`, `hasta`, `tipo_medida`) VALUES
(7, 5000, 8, 29, 146, 0, 300, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` int(11) NOT NULL,
  `foto_etiqueta` text COMMENT 'imagen en base64 /solo en caso de productos',
  `foto_producto` text NOT NULL COMMENT 'imagen en base64',
  `foto_home` text NOT NULL COMMENT 'imagen en base64',
  `foto_opcional` text,
  `sku` varchar(100) NOT NULL,
  `descuento` int(11) NOT NULL DEFAULT '0',
  `envio_gratis` tinyint(1) DEFAULT '0' COMMENT '0.- sin envio gratis 1.- envio gratis',
  `categoria_id` int(11) NOT NULL COMMENT 'id de categoria',
  `sub_categoria_id` int(11) DEFAULT NULL COMMENT 'puede o no tener subcategoria',
  `marca_id` int(11) NOT NULL,
  `stock` int(11) DEFAULT '0' COMMENT 'en caso de stack toma la cantidad de stock en base al stock de los productos',
  `tipo_entrega` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0.- local, 1.- envio, 2.- ambas',
  `regalos` text COMMENT 'regalos a entregar descritos como text/solo en caso de productos',
  `servicios` int(11) DEFAULT '0' COMMENT 'numero de servicios del producto /solo en caso de productos',
  `is_stack` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0.- es producto / 1.- es stack',
  `color` int(11) DEFAULT NULL,
  `medida_s` varchar(200) DEFAULT '0_0_0' COMMENT 'se toman las medidas en formato string [x_y_z] alto, ancho y largo',
  `medida_m` varchar(200) DEFAULT '0_0_0' COMMENT 'se toman las medidas en formato string [x_y_z] alto, ancho y largo',
  `medida_l` varchar(200) DEFAULT '0_0_0' COMMENT 'se toman las medidas en formato string [x_y_z] alto, ancho y largo',
  `medida_xl` varchar(200) DEFAULT '0_0_0' COMMENT 'se toman las medidas en formato string [x_y_z] alto, ancho y largo',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `precio_m` int(11) DEFAULT '0',
  `precio_l` int(11) DEFAULT '0',
  `precio_xl` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `servicios`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `fecha_creacion`, `precio_m`, `precio_l`, `precio_xl`) VALUES
(17, 'Arrimo', '', 100000, '', 'http://localhost/puya_muebles/assets/uploads/productos/Arrimo/d81491527a6344e3962fe94d5a1eb2e3.jpg', '', NULL, 'SKU-81917', 10, 1, 8, 1, 19, 2, 2, '', 0, 0, NULL, '30_60_73', '0_0_0', '0_0_0', '0_0_0', '2017-09-13 16:52:52', 0, 0, 0),
(18, 'Escritorio', '<p class=\"Prrafobsico\" xss=removed><span lang=\"ES-TRAD\" xss=\"removed\">Madera contrachapada</span></p><p class=\"Prrafobsico\" xss=removed><span lang=\"ES-TRAD\" xss=\"removed\">Terminación barniz incoloro</span></p><p class=\"Prrafobsico\" xss=removed><span xss=\"removed\">Pensadapara presentar solución a una mesa económica, práctica, pero por sobre todosimple y versátil. Optimiza un tablero contrachapado, combinando procesos defabricación digital y manual, para confeccionar una estructura liviana yresistente, con tan solo vínculos simples de unión en madera.</span></p><p class=\"Prrafobsico\" xss=removed><span xss=\"removed\">Esuna solución económica y de fácil montar, que deja fuera la idea del mobiliariode línea plana aburrido, para otorgar una estética simple y liviana a espaciosde trabajo.</span></p>', 150000, '', 'http://localhost/puya_muebles/assets/uploads/productos/Escritorio/1217eac6217ada8a2b3b82a1fb51ac3d.jpg', '', NULL, 'SKU-81918', 0, 1, 8, 2, 19, 9, 2, '', 0, 0, NULL, '75_60_120', '75_150_60', '90_180_60', '90_240_60', '2017-10-19 00:01:51', 0, 0, 0),
(19, 'Escritorio 2', '<p class=\"Prrafobsico\" xss=removed><span lang=\"ES-TRAD\" xss=\"removed\">Madera contrachapada</span></p><p class=\"Prrafobsico\" xss=removed><span lang=\"ES-TRAD\" xss=\"removed\">Terminación barniz incoloro</span></p><p class=\"Prrafobsico\" xss=removed><span xss=\"removed\">Pensadapara presentar solución a una mesa económica, práctica, pero por sobre todosimple y versátil. Optimiza un tablero contrachapado, combinando procesos defabricación digital y manual, para confeccionar una estructura liviana yresistente, con tan solo vínculos simples de unión en madera.</span></p><p class=\"Prrafobsico\" xss=removed><span xss=\"removed\">Esuna solución económica y de fácil montar, que deja fuera la idea del mobiliariode línea plana aburrido, para otorgar una estética simple y liviana a espaciosde trabajo.</span></p>', 210000, '', 'http://localhost/puya_muebles/assets/uploads/productos/Escritorio/1217eac6217ada8a2b3b82a1fb51ac3d.jpg', '', NULL, 'SKU-81918', 0, 1, 8, 2, 19, 10, 2, '', 0, 0, NULL, '75_60_120', '75_150_60', '90_180_60', '90_240_60', '2017-10-19 00:01:51', 0, 0, 0),
(20, 'Arrimo 2', '', 250000, '', 'http://localhost/puya_muebles/assets/uploads/productos/Arrimo/d81491527a6344e3962fe94d5a1eb2e3.jpg', '', NULL, 'SKU-81917', 10, 1, 8, 1, 19, 2, 2, '', 0, 0, NULL, '30_60_73', '0_0_0', '0_0_0', '0_0_0', '2017-09-13 16:52:52', 0, 0, 0),
(21, 'prueba imagenes', 'lñkasdfalmñasldfmasñldkfm asldfmaslñfkm asñldfmsañldkfm asñldfmasñl fk', 100000, '', 'http://localhost/puya_muebles/assets/uploads/productos/prueba_imagenes/14c726e4c23b5a94c3cb4c6683b5fb85.jpg', '', NULL, 'SKU-81921', 0, 1, 8, 1, 19, 500, 2, '', 0, 0, NULL, '30_30_30', '0_0_0', '0_0_0', '0_0_0', '2017-11-08 17:11:35', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_carro`
--

CREATE TABLE `producto_carro` (
  `id` int(11) NOT NULL,
  `id_carro` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `color` int(11) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL COMMENT 'precio actual del producto / se actualiza con el cambio de precio (descuento aplicado)',
  `descuento` int(11) NOT NULL DEFAULT '0',
  `fecha` date NOT NULL,
  `estado` int(11) NOT NULL COMMENT '0.- en carro / 1.- procesado / 2.- eliminado del carro / 3.- venta finalizada ',
  `mensaje` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto_carro`
--

INSERT INTO `producto_carro` (`id`, `id_carro`, `id_producto`, `color`, `cantidad`, `precio`, `descuento`, `fecha`, `estado`, `mensaje`) VALUES
(19, 7, 17, NULL, 1, 100000, 0, '2017-09-10', 3, NULL),
(20, 7, 17, NULL, 2, 100000, 10, '2017-09-13', 3, NULL),
(21, 8, 17, NULL, 1, 100000, 10, '2017-09-23', 3, NULL),
(22, 9, 17, NULL, 1, 100000, 10, '2017-09-25', 3, NULL),
(23, 10, 17, 68, 1, 100000, 10, '2017-09-27', 2, NULL),
(24, 10, 18, 8, 1, 150000, 0, '2017-11-09', 0, NULL),
(25, 10, 18, 8, 1, 150000, 0, '2017-11-09', 2, NULL),
(26, 10, 18, 8, 1, 150000, 0, '2017-11-09', 2, NULL),
(27, 10, 18, 9, 1, 150000, 0, '2017-11-09', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_destacado`
--

CREATE TABLE `producto_destacado` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_destacado` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_stack`
--

CREATE TABLE `producto_stack` (
  `id` int(11) NOT NULL,
  `id_stack` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `precio_producto` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promocion_temporal`
--

CREATE TABLE `promocion_temporal` (
  `id` int(11) NOT NULL,
  `mensaje` varchar(150) NOT NULL,
  `descuento` int(11) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `sub_categoria_id` int(11) DEFAULT NULL,
  `imagen` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_region` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `nombre`, `id_region`) VALUES
(1, 'Arica', 15),
(2, 'Parinacota', 15),
(3, 'Iquique', 1),
(4, 'Tamarugal', 1),
(5, 'Antofagasta', 2),
(6, 'El Loa', 2),
(7, 'Tocopilla', 2),
(8, 'Copiapó', 3),
(9, 'Chañaral', 3),
(10, 'Huasco', 3),
(11, 'Elqui', 4),
(12, 'Choapa', 4),
(13, 'Limarí', 4),
(14, 'Valparaíso', 5),
(15, 'Isla de Pascua', 5),
(16, 'Los Andes', 5),
(17, 'Petorca', 5),
(18, 'Quillota', 5),
(19, 'San Antonio', 5),
(20, 'San Felipe de Aconcagua', 5),
(21, 'Marga Marga', 5),
(22, 'Cachapoal', 6),
(23, 'Cardenal Caro', 6),
(24, 'Colchagua', 6),
(25, 'Talca', 7),
(26, 'Cauquenes', 7),
(27, 'Curicó', 7),
(28, 'Linares', 7),
(29, 'Concepción', 8),
(30, 'Arauco', 8),
(31, 'Biobío', 8),
(32, 'Ñuble', 8),
(33, 'Cautín', 9),
(34, 'Malleco', 9),
(35, 'Valdivia', 14),
(36, 'Ranco', 14),
(37, 'Llanquihue', 10),
(38, 'Chiloé', 10),
(39, 'Osorno', 10),
(40, 'Palena', 10),
(41, 'Coihaique', 11),
(42, 'Aisén', 11),
(43, 'Capitán Prat', 11),
(44, 'General Carrera', 11),
(45, 'Magallanes', 12),
(46, 'Antártica Chilena', 12),
(47, 'Tierra del Fuego', 12),
(48, 'Última Esperanza', 12),
(49, 'Santiago', 13),
(50, 'Cordillera', 13),
(51, 'Chacabuco', 13),
(52, 'Maipo', 13),
(53, 'Melipilla', 13),
(54, 'Talagante', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntos`
--

CREATE TABLE `puntos` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `cantidad_puntos` int(11) NOT NULL,
  `total_puntos` int(11) NOT NULL COMMENT 'puntos totales antes de esta operacion',
  `accion` tinyint(4) NOT NULL COMMENT '0.- agrega puntos / 1.- elimina (usa) puntos',
  `fecha_creacion` datetime NOT NULL COMMENT ' fecha en que se agregaron los puntos',
  `fecha_modificacion` datetime NOT NULL COMMENT 'fecha en que se modificarion los puntos desde admin'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puntos`
--

INSERT INTO `puntos` (`id`, `id_cliente`, `id_venta`, `cantidad_puntos`, `total_puntos`, `accion`, `fecha_creacion`, `fecha_modificacion`) VALUES
(3, 3, 4, 3800, 0, 0, '2017-09-23 18:16:05', '2017-09-23 18:16:05'),
(4, 3, 5, 3800, 3800, 0, '2017-09-25 13:29:04', '2017-09-25 13:29:04'),
(5, 3, 3, 0, 7600, 0, '2017-10-21 12:33:28', '2017-10-21 12:33:28'),
(6, 3, 3, 0, 7600, 0, '2017-10-21 12:34:41', '2017-10-21 12:34:41'),
(7, 3, 3, 4000, 7600, 0, '2017-10-21 12:36:04', '2017-10-21 12:36:04'),
(8, 3, 3, 4000, 11600, 0, '2017-10-21 12:36:40', '2017-10-21 12:36:40'),
(9, 3, 3, 4000, 15600, 0, '2017-10-21 12:41:29', '2017-10-21 12:41:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntos_historial`
--

CREATE TABLE `puntos_historial` (
  `id` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `puntos` int(11) NOT NULL DEFAULT '0',
  `estado` int(11) NOT NULL COMMENT '1.- esperando validacion / 2.- disponible / 3.- cancelado / 4.- ya convertidos / disponible en los descuentos',
  `fecha_operacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puntos_historial`
--

INSERT INTO `puntos_historial` (`id`, `id_venta`, `puntos`, `estado`, `fecha_operacion`) VALUES
(0, 4, 3800, 2, '2017-09-23 18:16:05'),
(0, 5, 3800, 2, '2017-09-25 13:29:04'),
(0, 3, 0, 2, '2017-10-21 12:33:28'),
(0, 3, 0, 2, '2017-10-21 12:34:41'),
(0, 3, 4000, 2, '2017-10-21 12:36:04'),
(0, 3, 4000, 2, '2017-10-21 12:36:40'),
(0, 3, 4000, 2, '2017-10-21 12:41:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `ISO_3166_2_CL` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`id`, `nombre`, `ISO_3166_2_CL`) VALUES
(1, 'Tarapacá', 'CL-TA'),
(2, 'Antofagasta', 'CL-AN'),
(3, 'Atacama', 'CL-AT'),
(4, 'Coquimbo', 'CL-CO'),
(5, 'Valparaíso', 'CL-VS'),
(6, 'Región del Libertador Gral. Bernardo O’Higgins', 'CL-LI'),
(7, 'Región del Maule', 'CL-ML'),
(8, 'Región del Biobío', 'CL-BI'),
(9, 'Región de la Araucanía', 'CL-AR'),
(10, 'Región de Los Lagos', 'CL-LL'),
(11, 'Región Aisén del Gral. Carlos Ibáñez del Campo', 'CL-AI'),
(12, 'Región de Magallanes y de la Antártica Chilena', 'CL-MA'),
(13, 'Región Metropolitana de Santiago', 'CL-RM'),
(14, 'Región de Los Ríos', 'CL-LR'),
(15, 'Arica y Parinacota', 'CL-AP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `result_webpay`
--

CREATE TABLE `result_webpay` (
  `id` int(11) NOT NULL,
  `accountingDate` varchar(6) NOT NULL,
  `buyOrder` varchar(26) NOT NULL,
  `cardDetail_number` varchar(16) NOT NULL,
  `cardDetail_expiration` varchar(4) DEFAULT NULL,
  `do_authorizationCode` varchar(6) NOT NULL,
  `do_paymentTypeCode` varchar(6) NOT NULL COMMENT 'Tipo de pago de la transacción. VD = Venta Debito VN = Venta Normal VC = Venta en cuotas SI = 3 cuotas sin interés S2=2 cuotas sin interés NC = N Cuotas sin interés',
  `do_responseCode` varchar(6) NOT NULL COMMENT '0 Transacción aprobada. -1 Rechazo de transacción. -2 Transacción debe reintentarse. -3 Error en transacción. -4 Rechazo de transacción. -5 Rechazo por error de tasa. -6 Excede cupo máximo mensual. -7 Excede límite diario por transacción. -8 Rubro no autorizado.',
  `do_sharesNumber` int(2) NOT NULL,
  `do_amount` decimal(15,0) NOT NULL,
  `do_commerceCode` varchar(12) NOT NULL,
  `do_buyOrder` varchar(26) NOT NULL,
  `session_id` varchar(61) NOT NULL,
  `transactionDate` datetime NOT NULL,
  `VCI` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_categoria`
--

CREATE TABLE `sub_categoria` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_marca` int(11) DEFAULT NULL COMMENT 'puede o no tener una marca asociada',
  `nombre` varchar(150) NOT NULL,
  `imagen` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sub_categoria`
--

INSERT INTO `sub_categoria` (`id`, `id_categoria`, `id_marca`, `nombre`, `imagen`) VALUES
(1, 8, 0, 'arrimos', ''),
(2, 8, 0, 'escritorios', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `direccion` text NOT NULL COMMENT 'direccion del local, solo calle y numero',
  `telefono_1` int(11) NOT NULL,
  `telefono_2` int(11) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellido`, `email`, `password`, `telefono`) VALUES
(1, 'admin', 'prueba', 'admin@admin.cl', '21232f297a57a5a743894a0e4a801fc3', 123456789);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `id_carro` int(11) NOT NULL,
  `estado` int(11) NOT NULL COMMENT '0.- esperando pago/1.- venta_cancelada / 2.- pagado / 3.- enviado-listo para retiro/4.-finalizado',
  `costo_envio` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `comuna_id` int(11) NOT NULL,
  `costo_productos` int(11) NOT NULL COMMENT 'valor total de productos vendidos (descuentos aplicados)',
  `metodo_pago` int(11) NOT NULL COMMENT '0.-efectivo / 1.- webpay',
  `fecha_proceso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha en que se crea la venta',
  `fecha_pago` datetime NOT NULL COMMENT 'fecha en que se procesa el pago',
  `codigo` varchar(200) NOT NULL COMMENT 'codigo de descuento usado en esta venta',
  `numero_orden` int(11) NOT NULL,
  `codigo_documento` varchar(200) NOT NULL COMMENT 'codigo del documento asignado a la venta (boleta o factura))',
  `codigo_seguimiento` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `id_carro`, `estado`, `costo_envio`, `region_id`, `provincia_id`, `comuna_id`, `costo_productos`, `metodo_pago`, `fecha_proceso`, `fecha_pago`, `codigo`, `numero_orden`, `codigo_documento`, `codigo_seguimiento`) VALUES
(3, 7, 4, 0, NULL, NULL, 146, 100000, 0, '2017-09-23 17:09:54', '0000-00-00 00:00:00', '', 1506199789, '1506199794', 'asdfasdf'),
(4, 8, 4, 5000, NULL, NULL, 146, 90000, 1, '2017-09-23 18:09:05', '0000-00-00 00:00:00', '', 1506199995, '1506201365', 'sdfasdfasdf'),
(5, 9, 1, 5000, NULL, NULL, 146, 90000, 1, '2017-09-25 13:09:04', '0000-00-00 00:00:00', '', 1506356570, '1506356944', 'asdfasdfsadf');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banner_home`
--
ALTER TABLE `banner_home`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carro_compra`
--
ALTER TABLE `carro_compra`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente_codigo_usado`
--
ALTER TABLE `cliente_codigo_usado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `codigo_promocion`
--
ALTER TABLE `codigo_promocion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `color_producto`
--
ALTER TABLE `color_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comuna`
--
ALTER TABLE `comuna`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `datos_servicio`
--
ALTER TABLE `datos_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `distribuidor`
--
ALTER TABLE `distribuidor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oferta_especial`
--
ALTER TABLE `oferta_especial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagos_webpay_flow`
--
ALTER TABLE `pagos_webpay_flow`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `parrafo_servicio`
--
ALTER TABLE `parrafo_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `precio_envio`
--
ALTER TABLE `precio_envio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto_carro`
--
ALTER TABLE `producto_carro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto_destacado`
--
ALTER TABLE `producto_destacado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto_stack`
--
ALTER TABLE `producto_stack`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promocion_temporal`
--
ALTER TABLE `promocion_temporal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `puntos`
--
ALTER TABLE `puntos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `result_webpay`
--
ALTER TABLE `result_webpay`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sub_categoria`
--
ALTER TABLE `sub_categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banner_home`
--
ALTER TABLE `banner_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `carro_compra`
--
ALTER TABLE `carro_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `cliente_codigo_usado`
--
ALTER TABLE `cliente_codigo_usado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `codigo_promocion`
--
ALTER TABLE `codigo_promocion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `color`
--
ALTER TABLE `color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `color_producto`
--
ALTER TABLE `color_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT de la tabla `comuna`
--
ALTER TABLE `comuna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=347;
--
-- AUTO_INCREMENT de la tabla `datos_servicio`
--
ALTER TABLE `datos_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `distribuidor`
--
ALTER TABLE `distribuidor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `oferta_especial`
--
ALTER TABLE `oferta_especial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `pagos_webpay_flow`
--
ALTER TABLE `pagos_webpay_flow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `parrafo_servicio`
--
ALTER TABLE `parrafo_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `precio_envio`
--
ALTER TABLE `precio_envio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `producto_carro`
--
ALTER TABLE `producto_carro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `producto_destacado`
--
ALTER TABLE `producto_destacado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `producto_stack`
--
ALTER TABLE `producto_stack`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `promocion_temporal`
--
ALTER TABLE `promocion_temporal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT de la tabla `puntos`
--
ALTER TABLE `puntos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `result_webpay`
--
ALTER TABLE `result_webpay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `sub_categoria`
--
ALTER TABLE `sub_categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
