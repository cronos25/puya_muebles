<?php 
class PWebpay_model extends CI_Model {

    function __construct(){
      $this->load->database();
    }

    function obtener_lista_pagos_webpay(){
      $query = $this->db->get('pagos_webpay_flow');
      return $query->result_array();
    }
   
    //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_pagos_webpay($id = FALSE){
        if ($id === FALSE){
          $query = $this->db->get('pagos_webpay_flow');
          return $query->result_array();
        }

        $query = $this->db->get_where('pagos_webpay_flow', array('id' => $id));
        return $query->row_array();
    }
    

    function inserta_pagos_webpay($datos = array()){
    //id  
    //codigo_carro  
    //txn 
    //email_paypal
      $this->db->insert('pagos_webpay_flow', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
    }

    function edita_pagos_webpay($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('pagos_webpay_flow', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }

    function elimina_pagos_webpay($id){
      $this->db->where('id', $id);
      $this->db->delete('pagos_webpay_flow');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }
}