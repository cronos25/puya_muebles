<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comuna_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	function existe_comuna($nombre,$id_provincia){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $this->db->where('id_provincia', $id_provincia); 
      $query = $this->db->get('comuna');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
	}

  function obtener_lista_comunas(){
    //$query = $this->db->get('comuna');
    $query = $this->db->query('SELECT id, nombre, id_provincia, (SELECT nombre FROM provincia WHERE id = c.id_provincia) as provincia_nombre FROM comuna as c');

    return $query->result_array();
  }

  function obtener_lista_comunas_provincia($id_provincia){
    $this->db->where('id_provincia', $id_provincia); 
    $query = $this->db->get('comuna');
    return $query->result_array();
  }
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_comuna($id = FALSE){
        if ($id === FALSE)
        {
                $query = $this->db->get('comuna');
                return $query->result_array();
        }

        $query = $this->db->get_where('comuna', array('id' => $id));
        return $query->row_array();
   }
   
   function inserta_comuna($datos = array()){
      if(empty($datos['nombre'])||
         empty($datos['id_provincia'])){
         return false;
      }
      $this->db->insert('comuna', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_comuna($datos = array()){
      if(empty($datos['nombre'])||
         empty($datos['id'])||
         empty($datos['id_provincia'])){
         return FALSE;
      }
      $this->db->where('id', $datos['id']);
      $this->db->update('comuna', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_comuna($id){
      $this->db->where('id', $id);
      $this->db->delete('comuna');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }


}

/* End of file  */
/* Location: ./application/models/ */