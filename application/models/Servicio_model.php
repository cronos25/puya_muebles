<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicio_model extends CI_Model {

/*

tabla parrafo_servicio
	1	id	int(11)	
	2	id_categoria	int(11)	
	3	mensaje_parrafo	text
	4	imagen_parrafo	text
	5	tipo_parrafo	tinyint(4)	
		0.-texto derecha / 
		1.- texto izquierda / 
		2.- sin imagen / 
		3.- sin texto	
	6	numero_parrafo	int(11)	


tabla datos_servicio

	1	id int(11)	
	2	id_categoria	int(11)
	3	email	varchar(250)  email del servicio / sin el no se muestra el formulario de contacto	
	4	telefono telefono del servicio		
	5	mail2	varchar(250) mail opcional 2
	6	mail3	varchar(250) mail opcional 3		
	7	mensaje	text
	8	imagen	text	se puede usar despues / no hablitado
*/

	public function __construct()
	{
		parent::__construct();
	}

  function obtener_lista_parrafos_categoria($cate){
    $this->db->where('id_categoria',$cate);
    $this->db->order_by('numero_parrafo', 'ASC');
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get('parrafo_servicio');
    return $query->result_array();
  }

  //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
  function get_datos_servicio_categoria($id = FALSE){
      $query = $this->db->get_where('datos_servicio', array('id_categoria' => $id));
      return $query->row_array();
  }

  function actual_imagen($id){
      $this->db->select('imagen_parrafo');
      $query = $this->db->get_where('parrafo_servicio', array('id' => $this->db->escape($id)));
      return $query->row_array();
   }

  function inserta_parrafo_servicio($datos = array()){
    $this->db->insert('parrafo_servicio', $datos);
     if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
    //return $this->db->insert_id();
  }

  function edita_parrafo_servicio($datos = array()){
    $this->db->where('id', $datos['id']);
    $this->db->update('parrafo_servicio', $datos);

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function elimina_parrafo_servicio($id){
    $this->db->where('id', $id);
    $this->db->delete('parrafo_servicio');

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function edita_datos_servicio($datos = array()){
    $query = $this->db->get_where('datos_servicio', array('id_categoria' => $datos['id_categoria']));

    if (count($query->row_array())>0) {
      $this->db->where('id_categoria', $datos['id_categoria']);
      $this->db->update('datos_servicio', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }else{
      $this->db->insert('datos_servicio', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }
  }

/*solo en caso de que se elimine la categoria*/
   function elimina_datos_servicio($id){
    $this->db->where('id_categoria', $id);
    $this->db->delete('datos_servicio');

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }


}

/* End of file  */
/* Location: ./application/models/ */