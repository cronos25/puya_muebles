<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Codigo_promocion_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

   function existe_codigo($data){
      //$this->db->select('codigo');
      $this->db->where('codigo', $data['codigo']); 
      $where = "`fecha_inicio` BETWEEN ".$this->db->escape($data['fecha_inicio'])." and ".$this->db->escape($data['fecha_fin'])." OR fecha_fin BETWEEN ".$this->db->escape($data['fecha_inicio'])." AND ".$this->db->escape($data['fecha_fin']);
      $this->db->where($where);
      $query = $this->db->get('codigo_promocion');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

	function obtener_lista_codigos(){
      $this->db->select("*");
      $this->db->select('(SELECT nombre FROM productos WHERE productos.id=codigo_promocion.producto_id) as producto');
      $this->db->select('(SELECT nombre FROM categorias WHERE categorias.id=codigo_promocion.categoria_id) as categoria');
      $this->db->select('(SELECT nombre FROM marca WHERE marca.id=codigo_promocion.marca_id) as marca');
      $this->db->select('(SELECT nombre FROM sub_categoria WHERE sub_categoria.id=codigo_promocion.sub_categoria_id) as sub_categoria');
      $query = $this->db->get('codigo_promocion');
      return $query->result_array();
   	}

   function obtener_codigo_promocion($id){
      $query = $this->db->get_where('codigo_promocion',array('id'=>$id));
      return $query->row_array();
      }

   function obtener_codigo_promocion_previa($codigo){
      $this->db->where('fecha_inicio <',date("Y-m-d H:i:s"));
      $this->db->where('fecha_fin >',date("Y-m-d H:i:s"));
      $query = $this->db->get_where('codigo_promocion',array('codigo'=>$codigo));
      return $query->row_array();
   }

	function obtener_lista_codigos_activos(){
      $this->db->where('fecha_inicio <',date("Y-m-d H:i:s"));
      $this->db->where('fecha_fin >',date("Y-m-d H:i:s"));
      $query = $this->db->get('codigo_promocion');
      return $query->result_array();
   	}

	function inserta_codigo_promocion($datos = array()){
      $this->db->insert('codigo_promocion', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function elimina_codigo_promocion($id){
      $this->db->where('id', $id);
      $this->db->delete('codigo_promocion');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   /*seccion de uso de codigos por cliente*/
   function inserta_codigo_promocion_cliente($datos = array()){
      $this->db->insert('cliente_codigo_usado', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function codigo_valido($codigo,$cliente){
      
      $this->db->where('id_cliente',$cliente);
      $this->db->where('id_codigo_promocion',$codigo);
      $this->db->join('codigo_promocion', 'codigo_promocion.id = '.$this->db->escape($codigo).' AND codigo_promocion.fecha_inicio > cliente_codigo_usado.fecha_uso AND codigo_promocion.fecha_fin < cliente_codigo_usado.fecha_uso', 'left');
      $query = $this->db->get('cliente_codigo_usado');
      if($this->db->affected_rows()>0){
         return false;
      }else{
         return true;
      }
      //return $this->db->insert_id();
   }
   /*******************/
}

/* End of file Codigo_promocion_model.php */
/* Location: ./application/models/Codigo_promocion_model.php */