<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

  function obtener_lista_newsletteres(){
    $query = $this->db->get('newsletter');
    return $query->result_array();
  }

  function existe_suscrito($email){
      $this->db->select('email');
      $this->db->where('email', $email); 
      $query = $this->db->get('newsletter');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
  }

  //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
  function get_newsletter($id = FALSE){
      if ($id === FALSE)
      {
              $query = $this->db->get('newsletter');
              return $query->result_array();
      }

      $query = $this->db->get_where('newsletter', array('id' => $id));
      return $query->row_array();
  }

  function inserta_newsletter($datos = array()){
    if(empty($datos['email'])){
       return false;
    }
    $this->db->insert('newsletter', $datos);
     if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
    //return $this->db->insert_id();
  }

  function edita_newsletter($datos = array()){
    if(empty($datos['email'])||empty($datos['id'])){
       return FALSE;
    }
    $this->db->where('id', $datos['id']);
    $this->db->update('newsletter', $datos);

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function elimina_newsletter($id){
    $this->db->where('id', $id);
    $this->db->delete('newsletter');

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function desuscribe($id){
    $this->db->where('email', $id);
    $this->db->delete('newsletter');

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }
}

/* End of file  */
/* Location: ./application/models/ */