<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Puntos_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    
  }

  //falta crear funcion para conseguir el ultimo valor de puntos o los puntos totales

   function obtener_lista_puntos(){
      $query = $this->db->get('puntos');

      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_puntos($id = FALSE){
        $query = $this->db->get_where('puntos', array('id' => $id));
        return $query->row_array();
   }
   
   function inserta_puntos($datos = array()){
      $this->db->insert('puntos', $datos);

      if($this->db->affected_rows()>0){
         $arrayHistorial = array('id_venta' => $datos['id_venta'],
                                  'puntos' =>  $datos['cantidad_puntos'],
                                  'estado' =>  2,
                                  'fecha_operacion' => date("Y-m-d H:i:s"));
         $arrayPuntosCliente = array('id' => $datos['id_cliente'],
                                      'puntos ' =>$datos['cantidad_puntos']+$datos['total_puntos']);


         $this->db->where('id', $arrayPuntosCliente['id']);
         $this->db->update('cliente', $arrayPuntosCliente);
         //falta crear funcion para actualizar la lista de puntos de la tabla cliente
         $this->inserta_historial($arrayHistorial);

         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
   function get_puntos_activos_todos($id = FALSE){
        $this->db->select_sum('cantidad_puntos');
        $query = $this->db->get_where('puntos', array('id_cliente' => $id,'accion'=>0));
        return $query->row_array();
   }
   function get_puntos_usados_todos($id = FALSE){
        $this->db->select_sum('cantidad_puntos');
        $query = $this->db->get_where('puntos', array('id_cliente' => $id,'accion'=>1));
        return $query->row_array();
   }

   private function inserta_historial($dataReg = array()){
      $this->db->insert('puntos_historial', $dataReg);
      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function edita_puntos($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('puntos', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_puntos($id){
      $this->db->where('id', $id);
      $this->db->delete('puntos');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }


}

/* End of file  */
/* Location: ./application/models/ */