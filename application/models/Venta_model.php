<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venta_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    $this->load->model('Puntos_model');
    $this->load->model('Carro_compra_model');
    $this->load->model('Cliente_model');
    $this->load->model('Codigo_promocion_model');
		
	}

   function obtener_lista_ventas(){
       $query = $this->db->query('SELECT id, id_carro, (SELECT codigo FROM carro_compra WHERE v.id_carro = id) as carro,(SELECT mensaje FROM carro_compra WHERE v.id_carro = id) as mensaje,(SELECT nombre from cliente WHERE id = (SELECT id_cliente FROM carro_compra WHERE v.id_carro = id)) as nombre_cliente,(SELECT apellido from cliente WHERE id = (SELECT id_cliente FROM carro_compra WHERE v.id_carro = id)) as apellido_cliente,(SELECT rut from cliente WHERE id = (SELECT id_cliente FROM carro_compra WHERE v.id_carro = id)) as rut_cliente, estado, costo_envio, region_id, provincia_id, comuna_id, costo_productos, metodo_pago, fecha_proceso, fecha_pago, codigo, numero_orden, codigo_documento FROM venta as v ORDER BY id DESC');
    return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_venta($id = FALSE){
        $query = $this->db->get_where('venta', array('id' => $id));
        return $query->row_array();
    }
   
   function get_venta_usuario($listaCarros){
        $arrayVentas = array();
        foreach ($listaCarros as $carro){
          if ($carro['id']) {
            $query =$this->db->get_where('venta', array('id_carro' => $carro['id']));
            if ($query->row_array()) {
              array_push($arrayVentas,$query->row_array());
            }
          }
          //$arrayVentas = array("mensaje"=>$productoasocItem['id_producto']);
        }
        return $arrayVentas;
   }

   function inserta_venta($datos = array()){
      $this->db->insert('venta', $datos);
      $idRegistro = $this->db->insert_id();
       if($this->db->affected_rows()>0){
        $arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra($datos['id_carro']);
        if (!empty($datos['codigo'])) {
          $arrayCodigo = array('id_cliente' => $arrayCarroRegistro['id_cliente'],
                              'id_codigo_promocion'=> $this->Codigo_promocion_model->obtener_codigo_promocion_previa($datos['codigo'])['id'],
                              'id_venta'=>$idRegistro,
                              'fecha_uso'=>date("Y-m-d H:i:s"));
          $this->Codigo_promocion_model->inserta_codigo_promocion_cliente($arrayCodigo);
        }
        if($datos['estado']==2){
            $arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra($datos['id_carro']);
            //los puntos equivalen al 4% de cada venta
            // falta tomar el total de puntos del modelo de puntos
            // en esta ocacion solo agregaremos puntos
            //las fechas de creacion y modificacion son las misma
            $puntosActivos = $this->Puntos_model->get_puntos_activos_todos($arrayCarroRegistro['id_cliente'])['cantidad_puntos'];
            $puntosUsados = $this->Puntos_model->get_puntos_usados_todos($arrayCarroRegistro['id_cliente'])['cantidad_puntos'];
            $puntosTotales = 0;
            $validacionPuntos = intVal($puntosActivos)-intVal($puntosUsados);
            if ($validacionPuntos >0) {
              $puntosTotales= $validacionPuntos;
            }
            $arrayPuntos = array('id_cliente' => $arrayCarroRegistro['id_cliente'],
                               'id_venta'=>$idRegistro,
                               'cantidad_puntos'=> intVal(($datos['costo_productos']+$datos['costo_envio'])*0.04),
                               'total_puntos' =>$puntosTotales,
                               'accion' => 0,
                               'fecha_creacion' => date("Y-m-d H:i:s"),
                               'fecha_modificacion' => date("Y-m-d H:i:s"));
              
            $this->Puntos_model->inserta_puntos($arrayPuntos);
        }
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

    function set_codigo_seguimiento($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('venta',array('codigo_seguimiento'=>$datos['codigo']));

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }

   private function set_codigo_venta($datos){
      $this->db->where('id', $datos['id']);
      $this->db->update('venta',array('codigo'=>$datos['codigo']));

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function edita_venta($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('venta', $datos);

      if($this->db->affected_rows()>0){

        if ($datos['estado']==2) {
          $arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra($datos['id_carro']);
            //los puntos equivalen al 4% de cada venta
            // falta tomar el total de puntos del modelo de puntos
            // en esta ocacion solo agregaremos puntos
            //las fechas de creacion y modificacion son las misma
            $puntosActivos = $this->Puntos_model->get_puntos_activos_todos($arrayCarroRegistro['id_cliente'])['cantidad_puntos'];
            $puntosUsados = $this->Puntos_model->get_puntos_usados_todos($arrayCarroRegistro['id_cliente'])['cantidad_puntos'];
            $arrayPuntos = array('id_cliente' => $arrayCarroRegistro['id_cliente'],
                               'id_venta'=>$datos['id'],
                               'cantidad_puntos'=> intVal(($datos['costo_productos']+$datos['costo_envio'])*0.04),
                               'total_puntos' =>(($puntosActivos-$puntosUsados)<0)?0:($puntosActivos-$puntosUsados),
                               'accion' => 0,
                               'fecha_creacion' => date("Y-m-d H:i:s"),
                               'fecha_modificacion' => date("Y-m-d H:i:s"));
              
            $this->Puntos_model->inserta_puntos($arrayPuntos);
        }
        return true;
      }else{
         return false;
      }
   }

   function obtener_ventas_pagadas_10dias(){
    //$query = $this->db->get('productos');
    $query = $this->db->query('SELECT fecha_pago, count(*) as pagofecha FROM venta where fecha_pago >= ( CURDATE() - INTERVAL 10 DAY ) AND estado = 2 GROUP BY fecha_pago');

    return $query->result_array();
  }

   function elimina_venta($id){
      $this->db->where('id', $id);
      //no se eliminan las ventas por concepto de registro
      $this->db->update('venta',array('estado'=>0));
      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}

/* End of file  */
/* Location: ./application/models/ */