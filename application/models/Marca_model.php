<?php 
class Marca_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_marcas(){
      $query = $this->db->get('marca');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_marca($id = FALSE){

        $query = $this->db->get_where('marca', array('id' => $id));
        return $query->row_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_productos_filtrados_marcas($id = FALSE,$marcas = array()){
      $stringcategorias = "(productos.categoria_id= ";
      for($i = 0; $i<count($marcas);$i++){
         if ($i < (count($marcas)-1)) {
            $stringcategorias .= $marcas[$i]." OR productos.categoria_id=";
         }else{
            $stringcategorias .= $marcas[$i].")";
         }
      }
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = $this->db->query('SELECT id, nombre, descripcion, precio, descuento, cantidad, (SELECT nombre FROM categorias WHERE id =productos.categoria_id) AS categoria, categoria_id, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca, marca_id, link_producto FROM productos WHERE productos.marca_id = '.$id .' AND productos.is_stack = 0'.' AND '.$stringcategorias);
      return $query->result_array();
   }

   function get_total_prods_marcas_categoria($categoria){
      //$query = $this->db->get('productos');
      $query = $this->db->query('SELECT *,(select count(productos.id) FROM productos WHERE productos.marca_id = marca.id AND productos.categoria_id = '.$categoria.') as productos FROM marca');

      return $query->result_array();
   }

   function get_total_prods_marcas_sub_categoria($subcategoria){
      //$query = $this->db->get('productos');
      $query = $this->db->query('SELECT *,(select count(productos.id) FROM productos WHERE productos.marca_id = marca.id AND productos.sub_categoria_id = '.$subcategoria.') as productos FROM marca');

      return $query->result_array();
   }

   function get_total_prods_marcas(){
      //$query = $this->db->get('producto');
      $query = $this->db->query('SELECT *,(select count(productos.id) from productos WHERE productos.marca_id = marca.id) as productos FROM marca');

      return $query->result_array();
   }

   function get_total_prods_marcas_buscar($productos=array()){
      //$query = $this->db->get('producto');
      if (count($productos)>0) { 
         $stringproductos = " AND (productos.id= ";
         for($i = 0; $i<count($productos);$i++){
            if ($i < (count($productos)-1)) {
               $stringproductos .= $this->db->escape($productos[$i])." OR productos.id=";
            }else{
               $stringproductos .= $this->db->escape($productos[$i]).")";
            }
         }
         $query = $this->db->query('SELECT *,(select count(productos.id) from productos WHERE productos.marca_id = marca.id '.$stringproductos.') as productos FROM marca');
         return $query->result_array();
      }else{
         return $productos;
      }
      

   }

    function get_productos_marca($id = FALSE){
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = $this->db->query('SELECT id, nombre, descripcion, precio, descuento, cantidad, foto_producto, (select nombre from categorias where id =productos.categoria_id)as categoria, categoria_id, (select nombre from marca where id =productos.marca_id) as marca, marca_id FROM productos WHERE productos.marca_id = '.$id.' AND productos.is_stack = 0');
      return $query->result_array();
   }

   function actual_logo($id){
      $this->db->select('logo');
      $query = $this->db->get_where('marca', array('id' => $id));
      return $query->row_array();
   }

   function existe_marca($nombre){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $query = $this->db->get('marca');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }
   
   function inserta_marca($datos = array()){
      $this->db->insert('marca', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_marca($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('marca', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_marca($id){
      $this->db->where('id', $id);
      $this->db->delete('marca');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}