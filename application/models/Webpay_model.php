<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webpay_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}
/*metodos transaction result*/

/*INSERT INTO `result_webpay`(`id`, `accountingDate`, `buyOrder`, `cardDetail_number`, `cardDetail_expiration`, `do_authorizationCode`, `do_paymentTypeCode`, `do_responseCode`, `do_sharesNumber`, `do_amount`, `do_commerceCode`, `do_buyOrder`, `session_id`, `transactionDate`, `VCI`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13],[value-14],[value-15])*/

	function existe_webpay_trans($code){
      $this->db->select('do_authorizationCode');
      $this->db->where('do_authorizationCode', $code); 
      $query = $this->db->get('result_webpay');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
	}

   function obtener_lista_webpay_trans(){
      $query = $this->db->get('result_webpay');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_webpay_trans($id = FALSE){

        $query = $this->db->get_where('result_webpay', array('id' => $id));
        return $query->row_array();
   }
   
   function inserta_webpay_trans($datos = array()){
      if(empty($datos['buyOrder'])){
         return false;
      }
      $this->db->insert('result_webpay', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function elimina_webpay_trans($id){
      $this->db->where('id', $id);
      $this->db->delete('result_webpay');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
/*fin metodos de getresult webpay*/
}

/* End of file Webpay_model.php */
/* Location: ./application/models/Webpay_model.php */