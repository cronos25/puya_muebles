<?php 
class Producto_Stack_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_producto_stacks(){
      $query = $this->db->get('producto_stack');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_producto_stack($id = FALSE){

        $query = $this->db->get_where('producto_stack', array('id' => $id));
        return $query->row_array();
   }

   function existe_producto_stack($stack,$producto){
      $this->db->select('id_stack');
      $this->db->where('id_stack', $stack); 
      $this->db->where('id_producto', $producto); 
      $query = $this->db->get('producto_stack');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

   function get_total_prods_stacks(){
      //$query = $this->db->get('producto');
      $query = $this->db->query('SELECT *,(select count(id_producto) from producto_stack WHERE producto_stack.id_stack = s.id) as cantidad_productos,(select sum(precio_producto) from producto_stack WHERE producto_stack.id_stack = s.id) as total FROM stack as s');

      return $query->result_array();
   }
      
   function get_productos_stack($id){
      //$query = $this->db->get('producto');
      $query = $this->db->query('SELECT * ,(select nombre from productos where id = producto_stack.id_producto) as nombre,(SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM producto_stack RIGHT JOIN productos ON productos.id = producto_stack.id_producto  WHERE id_stack ='.$id);

      return $query->result_array();
   }

   function inserta_producto_stack($datos = array()){
      $this->db->insert('producto_stack', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_producto_stack($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('producto_stack', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_producto_stack($id){
      $this->db->where('id', $id);
      $this->db->delete('producto_stack');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}