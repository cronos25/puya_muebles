<?php 
class Producto_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_productos(){
      //$query = $this->db->get('productos');
      $query = $this->db->query('SELECT `id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, fecha_creacion, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos WHERE productos.stock > 0 AND productos.is_stack = 0 ');

      return $query->result_array();
   }

   function obtener_lista_productos_todo(){
      //$query = $this->db->get('productos');
      $query = $this->db->query('SELECT `id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, fecha_creacion,(SELECT nombre FROM categorias WHERE id =productos.categoria_id) AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca, (SELECT nombre FROM sub_categoria WHERE id =productos.sub_categoria_id) AS sub_categoria, (SELECT nombre FROM color WHERE id =productos.color) AS color_nombre FROM productos  WHERE productos.is_stack = 0 ');

      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_producto($id = FALSE){
      //$query = $this->db->get_WHERE('producto', array('id' => $id));

      $query = $this->db->query('SELECT `id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, fecha_creacion,(SELECT nombre FROM categorias WHERE id =productos.categoria_id) AS categoria,(SELECT imagen FROM categorias WHERE id =productos.categoria_id) AS imagen_categoria , (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca, (SELECT nombre FROM sub_categoria WHERE id =productos.sub_categoria_id) AS sub_categoria, (SELECT nombre FROM color WHERE id =productos.color) AS color_nombre FROM productos WHERE productos.id = '.$this->db->escape($id));
      return $query->row_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_producto_espec($id = FALSE){
      //$query = $this->db->get_WHERE('producto', array('id' => $id));
      $query = $this->db->query('SELECT *,(SELECT nombre FROM productos WHERE id =especificacion_producto.id_producto) AS producto,(SELECT nombre FROM especificacion_categoria WHERE id =especificacion_producto.especificacion_id) AS especificacion FROM especificacion_producto WHERE id_producto ='.$this->db->escape($id));

        return $query->result_array();
   }


   function get_productos_buscador($strbuscador){
      $dataselect = "SELECT p.`id`, p.`nombre`, p.`descripcion`, p.`precio`, p.`foto_etiqueta`, p.`foto_producto`, p.`foto_home`, p.`sku`, p.`descuento`, p.`envio_gratis`, p.`categoria_id`, p.`sub_categoria_id`, p.`color`,p.`marca_id`, p.`stock`, p.`tipo_entrega`, p.`regalos`, p.`is_stack` ,c.nombre as categoria,c.descripcion as categoria_descripcion ,m.nombre as marca,m.descripcion as marca_descripcion FROM productos as p,categorias as c,marca as m, color_producto as cp WHERE p.categoria_id = c.id AND p.marca_id = m.id AND (
         p.nombre LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
         p.descripcion LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
         c.nombre LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
         c.descripcion LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
         m.nombre LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
         m.descripcion LIKE ".$this->db->escape('%'.$strbuscador.'%').")";
         $query = $this->db->query($dataselect);
         return $query->result_array();
   }

   function get_productos_filtrados($marcas = array(),$categorias = array(),$colores = array(),$strbuscador){
      $stringfiltro = '';
      $stringmarcas = "(p.marca_id= ";
      for($i = 0; $i<count($marcas);$i++){
         if ($i < (count($marcas)-1)) {
            $stringmarcas .= $this->db->escape($marcas[$i])." OR p.marca_id=";
         }else{
            $stringmarcas .= $this->db->escape($marcas[$i]).")";
         }
      }
      $stringcategorias = "(p.sub_categoria_id= ";
      for($i = 0; $i<count($categorias);$i++){
         if ($i < (count($categorias)-1)) {
            $stringcategorias .= $this->db->escape($categorias[$i])." OR p.sub_categoria_id=";
         }else{
            $stringcategorias .= $this->db->escape($categorias[$i]).")";
         }
      }


//falta agregar filtro de colores, la operacion esta realizada pero falta añadir el texto a la consulta sql
     $stringcolores = "(p.id = cp.id_producto AND cp.id_color= ";
      for($i = 0; $i<count($colores);$i++){
         if ($i < (count($colores)-1)) {
            $stringcolores .= $this->db->escape($colores[$i])." OR p.id = cp.id_producto AND cp.id_color=";
         }else{
            $stringcolores .= $this->db->escape($colores[$i]).")";
         }
      }

      /*if (count($marcas)>0 && count($categorias)==0){
         $stringfiltro = ' AND '.$stringmarcas;
      }
      if(count($marcas)==0 && count($categorias)>0){
         $stringfiltro = ' AND '.$stringcategorias;
      }
      if(count($marcas)>0 && count($categorias)>0){
         $stringfiltro = ' AND '.$stringcategorias.' OR '.$stringmarcas;
      }*/

      if (count($marcas)>0 && count($categorias)==0 && count($colores)==0){
         $stringfiltro = ' AND '.$stringmarcas;
      }
      if(count($marcas)==0 && count($categorias)>0 && count($colores)==0){
         $stringfiltro = ' AND '.$stringcategorias;
      }
      if(count($marcas)==0 && count($categorias)==0 && count($colores)>0){
         $stringfiltro = ' AND '.$stringcolores;
      }
      if(count($marcas)==0 && count($categorias)>0 && count($colores)>0){
         $stringfiltro = ' AND '.$stringcategorias.' AND '.$stringcolores;
      }
      if(count($marcas)>0 && count($categorias)==0 && count($colores)>0){
         $stringfiltro = ' AND '.$stringmarcas.' and '.$stringcolores;
      }
      if(count($marcas)>0 && count($categorias)>0 && count($colores)==0){
         $stringfiltro = ' AND '.$stringmarcas.' OR '.$stringcategorias;
      }
      if(count($marcas)>0 && count($categorias)>0 && count($colores)>0){
         $stringfiltro = ' AND '.$stringcategorias.' OR '.$stringmarcas.' AND '.$stringcolores;
      }
     
      //$query = $this->db->get_where('producto', array('id' => $id));
      $dataselect = "SELECT DISTINCT p.`id`, p.`nombre`, p.`descripcion`, p.`precio`, p.`foto_etiqueta`, p.`foto_producto`, p.`foto_home`, p.`sku`, p.`descuento`, p.`envio_gratis`, p.`categoria_id`, p.`sub_categoria_id`, p.`color`,p.`marca_id`, p.`stock`, p.`tipo_entrega`, p.`regalos`, p.`is_stack` ,sc.nombre as categoria, m.nombre as marca,m.descripcion as marca_descripcion FROM productos as p,marca as m, sub_categoria as sc, color_producto as cp WHERE p.sub_categoria_id = sc.id AND p.marca_id = m.id AND cp.id_producto = p.id ";
         if (!empty($strbuscador)) {
            $dataselect .="AND (
               p.nombre LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
               p.descripcion LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
               sc.nombre LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
               m.nombre LIKE ".$this->db->escape('%'.$strbuscador.'%')." OR
               m.descripcion LIKE ".$this->db->escape('%'.$strbuscador.'%').")";
         }
         
         $dataselect .= $stringfiltro." ORDER BY p.id DESC";



         $query = $this->db->query($dataselect);
         return $query->result_array();
   }

   function get_productos_relacionado($id_prod,$marca,$categoria){
      
      //$query = $this->db->get('productos');
      $query = 'SELECT `id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, fecha_creacion, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos WHERE id != ? AND marca_id = ? OR categoria_id = ? AND productos.stock > 0 ORDER BY id DESC LIMIT 4';
      $resquery = $this->db->query($query,array($id_prod,$marca,$categoria));
      return $resquery->result_array();
   }

   function get_productos_destacados_home($marcas = array(),$categorias = array()){
      
      //$query = $this->db->get('productos');

      $queryDestacado = $this->db->query('SELECT productos.`id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, fecha_creacion, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos RIGHT JOIN producto_destacado ON producto_destacado.id_producto = productos.id WHERE productos.stock > 0 AND productos.is_stack = 0 ORDER BY producto_destacado.id DESC LIMIT 4');

      if (count($queryDestacado->result_array())>=4) {
         return $queryDestacado->result_array();
      }else{

         $cantidadBuscar = 4-count($queryDestacado->result_array());

         $resultadoQueryProd = $this->db->query('SELECT productos.`id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, fecha_creacion, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos WHERE productos.stock > 0 AND productos.is_stack = 0  ORDER BY id DESC LIMIT '.$cantidadBuscar);
         $listaObtenidos = $queryDestacado->result_array();
        foreach ($resultadoQueryProd->result_array() as $ProdFaltante) {
            array_push($listaObtenidos, $ProdFaltante);
        }

        return $listaObtenidos;
      }
       
   }

   function get_productos_destacados($marcas = array(),$categorias = array()){
      $stringfiltro = "";
      $stringmarcas = "(productos.marca_id= ";
      for($i = 0; $i<count($marcas);$i++){
         if ($i < (count($marcas)-1)) {
            $stringmarcas .= $this->db->escape($marcas[$i])." OR productos.marca_id=";
         }else{
            $stringmarcas .= $this->db->escape($marcas[$i]).")";
         }
      }
      $stringcategorias = "(productos.categoria_id= ";
      for($i = 0; $i<count($categorias);$i++){
         if ($i < (count($categorias)-1)) {
            $stringcategorias .= $this->db->escape($categorias[$i])." OR productos.categoria_id=";
         }else{
            $stringcategorias .= $this->db->escape($categorias[$i]).")";
         }
      }

      if (count($marcas)>0 && count($categorias)==0){
         $stringfiltro = 'AND '.$stringmarcas;
      }
      if(count($marcas)==0 && count($categorias)>0){
         $stringfiltro = 'AND '.$stringcategorias;
      }
      if(count($marcas)>0 && count($categorias)>0){
         $stringfiltro = 'AND '.$stringcategorias.' OR '.$stringmarcas;
      }
      //$query = $this->db->get('productos');
      $query = $this->db->query('SELECT id, nombre, descripcion, sku, precio, descuento, cantidad,  (SELECT nombre FROM categorias WHERE id =productos.categoria_id) AS categoria, categoria_id, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca, marca_id, (SELECT count(*) FROM asociacion_carro_producto WHERE producto.id = id_producto ) AS vistos FROM productos WHERE productos.stock > 0 '.$this->db->escape($stringfiltro).' AND productos.is_stack = 0 ORDER BY vistos DESC');

      return $query->result_array();
   }

   function get_productos_nuevos_orden(){
      //$query = $this->db->get('productos');
      $query = $this->db->query('SELECT `id`, `nombre`, `descripcion`, `precio`, `foto_etiqueta`, `foto_producto`, `foto_home`, `foto_opcional`, `sku`, `descuento`, `envio_gratis`, `categoria_id`, `sub_categoria_id`, `marca_id`, `stock`, `tipo_entrega`, `regalos`, `is_stack`, `color`, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, fecha_creacion, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos WHERE productos.stock > 0 ORDER BY id DESC');

      return $query->result_array();
   }

   function get_productos_populares(){
      //$query = $this->db->get('productos');
      $query = $this->db->query('SELECT *,(select count(producto_carro.id) from producto_carro WHERE productos.id = producto_carro.id_producto ) as cotizado FROM productos ORDER BY cotizado DESC ');

      return $query->result_array();
   }



   function actual_imagen($id){
      $this->db->select('foto_etiqueta');
      $this->db->select('foto_producto');
      $this->db->select('foto_home');
      $query = $this->db->get_where('productos', array('id' => $id));
      return $query->row_array();
   }

   function existe_producto($nombre){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $query = $this->db->get('productos');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

   function get_last_id(){
      $this->db->select_max('id');
      $result= $this->db->get('productos')->row_array();
      return $result['id'];
   }

   function creaskupostregistro($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('productos', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
   
   function inserta_producto($datos = array()){
      $datos['is_stack'] = 0;
      $this->db->insert('productos', $datos);
       if($this->db->affected_rows()>0){
         
         return $this->db->insert_id();
      }else{
         return false;
      }
   }


   function elimina_colores($prod){
      $this->db->where('id_producto', $prod);
      $this->db->delete('color_producto');
      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
   function asigna_color($key,$prod){
      $arraySabor = array('id_color' => $key,'id_producto'=>$prod);
      $this->db->insert('color_producto', $arraySabor);
      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function asigna_destacado($prod){
      $arraySabor = array('fecha_destacado' => date("Y-m-d H:i:s"),'id_producto'=>$prod);
      $this->db->insert('producto_destacado', $arraySabor);
      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }


   function edita_producto($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('productos', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function edita_cantidad_producto($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('productos', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_producto($id){
      $this->db->where('id', $id);
      $this->db->delete('productos');

      if($this->db->affected_rows()>0){
         $this->db->where('id_producto', $id);
         $this->db->delete('color_producto');
         return true;
      }else{
         return false;
      }
   }

}