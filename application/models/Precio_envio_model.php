<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Precio_envio_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	function existe_precio_envio($datos = array()){
	  if(empty($datos['precio'])||
      	 empty($datos['region_id'])||
      	 empty($datos['provincia_id'])||
      	 empty($datos['comuna_id'])){
         return false;
      }

      $this->db->select('precio');
      $this->db->where('region_id', $datos['region_id']); 
      $this->db->where('comuna_id', $datos['comuna_id']); 
      $this->db->where('provincia_id', $datos['provincia_id']); 
      $query = $this->db->get('precio_envio');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
	}

   function obtener_lista_precio_envios(){
    $query = $this->db->query('SELECT id, precio, region_id, provincia_id, comuna_id, desde, hasta, tipo_medida, (SELECT nombre FROM region WHERE id = pe.region_id) as region_nombre, (SELECT nombre FROM provincia WHERE id = pe.provincia_id) as provincia_nombre, (SELECT nombre FROM comuna WHERE id = pe.comuna_id) as comuna_nombre FROM precio_envio as pe');

    return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_precio_envio($id = FALSE){
        if ($id === FALSE)
        {
                $query = $this->db->get('precio_envio');
                return $query->result_array();
        }

        $query = $this->db->get_where('precio_envio', array('id' => $id));
        return $query->row_array();
   }
   function get_precio_envio_comuna($id = FALSE){

        $query = $this->db->get_where('precio_envio', array('comuna_id' => $id));
        return $query->row_array();
   }
   
   function inserta_precio_envio($datos = array()){
      if(empty($datos['precio'])||
      	 empty($datos['region_id'])||
      	 empty($datos['provincia_id'])||
      	 empty($datos['comuna_id'])){
         return false;
      }
      $this->db->insert('precio_envio', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_precio_envio($datos = array()){
      if(empty($datos['precio'])||
      	 empty($datos['id'])||
      	 empty($datos['region_id'])||
      	 empty($datos['provincia_id'])||
      	 empty($datos['comuna_id'])){
         return FALSE;
      }
      $this->db->where('id', $datos['id']);
      $this->db->update('precio_envio', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_precio_envio($id){
      $this->db->where('id', $id);
      $this->db->delete('precio_envio');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}

/* End of file Precio_envio_model.php */
/* Location: ./application/models/Precio_envio_model.php */