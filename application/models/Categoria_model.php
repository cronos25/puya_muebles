<?php 
class Categoria_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_categorias(){
      $this->db->order_by('id', 'ASC');

      $query = $this->db->get('categorias');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_categoria($id = FALSE){
         $this->db->order_by('id','DESC');
        $query = $this->db->get_where('categorias', array('id' => $id));
        return $query->row_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_productos_categoria($id = FALSE){
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = $this->db->query('SELECT id, nombre, descripcion, precio, foto_etiqueta, foto_producto, foto_home, sku, descuento, envio_gratis, medida_s, medida_m, medida_l, medida_xl, categoria_id, color, marca_id, stock,tipo_entrega, regalos,sub_categoria_id, fecha_creacion, (SELECT nombre FROM categorias WHERE id =productos.categoria_id) AS categoria,(SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca,(SELECT nombre FROM sub_categoria WHERE id =productos.sub_categoria_id) AS sub_categoria FROM productos WHERE productos.categoria_id = '.$this->db->escape($id));
      return $query->result_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_productos_categoria_navbar($id = FALSE){
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = $this->db->query('SELECT id, nombre, descripcion, sku, precio, descuento, cantidad, foto_etiqueta, foto_producto, foto_home,  (SELECT nombre FROM categorias WHERE id =productos.categoria_id) AS categoria, categoria_id, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca, marca_id, informacion_nutricional, regalos, envio_gratis, tipo_entrega, stock, color FROM productos WHERE productos.categoria_id = '.$this->db->escape($id).' ORDER BY id DESC LIMIT 2');

      return $query->result_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_especificaciones_categoria($id = FALSE){
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = $this->db->query('SELECT *,(SELECT nombre from categorias where id =especificacion_categoria.categoria_id) as categoria FROM especificacion_categoria WHERE categoria_id ='.$this->db->escape($id));
      return $query->result_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
   function get_productos_filtrados_categoria($id = FALSE,$marcas = array()){

      
      $stringmarcas = "(p.`marca_id` =";
      for($i = 0; $i<count($marcas);$i++){
         if ($i < (count($marcas)-1)) {
            $stringmarcas .= $this->db->escape($marcas[$i])." OR p.`marca_id` =";
         }else{
            $stringmarcas .= $this->db->escape($marcas[$i]).")";
         }
      }
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = "SELECT p.`id`, p.`nombre`, p.`descripcion`, p.`precio`, p.`foto_etiqueta`, p.`foto_producto`, p.`foto_home`, p.`sku`, p.`descuento`, p.`envio_gratis`, p.`informacion_nutricional`, p.`categoria_id`, p.`sub_categoria_id`, p.`color`,p.`marca_id`, p.`stock`, p.`cantidad`, p.`tipo_entrega`, p.`regalos`, p.`is_stack` ,categorias.nombre as categoria,marca.nombre as marca FROM productos as p,categorias,marca WHERE p.categoria_id = categorias.id AND p.marca_id = marca.id AND p.`categoria_id` =".$this->db->escape($id)." AND ".$stringmarcas;
      $resulquery = $this->db->query($query);
      return $resulquery->result_array();
   }

   function get_total_prods_categorias_marcas($marca){
      //$query = $this->db->get('producto');
      $query = $this->db->query('SELECT * ,(select count(productos.id) from productos WHERE productos.marca_id = '.$marca.' AND productos.categoria_id = categorias.id) as productos FROM categorias');

      return $query->result_array();
   }

   function get_total_prods_categorias(){
      //$query = $this->db->get('producto');
      $query = $this->db->query('SELECT * ,(select count(productos.id) from productos WHERE productos.categoria_id = categorias.id) as productos FROM categorias');

      return $query->result_array();
   }

   function get_total_prods_categorias_buscar($productos=array()){
      //$query = $this->db->get('producto');
      $stringproductos="";
      if (count($productos)>0) {
         $stringproductos = " AND (productos.id= ";
         for($i = 0; $i<count($productos);$i++){
            if ($i < (count($productos)-1)) {
               $stringproductos .= $this->db->escape($productos[$i])." OR productos.id=";
            }else{
               $stringproductos .= $this->db->escape($productos[$i]).")";
            }
         }
         $query = $this->db->query('SELECT * ,(select count(productos.id) from productos WHERE productos.categoria_id = categorias.id '.$stringproductos.') as productos FROM categorias');
         return $query->result_array();
      }else{
         return $productos;
      }      
   }

   function actual_imagen($id){
      $this->db->select('imagen');
      $query = $this->db->get_where('categorias', array('id' => $this->db->escape($id)));
      return $query->row_array();
   }

   function existe_categoria($nombre){
      $this->db->select('nombre');
      $this->db->where('nombre', $this->db->escape($nombre)); 
      $query = $this->db->get('categorias');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }
   
   function inserta_categoria($datos = array()){
      $this->db->insert('categorias', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_categoria($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('categorias', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_categoria($id){
      $this->db->where('id', $id);
      $this->db->delete('categorias');

      if($this->db->affected_rows()>0){
         $this->db->where('categoria_id',$id);
         $this->db->update('productos', array('categoria_id'=>0));

         $this->db->affected_rows();

         return true;
      }else{
         return false;
      }
   }
}