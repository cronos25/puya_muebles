<?php 
class Banner_home_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_banners(){
      $query = $this->db->query('SELECT id, imagen, producto_id, estado, tipo_banner, texto_btn_banner, (SELECT nombre FROM productos WHERE id = banner_home.producto_id) as producto FROM banner_home');
      return $query->result_array();
   }

   function obtener_lista_banners_slider(){
      $this->db->where('estado',0);
      $query = $this->db->get('banner_home');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
   function get_banner($pos = null){
        $query = $this->db->get_where('banner_home', array('id' => $pos));
        return $query->row_array();
   }

   function actual_imagen($id){
      $this->db->select('imagen');
      $query = $this->db->get_where('banner_home', array('id' => $id));
      return $query->row_array();
   }

   function inserta_banner($datos = array()){
      $this->db->insert('banner_home', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_banner($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('banner_home', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function cambio_estado_banner($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('banner_home', array("estado"=>$datos['estado']));

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_banner($id){
      $this->db->where('id', $id);
      $this->db->delete('banner_home');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}