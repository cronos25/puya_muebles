<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocion_temporal_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

   function existe_promo($data){
      //$this->db->select('codigo'); 
      $where = "`fecha_inicio` BETWEEN ".$this->db->escape($data['fecha_inicio'])." and ".$this->db->escape($data['fecha_fin'])." OR `fecha_fin` BETWEEN ".$this->db->escape($data['fecha_inicio'])." AND ".$this->db->escape($data['fecha_fin']);
      $this->db->where($where);
      $query = $this->db->get('promocion_temporal');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

   function existe_promo_edit($data){
      //$this->db->select('codigo'); 
      $where = "(`fecha_inicio` BETWEEN ".$this->db->escape($data['fecha_inicio'])." and ".$this->db->escape($data['fecha_fin'])." OR `fecha_fin` BETWEEN ".$this->db->escape($data['fecha_inicio'])." AND ".$this->db->escape($data['fecha_fin']).") AND id !=".$this->db->escape($data['id']);
      $this->db->where($where);
      $query = $this->db->get('promocion_temporal');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

	function obtener_lista_promocion_temporal(){
      $this->db->select("*");
      $this->db->select('(SELECT nombre FROM productos WHERE productos.id=promocion_temporal.producto_id) as producto');
      $this->db->select('(SELECT nombre FROM categorias WHERE categorias.id=promocion_temporal.categoria_id) as categoria');
      $this->db->select('(SELECT nombre FROM marca WHERE marca.id=promocion_temporal.marca_id) as marca');
      $this->db->select('(SELECT nombre FROM sub_categoria WHERE sub_categoria.id=promocion_temporal.sub_categoria_id) as sub_categoria');

      $query = $this->db->get('promocion_temporal');
      return $query->result_array();
   	}

   function obtener_promocion_temporal($id){
      $query = $this->db->get_where('promocion_temporal',array('id'=>$id));
      return $query->row_array();
      }

	function obtener_lista_promociones_activos(){
      $query = $this->db->where('fecha_inicio <',date("Y-m-d H:i:s"));
      $query = $this->db->where('fecha_fin >',date("Y-m-d H:i:s"));
      $this->db->select('(SELECT nombre FROM productos WHERE productos.id=promocion_temporal.producto_id) as producto');
      $query = $this->db->get('promocion_temporal');
      return $query->result_array();
   }

   function obtener_lista_promocion_activo(){
      $query = $this->db->where('fecha_inicio <',date("Y-m-d H:i:s"));
      $query = $this->db->where('fecha_fin >',date("Y-m-d H:i:s"));
      $this->db->select("*");
      $this->db->select('(SELECT nombre FROM productos WHERE productos.id=promocion_temporal.producto_id) as producto');
      $query = $this->db->get('promocion_temporal');
      return $query->row_array();
   }

	function inserta_promocion_temporal($datos = array()){
      $this->db->insert('promocion_temporal', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_promocion_temporal($datos = array()){
      if(empty($datos['fecha_inicio'])||empty($datos['fecha_fin'])||empty($datos['id'])||empty($datos['mensaje'])){
         return FALSE;
      }

      $this->db->where('id', $datos['id']);
      $this->db->update('promocion_temporal', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function elimina_promocion_temporal($id){
      $this->db->where('id', $id);
      $this->db->delete('promocion_temporal');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}

/* End of file Codigo_promocion_model.php */
/* Location: ./application/models/Codigo_promocion_model.php */