<?php 
class Distribuidor_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_distribuidores(){
      $query = $this->db->query('SELECT id, nombre, link, autorizado, imagen, descripcion FROM distribuidor');
      return $query->result_array();
   }

   function obtener_lista_distribuidores_autorizados(){
      $this->db->where('autorizado',0);
      $query = $this->db->get('distribuidor');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
   function get_distribuidor($pos = null){
        $query = $this->db->get_where('distribuidor', array('id' => $pos));
        return $query->row_array();
   }

   function existe_distribuidor($nombre){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $query = $this->db->get('distribuidor');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

   function actual_imagen($id){
      $this->db->select('imagen');
      $query = $this->db->get_where('distribuidor', array('id' => $id));
      return $query->row_array();
   }

   function inserta_distribuidor($datos = array()){
      $this->db->insert('distribuidor', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_distribuidor($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('distribuidor', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function cambio_autorizado_distribuidor($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('distribuidor', array("autorizado"=>$datos['autorizado']));

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_distribuidor($id){
      $this->db->where('id', $id);
      $this->db->delete('distribuidor');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}