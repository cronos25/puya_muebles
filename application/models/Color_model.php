<?php 
class Color_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_colores(){
      $query = $this->db->get('color');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_color($id = FALSE){

        $query = $this->db->get_where('color', array('id' => $id));
        return $query->row_array();
   }


   function get_total_prods_colores(){
      //$query = $this->db->get('producto');
      $query = $this->db->query('SELECT *,(select count(producto.id) from productos WHERE producto.color = color.id) as productos FROM color');

      return $query->result_array();
   }

   function get_total_prods_colores_detalle(){
      $this->db->select('*');
      $this->db->select('(SELECT count(color_producto.id) FROM color_producto WHERE color_producto.id_color = color.id) AS productos');
      $query = $this->db->get('color');
      return $query->result_array();
   }

   function existe_color($nombre){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $query = $this->db->get('color');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }
   
   function inserta_color($datos = array()){
      $this->db->insert('color', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }


   function edita_color($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('color', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_color($id){
      $this->db->where('id', $id);
      $this->db->delete('color');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function get_total_prods_colores_producto($idProd){
      //$query = $this->db->get('producto');
      $this->db->select('id_color');
      $this->db->where('id_producto', $idProd); 
      $query = $this->db->get('color_producto');
      return $query->result_array();
   }

   function get_total_colores_producto_detalle($idProd){
      //$query = $this->db->get('producto');
      $this->db->select('*');
      $this->db->select('id_color');
      $this->db->select('(SELECT nombre FROM color WHERE color.id= color_producto.id_color) as color');
      $this->db->select('(SELECT hexa FROM color WHERE color.id= color_producto.id_color) as hexa');
      $this->db->where('id_producto', $idProd); 
      $query = $this->db->get('color_producto');
      return $query->result_array();
   }
}