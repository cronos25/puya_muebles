<?php 
class Sub_categoria_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_sub_categorias(){
      $query = $this->db->query('SELECT id, nombre, id_categoria, (SELECT nombre FROM categorias WHERE id = c.id_categoria) as categoria, id_marca, (SELECT nombre FROM marca WHERE id = c.id_marca) as marca FROM sub_categoria as c');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_sub_categoria($id = FALSE){

        //$query = $this->db->get('comuna');
      $query = $this->db->query('SELECT id, nombre, id_categoria, (SELECT nombre FROM categorias WHERE id = c.id_categoria) as categoria, id_marca, (SELECT nombre FROM marca WHERE id = c.id_marca) as marca FROM sub_categoria as c  where id ='.$id);

      return $query->row_array();
   }

   function obtener_lista_sub_categorias_categoria($id_categoria){

    $this->db->where('id_categoria', $id_categoria); 
    $this->db->order_by('id', 'ASC');
    $query = $this->db->get('sub_categoria');
    return $query->result_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_especificaciones_sub_categoria($id = FALSE){
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = $this->db->query('SELECT *,(SELECT nombre from categoria where id =especificacion_categoria.categoria_id) as categoria FROM especificacion_categoria WHERE categoria_id ='.$id);
      return $query->result_array();
   }

   function get_total_prods_sub_categorias(){
      //$query = $this->db->get('producto');
      $query = $this->db->query('SELECT * ,(select count(producto.id) from producto WHERE producto.sub_categoria_id = sub_categoria.id) as productos FROM sub_categoria');

      return $query->result_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_productos_sub_categoria($id = FALSE){
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = $this->db->query('SELECT id, nombre, descripcion, precio, foto_etiqueta, foto_producto, foto_home, sku, descuento, envio_gratis, informacion_nutricional, categoria_id, sabor, marca_id, stock, cantidad, tipo_entrega, informacion_producto, regalos, servicios, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria,(SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos WHERE productos.sub_categoria_id = '.$this->db->escape($id).' AND productos.is_stack = 0');
      return $query->result_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
   function get_productos_filtrados_sub_categoria($id = FALSE,$marcas = array()){

      
      $stringmarcas = "(p.`marca_id` =";
      for($i = 0; $i<count($marcas);$i++){
         if ($i < (count($marcas)-1)) {
            $stringmarcas .= $this->db->escape($marcas[$i])." OR p.`marca_id` =";
         }else{
            $stringmarcas .= $this->db->escape($marcas[$i]).")";
         }
      }
      //$query = $this->db->get_where('producto', array('id' => $id));
      $query = "SELECT p.`id`, p.`nombre`, p.`descripcion`, p.`precio`, p.`foto_etiqueta`, p.`foto_producto`, p.`foto_home`, p.`sku`, p.`descuento`, p.`envio_gratis`, p.`informacion_nutricional`, p.`categoria_id`, p.`sub_categoria_id`, p.`sabor`,p.`marca_id`, p.`stock`, p.`cantidad`, p.`tipo_entrega`, p.`informacion_producto`, p.`regalos`, p.`servicios`, p.`is_stack` ,categorias.nombre as categoria,marca.nombre as marca,sub_categoria.nombre as sub_categoria FROM productos as p,categorias,marca,sub_categoria WHERE p.categoria_id = categorias.id AND p.marca_id = marca.id AND p.is_stack = 0 AND p.`sub_categoria_id` =".$this->db->escape($id)." AND ".$stringmarcas;
      $resulquery = $this->db->query($query);
      return $resulquery->result_array();
   }

   function actual_imagen($id){
      $this->db->select('imagen');
      $query = $this->db->get_where('sub_categoria', array('id' => $id));
      return $query->row_array();
   }

   function existe_sub_categoria($nombre,$categoria){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $this->db->where('id_categoria', $categoria); 
      $query = $this->db->get('sub_categoria');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }
   
   function inserta_sub_categoria($datos = array()){
      $this->db->insert('sub_categoria', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_sub_categoria($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('sub_categoria', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_sub_categoria($id){
      $this->db->where('id', $id);
      $this->db->delete('sub_categoria');

      if($this->db->affected_rows()>0){
         $this->db->where('sub_categoria_id',$id);
         $this->db->update('producto', array('sub_categoria_id'=>null));

         $this->db->affected_rows();

         return true;
      }else{
         return false;
      }
   }
}