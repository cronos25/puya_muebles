<?php 
class Carro_compra_model extends CI_Model {
  
  function __construct(){
    $this->load->database();
  }

  function obtener_lista_carro_compras_todo(){
    $query = $this->db->query('SELECT id, codigo, estado, id_cliente, fecha_creacion AS fecha, mensaje,(SELECT email FROM cliente WHERE id= cp.id_cliente) AS email, (SELECT rut FROM cliente WHERE id= cp.id_cliente) AS rut_comprador, (SELECT SUM(precio) FROM producto_carro WHERE id_carro = cp.id) as total FROM carro_compra AS cp');
    return $query->result_array();
  }

  function obtener_carros_activos_10dias(){
    //$query = $this->db->get('productos');
    $query = $this->db->query('SELECT fecha_creacion, count(*) as carrofecha FROM carro_compra where fecha_creacion >= ( CURDATE() - INTERVAL 10 DAY ) AND estado = 0 GROUP BY fecha_creacion');

    return $query->result_array();
  }

  function obtener_carros_vendidos(){
    //$query = $this->db->get('productos');
    $query = $this->db->query('SELECT fecha_creacion, count(*) as carrofecha FROM carro_compra where fecha >= ( CURDATE() - INTERVAL 10 DAY ) AND estado = 2 GROUP BY fecha');

    return $query->result_array();
  }

  function obtener_lista_carro_compras(){
    $this->db->where('estado',1);
    $query = $this->db->get('carro_compra');
    return $query->result_array();
  }

  //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
  function get_carro_compra($id = FALSE){
      $query = $this->db->get_where('carro_compra', array('id' => $id));
      return $query->row_array();
  }

  function existe_direccion($id){
      $this->db->select(array(
        'nombre_carro',
        'email_carro',
        'telefono_carro',
        'region_carro',
        'provincia_carro',
        'comuna_carro',
        'direccion_carro',
        'retiro_tienda'
        ));
      $query = $this->db->get_where('carro_compra', array('id' => $id));
      $respuestaData = $query->row_array();

      //echo json_encode($respuestaData);
      if(!empty($respuestaData['nombre_carro']) &&
        !empty($respuestaData['email_carro']) &&
        !empty($respuestaData['telefono_carro']) &&
        !empty($respuestaData['region_carro']) &&
        !empty($respuestaData['provincia_carro']) &&
        !empty($respuestaData['comuna_carro']) &&
        !empty($respuestaData['direccion_carro']) ||
        $respuestaData['retiro_tienda'] == 1
      ){
        return true;
      }else{
        return false;
      }
  }

  function existe_carro_compra($id_user){
     $query = $this->db->query("SELECT codigo FROM carro_compra WHERE id_cliente=".$id_user." AND estado = 0");
      $respuestaData = $query->row_array();
      //echo json_encode($respuestaData);
      if(!empty($respuestaData['codigo'])){
        return true;
      }else{
        return false;
      }
  }

  function inserta_carro_compra($datos = array()){
    $datos['uuid'] = date("ymdHis");
    $this->db->insert('carro_compra', $datos);
     if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
    //return $this->db->insert_id();
  }

  function get_carro_compra_codigo($cod = FALSE){
      $this->db->where('codigo', $cod);
      $this->db->order_by('id', 'DESC');
      $this->db->limit(1);
      $query = $this->db->get('carro_compra');
      return $query->row_array();
  }

  function get_carros_compra_procesado_codigo($cod = FALSE){
      $this->db->where('codigo', $cod);
      $this->db->where('estado>',0);
      $this->db->order_by('id', 'DESC');
      $query = $this->db->get('carro_compra');
      return $query->result_array();
  }

  function edita_carro_compra($datos = array()){
    $this->db->where('id', $datos['id']);
    $this->db->update('carro_compra', $datos);

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function elimina_carro_compra($id){
    $this->db->where('id', $id);
    $this->db->delete('carro_compra');

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  //funciones de agregar productos al carro creado
  function agregar_producto_carro($datos = array()){
    $this->db->insert('producto_carro', $datos);
    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
    //return $this->db->insert_id();
  }

  function procesa_productos_carro($datos){
    $this->db->where('id_carro', $datos['id']);
    $this->db->where('estado !=',2);
    $this->db->update('producto_carro',array('estado'=>1));

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function modifica_cantidad_productos_carro($datos){
    $this->db->where('id', $datos['id']);
    $this->db->update('producto_carro',array('cantidad'=>$datos['cantidad']));

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function modifica_sabor_productos_carro($datos){
    $this->db->where('id', $datos['id']);
    $this->db->update('producto_carro',array('sabor'=>$datos['sabor']));

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function obtener_cantidad_productos_carro($datos){
      $this->db->where('id_carro', $datos['id-carro']);
      $this->db->where('estado', 0);
      $query = $this->db->get('producto_carro');
      return $query->num_rows();
  }

  function procesa_productos_carro_eliminado($datos){
    $this->db->where('id_carro', $datos['id']);
    $this->db->update('producto_carro',array('estado'=>3));

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  private function get_total_colores_producto_detalle($idProd){
      //$query = $this->db->get('producto');
      $this->db->select('*');
      $this->db->select('(SELECT nombre FROM color WHERE color.id=color_producto.id_color) as color');
      $this->db->where('id_producto', $idProd); 
      $query = $this->db->get('color_producto');
      return $query->result_array();
   }

  function llamar_productos_carro($datos = array()){
  if(!is_null($datos)){
      $arrayProductos = array();
      $this->db->select('*');
      $this->db->select('(SELECT nombre FROM color WHERE color.id = producto_carro.color ) as nombre_color');
    
      $this->db->where('id_carro', $datos['id-carro']);
      $this->db->where('estado', 0);
      $query = $this->db->get('producto_carro');
      if($query->num_rows() > 0){
        $productosAsociados = $query->result_array();

        foreach ($productosAsociados as $productoasocItem){
          $queryProd = $this->db->query('SELECT id, nombre, descripcion, precio, foto_etiqueta, foto_producto, foto_home, sku, descuento,color,envio_gratis,categoria_id, marca_id, stock, tipo_entrega, regalos, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, sub_categoria_id, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos WHERE id='.$this->db->escape($productoasocItem['id_producto']));

          //falta hacer el limite para permitir editar, se debe calcular, cuantos productos existen en carros vs, la cantidad del producto registrado
          $arrayproducto = array("id"=>$queryProd->row_array()['id'],
                                  "nombre"=>$queryProd->row_array()['nombre'],
                                  "codigo"=>$queryProd->row_array()['sku'],
                                  "precio"=>$queryProd->row_array()['precio'],
                                  "descuento"=>$queryProd->row_array()['descuento'],
                                  "regalos"=>$queryProd->row_array()['regalos'],
                                  "marca"=>$queryProd->row_array()['marca'],
                                  "imagen_prod"=>$queryProd->row_array()['foto_producto'],
                                  "disponible"=>$queryProd->row_array()['stock'],
                                  "categoria_id"=>$queryProd->row_array()['categoria_id'],
                                  "marca_id"=>$queryProd->row_array()['marca_id'],
                                  "sub_categoria_id"=>$queryProd->row_array()['sub_categoria_id'],
                                  "colores"=>$this->get_total_colores_producto_detalle($queryProd->row_array()['id']),
                                  "asoc"=>$productoasocItem['id'],
                                  "asoc_color"=>$productoasocItem['color'],
                                  "asoc_color_nombre"=>$productoasocItem['nombre_color'],
                                  "cart"=>$productoasocItem['id_carro'],
                                  "asoc_val"=>$productoasocItem['precio'],
                                  "asoc_val_desc"=>$productoasocItem['descuento'],
                                  "cantidad"=>$productoasocItem['cantidad']);
          array_push($arrayProductos, $arrayproducto);
          //$arrayProductos = array("mensaje"=>$productoasocItem['id_producto']);
        }
        return $arrayProductos;
      }
  }
  }

  function llamar_productos_carro_resumen($datos = array()){
  if(!is_null($datos)){
      $arrayProductos = array();

      $where = "id_carro=".$datos['id-carro']." AND (estado=1 OR estado=3)";
      $this->db->where($where);
      $query = $this->db->get('producto_carro');
      if($query->num_rows() > 0){
        $productosAsociados = $query->result_array();

        foreach ($productosAsociados as $productoasocItem){
          $queryProd = $this->db->query('SELECT id, nombre, descripcion, precio, foto_etiqueta, foto_producto, foto_home, sku, descuento, envio_gratis, color, categoria_id, marca_id, stock, tipo_entrega, regalos, `medida_s`, `medida_m`, `medida_l`, `medida_xl`, `precio_m`, `precio_l`, `precio_xl`, sub_categoria_id, (SELECT nombre FROM categorias WHERE id =productos.categoria_id)AS categoria, (SELECT nombre FROM marca WHERE id =productos.marca_id) AS marca FROM productos WHERE id='.$this->db->escape($productoasocItem['id_producto']));

          //falta hacer el limite para permitir editar, se debe calcular, cuantos productos existen en carros vs, la cantidad del producto registrado
          if ($productoasocItem['color'] != null) {
            $nombreColor = $this->db->query('SELECT nombre FROM color WHERE id = '.$productoasocItem['color'].';')->row_array()['nombre'];
          }else{
            $nombreColor = "sin color seleccionado";
          }

          $arrayproducto = array("id"=>$queryProd->row_array()['id'],
                                  "nombre"=>$queryProd->row_array()['nombre'],
                                  "codigo"=>$queryProd->row_array()['sku'],
                                  "precio"=>$queryProd->row_array()['precio'],
                                  "descuento"=>$queryProd->row_array()['descuento'],
                                  "regalos"=>$queryProd->row_array()['regalos'],
                                  "marca"=>$queryProd->row_array()['marca'],
                                  "imagen_prod"=>$queryProd->row_array()['foto_producto'],
                                  "disponible"=>$queryProd->row_array()['stock'],
                                  "categoria_id"=>$queryProd->row_array()['categoria_id'],
                                  "marca_id"=>$queryProd->row_array()['marca_id'],
                                  "sub_categoria_id"=>$queryProd->row_array()['sub_categoria_id'],
                                  "colores"=>$this->get_total_colores_producto_detalle($queryProd->row_array()['id']),
                                  "asoc"=>$productoasocItem['id'],
                                  "asoc_color"=>$nombreColor,
                                  "cart"=>$productoasocItem['id_carro'],
                                  "asoc_val"=>$productoasocItem['precio'],
                                  "asoc_val_desc"=>$productoasocItem['descuento'],
                                  "cantidad"=>$productoasocItem['cantidad']);
          array_push($arrayProductos, $arrayproducto);
          //$arrayProductos = array("mensaje"=>$productoasocItem['id_producto']);
        }
        return $arrayProductos;
      }
  }
  }
  

  function obtener_asoc_carro($asoc){
    $this->db->where('id', $asoc);
    $query = $this->db->get('producto_carro');
    return $query->row_array();
  }

  function elimina_productos_carro($asoc = array()){
    $this->db->where('id', $asoc['id']);
    $this->db->update('producto_carro', $asoc);

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }
}