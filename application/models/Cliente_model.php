<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Cliente_model extends CI_Model {
   private $ci;
   function __construct(){
      $this->load->database();
      $this->ci = & get_instance();
      $this->ci->load->library('encryption');
   }

   function login($username, $password){
      
      $select =   array(
                'cliente.id',
                'cliente.email',
                'cliente.rut',
                'cliente.nombre',
                'cliente.apellido'
            );  
      $this->db->where('email', $username);
      $this->db->where('password', MD5($this->ci->config->item('_COOKIE_KEY_').$password));
      $this->db->select($select);
      $this->db->from('cliente');

      /*$this->db->limit(1);*/

      $query = $this->db->get();

      if($query->num_rows()==1){
       return $query->result();
      }
      else{
       return false;
      }
   }
   
  function existe_email($email){
      $this->db->select('email');
      $this->db->where('email',$email); 
      $query = $this->db->get('cliente');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

   function obtener_lista_clientes(){
      $query = $this->db->get('cliente');
      return $query->result_array();
   }
   
   function obtener_clientes_nuevo_a_viejo(){
      
      $this->db->order_by('id', 'DESC');
      $query = $this->db->get('cliente');
      return $query->result_array();
   }

   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_cliente($id = FALSE){
        if ($id === FALSE)
        {
                $query = $this->db->get('cliente');
                return $query->result_array();
        }

        $query = $this->db->get_where('cliente', array('id' => $id));
        return $query->row_array();
   }
    function get_cliente_full($id = FALSE){
        $query = $this->db->query('SELECT id, username, password, email, nombre, apellido, direccion, id_region,sexo, (SELECT nombre FROM region WHERE id = c.id_region) as region, id_comuna, (SELECT nombre FROM comuna WHERE id = c.id_comuna) as comuna, id_provincia, (SELECT nombre FROM provincia WHERE id = c.id_provincia) as provincia, edad, puntos, rut, telefono, medio_pago_preferido,rut_fac, nombre_fac, giro_fac, telefono_fac, direccion_fac, region_id_fac, provincia_id_fac, comuna_id_fac, (SELECT nombre FROM region WHERE id = c.region_id_fac) as region_fac, id_comuna, (SELECT nombre FROM comuna WHERE id = c.comuna_id_fac) as comuna_fac, id_provincia, (SELECT nombre FROM provincia WHERE id = c.provincia_id_fac) as provincia_fac FROM cliente as c WHERE id='.$id);
      
        return $query->row_array();
    }
   
  function inserta_cliente($datos = array()){
      if(empty($datos['email'])||empty($datos['password'])){
         return false;
      }
      //clave, encripto
      $datos['password']=md5($this->ci->config->item('_COOKIE_KEY_').$datos['password']);
   
      $this->db->insert('cliente', $datos);
       if($this->db->affected_rows()>0){
        return array("user"=>$this->db->insert_id(),
              "estado"=>true);
      }else{
         return array("estado"=>false);
      }
      //return $this->db->insert_id();
  }

  function edita_cliente($datos = array()){
    if(empty($datos['email'])||empty($datos['id'])){
       return FALSE;
    }
    $this->db->where('id', $datos['id']);
    $this->db->update('cliente', $datos);

    if($this->db->affected_rows()>0){
       return true;
    }else{
       return false;
    }
  }

  function olvida_contrasenia($datos){
      if(empty($datos['email'])){
       return FALSE;
      }
      $this->db->select('id');
      $datos = $this->db->get_where('cliente', array('email' => $datos['email']))->row_array(0);
      
      //clave, encripto
      $temporalpass=uniqid();
      $datos['password']=md5($this->ci->config->item('_COOKIE_KEY_').$temporalpass);
      
      $this->db->where('id', $datos['id']);
      $this->db->update('cliente', $datos);

      if($this->db->affected_rows()>0){
         return array('estado'=>true,'timepass'=>$temporalpass);
      }else{
         return array('estado'=>false);
      }
   }

    function edita_cliente_perfil($datos = array()){
      if(empty($datos['email'])||empty($datos['id'])){
         return FALSE;
      }
      
      $this->db->where('id', $datos['id']);
      $this->db->update('cliente', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }

    function edita_cliente_perfil_pass($datos = array()){
      if(empty($datos['email'])||empty($datos['id'])||empty($datos['password']) ){
         return FALSE;
      }
      $datos['password']=md5($this->ci->config->item('_COOKIE_KEY_').$datos['password']);
      $this->db->where('id', $datos['id']);
      $this->db->update('cliente', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }

    function edita_cliente_perfil_direccion($datos = array()){
      if(empty($datos['id'])){
         return FALSE;
      }
      
      $this->db->where('id', $datos['id']);
      $this->db->update('cliente', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }

    function edita_cliente_perfil_facturacion($datos = array()){
      if(empty($datos['id']) ||
        empty($datos['rut_fac'])||
        empty($datos['nombre_fac'])||
        empty($datos['giro_fac'])||
        empty($datos['telefono_fac'])||
        empty($datos['id_region'])||
        empty($datos['id_provincia'])||
        empty($datos['id_comuna'])||
        empty($datos['direccion_fac'])){
         return FALSE;
      }
      
      $this->db->where('id', $datos['id']);
      $this->db->update('cliente', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
    }

   function elimina_cliente($id){
      $this->db->where('id', $id);
      $this->db->delete('cliente');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}