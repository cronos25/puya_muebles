<?php 
class Usuario_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }

   function login($username, $password){
     $this->db->select('id, email, password');
     $this->db->from('usuario');
     $this->db->where('email', $username);
     $this->db->where('password', MD5($password));
     $this->db->limit(1);
   
     $query = $this->db->get();
   
     if($query->num_rows() == 1)
     {
       return $query->result();
     }
     else
     {
       return false;
     }
   }
   
   function existe_email($email){
      $this->db->select('email');
      $this->db->where('email', $email); 
      $query = $this->db->get('usuario');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }

   function obtener_lista_usuarios(){
      $query = $this->db->get('usuario');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_usuario($id = FALSE){
        if ($id === FALSE)
        {
                $query = $this->db->get('usuario');
                return $query->result_array();
        }

        $query = $this->db->get_where('usuario', array('id' => $id));
        return $query->row_array();
   }
   
   function inserta_usuario($datos = array()){
      if(empty($datos['email'])||empty($datos['password'])){
         return false;
      }
      //clave, encripto
      $datos['password']=md5($datos['password']);
   
      $this->db->insert('usuario', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_usuario($datos = array()){
      if(empty($datos['email'])||empty($datos['password'])||empty($datos['id'])){
         return FALSE;
      }
      $this->db->select('password');
      $query = $this->db->get_where('usuario', array('id' => $datos['id']));
      $previewid = $query->row_array(0);
      //clave, encripto
      if($previewid != $datos['password']){
         $datos['password']=md5($datos['password']);
      }
      $this->db->where('id', $datos['id']);
      $this->db->update('usuario', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_usuario($id){
      $this->db->where('id', $id);
      $this->db->delete('usuario');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}