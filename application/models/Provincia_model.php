<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provincia_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	function existe_provincia($nombre,$id_region){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $this->db->where('id_region', $id_region); 
      $query = $this->db->get('provincia');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
	}

   function obtener_lista_provincias(){
      //$query = $this->db->get('provincia');
      $query = $this->db->query('SELECT id, nombre, id_region, (SELECT nombre FROM region WHERE id = c.id_region) as region_nombre FROM provincia as c');

      return $query->result_array();
   }
   
   function obtener_lista_provincias_region($id_region){
      $this->db->where('id_region', $id_region); 
      $query = $this->db->get('provincia');
      return $query->result_array();
   }
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_provincia($id = FALSE){
        if ($id === FALSE)
        {
                $query = $this->db->get('provincia');
                return $query->result_array();
        }

        $query = $this->db->get_where('provincia', array('id' => $id));
        return $query->row_array();
   }
   
   function inserta_provincia($datos = array()){
      if(empty($datos['nombre'])||
         empty($datos['id_region'])){
         return false;
      }
      $this->db->insert('provincia', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_provincia($datos = array()){
      if(empty($datos['nombre'])||
         empty($datos['id'])||
         empty($datos['id_region'])){
         return FALSE;
      }
      $this->db->where('id', $datos['id']);
      $this->db->update('provincia', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_provincia($id){
      $this->db->where('id', $id);
      $this->db->delete('provincia');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }


}

/* End of file  */
/* Location: ./application/models/ */