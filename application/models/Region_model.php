<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Region_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	function existe_region($nombre){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $query = $this->db->get('region');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
	}

   function obtener_lista_regiones(){
      $this->db->order_by('id', 'ASC');
      $query = $this->db->get('region');
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_region($id = FALSE){
        if ($id === FALSE)
        {
                $query = $this->db->get('region');
                return $query->result_array();
        }

        $query = $this->db->get_where('region', array('id' => $id));
        return $query->row_array();
   }
   
   function inserta_region($datos = array()){
      if(empty($datos['nombre'])){
         return false;
      }
      $this->db->insert('region', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_region($datos = array()){
      if(empty($datos['nombre'])||empty($datos['id'])){
         return FALSE;
      }
      $this->db->where('id', $datos['id']);
      $this->db->update('region', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_region($id){
      $this->db->where('id', $id);
      $this->db->delete('region');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }


}

/* End of file  */
/* Location: ./application/models/ */