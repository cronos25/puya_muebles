<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oferta_especial_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function obtener_lista_oferta_especial(){
      $this->db->select("*");
      $this->db->select('(SELECT nombre FROM productos WHERE productos.id=oferta_especial.id_producto) as producto');

      $query = $this->db->get('oferta_especial');
      return $query->result_array();
   	}

   	function oferta_especial_actual(){
   	  $this->db->select("*");
      $this->db->join('productos as p','p.id=oferta_especial.id_producto','left');
      $this->db->select('(SELECT nombre FROM categorias WHERE categorias.id=p.categoria_id) as categoria');
      $this->db->select('(SELECT nombre FROM marca WHERE marca.id=p.marca_id) as marca');
      $query = $this->db->get_where('oferta_especial',array('estado'=>0));
      return $query->row_array();
   	}

   	function obtener_oferta_especial($id){
   	  $this->db->select("*");
      $this->db->select('(SELECT nombre FROM productos WHERE productos.id=oferta_especial.id_producto) as producto');
      $query = $this->db->get_where('oferta_especial',array('id'=>$id));
      return $query->row_array();
      }

	function inserta_oferta_especial($datos = array()){
      $this->db->insert('oferta_especial', $datos);
       if($this->db->affected_rows()>0){
         $registro = $this->db->insert_id();
	  	     $this->db->update('oferta_especial', array('estado'=>1));
           $this->db->where('id',$registro );
   	  	  $this->db->update('oferta_especial', array('estado'=>0));
        return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function cambio_estado_oferta($datos = array()){
      $this->db->update('oferta_especial', array("estado"=>1));
      $this->db->where('id', $datos['id']);
      $this->db->update('oferta_especial', array("estado"=>$datos['estado']));

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function edita_oferta_especial($datos = array()){
      if(empty($datos['id'])){
         return FALSE;
      }

      $this->db->where('id', $datos['id']);
      $this->db->update('oferta_especial', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function elimina_oferta_especial($id){
      $this->db->where('id', $id);
      $this->db->delete('oferta_especial');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}

/* End of file oferta_especial.php */
/* Location: ./application/models/oferta_especial.php */
 ?>