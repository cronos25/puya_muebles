<?php 
class Stack_model extends CI_Model {

   function __construct(){
      $this->load->database();
   }
   
   function obtener_lista_stacks(){
      $query = $this->db->get_where('productos',array('is_stack' => 1));
      return $query->result_array();
   }
   
   //ejemplo de llamado a todas los datos de una tabla, sin limite, limitando la id si es recibida
    function get_stack($id = FALSE){

        $query = $this->db->get_where('productos', array('id' => $id));
        return $query->row_array();
   }

   function actual_imagen($id){
      $this->db->select('foto_producto');
      $query = $this->db->get_where('productos', array('id' => $id));
      return $query->row_array();
   }

   function existe_stack($nombre){
      $this->db->select('nombre');
      $this->db->where('nombre', $nombre); 
      $this->db->where('is_stack', 1); 
      $query = $this->db->get('productos');
      if ($query->num_rows() > 0){
         return true;
      }
      return false;
   }
   
   function inserta_stack($datos = array()){
      $datos['is_stack'] = 1;
      $this->db->insert('productos', $datos);
       if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
      //return $this->db->insert_id();
   }

   function edita_stack($datos = array()){
      $this->db->where('id', $datos['id']);
      $this->db->update('productos', $datos);

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }

   function elimina_stack($id){
      $this->db->where('id', $id);
      $this->db->delete('productos');

      if($this->db->affected_rows()>0){
         return true;
      }else{
         return false;
      }
   }
}