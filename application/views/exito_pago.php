<div class="container mensaje">
	<div class="row">
    <img src="<?= base_url('res/')?>ok.png" width="200" class="img-resposive center-block"/> 
    <h4 class="verde">Su compra fue realizada con exito!</h4>        
		<div class="row">                           
			<p>La orden de pago numero <b><?= $data_flow['orden_compra']; ?>
			<?php if ($data_carro['retiro_tienda']==0): ?>
				</b> ha sido procesada con exito, los productos seran despachados a la dirección <b><?= $data_carro['direccion_carro'];?>,<?= $region_envio['nombre'];?>,<?= $provincia_envio['nombre'];?>,<?= $comuna_envio['nombre'];?> </b> ingresada anteriormente </p>
				<p>se le comunicara al mail <b><?= $data_carro['email_carro'];?></b> cuando los productos sean despachados. En el mail incluira el numero de seguimiento de dicho despacho.</p>
			<?php else: ?>
				</b> ha sido procesada con exito
				<p>se le comunicara al mail <b><?= $data_flow['pagador'];?></b> cuando los productos esten disponibles para su retiro.</p>
			<?php endif ?>
    		<a href="<?= base_url('cliente/?vista=pedidos')?>" class="btn-primary btn">IR AL PERFIL</a>
        </div>
    </div>
</div>