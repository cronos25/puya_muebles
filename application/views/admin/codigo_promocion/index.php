<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Codigos Promocionales</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-ticket"></i> Codigos Promocionales del sistema</h3> <em>- Lista de Codigos Promocionales</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/codigo_promocion/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Codigo</th>
								<th>Descuento</th>
								<th>Fecha Inicio</th>
								<th>Fecha Fin</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($dataCodigos as $codigo_item): ?>
								<tr>
									<td class="col-md-1"><a href="#"><?= $codigo_item['id']; ?></a></td>
									<td class="col-md-2"><?= $codigo_item['codigo']; ?></td>
									<td class="col-md-2"><?= $codigo_item['descuento']; ?> %</td>
									<td class="col-md-2"><?= date('d-m-Y',strtotime($codigo_item['fecha_inicio']));?></td>
									<td class="col-md-2"><?= date('d-m-Y',strtotime($codigo_item['fecha_fin'])); ?></td>
									<td class="col-md-3">
										<a href="<?= base_url()?>admin/codigo_promocion/edit/<?= $codigo_item['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-val="<?= $codigo_item['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/codigo_promocion.js"></script>