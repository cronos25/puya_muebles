<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Ofertas Especiales</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-ticket"></i> Ofertas Especiales del sistema</h3> <em>- Lista de Ofertas Especiales</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/oferta_especial/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Producto</th>
								<th>Descuento</th>
								<th>Estado</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($dataOfertas as $ofertaItem): ?>
								<tr>
									<td class="col-md-1"><a href="#"><?= $ofertaItem['id']; ?></a></td>
									<td class="col-md-2"><?= $ofertaItem['producto']; ?> </td>
									<td class="col-md-1"><?= $ofertaItem['descuento']; ?> %</td>
									<td class="col-md-2">
										<input type="checkbox" <?=($ofertaItem['estado']==0)?'checked':''?> class="switch-demo estadoOferta" data-item="<?= $ofertaItem['id']; ?>" data-on-label="Activa" data-off-label="Inactiva">
									</td>
									<td class="col-md-2">
										<a href="<?= base_url()?>admin/oferta_especial/edit/<?= $ofertaItem['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-val="<?= $ofertaItem['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/oferta_especial.js"></script>