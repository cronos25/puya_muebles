<div class="inner-page">
		
	<div class="logo logo_login"><a href="<?= site_url('admin');?>"><img src="<?=base_url();?>assets/img/logo/logo.png" alt="PuyaMuebles"></a></div>

	<div class="login-box center-block">
		<form id="form_login" class="form-horizontal" role="form" method="POST">
			<p class="title">Nombre de usuario</p>
			<div class="form-group">
				<label for="username" class="control-label sr-only">Username</label>
				<div class="col-sm-12">
					<div class="input-group">
						<input type="text" placeholder="username" name="username" class="form-control" required="required">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
					</div>
				</div>
			</div>
			<label for="password" class="control-label sr-only">Password</label>
			<div class="form-group">
				<div class="col-sm-12">
					<div class="input-group">
						<input type="password" name="password" placeholder="password" class="form-control" required="required">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					</div>
				</div>
			</div>
			<!--<div class="simple-checkbox">
				<input type="checkbox" id="checkbox1">
				<label for="checkbox1">Remember me next time</label>
			</div>-->
			<button type="submit" class="btn btn-custom-primary btn-lg btn-block btn-login"><i class="fa fa-arrow-circle-o-right"></i> Login</button>
		</form>
		
		<!--<div class="links">
			<p><a href="#">Forgot Username or Password?</a></p>
			<p><a href="#">Create New Account</a></p>
		</div>-->
	</div>
</div>
<script src="<?= base_url(); ?>assets/admin/js/interno/admin.js"></script>