<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/distribuidor">Distribuidores</a></li>
                  <li class="active">Editar distribuidor</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Editar</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_actualizar">
                      <input type="hidden" value="<?= $arrayDistribuidor['id'];?>" name="id_distribuidor" id="id_distribuidor">
                    <div class="form-group">
                      <label for="foto_imagen_distribuidor" class="col-sm-3 control-label">Foto Distribuidor </label>
                      <div class="col-sm-9"  id="content_imagen_distribuidor">
                        <button type="button" id="btnCortarDistribuidor" class="btn btn-success">Cortar</button>
                        <input type="file" id="uploadDistribuidor" >
                        <div id="contentFotoDistribuidor">
                          <img id="fotoDistribuidor" accept="image/*">
                        </div>
                        <img src="" id="resultFotoDistribuidor" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="uploadBannerHome" class="col-sm-3 control-label">Imagen Actual</label>
                      <div class="col-sm-6">
                        <img src="<?= $arrayDistribuidor['imagen'];?>" alt="" class="img-responsive">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label for="autorizado_distribuidor" class="col-sm-3 control-label">Autorizado </label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="autorizado_distribuidor" id="autorizado_distribuidor" class="form-control" required="required">
                            <option value="0" <?=($arrayDistribuidor['autorizado']==0)?'selected':''?>>Autorizado</option>
                            <option value="1" <?=($arrayDistribuidor['autorizado']==1)?'selected':''?>>No Autorizado</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="content_texto_boton">
                      <label for="nombre_distribuidor" class="col-sm-3 control-label">Nombre Distribuidor</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_distribuidor" maxlength="200" class="form-control" id="nombre_distribuidor" value="<?= $arrayDistribuidor['nombre'];?>" placeholder="Texto de boton" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="content_texto_boton">
                      <label for="link_distribuidor" class="col-sm-3 control-label">link Pagina distribuidor</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="link_distribuidor" maxlength="200" class="form-control" id="link_distribuidor" value="<?= $arrayDistribuidor['link'];?>" placeholder="Texto de boton">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion_distribuidor" class="col-sm-3 control-label">Descripción</label>
                      <div class="col-sm-9">
                        <textarea id="descripcion_distribuidor" name="descripcion_distribuidor" class="form-control jqte-test" rows="6" cols="30" ><?= $arrayDistribuidor['descripcion'];?></textarea>
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Distribuidor</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/distribuidor.js"></script>