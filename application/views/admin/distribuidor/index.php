<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Distribuidores</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-archive"></i> Distribuidores</h3> <em>- Lista de Distribuidores</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/distribuidor/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Imagen</th>
								<th>Nombre</th>
								<th>Descripcion</th>
								<th>Link</th>
								<th>Autorizado</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($lista_distribuidores as $distribuidorItem): ?>
								<tr>
									<td class="col-md-1"><a href="#"><?= $distribuidorItem['id']; ?></a></td>
									<td class="col-md-2"><img src="<?= $distribuidorItem['imagen']; ?>" alt="" class="img-responsive "></td>
									<td class="col-md-1">
										<?= $distribuidorItem['nombre']; ?>
									</td>
									<td class="col-md-2">
										<?= $distribuidorItem['descripcion']; ?>
									</td>
									<td class="col-md-2">
										<?= $distribuidorItem['link']; ?>
									</td>
									<td class="col-md-2">
										<input type="checkbox" <?=($distribuidorItem['autorizado']==0)?'checked':''?> class="switch-demo estadoAutorizado" data-item="<?= $distribuidorItem['id']; ?>" data-on-label="Autorizado" data-off-label="No Autorizado">
									</td>
									<td class="col-md-2">
										<a href="<?= base_url()?>admin/distribuidor/edit/<?= $distribuidorItem['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-val="<?= $distribuidorItem['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/distribuidor.js"></script>