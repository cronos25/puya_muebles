<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/color">Colores</a></li>
                  <li class="active">Agregar Color</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_agregar">
                    <div class="form-group">
                      <label for="nombre_color" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_color" class="form-control" id="nombre_color" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nombre_color" class="col-sm-3 control-label">Descripcion</label>
                      <div class="col-sm-9">
                        <textarea name="descripcion_color" id="descripcion_color" class="form-control jqte-test" rows="5" placeholder="Agrege una descripcion para este color (opcional)"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="hexa_color" class="col-sm-3 control-label">hexadecimal</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="hexa_color" class="form-control" id="hexa_color" placeholder="ej: #000000" required="required">
                          <span class="input-group-addon"><i class="fa fa-tint"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Crear Color</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/color.js"></script>