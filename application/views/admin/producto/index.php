<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Productos</li>
			</ul>
		</div>
	</div>
	<!-- main -->
	<div class="content">
			<div class="main-header">
				<h2>Productos Destacados</h2>
				<em>Si existen menos de 4 asignados, se usaran los ultimos productos para completar los destacados a mostrar</em>
			</div>

			<div class="main-content">

				<form id="asigna_destacado_prod">
					<div class="form-group">
                      <label for="categoria_producto" class="col-sm-3 control-label">Asignar producto como destacado</label>
                      <div class="col-sm-9">
                      <div class="input-group">
                  			<select name="id_producto_dest" id="id_producto_dest" class="select2" required="required">
	                            <option value="">Seleccione Producto</option>
	                            <?php foreach ($lista_productos as $prodDestacadoItem): ?>
	                              <option value="<?= $prodDestacadoItem['id']; ?>"><?= $prodDestacadoItem['sku']; ?> <?= $prodDestacadoItem['nombre']; ?></option>
	                            <?php endforeach; ?>
	                        </select>
		                    <span class="input-group-btn">
					        	<button type="button" class="btn btn-warning" id="btnAddDestacado">Agregar  Destacado</button>
					      	</span>
                        </div>
                        </div>
                    </div>
				</form>


					<div class="row list-group king-gallery">
					
					<?php foreach ($lista_destacados as $destacadoItem ): ?>
						<div class="item col-md-3 col-sm-6 text-center">
							<div class="thumbnail">
								<img class="list-group-image" src="<?= $destacadoItem['foto_producto'] ?>" alt="" />
								<div class="caption">
									<h3 class="inner list-group-item-heading"><?= $destacadoItem['nombre'] ?></h3>
									<ul class="list-unstyled">
										<li><strong>Categoria</strong> <em><?= $destacadoItem['categoria'] ?></em></li>
										<li><strong>Marca:</strong> <em><?= $destacadoItem['marca'] ?></em></li>
										<li><strong>Sku:</strong> <em><?= $destacadoItem['sku'] ?></em></li>
										<li><strong>Precio :</strong> <em>$<?= number_format(ceil($destacadoItem['precio']) ,0, "," ,"."); ?></em></li>
									</ul>
								</div>
							</div>

							<?= $destacadoItem['nombre'] ?>
						</div>
					<?php endforeach ?>
					</div>
			</div>
			<!-- end king gallery -->
	</div><!-- /main -->
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-cube"></i> Productos del sistema</h3> <em>- Lista de Productos</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/productos/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">				
					
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nombre</th>
								<th>Sku</th>
								
								<th>Stock</th>
								<th>Precio</th>
								<!--<th>Descuento</th>-->
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($lista_productos as $producto_item): ?>
								<tr>
									<td><a href="#"><?= $producto_item['id']; ?></a></td>
									<td><?= $producto_item['nombre']; ?></td>
									<td><?= $producto_item['sku']; ?></td>
									<td><?= $producto_item['stock']; ?></td>
									<td>$ <?= number_format(ceil($producto_item['precio']) ,0, "," ,"."); ?></td>
									<!--<td>%</td>-->
									<td>
										<a href="<?= base_url()?>admin/productos/edit/<?= $producto_item['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-loading-text="Eliminando" data-val="<?= $producto_item['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->
		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/productos.js"></script>