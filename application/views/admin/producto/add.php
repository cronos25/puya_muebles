<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/productos">Productos</a></li>
                  <li class="active">Agregar Producto</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_agregar">
                    <div class="form-group">
                      <label for="nombre_producto" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_producto" class="form-control" id="nombre_producto" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion_producto" class="col-sm-3 control-label">Descripción</label>
                      <div class="col-sm-9">
                        <textarea id="descripcion_producto" name="descripcion_producto" class="form-control jqte-test" rows="6" cols="30" ></textarea>
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div>                   
                    <div class="form-group">
                      <label for="precio_producto" class="col-sm-3 control-label">Precio (precio de las medidas S)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" name="precio_producto" min="0" class="form-control" id="precio_producto" value="0" placeholder="Precio" required="required">
                          <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descuento_producto" class="col-sm-3 control-label">Descuento</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" min="0" max="100" name="descuento_producto" class="form-control" id="descuento_producto" value="0" placeholder="10">
                          <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="stock_producto" class="col-sm-3 control-label">Stock</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" min="0" name="stock_producto" class="form-control" id="stock_producto" value="0" placeholder="1" required="required">
                          <span class="input-group-addon"><i class="fa fa-toggle-up"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="medida_s_alto_producto" class="col-sm-3 control-label">Medidas S (alto, ancho, largo)</label>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_s_alto_producto" class="form-control" id="medida_s_alto_producto" placeholder="alto" required="required">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_s_ancho_producto" class="form-control" id="medida_s_ancho_producto" placeholder="ancho" required="required">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_s_largo_producto" class="form-control" id="medida_s_largo_producto" placeholder="largo" required="required">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="medida_m_alto_producto" class="col-sm-3 control-label">Medidas M  (alto, ancho, largo)</label>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_m_alto_producto" class="form-control" id="medida_m_alto_producto" placeholder="alto">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_m_ancho_producto" class="form-control" id="medida_m_ancho_producto" placeholder="ancho">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_m_largo_producto" class="form-control" id="medida_m_largo_producto" placeholder="largo">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="preciom_producto" class="col-sm-3 control-label">Precio medidas M</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" name="preciom_producto" min="0" class="form-control" id="preciom_producto" value="0" placeholder="Precio">
                          <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="medida_l_alto_producto" class="col-sm-3 control-label">Medidas L  (alto, ancho, largo)</label>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_l_alto_producto" class="form-control" id="medida_l_alto_producto" placeholder="alto">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_l_ancho_producto" class="form-control" id="medida_l_ancho_producto" placeholder="ancho">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_l_largo_producto" class="form-control" id="medida_l_largo_producto" placeholder="largo">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="preciol_producto" class="col-sm-3 control-label">Precio medidas L</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" name="preciol_producto" min="0" class="form-control" id="preciol_producto" value="0" placeholder="Precio">
                          <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="medida_xl_alto_producto" class="col-sm-3 control-label">Medidas XL (alto, ancho, largo)</label>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_xl_alto_producto" class="form-control" id="medida_xl_alto_producto" placeholder="alto">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_xl_ancho_producto" class="form-control" id="medida_xl_ancho_producto" placeholder="ancho">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="number" min="0" name="medida_xl_largo_producto" class="form-control" id="medida_xl_largo_producto" placeholder="largo">
                          <span class="input-group-addon">CM</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="precioxl_producto" class="col-sm-3 control-label">Precio medidas XL</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" name="precioxl_producto" min="0" class="form-control" id="precioxl_producto" value="0" placeholder="Precio">
                          <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="categoria_producto" class="col-sm-3 control-label">Categoria</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="categoria_producto" id="categoria_producto" class="select2" required="required">
                            <option value="">Seleccione Categoria</option>
                            <?php foreach ($lista_categorias as $categoria_item): ?>
                              <option value="<?= $categoria_item['id']; ?>"><?= $categoria_item['nombre']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-th"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="sub_categoria_producto" class="col-sm-3 control-label">Sub Categoria (opcional)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="sub_categoria_producto" id="sub_categoria_producto" class="select2">
                            <option value="">Seleccione Sub Categoria</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-th"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="marca_producto" class="col-sm-3 control-label">Marca</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="marca_producto" id="marca_producto" class="select2" required="required">
                            <option value="">Seleccione Marca</option>
                            <?php foreach ($lista_marcas as $marca_item): ?>
                              <option value="<?= $marca_item['id']; ?>"><?= $marca_item['nombre']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="color_producto" class="col-sm-3 control-label">Color</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                        <select id="color_producto" name="color_producto[]" class="multiselect" multiple="multiple">
                          <?php foreach ($lista_colores as $color_item): ?>
                              <option value="<?= $color_item['id']; ?>"><?= $color_item['nombre']; ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="codigo_producto" class="col-sm-3 control-label">Sku</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" data-val="SKU-" name="codigo_producto" class="form-control" id="codigo_producto" placeholder="Sku" value="SKU-" readonly="readonly">
                          <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="foto_producto" class="col-sm-3 control-label">Foto Producto </label>
                      <div class="col-sm-9"  id="content_foto_producto">
                        <button type="button" id="btnCortarProductos" class="btn btn-success">Cortar</button>

                        <input type="file" name="image_producto" id="uploadProducto" required="required">
                        <div id="contentFotoProducto">
                          <img id="fotoProducto" accept="image/*">
                        </div>
                        <img src="" id="resultFotoProducto" alt="">
                      </div>
                      
                    </div>
                    <div class="form-group">
                      <label for="foto_etiqueta_producto" class="col-sm-3 control-label">Foto Etiqueta </label>
                        <div class="col-sm-9 upload-demo" id="content_foto_etiqueta">
                          <button type="button" id="btnCortarEtiquetas" class="btn btn-success">Cortar</button>

                          <input type="file" name="image_etiqueta" id="uploadEtiqueta">
                          <div id="contentFotoEtiquetas">
                            <img id="fotoEtiquetas" accept="image/*">
                          </div>
                          <img src="" id="resultFotoEtiqueta" alt="">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="foto_home_producto" class="col-sm-3 control-label">Foto Secundaria </label>

                      <div class="col-sm-9 upload-demo" id="content_foto_home">
                          <button type="button" id="btnCortarHome" class="btn btn-success">Cortar</button>

                          <input type="file" name="image_home" id="uploadHome">
                          <div id="contentFotoHome">
                            <img id="fotoHome" accept="image/*">
                          </div>
                          <img src="" id="resultFotoHome" alt="">

                      </div>
                    </div>
                    <div class="form-group">
                      <label for="regalo_producto" class="col-sm-3 control-label">Regalo</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="regalo_producto" class="form-control" id="regalo_producto" placeholder="Regalos">
                          <span class="input-group-addon"><i class="fa fa-gift"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="envio_gratis_producto" class="col-sm-3 control-label">Envio Gratis</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="envio_gratis_producto" id="envio_gratis_producto" class=" form-control">
                            <option value="1">No</option>
                            <option value="0">Si</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="tipo_entrega_producto" class="col-sm-3 control-label">Tipo entrega</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="tipo_entrega_producto" id="tipo_entrega_producto" class=" form-control">
                            <option value="2">ambas (envio + local)</option>
                            <option value="1">envio</option>
                            <option value="0">local</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Crear Producto</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/productos.js"></script>
