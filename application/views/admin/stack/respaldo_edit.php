<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/stack">Stacks</a></li>
                  <li class="active">Editar Stack</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Editar</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" method="POST" id="form_actualizar">
                  <input type="hidden" value="<?= $arrayStack['id'];?>" name="id_stack" id="id_stack">
                    <div class="form-group">
                      <label for="nombre_stack" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_stack" class="form-control" id="nombre_stack" placeholder="Nombre" value="<?=$arrayStack['nombre'];?>">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="precio_stack" class="col-sm-3 control-label">Precio</label>
                      <div class="col-sm-9">
                        <input type="text" name="precio_stack" min="0" class="form-control" id="precio_stack" placeholder="ingrese precio del stack" value="<?=$arrayStack['precio'];?>" required="required">
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label for="descripcion_stack" class="col-sm-3 control-label">Descripción</label>
                      <div class="col-sm-9">
                        <textarea id="descripcion_stack" name="descripcion_stack" class="form-control jqte-test" rows="6" cols="30" ><?=$arrayStack['descripcion'];?></textarea>
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div>                    
                    <div class="form-group">
                      <label for="foto_stack" class="col-sm-3 control-label">Foto Stack </label>
                      <div class="col-sm-9 upload-demo ready"  id="content_foto_stack">
                        <input type="hidden" name="foto_stack_base64" id="foto_stack_base64" 
                        value="<?= $arrayStack['imagen'];?>">
                        <div class="actions">
                                <a class="btn file-btn">
                                    <input type="file" id="uploadStack" value="Choose a file" accept="image/*" />
                                </a>
                                <button type="button" id="upload-result-stack" class="upload-result">Cortar</button>
                            </div>
                            <div class="upload-msg">
                                Seleccione una Imagen para cortar
                            </div>
                            <div class="upload-demo-wrap">
                                <div id="fotoAddStack"></div>
                                <div id="fotoAddStackResult"><img src="<?= $arrayStack['imagen']; ?>" alt=""></div>
                            </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnEdit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Stack</button>
                        <a href="<?= base_url('admin/stack'); ?>" class="btn btn-danger"><i class="fa fa-close"></i> Cancelar Edición</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/stack.js"></script>
<script>
$(document).ready(function(){
  
  $("#fotoAddStack").hide();
  $("#upload-result-stack").hide();
  
  function popupResult(result,imgResult,divCorte,inputValor,botonEsconder) {
    var html;
    
    html = '<img src="' + result + '" />';
    
    $(divCorte).hide();
    $(botonEsconder).hide();
    $(imgResult).empty();
    $(imgResult).append(html);
    $(inputValor).val(result);
  }

    var $uploadCropStack;

    function readFile(input,uploadItem) {
      if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
              $('#upload-result-stack').show();
              $('#content_foto_stack').addClass('ready');
              $('#fotoAddStack').show();
              $('#fotoAddStackResult').empty();

              uploadItem.croppie('bind', {
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
              
            }
                          
            reader.readAsDataURL(input.files[0]);
          }
          else {
            $('#upload-result-stack').show();
            $('#content_foto_stack').removeClass('ready');
            $('#fotoAddStack').show();
            $('#fotoAddStackResult').empty();
        
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }


    $uploadCropStack = $('#fotoAddStack').croppie({
      viewport: {
        width: 450,
        height: 250,
        type: 'square'
      },
      enableExif: true
    });

    $('#uploadStack').on('change', function () { readFile(this,$uploadCropStack); });

    $('#upload-result-stack').on('click', function (ev) {
      $uploadCropStack.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function (resp) {
        //$('#fotoAddStackResult').attr('src', resp);
        popupResult(resp,
          '#fotoAddStackResult',
          '#fotoAddStack',
          '#foto_stack_base64',
          "#upload-result-stack");
      });
    });
  });
</script>



public function editDataStack($user = null){
    if ($this->input->is_ajax_request()) {
      //upload file
      $url_final = "";
          $config['upload_path'] = './assets/uploads/stacks/';
          $config['allowed_types'] = '*';
          $config['max_filename'] = '200';
          $config['encrypt_name'] = TRUE;
          $config['max_size'] = '2048'; //2 MB
      
          if(!file_exists("./assets/uploads/stacks/")){
            if(!mkdir("./assets/uploads/stacks", 0777, true)) {
              die('Fallo al crear las carpetas...');
            }
          }

          if (isset($_FILES['croppedImage']['name'])) {
              if (0 < $_FILES['croppedImage']['error']) {
                  echo 'Error during file upload' . $_FILES['croppedImage']['error'];
              } else {
                  if (file_exists('./assets/uploads/category/' . $_FILES['croppedImage']['name'])) {
                      echo 'File already exists : assets/uploads/category/' . $_FILES['croppedImage']['name'];           } else {
                      $this->upload->initialize($config);
                      if (!$this->upload->do_upload('croppedImage')){
                          echo $this->upload->display_errors();
                      } else {
                        echo "imagen subida con exito";
                        $file_data = $this->upload->data();
                $url_final = base_url().'assets/uploads/stacks/'.$file_data['file_name'];
                      }
                  }
              }
          }

      //upload file
      $arrayStack = array('id'=>$this->input->post("id_stack"),
              'nombre'=>$this->input->post("nombre_stack"),
              'descripcion'=>$this->input->post("descripcion_stack")
              'precio'=>$this->input->post("precio_stack"));
      if ($url_final != ""){
        $actual_imagen = str_replace(base_url(), './', $this->Stack_model->actual_imagen($arrayStack['id']));
        unlink($actual_imagen['imagen']);
        $arrayStack['imagen']=$url_final;
      }

      

      if($this->Stack_model->edita_stack($arrayStack)){
        echo "Editado con éxito";
      }else{
        echo "Error al editar";
      }
    }else{
      show_404();
    }
  }