<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/stack">Stacks</a></li>
                  <li class="active">Editar Stack</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Editar</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_actualizar">
                  <input type="hidden" value="<?= $arrayStack['id'];?>" name="id_stack" id="id_stack">
                    <div class="form-group">
                      <label for="nombre_stack" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_stack" class="form-control" id="nombre_stack" placeholder="Nombre" value="<?= $arrayStack['nombre'];?>" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="precio_stack" class="col-sm-3 control-label">Precio</label>
                      <div class="col-sm-9">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                        
                        <input type="text" name="precio_stack" min="0" class="form-control" id="precio_stack" placeholder="ingrese precio del stack" value="<?= $arrayStack['precio'];?>" required="required">
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label for="descuento_stack" class="col-sm-3 control-label">Descuento</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                        <input type="text" name="descuento_stack" min="0" class="form-control" id="descuento_stack" placeholder="10" value="<?= $arrayStack['descuento'];?>" required="required">
                        <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="stock_stack" class="col-sm-3 control-label">Stock</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" min="0" name="stock_stack" class="form-control" id="stock_stack" value="<?= $arrayStack['stock'];?>" placeholder="1" required="required">
                          <span class="input-group-addon"><i class="fa fa-toggle-up"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion_stack" class="col-sm-3 control-label">Descripción</label>
                      <div class="col-sm-9">
                        <textarea id="descripcion_stack" name="descripcion_stack" class="form-control jqte-test" rows="6" cols="30" ><?= $arrayStack['descripcion'];?></textarea>
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="foto_stack" class="col-sm-3 control-label">Foto Stack </label>
                      <div class="col-sm-9" id="content_foto_stack">
                        <button type="button" id="btnCortarStacks" class="btn btn-success">Cortar</button>

                        <input type="file" name="image_stack" id="uploadStack">
                        <div id="contentFotoStack">
                          <img id="fotoStack" accept="image/*">
                        </div>
                        <img src="" id="resultFotoStack" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="foto_stack" class="col-sm-3 control-label">Foto Producto Actual</label>
                      <div class="col-sm-6">
                        <img src="<?= $arrayStack['foto_producto']; ?>" alt="" class="img-responsive">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="categoria_stack" class="col-sm-3 control-label">Categoria</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="categoria_stack" id="categoria_stack" class="select2" required="required">
                            <option value="">Seleccione Categoria</option>
                            <?php foreach ($lista_categorias as $categoria_item): ?>
                              <option <?php if($arrayStack['categoria_id']==$categoria_item['id']){ echo "selected";}?> value="<?= $categoria_item['id']; ?>"><?= $categoria_item['nombre']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="sub_categoria_stack" class="col-sm-3 control-label">Sub Categoria (opcional)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="sub_categoria_stack" id="sub_categoria_stack" class="select2">
                            <option value="">Seleccione Sub Categoria</option>
                            <?php foreach ($lista_sub_categorias as $sub_categoria_item): ?>
                              <option <?php if($arrayStack['sub_categoria_id']==$sub_categoria_item['id']){ echo "selected";}?> value="<?= $sub_categoria_item['id']; ?>"><?= $sub_categoria_item['nombre']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="marca_stack" class="col-sm-3 control-label">Marca</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="marca_stack" id="marca_stack" class="select2" required="required">
                            <option value="">Seleccione Marca</option>
                            <?php foreach ($lista_marcas as $marca_item): ?>
                              <option <?php if($arrayStack['marca_id']==$marca_item['id']){ echo "selected";}?> value="<?= $marca_item['id']; ?>"><?= $marca_item['nombre']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="codigo_stack" class="col-sm-3 control-label">Sku</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" data-val="SKU-" name="codigo_stack" class="form-control" id="codigo_stack" placeholder="Sku" value="<?= $arrayStack['sku']; ?>" readonly="readonly">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="envio_gratis_stack" class="col-sm-3 control-label">Envio Gratis</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="envio_gratis_stack" id="envio_gratis_stack" class=" form-control" required="required">
                            <option value="">Seleccione una opcion</option>
                            <option <?php if($arrayStack['envio_gratis']==0){ echo "selected";}?>  value="0">Si</option>
                            <option <?php if($arrayStack['envio_gratis']==1){ echo "selected";}?>  value="1">No</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="tipo_entrega_stack" class="col-sm-3 control-label">Tipo entrega</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="tipo_entrega_stack" id="tipo_entrega_stack" class=" form-control" required="required">
                            <option value="">Seleccione una opcion</option>
                            <option <?php if($arrayStack['tipo_entrega']==0){ echo "selected";}?>  value="0">local</option>
                            <option <?php if($arrayStack['tipo_entrega']==1){ echo "selected";}?>  value="1">envio</option>
                            <option <?php if($arrayStack['tipo_entrega']==2){ echo "selected";}?>  value="2">ambas</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnEdit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Stack</button>
                        <a href="<?= base_url('admin/stack'); ?>" class="btn btn-danger"><i class="fa fa-close"></i> Cancelar Edición</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/stack.js"></script>