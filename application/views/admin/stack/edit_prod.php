<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/stack">Stacks</a></li>
                  <li class="active">Editar Stacks</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-inline">
                    <div class="form-group">
                      <label class=" control-label">Nombre</label>
                        <div class="input-group">
                          <input type="text" value="<?= $arrayStack['nombre'];?>" class="form-control" disabled="disabled">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div> 
                    <div class="form-group">
                      <label  class="control-label">Precio Stack</label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                          <input type="text" value="<?= $arrayStack['precio'];?>" class="form-control" disabled="disabled">
                          <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        </div>
                    </div> 
                  </form>
                  <form class="form-inline" role="form" id="form_edit_producto_stack">
                    <input type="hidden" value="<?= $arrayStack['id'];?>" name="id_stack" id="id_stack">
                      <div class="form-group">
                          <label for="id_producto" class="control-label">Agregar Producto al Stack</label>
                          <div class="input-group">
                            <select name="id_producto" id="id_producto" class="select2" required="required">
                              <option value="">Seleccione Producto</option>
                              <?php foreach ($lista_productos as $producto_item): ?>
                                <option value="<?= $producto_item['id']; ?>"><?= $producto_item['nombre']; ?></option>
                              <?php endforeach; ?>
                            </select>
                            <span class="input-group-btn">
                                <button type="button" id="btnAddProdStack" class="btn btn-primary"><i class="fa fa-check-circle"></i> Confirmar</button>
                              </span>
                            </div>
                        </div>
                    </form>
                  </div>
                 <div class="widget-content"> 
                  <table class="table table-sorting">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $totalStack = 0;
                      foreach ($lista_productos_stack as $stack_item): 
                        $totalStack= $totalStack+$stack_item['precio_producto'];?>
                        <tr>
                          <td><?= $stack_item['nombre']; ?></td>
                          <td>$ <?= $stack_item['precio_producto']; ?></td>
                          <td><button type="button" data-val="<?= $stack_item['id']; ?>" class="btn btn-danger btnDeleteProdStack">Eliminar</button></td>
                        </tr>
                      <?php endforeach; ?>
                      <tr>
                        <th>Total</th>
                        <th>$ <?= $totalStack;?></th>
                        <th>
                          
                        </th>
                      </tr>
                      <tr>
                        <th>
                          <?php $precioComparado = $totalStack-$arrayStack['precio'];
                          if ($precioComparado==0): ?>
                            <div class="alert alert-warning" role="alert">El Stack Tiene el mismo precio que los productos por separado</div>
                          <?php elseif($precioComparado>0): ?>
                            <div class="alert alert-success" role="alert">El Stack cuesta $<?= $precioComparado; ?> (<?=number_format( ($precioComparado*100)/ $arrayStack['precio'], 1 , "," ,"." )?> % ) menos que el total de los productos</div>
                          <?php else: ?>
                            <div class="alert alert-danger" role="alert">El Stack cuesta $<?= $precioComparado*-1; ?> (<?= number_format ( (($precioComparado*-1)*100)/$arrayStack['precio'], 1 , "," ,"." )?> % ) mas que el total de los productos, agregue mas productos o modifique el precio del stack</div>
                          <?php endif ?>
                        </th>
                        <th></th>
                        <th></th>
                      </tr>
                    </tbody>
                  </table>

                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/stack.js"></script>