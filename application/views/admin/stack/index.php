<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Stacks</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-cubes"></i> Stacks del sistema</h3> <em>- Lista de Stacks</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/stack/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nombre</th>
								<th>Descripcion</th>
								<th>Imagen</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($lista_stacks as $stack_item): ?>
								<tr>
									<td><a href="#"><?= $stack_item['id']; ?></a></td>
									<td><?= $stack_item['nombre']; ?></td>
									<td><?= $stack_item['descripcion']; ?></td>
									<td class="col-md-3">
										<img src="<?= $stack_item['foto_producto']; ?>" alt="" class="img-responsive ">
									</td>
									<td>
										<a href="<?= base_url()?>admin/stack/edit/<?= $stack_item['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-val="<?= $stack_item['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
										<a href="<?= base_url()?>admin/stack/edit_prod/<?= $stack_item['id']; ?>" class="btn btn-success">Productos</a>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/stack.js"></script>