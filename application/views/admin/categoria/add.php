<div class="col-md-10 content-wrapper">
      <div class="row">
        <div class="col-md-4 ">
          <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
            <li class="active"><a href="<?= base_url(); ?>admin/categoria">Categorias</a></li>
            <li class="active">Agregar Categorias</li>
          </ul>
        </div>
      </div>
      
      <!-- main -->
      <div class="content">
        <div class="widget">
          <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
          <div class="widget-content">
            <form class="form-horizontal" role="form" id="form_agregar">
              <div class="form-group">
                <label for="nombre_categoria" class="col-sm-3 control-label">Nombre</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="text" name="nombre_categoria" class="form-control" id="nombre_categoria" placeholder="Nombre" required="required">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="descripcion_categoria" class="col-sm-3 control-label">Descripción</label>
                <div class="col-sm-9">
                  <textarea id="descripcion_categoria" name="descripcion_categoria" class="form-control jqte-test" rows="6" cols="30" ></textarea>
                  <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                </div>
              </div>                    
              <div class="form-group">
                <label for="tipo_categoria" class="col-sm-3 control-label">Tipo de Categoria</label>
                <div class="col-sm-9">
                  <select name="tipo_categoria" id="tipo_categoria" class="form-control">
                    <option value="0">Producto</option>
                    <option value="1">Servicio</option>
                  </select>
                </div>
              </div> 
              <div class="form-group">
                <label for="foto_producto" class="col-sm-3 control-label">Foto Categoria </label>
                <div class="col-sm-9"  id="content_imagen_categoria">
                  <button type="button" id="btnCortarCategorias" class="btn btn-success">Cortar</button>

                  <input type="file" id="uploadCategoria" required="required">
                  <div id="contentFotoCategoria">
                    <img id="fotoCategoria" accept="image/*">
                  </div>
                  <img src="" id="resultFotoCategoria" alt="" class="img-responsive">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9">
                  <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Subir Categoria</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/categoria.js"></script>