<div class="col-md-10 content-wrapper">
      <div class="row">
        <div class="col-md-4 ">
          <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
            <li class="active"><a href="<?= base_url(); ?>admin/categoria">Categorias</a></li>
            <li class="active">Contenido Servicio</li>
          </ul>
        </div>
      </div>
      
      <!-- main -->
      <div class="content">
        <div class="widget">
          <div class="widget-header"><h3><i class="fa fa-edit"></i> Datos Contacto Servicio</h3></div>
          <div class="widget-content">
            <form class="form-horizontal" role="form" id="form_agregar_datos_servicio">
              <input type="hidden" value="<?= $arrayCategoria['id'] ?>" name="id_categoria">
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Nombre Servicio</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="text" class="form-control" disabled="disabled" value="<?= $arrayCategoria['nombre'] ?>">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="email_servicio_categoria" class="col-sm-3 control-label">Email (sin este no se mostrara el formulario de contacto)</label>
                <div class="col-sm-9">
                  <input type="text" name="email_servicio_categoria" class="form-control" id="email_servicio_categoria" value="<?=$datosServicio['email']?>" placeholder="Email">
                  <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                </div>
              </div>
              <div class="form-group">
                <label for="mail2_servicio_categoria" class="col-sm-3 control-label">Email 2 (opcional)</label>
                <div class="col-sm-9">
                  <input type="text" name="mail2_servicio_categoria" value="<?=$datosServicio['mail2']?>" class="form-control" id="mail2_servicio_categoria" placeholder="Email">
                  <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                </div>
              </div>
              <div class="form-group">
                <label for="mail3_servicio_categoria" class="col-sm-3 control-label">Email 3 (opcional)</label>
                <div class="col-sm-9">
                  <input type="text" name="mail3_servicio_categoria" value="<?=$datosServicio['mail3']?>" class="form-control" id="mail3_servicio_categoria" placeholder="Email">
                  <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                </div>
              </div>
              <div class="form-group">
                <label for="telefono_servicio_categoria" class="col-sm-3 control-label">Telefono servicio</label>
                <div class="col-sm-9">
                  <input type="text" name="telefono_servicio_categoria" class="form-control" id="telefono_servicio_categoria" placeholder="+56 9 1234 5678" value="<?=$datosServicio['telefono']?>" >
                  <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9">
                  <button type="button" id="btnFijaDatoServ" class="btn btn-primary"><i class="fa fa-check-circle"></i> Fijar Datos Servicio</button>
                </div>
              </div>
            </form>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header"><h3><i class="fa fa-edit"></i> Contenido del Servicio</h3></div>
          <div class="widget-content">
            <form class="form-horizontal" role="form" id="form_agregar_parrafo_servicio">
              <input type="hidden" value="<?= $arrayCategoria['id'] ?>" name="categoria_parrafo">
              <div class="form-group">
                  <div class="col-sm-3 checkbox">
                    <label>
                      <input checked="checked" class="col-md-3" type="radio" name="tipo_parrafo_servicio" id="inlineRadio1" value="0">
                      <img class="img img-responsive col-md-9" src="<?= base_url('assets/img/tipo1.jpg') ?>" alt="">
                      </label>
                  </div>
                
                  <div class="col-sm-3 checkbox">
                    <label>
                  <input class="col-md-3" type="radio" name="tipo_parrafo_servicio" id="inlineRadio2" value="1"> 
                  <img class="img img-responsive col-md-9" src="<?= base_url('assets/img/tipo2.jpg') ?>" alt="texto a la izquierda">
                      </label>
                  </div>
                
                  <div class="col-sm-3 checkbox">
                    <label>
                  <input class="col-md-3" type="radio" name="tipo_parrafo_servicio" id="inlineRadio3" value="2">
                  <img class="img img-responsive col-md-9" src="<?= base_url('assets/img/tipo4.jpg') ?>" alt="solo texto">
                  </div>
                      </label>
                
                  <div class="col-sm-3 checkbox">
                    <label>
                  <input class="col-md-3" type="radio" name="tipo_parrafo_servicio" id="inlineRadio3" value="3">
                  <img class="img img-responsive col-md-9" src="<?= base_url('assets/img/tipo3.jpg') ?>" alt="solo imagen">
                      </label>
                  </div>
              
              </div>
              <div class="form-group">
                <label for="mensaje_parrafo_servicio" class="col-sm-3 control-label">Texto</label>
                <div class="col-sm-9">
                  <textarea id="mensaje_parrafo_servicio" name="mensaje_parrafo_servicio" class="form-control jqte-test" rows="6" cols="30" ></textarea>
                  <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                </div>
              </div>                    
              <div class="form-group">
                <label for="numero_parrafo_servicio" class="col-sm-3 control-label">numero parrafo</label>
                <div class="col-sm-9">
                  <input type="number" min="1" name="numero_parrafo_servicio" class="form-control" id="numero_parrafo_servicio" value="1" placeholder="ej: 2">
                  <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                </div>
              </div> 
              <div class="form-group">
                <label for="foto_producto" class="col-sm-3 control-label">Imagen Parrafo </label>
                <div class="col-sm-9"  id="content_imagen_parrafo">
                  <button type="button" id="btnCortarParrafos" class="btn btn-success">Cortar</button>

                  <input type="file" id="uploadParrafo">
                  <div id="contentFotoParrafo">
                    <img id="fotoParrafo" accept="image/*">
                  </div>
                  <img src="" id="resultFotoParrafo" alt="" class="img-responsive">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9">
                  <button type="button" id="btnAddParrafo" class="btn btn-primary"><i class="fa fa-check-circle"></i> Agregar Parrafo al servicio</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      <!-- END WIDGET TICKET TABLE -->
        <!-- WIDGET TICKET TABLE -->
      <div class="widget widget-table">
        <div class="widget-header">
          <h3><i class="fa fa-th"></i> Parrafos</h3> <em>- Lista de Categorias</em>
          <div class="btn-group widget-header-toolbar">
            <a href="<?= base_url()?>admin/categoria/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
          </div>
          <div class="btn-group widget-header-toolbar">
            <a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
          </div>
        </div>
        <div class="widget-content">
          <table class="table table-sorting datatablecotizados">
            <thead>
              <tr>
                <th>Id</th>
                <th>Imagen</th>
                <th>Descripcion</th>
                <th>Tipo Parrafo</th>
                <th>Numero Parrafo</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($ParrafosServicio as $parrafoItem): ?>
                <tr>
                  <td class="col-md-1"><a href="#"><?= $parrafoItem['id']; ?></a></td>
                  <td class="col-md-2">
                    <img src="<?= $parrafoItem['imagen_parrafo']; ?>" alt="" class="img-responsive ">
                  </td>
                  <td class="col-md-3"><?= $parrafoItem['mensaje_parrafo']; ?></td>
                  <td class="col-md-2">
                    <?php 
                      switch ($parrafoItem['tipo_parrafo']) {
                        case 0:
                           echo "texto a la derecha con imagen a la izquierda";
                           break;
                        case 1:
                           echo "texto a la izquierda con imagen a la derecha";
                           break;
                        case 2:
                           echo "solo texto";
                           break;
                        case 3:
                           echo "solo imagen";
                           break;
                         default:
                           echo "no definido";
                           break;
                       } ?>
                  </td>
                  <td class="col-md-1"><?= $parrafoItem['numero_parrafo']; ?></td>
                  <td class="col-md-3">
                    <button type="button" data-val="<?= $parrafoItem['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
                  </td>
                </tr>

              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- END WIDGET TICKET TABLE -->
    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/servicio.js"></script>