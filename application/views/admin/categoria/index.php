<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Categorias</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-th"></i> Categorias del sistema</h3> <em>- Lista de Categorias</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/categoria/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatablecotizados">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nombre</th>
								<th>Descripcion</th>
								<th>Imagen</th>
								<th>Tipo Categoria</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($lista_categorias as $categoria_item): ?>
								<tr>
									<td class="col-md-1"><a href="#"><?= $categoria_item['id']; ?></a></td>
									<td class="col-md-2"><?= $categoria_item['nombre']; ?></td>
									<td class="col-md-3"><?= $categoria_item['descripcion']; ?></td>
									<td class="col-md-2">
										<img src="<?= $categoria_item['imagen']; ?>" alt="" class="img-responsive ">
									</td>
									<td class="col-md-1"><?= ($categoria_item['tipo_categoria']==0)?'Producto':'Servicio'; ?></td>
									<td class="col-md-3">
										<a href="<?= base_url()?>admin/categoria/edit/<?= $categoria_item['id']; ?>" class="btn btn-warning">Editar</a>
										<?php if ($categoria_item['tipo_categoria']==1): ?>
											<a href="<?= base_url()?>admin/categoria/servicio/<?= $categoria_item['id']; ?>" class="btn btn-success">Contenido Servicio</a>
										<?php endif ?>
										
										<button type="button" data-val="<?= $categoria_item['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/categoria.js"></script>