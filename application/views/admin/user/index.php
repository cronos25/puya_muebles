<div class="col-md-10 content-wrapper">
<div class="row">
	<div class="col-md-4 ">
		<ul class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
			<li class="active">Usuarios</li>
		</ul>
	</div>
</div>

<!-- main -->
<div class="content">
		
		<!-- WIDGET TICKET TABLE -->
		<div class="widget widget-table">
			<div class="widget-header">
				<h3><i class="fa fa-group"></i> Usuarios del sistema</h3> <em>- Lista de usuarios</em>
				<div class="btn-group widget-header-toolbar">
					<a href="<?= base_url()?>admin/user/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
				</div>
				<div class="btn-group widget-header-toolbar">
					<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
				</div>
			</div>
			<div class="widget-content">
				<table class="table table-sorting datatable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Email</th>
							<th>Telefono</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista_usuarios as $usuario_item): ?>
							<tr>
								<td><a href="#"><?= $usuario_item['id']; ?></a></td>
								<td><?= $usuario_item['nombre'].' '.$usuario_item['apellido']; ?></td>
								<td><?= $usuario_item['email']; ?></td>
								<td><?= $usuario_item['telefono']; ?></td>
								<td>
									<a href="<?= base_url()?>admin/user/edit/<?= $usuario_item['id']; ?>" class="btn btn-warning">Editar</a>
									<button type="button" data-val="<?= $usuario_item['id']; ?>" class="btnDelete btn btn-danger">Eliminar</button>
								</td>
							</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END WIDGET TICKET TABLE -->

	</div><!-- /main-content -->
</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/usuario.js"></script>