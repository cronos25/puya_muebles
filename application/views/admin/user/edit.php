<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/user">Usuarios</a></li>
                  <li class="active">Editar Usuario</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                <form class="form-horizontal" id="form_actualizar" role="form" method="POST">
                    <input type="hidden" value="<?= $arrayUsuario['id'];?>" name="id_usuario" id="id_usuario">
                    <div class="form-group">
                      <label for="nombre_usuario" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_usuario" class="form-control" id="nombre_usuario" value="<?= $arrayUsuario['nombre'];?>" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="apellido_usuario" class="col-sm-3 control-label">Apellido</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="apellido_usuario" class="form-control" id="apellido_usuario" value="<?= $arrayUsuario['apellido'];?>" placeholder="Apellido" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="telefono_usuario" class="col-sm-3 control-label">Telefono</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" class="form-control" name="telefono_usuario" id="telefono_usuario" value="<?= $arrayUsuario['telefono'];?>" placeholder="91234567" required="required">
                          <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="email_usuario" class="col-sm-3 control-label">Email</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="email" value="<?= $arrayUsuario['email'];?>" class="form-control" id="email_usuario" name="email_usuario" placeholder="Email" required="required">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="password_usuario" class="col-sm-3 control-label">Password</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="password" value="<?= $arrayUsuario['password'];?>" class="form-control" name="password_usuario" id="password_usuario" placeholder="Password" required="required">
                          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="submit" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Cuenta</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->

<script src="<?= base_url(); ?>assets/admin/js/interno/usuario.js"></script>