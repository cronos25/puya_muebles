<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/banner_home">Banner Home</a></li>
                  <li class="active">Agregar Banner Home</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_agregar">
                     <div class="form-group">
                      <label for="foto_imagen_banner_home" class="col-sm-3 control-label">Foto Banner </label>
                      <div class="col-sm-9"  id="content_imagen_banner_home">
                        <button type="button" id="btnCortarBannerHome" class="btn btn-success">Cortar</button>
                        <input type="file" id="uploadBannerHome" required="required">
                        <div id="contentFotoBannerHome">
                          <img id="fotoBannerHome" accept="image/*">
                        </div>
                        <img src="" id="resultFotoBannerHome" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="estado_banner" class="col-sm-3 control-label">Estado Banner</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="estado_banner" id="estado_banner" class="form-control" required="required">
                            <option value="0">activo</option>
                            <option value="1">inactivo</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="tipo_banner" class="col-sm-3 control-label">Tipo Banner</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="tipo_banner" id="tipo_banner" class="form-control" required="required">
                            <option value="0">producto</option>
                            <option value="1">informativo</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="content_producto">
                      <label for="producto_banner" class="col-sm-3 control-label">Seleccione Producto</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="producto_banner" id="producto_banner" class="form-control" required="required">
                            <option value="">Seleccione Producto</option>
                            <?php foreach ($lista_productos as $producto_item): ?>
                              <option value="<?= $producto_item['id']; ?>"><?= $producto_item['nombre']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="content_texto_boton">
                      <label for="texto_boton_banner_home" class="col-sm-3 control-label">Texto Boton</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="texto_boton_banner_home" maxlength="50" class="form-control" id="texto_boton_banner_home" placeholder="Texto de boton">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Subir Banner Home</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/banner_home.js"></script>