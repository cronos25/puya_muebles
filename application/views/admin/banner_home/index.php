<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Banners</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-file-photo-o"></i> Banners del sistema</h3> <em>- Lista de Banners</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/banner_home/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Imagen</th>
								<th>Tipo Banner</th>
								<th>Producto</th>
								<th>Texto Boton Banner</th>
								<th>estado</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($lista_banners as $bannerItem): ?>
								<tr>
									<td class="col-md-1"><a href="#"><?= $bannerItem['id']; ?></a></td>
									<td class="col-md-2"><img src="<?= $bannerItem['imagen']; ?>" alt="" class="img-responsive "></td>
									<td class="col-md-1">
										<?= ($bannerItem['tipo_banner']==0)?'Producto':'Informativo'?>
									</td>
									<td class="col-md-2">
										<?= $bannerItem['producto']; ?>
									</td>
									<td class="col-md-2">
										<?= $bannerItem['texto_btn_banner']; ?>
									</td>
									<td class="col-md-2">
										<input type="checkbox" <?=($bannerItem['estado']==0)?'checked':''?> class="switch-demo estadoBanner" data-item="<?= $bannerItem['id']; ?>" data-on-label="Activo" data-off-label="Inactivo">
									</td>
									<td class="col-md-2">
										<a href="<?= base_url()?>admin/banner_home/edit/<?= $bannerItem['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-val="<?= $bannerItem['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/banner_home.js"></script>