<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/banner_home">Banner Home</a></li>
                  <li class="active">Editar Banner Home</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Editar</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_actualizar">
                      <input type="hidden" value="<?= $arrayBanner['id'];?>" name="id_banner" id="id_banner">
                     <div class="form-group">
                      <label for="foto_imagen_banner_home" class="col-sm-3 control-label">Foto Banner </label>
                      <div class="col-sm-9"  id="content_imagen_banner_home">
                        <button type="button" id="btnCortarBannerHome" class="btn btn-success">Cortar</button>
                        <input type="file" id="uploadBannerHome">
                        <div id="contentFotoBannerHome">
                          <img id="fotoBannerHome" accept="image/*">
                        </div>
                        <img src="" id="resultFotoBannerHome" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="uploadBannerHome" class="col-sm-3 control-label">Imagen Actual</label>
                      <div class="col-sm-6">
                        <img src="<?= $arrayBanner['imagen'];?>" alt="" class="img-responsive">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label for="estado_banner" class="col-sm-3 control-label">Estado Banner</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="estado_banner" id="estado_banner" class="form-control" required="required">
                            <option value="0" <?=($arrayBanner['estado']==0)?'selected':''?>>activo</option>
                            <option value="1" <?=($arrayBanner['estado']==1)?'selected':''?>>inactivo</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="tipo_banner" class="col-sm-3 control-label">Tipo Banner</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="tipo_banner" id="tipo_banner" class="form-control" required="required">
                            <option value="0" <?=($arrayBanner['tipo_banner']==0)?'selected':''?>>producto</option>
                            <option value="1" <?=($arrayBanner['tipo_banner']==1)?'selected':''?>>informativo</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group <?=($arrayBanner['tipo_banner']==1)?'hide':''?>" id="content_producto">
                      <label for="producto_banner" class="col-sm-3 control-label">Seleccione Producto</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="producto_banner" id="producto_banner" class="form-control"  <?=($arrayBanner['tipo_banner']==1)?'required="required"':''?>>
                            <option value="">Seleccione Producto</option>
                            <?php foreach ($lista_productos as $producto_item): ?>
                              <option value="<?= $producto_item['id']; ?>" <?=($arrayBanner['producto_id']==$producto_item['id'])?'selected':''?>><?= $producto_item['nombre']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group <?=($arrayBanner['tipo_banner']==1)?'hide':''?>" id="content_texto_boton">
                      <label for="texto_boton_banner_home" class="col-sm-3 control-label">Texto Boton</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="texto_boton_banner_home" maxlength="50" class="form-control" id="texto_boton_banner_home" value="<?= $arrayBanner['texto_btn_banner'];?>" placeholder="Texto de boton">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                     
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Banner Home</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/banner_home.js"></script>