<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/provincia">Provincias</a></li>
                  <li class="active">Agregar Provincia</li>
                </ul>
              </div>
            </div>
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_agregar">
                    <div class="form-group">
                      <label for="nombre_provincia" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_provincia" class="form-control" id="nombre_provincia" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_region" class="col-sm-3 control-label">Region</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_region" id="id_region" class="form-control" required="required">
                            <option value="">Seleccione Region</option>
                            <?php foreach ($lista_regiones as $regionItem):?>
                              <option value="<?= $regionItem['id'];?>"><?= $regionItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <button type="submit" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Crear Provincia</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/provincia.js"></script>