<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/marca">Marcas</a></li>
                  <li class="active">Editar Marcas</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_actualizar">
                    <input type="hidden" value="<?= $arrayMarca['id'];?>" name="id_marca" id="id_marca" required="required">
                    <div class="form-group">
                      <label for="nombre_marca" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" value="<?= $arrayMarca['nombre'];?>" name="nombre_marca" class="form-control" id="nombre_marca" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion_marca" class="col-sm-3 control-label">Descripción</label>
                      <div class="col-sm-9">
                        <textarea id="descripcion_marca" name="descripcion_marca" class="form-control jqte-test" rows="6" cols="30"><?= $arrayMarca['descripcion'];?></textarea>
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div>                    
                    <div class="form-group">
                      <label for="foto_marca" class="col-sm-3 control-label">Imagen</label>
                      <div class="col-sm-9"  id="content_imagen_categoria">
                        <button type="button" id="btnCortarMarcas" class="btn btn-success">Cortar</button>

                        <input type="file" id="uploadMarca" accept="image/*" <?= (empty($arrayMarca['logo']))?'required="required"':'';?>>
                        <div id="contentFotoMarca">
                          <img id="fotoMarca" accept="image/*">
                        </div>
                        <img src="" id="resultFotoMarca" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Logo Actual</label>
                      <div class="col-sm-6">
                        <img src="<?= $arrayMarca['logo'];?>" alt="" class="img-responsive">
                      </div>
                    </div>       
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar marca</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/marca.js"></script>