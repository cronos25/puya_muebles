<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Marcas</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-tag"></i> Marcas del sistema</h3> <em>- Lista de Marcas</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/marca/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nombre</th>
								<th>Descripcion</th>
								<th>Logo</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($lista_marcas as $marca_item): ?>
								<tr>
									<td class="col-md-1"><a href="#"><?= $marca_item['id']; ?></a></td>
									<td class="col-md-2"><?= $marca_item['nombre']; ?></td>
									<td class="col-md-4"><?= $marca_item['descripcion']; ?></td>
									<td class="col-md-3">
										<img src="<?= $marca_item['logo']; ?>" alt="" class="img-responsive ">
									</td>
									<td class="col-md-2">
										<a href="<?= base_url()?>admin/marca/edit/<?= $marca_item['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-val="<?= $marca_item['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/marca.js"></script>