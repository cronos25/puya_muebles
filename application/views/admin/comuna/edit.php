<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/comuna">Comunas</a></li>
                  <li class="active">Editar Comuna</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                <form class="form-horizontal" id="form_actualizar" role="form" method="POST">
                    <input type="hidden" value="<?= $arrayComuna['id'];?>" name="id_comuna" id="id_comuna">
                    <div class="form-group">
                      <label for="nombre_comuna" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_comuna" class="form-control" id="nombre_comuna" value="<?= $arrayComuna['nombre'];?>" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_provincia" class="col-sm-3 control-label">Provincia</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_provincia" id="id_provincia" class="form-control" required="required">
                            <option value="">Seleccione Provincia</option>
                            <?php foreach ($lista_provincias as $provinciaItem):?>
                              <option value="<?= $provinciaItem['id'];?>" <?php if($arrayComuna['id_provincia']== $provinciaItem['id']) echo 'selected'?>><?= $provinciaItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Comuna</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->

<script src="<?= base_url(); ?>assets/admin/js/interno/comuna.js"></script>