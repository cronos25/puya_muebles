<div class="col-md-10 content-wrapper">
<div class="row">
	<div class="col-md-4 ">
		<ul class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
			<li class="active">Comunas</li>
		</ul>
	</div>
</div>

<!-- main -->
<div class="content">
		
		<!-- WIDGET TICKET TABLE -->
		<div class="widget widget-table">
			<div class="widget-header">
				<h3><i class="fa fa-map-marker"></i> Comunas del sistema</h3> <em>- Lista de comunas</em>
				<div class="btn-group widget-header-toolbar">
					<a href="<?= base_url()?>admin/comuna/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
				</div>
				<div class="btn-group widget-header-toolbar">
					<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
				</div>
			</div>
			<div class="widget-content">
				<table class="table table-sorting datatable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Provincia</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista_comunas as $comuna_item): ?>
							<tr>
								<td><a href="#"><?= $comuna_item['id']; ?></a></td>
								<td><?= $comuna_item['nombre']; ?></td>
								<!--se debe colocar el nombre-->
								<td><?= $comuna_item['provincia_nombre']; ?></td>
								<td>
									<a href="<?= base_url()?>admin/comuna/edit/<?= $comuna_item['id']; ?>" class="btn btn-warning">Editar</a>
									<button type="button" data-val="<?= $comuna_item['id']; ?>" class="btnDelete btn btn-danger">Eliminar</button>
								</td>
							</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END WIDGET TICKET TABLE -->

	</div><!-- /main-content -->
</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/comuna.js"></script>