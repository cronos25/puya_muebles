<div class="col-md-10 content-wrapper">
<div class="row">
	<div class="col-md-4 ">
		<ul class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
			<li class="active">Sub Categorias</li>
		</ul>
	</div>
</div>

<!-- main -->
<div class="content">
		
		<!-- WIDGET TICKET TABLE -->
		<div class="widget widget-table">
			<div class="widget-header">
				<h3><i class="fa fa-th"></i> Sub Categorias del sistema</h3> <em>- Lista de sub categorias</em>
				<div class="btn-group widget-header-toolbar">
					<a href="<?= base_url()?>admin/sub_categoria/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
				</div>
				<div class="btn-group widget-header-toolbar">
					<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
				</div>
			</div>
			<div class="widget-content">
				<table class="table table-sorting datatable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Categoria</th>
							<th>Marca</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista_sub_categorias as $subCategoriaItem): ?>
							<tr>
								<td><a href="#"><?= $subCategoriaItem['id']; ?></a></td>
								<td><?= $subCategoriaItem['nombre']; ?></td>
								<!--se debe colocar el nombre-->
								<td><?= $subCategoriaItem['categoria']; ?></td>
								<td><?= $subCategoriaItem['marca']; ?></td>
								<td>
									<a href="<?= base_url()?>admin/sub_categoria/edit/<?= $subCategoriaItem['id']; ?>" class="btn btn-warning">Editar</a>
									<button type="button" data-val="<?= $subCategoriaItem['id']; ?>" class="btnDelete btn btn-danger">Eliminar</button>
								</td>
							</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END WIDGET TICKET TABLE -->

	</div><!-- /main-content -->
</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/sub_categoria.js"></script>