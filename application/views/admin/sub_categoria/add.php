<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/sub_categoria">Sub Categorias</a></li>
                  <li class="active">Agregar Sub Categorias</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_agregar">
                    <div class="form-group">
                      <label for="nombre_sub_categoria" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_sub_categoria" class="form-control" id="nombre_sub_categoria" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_categoria" class="col-sm-3 control-label">Categoria</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_categoria" id="id_categoria" class="form-control" required="required">
                            <option value="">Seleccione Categoria</option>
                            <?php foreach ($lista_categorias as $categoriaItem):?>
                              <?php if ($categoriaItem['tipo_categoria']==0): ?>
                              <option value="<?= $categoriaItem['id'];?>"><?= $categoriaItem['nombre'];?></option>
                              <?php endif ?>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_marca" class="col-sm-3 control-label">Marca (opcional)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_marca" id="id_marca" class="form-control">
                            <option value="">Seleccione Marca</option>
                            <?php foreach ($lista_marcas as $marcaItem):?>
                              <option value="<?= $marcaItem['id'];?>"><?= $marcaItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Crear Sub Categorias</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/sub_categoria.js"></script>