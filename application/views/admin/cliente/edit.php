<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/cliente">Clientes</a></li>
                  <li class="active">Editar Cliente</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                <form class="form-horizontal" id="form_actualizar" role="form" method="POST">
                    <input type="hidden" value="<?= $arrayCliente['id'];?>" name="id_cliente" id="id_cliente">
                    
                    <div class="form-group">
                      <label for="rut_cliente" class="col-sm-3 control-label">Rut</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" value="<?= $arrayCliente['rut'];?>" class="form-control" id="rut_cliente" name="rut_cliente" placeholder="Rut">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nombre_cliente" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_cliente" class="form-control" id="nombre_cliente" value="<?= $arrayCliente['nombre'];?>" placeholder="Nombre" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="apellido_cliente" class="col-sm-3 control-label">Apellido</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="apellido_cliente" class="form-control" id="apellido_cliente" value="<?= $arrayCliente['apellido'];?>" placeholder="Apellido" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="telefono_cliente" class="col-sm-3 control-label">Telefono</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" class="form-control" name="telefono_cliente" id="telefono_cliente" value="<?= $arrayCliente['telefono'];?>" placeholder="91234567" required="required">
                          <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="email_cliente" class="col-sm-3 control-label">Email</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="email" value="<?= $arrayCliente['email'];?>" class="form-control" id="email_cliente" name="email_cliente" placeholder="Email" required="required">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="pass" class="col-sm-3 control-label">Password</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" value="La Contraseña no es editable" class="form-control" name="pass" id="pass" placeholder="Password" disabled="disabled">
                          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="direccion_cliente" class="col-sm-3 control-label">Direccion</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" class="form-control" id="direccion_cliente" name="direccion_cliente" value="<?= $arrayCliente['direccion'];?>" placeholder="Direccion" required="required">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="region_cliente" class="col-sm-3 control-label">Region</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="region_cliente" id="region_cliente" class="form-control" required="required">
                            <option value="">Seleccione Region</option>
                            <?php foreach ($lista_regiones as $regionItem):?>
                              <option value="<?= $regionItem['id'];?>" <?php if($arrayCliente['id_region']== $regionItem['id']) echo 'selected';?>><?= $regionItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="provincia_cliente" class="col-sm-3 control-label">Provincia</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="provincia_cliente" id="provincia_cliente" class="form-control" required="required">
                            <option value="">Seleccione Provincia</option>
                            <?php foreach ($lista_provincias as $provinciaItem):?>
                              <option value="<?= $provinciaItem['id'];?>" <?php if($arrayCliente['id_provincia']== $provinciaItem['id']) echo 'selected';?>><?= $provinciaItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="comuna_cliente" class="col-sm-3 control-label">Comuna</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="comuna_cliente" id="comuna_cliente" class="form-control" required="required">
                            <option value="">Seleccione Comuna</option>
                            <?php foreach ($lista_comunas as $comunaItem):?>
                              <option value="<?= $comunaItem['id'];?>" <?php if($arrayCliente['id_comuna']== $comunaItem['id']) echo 'selected';?>><?= $comunaItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="edad_cliente" class="col-sm-3 control-label">Edad</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" value="<?= $arrayCliente['edad'];?>" class="form-control" id="edad_cliente" name="edad_cliente" placeholder="Edad">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="puntos_cliente" class="col-sm-3 control-label">Puntos</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="number" class="form-control" value="<?= $arrayCliente['puntos'];?>" id="puntos_cliente" name="puntos_cliente" placeholder="Puntos">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="sexo_cliente" class="col-sm-3 control-label">Sexo</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="sexo_cliente" id="sexo_cliente" class="form-control" >
                            <option value="">Seleccione Sexo</option>
                            <option <?= ($arrayCliente['sexo']==0)?'selected="selected"':'';?> value="0">Masculino</option>
                            <option <?= ($arrayCliente['sexo']==1)?'selected="selected"':'';?> value="1">Femenino</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="submit" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Guardar Cambios Clientes</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/js/jquery.Rut.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/interno/cliente.js"></script>