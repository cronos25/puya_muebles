<div class="col-md-10 content-wrapper">
<div class="row">
	<div class="col-md-4 ">
		<ul class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?= base_url('admin'); ?>">Home</a></li>
			<li class="active">Carros de compras</li>
		</ul>
	</div>
</div>

<!-- main -->
<div class="content">
		
		<!-- WIDGET TICKET TABLE -->
		<div class="widget widget-table">
			<div class="widget-header">
				<h3><i class="fa fa-group"></i> Carros de compras del sistema</h3> <em>- Lista de carros de compras</em>
				<div class="btn-group widget-header-toolbar">
					<!--<a href="<?= base_url('admin/shopping_cart/add')?>" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>-->
				</div>
				<div class="btn-group widget-header-toolbar">
					<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
				</div>
			</div>
			<div class="widget-content">
				<table class="table table-sorting datatable">
					<thead>
						<tr>
							<th>Codigo</th>
							<th>Mail</th>
							<th>Precio Total</th>
							<th>Fecha</th>
							<th>Estado</th>
							<th>Comprador</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista_carro_compra as $carro_compra_item): ?>
							<tr>
								<td><a href="#"><?= $carro_compra_item['codigo']; ?></a></td>
								<td><?= $carro_compra_item['email']; ?></td>
								<td>
								$<?= number_format($carro_compra_item['total'] ,0, "," ,"."); ?>
								</td>
								<td><?= date('d-m-Y H:i:s',strtotime($carro_compra_item['fecha'])); ?></td>
								<td>
								<?php 
									switch ($carro_compra_item['estado']) {
										case 0:
											echo "abierto";
											break;
										case 1:
											echo "pago pendiente";
											break;
										case 2:
											echo "pagado";
											break;
										case 3:
											echo "finalizado";
											break;
										case 4:
											echo "Expirado";
											break;
										default:
											echo "abierto";
											break;
									}
								?></td>
								<td><?= $carro_compra_item['rut_comprador']; ?></td>
								<td>
									<a href="<?= base_url('admin/shopping_cart/edit/'.$carro_compra_item['id']); ?>" class="btn btn-warning" title="editar"><i class="fa fa-edit"></i></a>
									<button type="button" data-val="<?= $carro_compra_item['id']; ?>" class="btnDelete btn btn-danger" title="Eliminar"><i class="fa fa-trash-o"></i></button>
									<button type="button" data-val="<?= $carro_compra_item['id']; ?>" class="btnResend btn btn-info" title="reenviar correo"><i class="fa fa-envelope-o"></i></button>
									<button type="button" data-val="<?= $carro_compra_item['id']; ?>" class="btnReopen btn btn-success" title="reactivar carro"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END WIDGET TICKET TABLE -->

	</div><!-- /main-content -->
</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/carro_compra.js"></script>