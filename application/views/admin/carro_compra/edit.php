<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-5">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url('admin'); ?>">Home</a></li>
                  <li class="active"><a href="<?= base_url('admin/carro_compra'); ?>">Carros de compra</a></li>
                  <li class="active">Editar Carro de compra</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                <form class="form-horizontal" id="form_actualizar" role="form" method="POST">
                    <input type="hidden" value="<?= $arrayCarroCompra['id'];?>" name="id_carro" id="id_carro">
                    <div class="form-group">
                      <label for="nombre_carro" class="col-sm-3 control-label">codigo carro compra</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" class="form-control" value="<?= $arrayCarroCompra['codigo'];?>" disabled>
                          <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nombre_comprador_carro" class="col-sm-3 control-label">nombre comprador</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_comprador_carro" class="form-control" id="nombre_comprador_carro" value="<?= $arrayUser['nombre'];?>" placeholder="nombre comprador">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="rut_comprador_carro" class="col-sm-3 control-label">rut comprador</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="rut_comprador_carro" class="form-control" id="rut_comprador_carro" value="<?= $arrayUser['rut'];?>" placeholder="rut_comprador_carro">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="direccion_comprador_carro" class="col-sm-3 control-label">direccion comprador</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="direccion_comprador_carro" class="form-control" id="direccion_comprador_carro" value="<?= $arrayUser['direccion'];?>" placeholder="direccion_comprador_carro">
                          <span class="input-group-addon"><i class="fa fa-home"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="email_carro" class="col-sm-3 control-label">Email</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="email" value="<?= $arrayUser['email'];?>" class="form-control" id="email_carro" name="email_carro" placeholder="Email">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="telefono_carro" class="col-sm-3 control-label">Telefono</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" class="form-control" name="telefono_carro" id="telefono_carro" value="<?= $arrayUser['telefono'];?>" placeholder="91234567">
                          <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="observacion_carro" class="col-sm-3 control-label">Observacion</label>
                      <div class="col-sm-9">
                        <textarea rows="10" class="form-control" maxlength="250" type="text" name="observacion_carro" id="observacion_carro" placeholder="(opcional) Agrega indicaciones para la entrega de tus productos, por ejemplo: quiero retirar en la sucursal xxxxxxxxx" ><?= $arrayCarroCompra['mensaje'];?></textarea>
                      </div>
                    </div>
                    <!--<div class="form-group">
                      <label for="fecha_carro" class="col-sm-3 control-label">Fecha</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" class="form-control" value="<?= date('d-m-Y H:i:s',strtotime($arrayCarroCompra['fecha']));?>" disabled>
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>-->
                    <div class="form-group">
                      <label for="estado_carro" class="col-sm-3 control-label">Estado</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <?php 
                            switch ($arrayCarroCompra['estado']) {
                              case 0:
                                ?>
                                <input type="text" class="form-control" value="Abierto" disabled>
                                <?php
                                break;
                              case 1:
                                ?>
                                <input type="text" class="form-control" value="Cancelado" disabled>
                                <?php
                                break;
                              case 2:
                                ?>
                                <input type="text" class="form-control" value="Procesado a venta" disabled>
                                <?php
                                break;
                              default:
                                # code...
                                break;
                            }
                          ?>
                          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="submit" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Carro</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->

<script src="<?= base_url(); ?>assets/admin/js/interno/carro_compra.js"></script>