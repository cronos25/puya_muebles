<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/precio_envio">Precios de Envio</a></li>
                  <li class="active">Editar Precio de Envio</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                <form class="form-horizontal" id="form_actualizar" role="form" method="POST">
                    <input type="hidden" value="<?= $arrayPrecioEnvio['id'];?>" name="id_precio_envio" id="id_precio_envio">
                    <div class="form-group">
                      <label for="precio_precio_envio" class="col-sm-3 control-label">Precio</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                          <input type="number" min="0" name="precio_precio_envio" class="form-control" id="precio_precio_envio" value="<?= $arrayPrecioEnvio['precio'];?>" placeholder="1500" required="required">
                          <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_region" class="col-sm-3 control-label">Region</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_region" id="id_region" class="form-control" required="required">
                            <option value="">Seleccione Region</option>
                            <?php foreach ($lista_regiones as $regionItem):?>
                              <option value="<?= $regionItem['id'];?>" <?php if($arrayPrecioEnvio['region_id']== $regionItem['id']) echo 'selected';?>><?= $regionItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_provincia" class="col-sm-3 control-label">Provincia</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_provincia" id="id_provincia" class="form-control" required="required">
                            <option value="">Seleccione Provincia</option>
                            <?php foreach ($lista_provincias as $provinciaItem):?>
                              <option value="<?= $provinciaItem['id'];?>" <?php if($arrayPrecioEnvio['provincia_id']== $provinciaItem['id']) echo 'selected';?>><?= $provinciaItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_comuna" class="col-sm-3 control-label">Comuna</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_comuna" id="id_comuna" class="form-control" required="required">
                            <option value="">Seleccione Comuna</option>
                            <?php foreach ($lista_comunas as $comunaItem):?>
                              <option value="<?= $comunaItem['id'];?>" <?php if($arrayPrecioEnvio['comuna_id']== $comunaItem['id']) echo 'selected';?>><?= $comunaItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="desde" class="col-sm-3 control-label">Medida desde (cm)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" placeholder="ej 5.3" value="<?= $arrayPrecioEnvio['desde'];?>" class="form-control" name="desde">
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="hasta" class="col-sm-3 control-label">Medida hasta (cm)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" placeholder="ej 5.3" value="<?= $arrayPrecioEnvio['hasta'];?>" class="form-control" name="hasta">
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="tipo_medida" class="col-sm-3 control-label">Tipo Medida</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="tipo_medida" id="tipo_medida" class="form-control" required="required">
                            <option value="">Seleccione Medida</option>
                            <option value="0" <?php if($arrayPrecioEnvio['tipo_medida']== 0) echo 'selected';?>>S</option>
                            <option value="1" <?php if($arrayPrecioEnvio['tipo_medida']== 1) echo 'selected';?>>M</option>
                            <option value="2" <?php if($arrayPrecioEnvio['tipo_medida']== 2) echo 'selected';?>>L</option>
                            <option value="3" <?php if($arrayPrecioEnvio['tipo_medida']== 3) echo 'selected';?>>XL</option>
                            <option value="4" <?php if($arrayPrecioEnvio['tipo_medida']== 4) echo 'selected';?>>XXL</option>
                            <option value="5" <?php if($arrayPrecioEnvio['tipo_medida']== 5) echo 'selected';?>>XXS</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnUpdate" class="btn btn-primary"><i class="fa fa-check-circle"></i> Editar Precio de Envio</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->

<script src="<?= base_url(); ?>assets/admin/js/interno/precio_envio.js"></script>