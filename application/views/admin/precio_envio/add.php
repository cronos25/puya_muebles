<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/precio_envio">Precios Envio</a></li>
                  <li class="active">Agregar Precio de Envio</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_agregar">
                    <div class="form-group">
                      <label for="precio_precio_envio" class="col-sm-3 control-label">Precio</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                          <input type="number" min="0" name="precio_precio_envio" class="form-control" id="precio_precio_envio" placeholder="Precio" required="required">
                          <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_region" class="col-sm-3 control-label">Region</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_region" id="id_region" class="form-control" required="required">
                            <option value="">Seleccione Region</option>
                            <?php foreach ($lista_regiones as $regionItem):?>
                              <option value="<?= $regionItem['id'];?>"><?= $regionItem['nombre'];?></option>
                            <?php endforeach;?>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_provincia" class="col-sm-3 control-label">Provincia</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_provincia" id="id_provincia" class="form-control" required="required">
                            <option value="">Seleccione Provincia</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_comuna" class="col-sm-3 control-label">Comuna</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="id_comuna" id="id_comuna" class="form-control" required="required">
                            <option value="">Seleccione Comuna</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="desde" class="col-sm-3 control-label">Medida desde (cm)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" placeholder="ej 5.3" class="form-control" name="desde">
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="hasta" class="col-sm-3 control-label">Medida hasta (cm)</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" placeholder="ej 5.3" class="form-control" name="hasta">
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="tipo_medida" class="col-sm-3 control-label">Tipo Medida</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <select name="tipo_medida" id="tipo_medida" class="form-control" required="required">
                            <option value="">Seleccione Medida</option>
                            <option value="0">S</option>
                            <option value="1">M</option>
                            <option value="2">L</option>
                            <option value="3">XL</option>
                            <option value="4">XXL</option>
                            <option value="5">XXS</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Agregar Precio de Envio</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/precio_envio.js"></script>