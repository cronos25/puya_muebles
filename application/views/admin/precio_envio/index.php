<div class="col-md-10 content-wrapper">
<div class="row">
	<div class="col-md-4 ">
		<ul class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
			<li class="active">Precios de envio</li>
		</ul>
	</div>
</div>

<!-- main -->
<div class="content">
		
		<!-- WIDGET TICKET TABLE -->
		<div class="widget widget-table">
			<div class="widget-header">
				<h3><i class="fa fa-truck"></i> Precios de envio del sistema</h3> <em>- Lista de precis de envio</em>
				<div class="btn-group widget-header-toolbar">
					<a href="<?= base_url()?>admin/precio_envio/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
				</div>
				<div class="btn-group widget-header-toolbar">
					<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
				</div>
			</div>
			<div class="widget-content">
				<table class="table table-sorting datatable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Precio</th>
							<th>Región</th>
							<th>Ciudad</th>
							<th>Comuna</th>
							<th>Desde (cm)</th>
							<th>Hasta (cm)</th>
							<th>Tipo Medida</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista_precio_envios as $precio_envio_item): ?>
							<tr>
								<td><a href="#"><?= $precio_envio_item['id']; ?></a></td>
								<td>$ <?= number_format($precio_envio_item['precio'],0, "," ,".");?></td>
								<!--se debe colocar el nombre-->
								<td><?= $precio_envio_item['region_nombre']; ?></td>
								<td><?= $precio_envio_item['provincia_nombre']; ?></td>
								<td><?= $precio_envio_item['comuna_nombre']; ?></td>
								<td><?= $precio_envio_item['desde']; ?></td>
								<td><?= $precio_envio_item['hasta']; ?></td>
								<td><?= $precio_envio_item['tipo_medida']; ?></td>
								<td>
									<a href="<?= base_url()?>admin/precio_envio/edit/<?= $precio_envio_item['id']; ?>" class="btn btn-warning">Editar</a>
									<button type="button" data-val="<?= $precio_envio_item['id']; ?>" class="btnDelete btn btn-danger">Eliminar</button>
								</td>
							</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END WIDGET TICKET TABLE -->

	</div><!-- /main-content -->
</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/precio_envio.js"></script>