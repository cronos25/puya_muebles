<div class="col-md-10 content-wrapper">
<div class="row">
	<div class="col-md-4 ">
		<ul class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
			<li class="active">Ventas</li>
		</ul>
	</div>
</div>

<!-- main -->
<div class="content">
		
		<!-- WIDGET TICKET TABLE -->
		<div class="widget widget-table">
			<div class="widget-header">
				<h3><i class="fa fa-group"></i> Ventas Activas del sistema</h3> <em>- Lista de ventas</em>
				
				<div class="btn-group widget-header-toolbar">
					<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
				</div>
			</div>
			<div class="widget-content">
				<table class="table table-sorting datatablecotizados">

					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre cliente</th>
							<th>Rut Cliente</th>
							<th>Mensaje</th>
							<th>estado</th>
							<th>costo envio</th>
							<th>costo productos</th>
							<th>metodo pago</th>
							<th>fecha procesado</th>
							<th>fecha pagado</th>
							<th>codigo</th>
							<th>numero orden</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista_ventas as $venta_item):
							if ($venta_item['estado']!=4 && $venta_item['estado']!=1 ): ?>
							<tr>
								<td><a href="#"><?= $venta_item['id']; ?></a></td>
								<td><?= $venta_item['nombre_cliente'].' '.$venta_item['apellido_cliente']; ?></td>
								<td><?= $venta_item['rut_cliente']; ?></td>
								<td><?= $venta_item['mensaje']; ?></td>
								<td><?php switch ($venta_item['estado']) {
									case 0:
										echo 'esperando pago';
										break;
									case 1:
										echo 'venta cancelada';
										break;
									case 2:
										echo 'pagado';
										break;
									case 3:
										echo 'enviado - <br> listo para retiro';
										break;
									case 4:
										echo 'finalizado';
										break;
									default:
										echo 'esperando pago';
										break;
								}?>
								</td>
								<td>$<?= number_format($venta_item['costo_envio'],0,",",".") ; ?></td>
								<td>$<?= number_format($venta_item['costo_productos'],0,",","."); ?></td>
								<td><?php switch ($venta_item['metodo_pago']) {
									case 0:
										echo 'efectivo';
										break;
									case 1:
										echo 'webpay';
										break;
									case 2:
										echo 'transferencia';
										break;
									default:
										echo 'efectivo';
										break;
								}?>
								</td>
								<td><?= date('d-m-Y H:i:s',strtotime($venta_item['fecha_proceso'])); ?></td>
								<td><?= $venta_item['fecha_pago']; ?></td>
								<td><?= $venta_item['codigo_documento']; ?></td>
								<td><?= $venta_item['numero_orden']; ?></td>
								<td>
									<div class="btn-group" role="group" aria-label="">
										<!--<a href="<?= base_url()?>admin/venta/edit/" class="btn btn-warning"><i class="fa fa-edit"></i></a>-->
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-default" data-toggle="modal" data-target=".modal-productos" title="ver productos de esta venta"><i class="fa fa-eye"></i></button>
										<?php if(
											$venta_item['estado']!=0 &&
											$venta_item['estado']!=4 &&
											$venta_item['estado']!=1 &&
											$venta_item['estado']!=3){?>
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-warning" data-toggle="modal" data-target=".modal-pendiente" title="marcar como pendiente pago"><i class="fa fa-clock-o"></i></button>
									<?php
									}
									if($venta_item['estado']!=2 &&
										$venta_item['estado']!=3 &&
										$venta_item['estado']!=4 &&
											$venta_item['estado']!=1) {
									?>
										<button type="button" data-ven="<?= $venta_item['id'];?>"  class="btn btn-success" data-toggle="modal" data-target=".modal-pagado" title="marcar como pagado"><i class="fa fa-money"></i></button>
									<?php
									}
									if(
										$venta_item['estado']!=3 &&
										$venta_item['estado']!=4 &&
											$venta_item['estado']!=1) {
									?>
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-info" data-toggle="modal" data-target=".modal-enviado" title="marcar como producto enviado"><i class="fa fa-truck"></i></button>
									<?php
									}
									if(
										$venta_item['estado']!=1 &&
										$venta_item['estado']!=4) {
									?>	
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-primary" data-toggle="modal" data-target=".modal-finalizado" title="marcar como finalizado (producto entregado)"><i class="fa fa-check"></i></button>
									<?php
									}
									if(
										$venta_item['estado']!=1 &&
										$venta_item['estado']!=4) {
									?>
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-danger" data-toggle="modal" data-target=".modal-cancelado" title="marcar como cancelado"><i class="fa fa-close"></i></button>
									<?php
									}
									?>
									</div>
								</td>
							</tr>

						<?php endif;
						 endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END WIDGET TICKET TABLE -->
		<!-- WIDGET TICKET TABLE -->
		<div class="widget widget-table">
			<div class="widget-header">
				<h3><i class="fa fa-group"></i> Ventas Finalizadas del sistema</h3> <em>- Lista de ventas</em>
				
				<div class="btn-group widget-header-toolbar">
					<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
				</div>
			</div>
			<div class="widget-content">
				<table class="table table-sorting datatablecotizados">

					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre cliente</th>
							<th>Rut Cliente</th>
							<th>Mensaje</th>
							<th>estado</th>
							<th>costo envio</th>
							<th>costo productos</th>
							<th>metodo pago</th>
							<th>fecha procesado</th>
							<th>fecha pagado</th>
							<th>codigo</th>
							<th>numero orden</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista_ventas as $venta_item):
							if ($venta_item['estado']==4 || $venta_item['estado']==1 ): ?>
							<tr>
								<td><a href="#"><?= $venta_item['id']; ?></a></td>
								<td><?= $venta_item['nombre_cliente'].' '.$venta_item['apellido_cliente']; ?></td>
								<td><?= $venta_item['rut_cliente']; ?></td>
								<td><?= $venta_item['mensaje']; ?></td>
								<td><?php switch ($venta_item['estado']) {
									case 0:
										echo 'esperando pago';
										break;
									case 1:
										echo 'venta cancelada';
										break;
									case 2:
										echo 'pagado';
										break;
									case 3:
										echo 'enviado - <br> listo para retiro';
										break;
									case 4:
										echo 'finalizado';
										break;
									default:
										echo 'esperando pago';
										break;
								}?>
								</td>
								<td>$<?= number_format($venta_item['costo_envio'],0,",",".") ; ?></td>
								<td>$<?= number_format($venta_item['costo_productos'],0,",","."); ?></td>
								<td><?php switch ($venta_item['metodo_pago']) {
									case 0:
										echo 'efectivo';
										break;
									case 1:
										echo 'webpay';
										break;
									case 2:
										echo 'transferencia';
										break;
									default:
										echo 'efectivo';
										break;
								}?>
								</td>
								<td><?= date('d-m-Y H:i:s',strtotime($venta_item['fecha_proceso'])); ?></td>
								<td><?= $venta_item['fecha_pago']; ?></td>
								<td><?= $venta_item['codigo_documento']; ?></td>
								<td><?= $venta_item['numero_orden']; ?></td>
								<td>
									<div class="btn-group" role="group" aria-label="">
										<!--<a href="<?= base_url()?>admin/venta/edit/" class="btn btn-warning"><i class="fa fa-edit"></i></a>-->
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-default" data-toggle="modal" data-target=".modal-productos" title="ver productos de esta venta"><i class="fa fa-eye"></i></button>
										<?php if(
											$venta_item['estado']!=0 &&
											$venta_item['estado']!=4 &&
											$venta_item['estado']!=1 &&
											$venta_item['estado']!=3){?>
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-warning" data-toggle="modal" data-target=".modal-pendiente" title="marcar como pendiente pago"><i class="fa fa-clock-o"></i></button>
									<?php
									}
									if($venta_item['estado']!=2 &&
										$venta_item['estado']!=3 &&
										$venta_item['estado']!=4 &&
											$venta_item['estado']!=1) {
									?>
										<button type="button" data-ven="<?= $venta_item['id'];?>"  class="btn btn-success" data-toggle="modal" data-target=".modal-pagado" title="marcar como pagado"><i class="fa fa-money"></i></button>
									<?php
									}
									if(
										$venta_item['estado']!=3 &&
										$venta_item['estado']!=4 &&
											$venta_item['estado']!=1) {
									?>
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-info" data-toggle="modal" data-target=".modal-enviado" title="marcar como producto enviado"><i class="fa fa-truck"></i></button>
									<?php
									}
									if(
										$venta_item['estado']!=1 &&
										$venta_item['estado']!=4) {
									?>	
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-primary" data-toggle="modal" data-target=".modal-finalizado" title="marcar como finalizado (producto entregado)"><i class="fa fa-check"></i></button>
									<?php
									}
									if(
										$venta_item['estado']!=1 &&
										$venta_item['estado']!=4) {
									?>
										<button type="button" data-ven="<?= $venta_item['id']; ?>" class="btn btn-danger" data-toggle="modal" data-target=".modal-cancelado" title="marcar como cancelado"><i class="fa fa-close"></i></button>
									<?php
									}
									?>
									</div>
								</td>
							</tr>

						<?php endif;
						 endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END WIDGET TICKET TABLE -->

	</div><!-- /main-content -->
</div><!-- /main -->
</div><!-- /content-wrapper -->
<div class="modal fade modal-productos" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Descripcion Productos</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade modal-pagado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atención</h4>
      </div>
      <div class="modal-body">
        Al marcar como <strong>pagado</strong> se le enviara un mail al comprador del cambio de estado y los productos pasaran al estado de procesado
      </div>
      <div class="modal-footer">
      	<button type="button" data-val="2" data-sel="2" data-ven="" class="setEstado btn btn-success" data-loading-text="<i class='fa fa-money'></i> marcar como pagado" title="marcar como pagado"><i class="fa fa-money"></i> marcar como pagado</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-pendiente" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atención</h4>
      </div>
      <div class="modal-body">
        Al marcar como  <strong>pendiente de pago</strong> se le enviara un mail al comprador del cambio de estado y los productos pasaran al estado de procesado
      </div>
      <div class="modal-footer">
      	<button type="button" data-val="0" data-sel="0" data-ven="" class="btnDelete setEstado btn btn-warning" data-loading-text="<i class='fa fa-clock-o'></i> marcar como pendiente de pago"  title="marcar como pendiente pago"><i class="fa fa-clock-o"></i> marcar como pendiente de pago</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-enviado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atención</h4>
      </div>
      <div class="modal-body">
        <p>Al marcar como  <strong>enviado</strong> de pago se le enviara un mail al comprador del cambio de estado y los productos pasaran a estado de procesado
        </p>
        <input type="text" id="input_codigo_seguimiento" class="col-md-12" placeholder="http://www.starken.cl/seguimiento?codigo=1312312312312"  value="">
      </div>
      <div class="modal-footer">
      	<button type="button" data-val="3" data-sel="3" data-ven="" data-loading-text="<i class='fa fa-truck'></i> Marcar enviado" class="btnEnviado setEstado btn btn-info" title="producto enviado"><i class="fa fa-truck"></i> Marcar enviado</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade modal-finalizado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atención</h4>
      </div>
      <div class="modal-body">
        Al marcar como <strong>finalizado</strong> se le enviara un mail al comprador del cambio de estado y los productos pasaran al estado de finalizado
      </div>
      <div class="modal-footer">
      	<button type="button" data-val="4" data-sel="4" data-ven="" class="setEstado btn btn-primary" data-loading-text="<i class='fa fa-check'></i> Marcar como finalizado" title="marcar como finalizado"><i class="fa fa-check"></i> Marcar como finalizado</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade modal-cancelado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atención</h4>
      </div>
      <div class="modal-body">
        Al marcar como <strong>Cancelado</strong> se le enviara un mail al comprador del cambio de estado y los productos pasaran al estado de finalizado
      </div>
      <div class="modal-footer">
      	<button type="button" data-val="1" data-sel="1" data-ven="" class="btnCancelado setEstado btn btn-danger" data-loading-text="<i class='fa fa-close'></i> Marcar como cancelado" title="marcar como cancelado"><i class="fa fa-close"></i>Marcar como cancelado</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/admin/js/interno/venta.js"></script>