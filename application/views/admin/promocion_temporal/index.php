<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Promociones Temporales</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-ticket"></i> Promociones Temporales del sistema</h3> <em>- Lista de Promociones Temporales</em>
					<div class="btn-group widget-header-toolbar">
						<a href="<?= base_url()?>admin/promocion_temporal/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Mensaje</th>
								<th>Producto</th>
								<!--<th>Categoria</th>
								<th>Marca</th>
								<th>Sub Categoria</th>-->
								<th>Descuento</th>
								<th>Fecha Inicio</th>
								<th>Fecha Fin</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($dataPromos as $promoItem): ?>
								<tr>
									<td class="col-md-1"><a href="#"><?= $promoItem['id']; ?></a></td>
									<td class="col-md-2"><?= $promoItem['mensaje']; ?> </td>
									<td class="col-md-2"><?= $promoItem['producto']; ?> </td>
									<!--<td class="col-md-1"><?= $promoItem['categoria']; ?> </td>
									<td class="col-md-1"><?= $promoItem['marca']; ?> </td>
									<td class="col-md-2"><?= $promoItem['sub_categoria']; ?> </td>-->
									<td class="col-md-1"><?= $promoItem['descuento']; ?> %</td>
									<td class="col-md-1"><?= date('d-m-Y H:i:s',strtotime($promoItem['fecha_inicio']));?></td>
									<td class="col-md-1"><?= date('d-m-Y H:i:s',strtotime($promoItem['fecha_fin'])); ?></td>
									<td class="col-md-2">
										<a href="<?= base_url()?>admin/promocion_temporal/edit/<?= $promoItem['id']; ?>" class="btn btn-warning">Editar</a>
										<button type="button" data-val="<?= $promoItem['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/promocion_temporal.js"></script>