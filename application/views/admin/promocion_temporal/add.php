<div class="col-md-10 content-wrapper">
      <div class="row">
        <div class="col-md-5">
          <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
            <li class="active"><a href="<?= base_url(); ?>admin/promocion_temporal">Promociones Temporales</a></li>
            <li class="active">Agregar Promocion Temporal</li>
          </ul>
        </div>
      </div>
      
      <!-- main -->
      <div class="content">
        <div class="widget">
          <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
          <div class="widget-content">
            <form class="form-horizontal" role="form" id="form_agregar">
            <label for="">* debe seleccionar al menos un item</label>
              <div class="form-group">
                <label for="producto_promo_temporal" class="col-sm-3 control-label">*Producto</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <select name="producto_promo_temporal" id="producto_promo_temporal" class="select2 form-control" required="required">
                      <option value="">Seleccione Producto</option>
                      <?php foreach ($lista_productos as $producto_item): ?>
                        <option value="<?= $producto_item['id']; ?>"><?= $producto_item['nombre']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  </div>
                </div>
              </div>
              <!--
              <div class="form-group">
                <label for="categoria_promo_temporal" class="col-sm-3 control-label">*Categoria</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <select name="categoria_promo_temporal" id="categoria_promo_temporal" class="select2 form-control" required="required">
                      <option value="">Seleccione Categoria</option>
                      <?php foreach ($lista_categorias as $categoria_item): ?>
                        <option value="<?= $categoria_item['id']; ?>"><?= $categoria_item['nombre']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="sub_categoria_promo_temporal" class="col-sm-3 control-label">*Sub Categoria (opcional)</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <select name="sub_categoria_promo_temporal" id="sub_categoria_promo_temporal" class="select2 form-control" required="required">
                      <option value="">Seleccione Sub Categoria</option>
                      <?php foreach ($lista_sub_categorias as $sub_categoria_item): ?>
                        <option value="<?= $sub_categoria_item['id']; ?>"><?= $sub_categoria_item['nombre']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="marca_promo_temporal" class="col-sm-3 control-label">*Marca</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <select name="marca_promo_temporal" id="marca_promo_temporal" class="select2 form-control" required="required">
                      <option value="">Seleccione Marca</option>
                      <?php foreach ($lista_marcas as $marca_item): ?>
                        <option value="<?= $marca_item['id']; ?>"><?= $marca_item['nombre']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  </div>
                </div>
              </div>-->
              <div class="form-group">
                <label for="mensaje_promo_temporal" class="col-sm-3 control-label">Mensaje</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="text" maxlength="150" name="mensaje_promo_temporal" class="form-control" id="mensaje_promo_temporal" placeholder="ejemplo: descuento por el fin de semana " required="required">
                    <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="descuento_promo_temporal" class="col-sm-3 control-label">Descuento</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="number" min="0" max="100" name="descuento_promo_temporal" class="form-control" id="descuento_promo_temporal" value="0" placeholder="10" required="required">
                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="fecha_promo_temporal" class="col-sm-3 control-label">Tiempo Activo (inicio - fin)</label>
                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="text" id="datepicker" class="form-control"  placeholder="DD-MM-YY - DD-MM-YYYY" readonly="readonly" required="required">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Crear Promocion Temporal</button>
                </div>
              </div>
            </form>
          </div>
        </div>

      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/promocion_temporal.js"></script>