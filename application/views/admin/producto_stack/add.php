<div class="col-md-10 content-wrapper">
            <div class="row">
              <div class="col-md-4 ">
                <ul class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
                  <li class="active"><a href="<?= base_url(); ?>admin/stack">Marcas</a></li>
                  <li class="active">Agregar Marcas</li>
                </ul>
              </div>
            </div>
            
            <!-- main -->
            <div class="content">
              <div class="widget">
                <div class="widget-header"><h3><i class="fa fa-edit"></i> Registro</h3></div>
                <div class="widget-content">
                  <form class="form-horizontal" role="form" id="form_agregar">
                    <div class="form-group">
                      <label for="nombre_stack" class="col-sm-3 control-label">Nombre</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <input type="text" name="nombre_stack" class="form-control" id="nombre_stack" placeholder="Nombre">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="precio_stack" class="col-sm-3 control-label">Descripción</label>
                      <div class="col-sm-9">
                        <input type="text" name="precio_stack" min="0" class="form-control" id="precio_stack" placeholder="ingrese precio del stack" value="0" required="required">
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label for="descripcion_stack" class="col-sm-3 control-label">Descripción</label>
                      <div class="col-sm-9">
                        <textarea id="descripcion_stack" name="descripcion_stack" class="form-control jqte-test" rows="6" cols="30" ></textarea>
                        <p class="help-block text-right js-textarea-help"><span class="text-muted"></span></p>
                      </div>
                    </div>                    
                    <div class="form-group">
                      <label for="foto_stack" class="col-sm-3 control-label">Foto Stack </label>
                      <div class="col-sm-9 upload-demo"  id="content_foto_stack">
                        <input type="hidden" name="foto_stack_base64" id="foto_stack_base64" >
                        <div class="actions">
                                <a class="btn file-btn">
                                    <input type="file" id="uploadStack" value="Choose a file" accept="image/*" />
                                </a>
                                <button type="button" id="upload-result-stack" class="upload-result">Cortar</button>
                            </div>
                            <div class="upload-msg">
                                Seleccione una Imagen para cortar
                            </div>
                            <div class="upload-demo-wrap">
                                <div id="fotoAddStack"></div>
                                <div id="fotoAddStackResult"></div>
                            </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9">
                        <button type="button" id="btnAdd" class="btn btn-primary"><i class="fa fa-check-circle"></i> Crear Stack</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
      <!-- END WIDGET TICKET TABLE -->

    </div><!-- /main-content -->
  </div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/stack.js"></script>
<script>
$(document).ready(function(){
  
  function popupResult(result,imgResult,divCorte,inputValor,botonEsconder) {
    var html;
    
    html = '<img src="' + result + '" />';
    
    $(divCorte).hide();
    $(botonEsconder).hide();
    $(imgResult).empty();
    $(imgResult).append(html);
    $(inputValor).val(result);
  }

    var $uploadCropStack;

    function readFile(input,uploadItem) {
      if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                $('#upload-result-stack').show();
                  $('#content_foto_stack').addClass('ready');
                  $('#fotoAddStack').show();
                  $('#fotoAddStackResult').empty();

                uploadItem.croppie('bind', {
                  url: e.target.result
                }).then(function(){
                  console.log('jQuery bind complete');
                });
                
              }
              
              reader.readAsDataURL(input.files[0]);
          }
          else {
                  $('#upload-result-stack').show();
                  $('#content_foto_stack').removeClass('ready');
                  $('#fotoAddStack').show();
                  $('#fotoAddStackResult').empty();
              
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }


    $uploadCropStack = $('#fotoAddStack').croppie({
      viewport: {
        width: 450,
        height: 250,
        type: 'square'
      },
      enableExif: true
    });

    $('#uploadStack').on('change', function () { readFile(this,$uploadCropStack); });

    $('#upload-result-stack').on('click', function (ev) {
      $uploadCropStack.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function (resp) {
        //$('#fotoAddStackResult').attr('src', resp);
        popupResult(resp,
          '#fotoAddStackResult',
          '#fotoAddStack',
          '#foto_stack_base64',
          "#upload-result-stack");
      });
    });
  });
</script>