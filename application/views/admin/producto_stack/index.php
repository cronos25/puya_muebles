<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-md-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url(); ?>admin">Home</a></li>
				<li class="active">Stacks</li>
			</ul>
		</div>
	</div>
	
	<!-- main -->
	<div class="content">
			
			<!-- WIDGET TICKET TABLE -->
			<div class="widget widget-table">
				<div class="widget-header">
					<h3><i class="fa fa-tag"></i> Stacks del sistema</h3> <em>- Lista de Stacks</em>
					<div class="btn-group widget-header-toolbar">
						<!--<a href="<?= base_url()?>admin/producto_stack/add" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i>Agregar</a>-->
					</div>
					<div class="btn-group widget-header-toolbar">
						<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
					</div>
				</div>
				<div class="widget-content">
					<table class="table table-sorting datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Stack</th>
								<th>Productos</th>
								<th>Descuento</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($lista_producto_stacks as $producto_stack_item): 
								/*if ($producto_stack_item['total']<1) {
									$producto_stack_item['total']=1;
								}*/
							?>
								<tr>
									<td><a href="#"><?= $producto_stack_item['id']; ?></a></td>
									<td><?= $producto_stack_item['nombre']; ?></td>
									<td><?= $producto_stack_item['cantidad_productos']; ?></td>
									<td><?= number_format(ceil(($producto_stack_item['total']*100)/$producto_stack_item['precio']) ,0, "," ,"."); ?></td>

									<td class="col-md-3">
										<img src="<?= $producto_stack_item['imagen']; ?>" alt="" class="img-responsive ">
									</td>
									<td>
										<a href="<?= base_url()?>admin/producto_stack/edit/<?= $producto_stack_item['id']; ?>" class="btn btn-warning">Moficar Productos de este stack</a>
										<button type="button" data-val="<?= $producto_stack_item['id']; ?>" class="btn btn-danger btnDelete">Eliminar</button>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END WIDGET TICKET TABLE -->

		</div><!-- /main-content -->
	</div><!-- /main -->
</div><!-- /content-wrapper -->
<script src="<?= base_url(); ?>assets/admin/js/interno/producto_stack.js"></script>