<!-- HEADING-BANNER START -->
<div class="heading-banner-area overlay-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading-banner">
					<div class="heading-banner-title">
						<h2>Contacto</h2>
					</div>
					<div class="breadcumbs pb-15">
						<ul>
							<li><a href="index.html">Home</a></li>
							
							<li>Contacto</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- HEADING-BANNER END -->
<!-- contact-AREA -->
<div class="contact-us-area  pt-80 pb-80">
	<div class="container">	
		<div class="contact-us customer-login bg-white">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					
					
					<div class="contact-details">
						<h4 class="title-1 title-border text-uppercase mb-30">Detalles de Contacto</h4>
						<ul>
							<li>
								<i class="zmdi zmdi-pin"></i>
								<span>Salas 960,</span>
								<span>Concepción, Chile</span>
							</li>
							<li>
								<i class="zmdi zmdi-phone"></i>
								<span>+569 8207 1633</span>
							</li>
							<li>
								<i class="zmdi zmdi-email"></i>
								<span>contacto@puyamuebles.cl</span>
							</li>
						</ul>
					</div>
					<div class="send-message mt-60">
						<form action="<?=base_url('contacto');?>" method="POST">
							<h4 class="title-1 title-border text-uppercase mb-30">Envia un Mensaje</h4>
							<input type="text" name="contacto_nombre" placeholder="Su nombre aquí..." required="required" />
							<input type="email" name="contacto_mail" placeholder="Su email aquí..." required="required" />
							<input type="text" name="contacto_asunto" placeholder="Su email aquí..." required="required" />
							<textarea class="custom-textarea" name="contacto_comentario" placeholder="Su mensaje aquí..." required="required"></textarea>
							<button class="button-one submit-button mt-20" data-text="Enviar Mensaje" type="submit">Enviar Mensaje</button>
						</form>
						<?php if ($respmail): 
							echo $respmail;
						endif; ?>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 mt-xs-30">
					<div class="map-area">
						<div id="googleMap" style="width:100%;height:600px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- contact-AREA -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuIU9msivfISdbaosc3tPI1HCZyPfZBNo"></script>
		<script>
			function initialize() {
				var mapOptions = {
					zoom: 16,
					scrollwheel: false,
					center: new google.maps.LatLng( -36.8243909, -73.0589409 ),
				};
				var map = new google.maps.Map(document.getElementById('googleMap'),
					mapOptions
				);
				var marker = new google.maps.Marker({
					position: map.getCenter(),
					icon: ' ',
					map: map
				});
				var contentString = '<div id="content">'+
					'<div id="siteNotice">'+
					'</div>'+
					'<div id="bodyContent">'+
					'<p>Salas 960,</br>Concepción, Chile</p>'+		
					'</div>'+
					'</div>';
				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});
					infowindow.open(map, marker);
	
				var styles = 
				[
					{
						"featureType": "all",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#636363"
							}
						]
					},
					{
						"featureType": "all",
						"elementType": "labels.text.stroke",
						"stylers": [
							{
								"visibility": "on"
							},
							{
								"color": "#1f1f1f"
							}
						]
					},
					{
						"featureType": "all",
						"elementType": "labels.icon",
						"stylers": [
							{
								"visibility": "off"
							}
						]
					},
					{
						"featureType": "administrative",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#1F1F1F"
							}
						]
					},
					{
						"featureType": "administrative",
						"elementType": "geometry.stroke",
						"stylers": [
							{
								"color": "#1F1F1F"
							}
						]
					},
					{
						"featureType": "landscape",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#2A2A2A"
							},
						]
					},
					{
						"featureType": "poi",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#2A2A2A"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#2A2A2A"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.stroke",
						"stylers": [
							{
								"color": "#2A2A2A"
							}
						]
					},
					{
						"featureType": "road.arterial",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#1a1a1a"
							}
						]
					},
					{
						"featureType": "road.local",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#1a1a1a"
							}
						]
					},
					{
						"featureType": "transit",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#1a1a1a"
							}
						]
					},
					{
						"featureType": "water",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#1F1F1F"
							},
						]
					}
				];

				map.setOptions({styles: styles});
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>