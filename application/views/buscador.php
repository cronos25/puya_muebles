<!-- <div class="row ">
  <div class="baner-category" style="background:url();background-size:cover;background-position:center;">


  </div>
</div> -->
<!--
<div class="row">
  <div class="col-md-12">
    <h3 class="title-resultado">Resultados para : <b><?= $busqueda;?></b> </h3>
  </div>
</div>

<div class="row">
<?php if (count($lista_productos)>0): ?>
  <div class="col-md-3">
    <div class="box-filtros">
      <div class="row">
        <div class="col-md-6">
          <div class="title-filtro">Marca</div>
        </div>
        <div class="col-md-6">
          <div class="title-filtro text-right">Cantidad</div>
        </div>
      </div>
      <?php foreach($lista_marcas as $marcaItem):
        if($marcaItem['productos']>0):
      ?>
          <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
             <input id="checkboxmarca<?= $marcaItem['id']; ?>" type="checkbox" class="check_filter_brand_search" name="chk_filtro_marcas_buscar" value="<?= $marcaItem['id']; ?>" aria-label="<?= $marcaItem['nombre']; ?>">
             <label class="nombre-opcion" for="checkboxmarca<?= $marcaItem['id']; ?>">
                <?= $marcaItem['nombre']; ?>
              </label>
            <span class="cantidad"><?= $marcaItem['productos']; ?></span>
          </div>
      <?php
        endif;
      endforeach; ?>
      <div class="row">
        <div class="col-md-6">
          <div class="title-filtro">Categoria</div>
        </div>
        <div class="col-md-6">
          <div class="title-filtro text-right">Cantidad</div>
        </div>
      </div>
      <?php foreach($lista_categorias_filtros as $categoriaItem):
        if($categoriaItem['productos']>0):
      ?>
          <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
             <input id="checkboxcate<?= $categoriaItem['id']; ?>" type="checkbox" class="check_filter_cate_search" name="chk_filtro_categorias_buscar" value="<?= $categoriaItem['id']; ?>" aria-label="<?= $categoriaItem['nombre']; ?>">
             <label class="nombre-opcion" for="checkboxcate<?= $categoriaItem['id']; ?>">
                <?= $categoriaItem['nombre']; ?>
              </label>
            <span class="cantidad"><?= $categoriaItem['productos']; ?></span>
          </div>
      <?php
        endif;
      endforeach; ?>

      <div class="row">
        <div class="col-md-6">
          <div class="title-filtro">Precio</div>
        </div>
        <div class="col-md-6">
          <div class="title-filtro text-right">Cantidad</div>
        </div>
      </div>

      <?php
      $precio0to10 = 0;
      $precio11to20 = 0;
      $precio21to35 = 0;
      $precio36to50 = 0;
      $precio50toPlus = 0;
      $valorData = 0;

      foreach($lista_productos as $productoItem):
        #if($productoItem['productos']>0):
        if ($productoItem['precio']<=10000) {
          $precio0to10+=1;
        }else if($productoItem['precio']>10000 && $productoItem['precio']<=20000 ) {
          $precio11to20+=1;
        }else if($productoItem['precio']>20000 && $productoItem['precio']<=35000) {
          $precio21to35+=1;
        }else if($productoItem['precio']>35000 && $productoItem['precio']<=50000) {
          $precio36to50+=1;
        }else if($productoItem['precio']>50000) {
          $precio50toPlus+=1;
        }

      endforeach;
        if ($precio0to10>0): ?>
          <div class="opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek1" class="check_filter_precio_busca" name="chk_filtro_precio_busca" value="1" aria-label="menos de $10.000">
            <label class="nombre-opcion" for="chek1">
            menos de $10.000
            </label>
            <span class="cantidad"><?= $precio0to10; ?></span>
          </div>
        <?php
        endif;
        if ($precio11to20>0): ?>
        <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek2" class="check_filter_precio_busca" name="chk_filtro_precio_busca" value="2" aria-label="$11.000 a $20.000">
            <label class="nombre-opcion" for="chek2">
             $11.000 a $20.000
            </label>
            <span class="cantidad"><?= $precio11to20; ?></span>
          </div>
        <?php
        endif;
        if ($precio21to35>0): ?>
        <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek3" class="check_filter_precio_busca" name="chk_filtro_precio_busca" value="3" aria-label="$21.000 a $35.000">
            <label class="nombre-opcion" for="chek3">
            $21.000 a $35.000
            </label>
            <span class="cantidad"><?= $precio21to35; ?></span>
          </div>
        <?php
        endif;
        if ($precio36to50>0): ?>
        <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek4" class="check_filter_precio_busca" name="chk_filtro_precio_busca" value="4" aria-label="$36.000 a $50.000">
            <label class="nombre-opcion" for="chek4">
            $36.000 a $50.000
            </label>
            <span class="cantidad"><?= $precio36to50; ?></span>
          </div>
        <?php
        endif;
        if ($precio50toPlus>0): ?>
          <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek5" class="check_filter_precio_busca" name="chk_filtro_precio_busca" value="5" aria-label="$50.000 o mas">
            <label class="nombre-opcion" for="chek5">
            $50.000 o más
            </label>
            <span class="cantidad"><?= $precio50toPlus; ?></span>
          </div>
        <?php
        endif;
        ?>
    </div>
</div>

<div class="col-md-9">
  <div class="row tab-content" id="content_productos_buscar" data-view="b" data-textbusca="<?= $busqueda;?>">
    <?php $i=0;
      $paneles=0;
      foreach ($lista_productos as $productoItem):
       if($i%12==0):
        $paneles++;
      ?>
        <div class="tab-pane col-md-12 fade <?=($paneles==1)?'in active':'';?>" id="tab<?=$paneles;?>" role="tabpanel">
      <?php
        endif;
      ?>

      <div class="col-md-4">
        <div class="item-thumbnail">
          <a href="<?= base_url('productos/view/'.$productoItem['id'].'/'.str_replace('%','',$productoItem['nombre']));?>"><img src="<?=$productoItem['foto_producto'] ?>" alt=""></a>
        </div>
        <div class="item-details">
          <h1 class="item-title"><b><?=$productoItem['marca'] ?></b> <?=$productoItem['nombre'] ?> </h1>
          <div class="item-info"><?=$productoItem['descripcion'] ?></div>
          <div class="row grid-pre">
            <div class="col-md-6">
              <?php if ($productoItem['envio_gratis']==0): ?>
                <div class="item-envio">
                  <span>
                    <svg class="icon" viewBox="0 0 46 32">
                    <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>
                    </svg></span>
                    Envio gratis
                </div>
              <?php endif;?>
            </div>
            <div class="col-md-6">
              <?php if (!empty($productoItem['regalo'])): ?>
                <div class="item-regalo">
                  <span><svg class="icon" viewBox="0 0 29 32">
                  <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>
                  <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>
                  <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>
                  </svg></span>
                  Regalo
                </div>
              <?php endif ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="item-price">
                $<?= number_format(ceil($productoItem['precio']) ,0, "," ,"."); ?>
              </div>
            </div>
            <div class="col-md-6 text-right">
              <?php if (isset($_SESSION['cliente_data_puyasess']['logeado']) && $_SESSION['cliente_data_puyasess']['logeado']): ?>
                <a class="btn-item-car" data-prod="<?=$productoItem['id'];?>"
                href="<?= base_url('productos/view/'.$productoItem['id'].'/'.str_replace('%','',$productoItem['nombre']));?>">
                  <svg class="icon" viewBox="0 0 36 32">
                    <path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>
                    <path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>
                    <path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>
                    <path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>
                  </svg>
                </a>
            <?php else: ?>
                <a href="<?= base_url('cliente/');?>" class="btn-item-car">
                  <svg class="icon" viewBox="0 0 36 32">
                    <path d="M34.88 6.035h-20.768c-0.023-0.002-0.050-0.003-0.077-0.003-0.469 0-0.849 0.38-0.849 0.849 0 0.117 0.024 0.228 0.066 0.329l-0.002-0.006 3.286 9.621c0.196 0.535 0.69 0.915 1.276 0.94l0.003 0h13.615c0.588-0.026 1.081-0.406 1.273-0.931l0.003-0.010 2.863-9.852c0.032-0.076 0.051-0.165 0.051-0.259 0-0.377-0.305-0.682-0.682-0.682-0.021 0-0.042 0.001-0.062 0.003l0.003-0z"></path>
                    <path d="M19.097 28.854c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>
                    <path d="M32.242 28.893c0 1.716-1.391 3.107-3.107 3.107s-3.107-1.391-3.107-3.107c0-1.716 1.391-3.107 3.107-3.107s3.107 1.391 3.107 3.107z"></path>
                    <path d="M32.865 20.929h-18.169l-7.274-20.929h-5.664c-0.972 0-1.759 0.788-1.759 1.759s0.788 1.759 1.759 1.759v0h3.162l7.274 20.929h20.671c0.972 0 1.759-0.788 1.759-1.759s-0.788-1.759-1.759-1.759v0z"></path>
                  </svg>
                </a>
            <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    <?php
      $i++;
      echo ($i%12==0)?'</div>':'';
     endforeach;?>
</div>
  <div class="row">
    <div class="col-md-12 text-center grid-pagination">
      <nav aria-label="Page navigation ">
      <ul class="pagination" id="pagination_productos" role="tablist">
        <li class="previous" onclick="goTo(1);">
          <a href="#previous" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <?php
          for($j=1;$j<=$paneles;$j++){
            ?>
            <li class="pagination_sel <?=($j==1)?'active':'';?>" <?=($j==1)?'id="first"':'';?>>
              <a aria-controls="tab<?=$j;?>" data-toggle="tab" data-target="#tab<?=$j;?>" href="#tab<?=$j;?>" role="tab"><?=$j;?></a>
            </li>
        <?php
          }
         ?>
        <li class="next" onclick="goTo(<?=$paneles?>);">
          <a href="#next" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
    </div>
  </div>
<?php else: ?>
    <h2>No se encontrar registros para la busqueda</h2>
<?php endif ?>

</div>
</div>
</div>-->


<!-- HEADING-BANNER START -->
      <div class="heading-banner-area-muebles overlay-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="heading-banner">
                <div class="heading-banner-title">
                  <h2>puya muebles</h2>
                </div>
                <div class="breadcumbs pb-15">
                  <ul>
                    <li><a href="<?= base_url();?>">Home</a></li>
                    <li>Busqueda</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- HEADING-BANNER END -->
      <!-- PRODUCT-AREA START -->
      <div class="product-area pt-80 pb-80 product-style-2">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
              <!-- Widget-Search start -->
              <aside class="widget widget-search mb-30">
                <form>
                  <input type="text" id="textoBuscadorSite" placeholder="Busca Aquí..." />
                  <button type="button" id="btnBuscadorSite">
                    <i class="zmdi zmdi-search"></i>
                  </button>
                </form>
              </aside>
              <!-- Widget-search end -->
              <!-- Widget-Categories start -->
              <aside class="widget widget-categories  mb-30">
                <div class="widget-title">
                  <h4>Categorias</h4>
                </div>
                <div class="widget-info">
                  <ul>
                    <?php foreach ($lista_sub_categorias as $subItem): ?>
                      <li>
                        <label>

                          <input type="checkbox"  class="check_filter_cate_search" name="chk_filtro_categorias_buscar" value="<?= $subItem['id']; ?>"><?= $subItem['nombre']; ?></span>
                        </label>
                      </li>
                    <?php endforeach;?>
                    <!--<li><span>Sillas</span>
                      <ul>
                        <li><a href="#">Silla 1</a></li>
                        <li><a href="#">Silla 2</a></li>
                        <li><a href="#">Silla 3</a></li>
                        <li><a href="#">Silla 4</a></li>
                        <li><a href="#">Silla 5</a></li>
                      </ul>
                    </li>  -->
                  </ul>
                </div>
              </aside>
              <!-- Widget-categories end -->
              <!-- Widget-Categories start -->
              <aside class="widget widget-categories  mb-30">
                <div class="widget-title">
                  <h4>Marcas</h4>
                </div>
                <div class="widget-info">
                  <ul>
                    <?php foreach ($lista_marcas as $marcaItem): ?>
                      <li>
                        <label>
                          <input id="checkboxmarca<?= $marcaItem['id']; ?>" type="checkbox" class="check_filter_brand_search" name="chk_filtro_marcas_buscar" value="<?= $marcaItem['id']; ?>" aria-label="<?= $marcaItem['nombre']; ?>"><?= $marcaItem['nombre']; ?></span>
                        </label>
                      </li>
                    <?php endforeach;?>
                    <!--<li><span>Sillas</span>
                      <ul>
                        <li><a href="#">Silla 1</a></li>
                        <li><a href="#">Silla 2</a></li>
                        <li><a href="#">Silla 3</a></li>
                        <li><a href="#">Silla 4</a></li>
                        <li><a href="#">Silla 5</a></li>
                      </ul>
                    </li>  -->
                  </ul>
                </div>
              </aside>
              <!-- Widget-categories end -->
              <!-- Shop-Filter start -->
              <aside class="widget shop-filter mb-30">
                <div class="widget-title">
                  <h4>Rango de Precio</h4>
                </div>
                <div class="widget-info">
                  <div class="price_filter">
                    <div class="price_slider_amount">
                      <input type="submit"  value="Su rango :"/> 
                      <input type="text" id="amountBuscador" name="price"  placeholder="Agrega un precio" /> 
                    </div>
                    <div id="slider-range"></div>
                  </div>
                </div>
              </aside>
              <!-- Shop-Filter end -->
              <!-- Widget-Color start -->
              <aside class="widget widget-color mb-30">
                <div class="widget-title">
                  <h4>Color</h4>
                </div>
                <div class="widget-info color-filter clearfix">
                  <ul>
                    <?php foreach ($lista_colores as $colorItem):
                      if ($colorItem['productos']>0):
                     ?>
                      <li>
                        <input type="checkbox" name="color_data_filtro" value="<?= $colorItem['id'] ?>" class="hide">
                        <span class="color selector_color_filtro" style="background: <?= $colorItem['hexa'] ?>;"></span><?= $colorItem['nombre'] ?><span class="count"><?= $colorItem['productos'] ?></span></li>
                    <?php
                      endif;
                     endforeach; ?>
                    <!--<li><a href="#"><span class="color color-1"></span>Color<span class="count">12</span></a></li>
                    <li><a href="#"><span class="color color-2"></span>Color<span class="count">20</span></a></li>
                    <li><a href="#"><span class="color color-3"></span>Color<span class="count">59</span></a></li>
                    <li class="active"><a href="#"><span class="color color-4"></span>Color<span class="count">45</span></a></li>
                    <li><a href="#"><span class="color color-5"></span>Color<span class="count">78</span></a></li>
                    <li><a href="#"><span class="color color-6"></span>Color<span class="count">10</span></a></li>
                    <li><a href="#"><span class="color color-7"></span>Color<span class="count">15</span></a></li>-->
                  </ul>
                </div>
              </aside>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
              <!-- Shop-Content End -->
              <div class="shop-content mt-tab-30 mt-xs-30">
                <div class="product-option mb-30 clearfix">
                  <!-- Nav tabs -->
                  <ul class="shop-tab">
                    <li class="active"><a href="#" data-target="#content_productos_buscar" data-toggle="tab"><i class="zmdi zmdi-view-module"></i></a></li>
                    <!--<li><a href="#list-view"  data-toggle="tab"><i class="zmdi zmdi-view-list"></i></a></li>-->
                  </ul>
                  <div class="showing text-right hidden-xs">
                    <p class="mb-0">Mostrando 01-02 de <?= count($lista_productos_ordenados); ?> Resultados</p>
                  </div>
                </div>
                <!-- Tab panes -->
                <div class="tab-content">
                  <div class="tab-content" id="content_productos_buscar">  
                  <?php $i=0;
                    $paneles=0;
                    foreach ($lista_productos_ordenados as $productoItem):
                     if($i%9==0):
                      $paneles++;
                    ?><div class="tab-pane fade <?=($paneles==1)?'in active':'';?>" id="tab<?=$paneles;?>" role="tabpanel"><?php
                      endif;
                    ?>
                    <!-- Single-product start -->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="single-product">
                          <div class="product-img">
                            <?php
                              $date1 = new DateTime("now");
                              $date2 = new DateTime($productoItem['fecha_creacion']);
                              $diff = $date1->diff($date2);
                              //un proucto creado hace menos de 3 semanas es considerado nuevo
                             if (($diff->d)<21 &&
                                 ($diff->m)<1 &&
                                 ($diff->y)<1 ): ?>
                              <span class="pro-label new-label">Nuevo</span>
                            <?php endif ?>
                           
                            <span class="pro-price-2"><?php 
                                if ($productoItem['descuento']>0):
                                    
                                    echo "$".number_format(ceil($productoItem['precio']-(($productoItem['precio']*$productoItem['descuento'])/100)) ,0, "," ,".")."<s><small>$".number_format(ceil($productoItem['precio']) ,0, "," ,".")."</small></s>";
                                else:
                                   echo "$".number_format(ceil($productoItem['precio']) ,0, "," ,".");
                                endif; ?></span>
                            <a href="<?= base_url('productos/view/'.$productoItem['id'].'/'.str_replace("'","",str_replace('%','',$productoItem['nombre'])));?>"><img src="<?=$productoItem['foto_producto'] ?>" alt="" /></a>
                          </div>
                          <div class="product-info clearfix text-center">
                            <div class="fix">
                              <h4 class="post-title"><a href="#"><?=$productoItem['nombre'] ?></a></h4>
                            </div>
                            <!--<div class="fix">
                              <span class="pro-rating">
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                                <a href="#"><i class="zmdi zmdi-star-half"></i></a>
                                <a href="#"><i class="zmdi zmdi-star-half"></i></a>
                              </span>
                            </div>-->
                            <div class="product-action product-action-busca clearfix">

                              <a href="<?= base_url('productos/view/'.$productoItem['id'].'/'.str_replace("'","",str_replace('%','',$productoItem['nombre'])));?>" data-toggle="tooltip" data-placement="top" title="Ver Producto"><i class="zmdi zmdi-zoom-in"></i></a>
                              
                              <a href="#" data-toggle="tooltip" data-placement="top" title="Agregar al carrito"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Single-product end -->
                    <?php
                      $i++;
                      echo ($i%9==0)?'</div>':'';
                     endforeach;?>
                     <?= (count($lista_productos_ordenados)%2==0)?'':'</div>';?>

                     <?php if ($i%9!=0&&count($lista_productos_ordenados)%2==0 ): ?>
                       </div>
                       </div>
                     <?php endif ?>
                </div>
                <!--  pagination-->
                <div class="col-md-12 text-center grid-pagination">
                  <div class="pagination">
                    <nav aria-label="Page navigation ">
                      <ul id="pagination_productos" class="pagination_pag" role="tablist">
                        <li class="previous" onclick="goTo(1);">
                          <a href="#previous" aria-label="Previous">
                            <i class="zmdi zmdi-long-arrow-left"></i>
                          </a>
                        </li>
                        <?php
                          for($j=1;$j<=$paneles;$j++){
                            ?>
                            <li class="pagination_sel <?=($j==1)?'active':'';?>" <?=($j==1)?'id="first"':'';?>>
                              <a aria-controls="tab<?=$j;?>" data-toggle="tab" data-target="#tab<?=$j;?>" href="#tab<?=$j;?>" role="tab"><?=$j;?></a>
                            </li>
                        <?php
                          }
                         ?>
                        <li class="next" onclick="goTo(<?= ($paneles>1)?2:1;?>);">
                          <a href="#next" aria-label="Next">
                            <i class="zmdi zmdi-long-arrow-right"></i>
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div><!-- fin pagination -->
              </div>
              <!-- Shop-Content End -->
            </div>
          </div>
        </div>
      </div>
      <!-- PRODUCT-AREA END -->

  <!-- Fin producto destacado -->

  <?php 
    $menorPrecio = 0;
    $mayorPrecio = 0;
    if (count($lista_productos_ordenados)>= 1) {
        $menorPrecio = $lista_productos_ordenados[0]['precio'];
    }
    foreach ($lista_productos_ordenados as $value) {
      $precioProducto = 0;
      if ($value['descuento'] > 0 ) {
         $precioProducto = ceil($value['precio']-(($value['precio']*$value['descuento'])/100));
      }else{
        $precioProducto = $value['precio'];
      }

      if ($menorPrecio > $precioProducto) {
        $menorPrecio = $precioProducto;
      }

      if ($mayorPrecio < $precioProducto) {
        $mayorPrecio = $precioProducto;
      }
    }
   ?>

<script>
  $(document).ready(function(){
    /*----------------------------
      price-slider active
    ------------------------------*/   
      $( "#slider-range" ).slider({
        range: true,
        min: <?= $menorPrecio;?>,
        max: <?= $mayorPrecio;?>,
        values: [ <?= $menorPrecio;?>, <?= $mayorPrecio;?> ],
        slide: function( event, ui ) {
          $( "#amountBuscador" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
      });

      $( "#amountBuscador" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );

    /*---------------------------
      price-slider active
    ------------------------------ */
  });
</script>