
      <!-- HEADING-BANNER START -->
      <div class="heading-banner-area overlay-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="heading-banner">
                <div class="heading-banner-title">
                  <h2>Preguntas Frecuentes</h2>
                </div>
                <div class="breadcumbs pb-15">
                  <ul>
                    <li><a href="<?= base_url();?>">Home</a></li>
                    
                    <li>Preguntas Frecuentes</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- HEADING-BANNER END -->
      <!-- ABOUT-US-AREA START -->
      <div class="elements-accordions pt-80 pb-80">
        <div class="container"> 
          <div class="row">
           <h3 class="tab-title title-border mb-30">Preguntas Frecuentes</h3>
             <div class="accordion-2" id="accordion2" >
<div class="panel">
<div class="accordion-title">
<a  data-toggle="collapse" href="#accor-content-1"  >
¿Pregunta frecuente 1?
</a>
</div>
<div id="accor-content-1" class="panel-collapse collapse in"  >
<div class="accor-content">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mollis magna id sem varius viverra. Quisque varius, purus sit amet elementum consequat, est nisl scelerisque ex, volutpat posuere elit turpis a nisi. Aliquam erat volutpat. Integer ex urna, efficitur sit amet mauris eget, tincidunt posuere enim. Nunc felis ante, rhoncus ac dictum ut, faucibus id dolor. Aliquam condimentum, ex vel tincidunt sagittis.
</div>
</div>
</div>
<div class="panel">
<div class="accordion-title">
<a class="collapsed"  data-toggle="collapse" href="#accor-content-2"  >
¿Pregunta frecuente 2?
</a>
</div>
<div id="accor-content-2" class="panel-collapse collapse"  >
<div class="accor-content">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mollis magna id sem varius viverra. Quisque varius, purus sit amet elementum consequat, est nisl scelerisque ex, volutpat posuere elit turpis a nisi. Aliquam erat volutpat. Integer ex urna, efficitur sit amet mauris eget, tincidunt posuere enim. Nunc felis ante, rhoncus ac dictum ut, faucibus id dolor. Aliquam condimentum, ex vel tincidunt sagittis.
</div>
</div>
</div>
<div class="panel">
<div class="accordion-title">
<a class="collapsed"  data-toggle="collapse" href="#accor-content-3"  >
¿Pregunta frecuente 3?
</a>
</div>
<div id="accor-content-3" class="panel-collapse collapse"  >
<div class="accor-content">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mollis magna id sem varius viverra. Quisque varius, purus sit amet elementum consequat, est nisl scelerisque ex, volutpat posuere elit turpis a nisi. Aliquam erat volutpat. Integer ex urna, efficitur sit amet mauris eget, tincidunt posuere enim. Nunc felis ante, rhoncus ac dictum ut, faucibus id dolor. Aliquam condimentum, ex vel tincidunt sagittis.
</div>
</div>
</div>
<div class="panel">
<div class="accordion-title">
<a class="collapsed"  data-toggle="collapse" href="#accor-content-4"  >
¿Pregunta frecuente 4?
</a>
</div>
<div id="accor-content-4" class="panel-collapse collapse"  >
<div class="accor-content">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mollis magna id sem varius viverra. Quisque varius, purus sit amet elementum consequat, est nisl scelerisque ex, volutpat posuere elit turpis a nisi. Aliquam erat volutpat. Integer ex urna, efficitur sit amet mauris eget, tincidunt posuere enim. Nunc felis ante, rhoncus ac dictum ut, faucibus id dolor. Aliquam condimentum, ex vel tincidunt sagittis.
</div>
</div>
</div>
<div class="panel">
<div class="accordion-title">
<a class="collapsed"  data-toggle="collapse" href="#accor-content-5"  >
¿Pregunta frecuente 5?
</a>
</div>
<div id="accor-content-5" class="panel-collapse collapse"  >
<div class="accor-content">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mollis magna id sem varius viverra. Quisque varius, purus sit amet elementum consequat, est nisl scelerisque ex, volutpat posuere elit turpis a nisi. Aliquam erat volutpat. Integer ex urna, efficitur sit amet mauris eget, tincidunt posuere enim. Nunc felis ante, rhoncus ac dictum ut, faucibus id dolor. Aliquam condimentum, ex vel tincidunt sagittis.
</div>
</div>
</div>
</div>
          </div>
        </div>
      </div>
      <!-- ABOUT-US-AREA END -->