<footer>
				<!-- Footer-area start -->
				<div class="footer-area">
					<div class="container">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="single-footer">
									<h3 class="footer-title  title-border">Contáctenos</h3>
									<ul class="footer-contact">
										<li><span>Dirección :</span>28 de Septiembre,<br>Concepción, Chile</li>
										<li><span>Telefonos :</span>012345 - 123456789</li>
										<li><span>Email :</span>contacto@puya.cl</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
								<div class="single-footer">
									<h3 class="footer-title  title-border">Cuentas</h3>
									<ul class="footer-menu">
										
										<?php if(isset($_SESSION['cliente_data_puyasess']['logeado'])): ?>
											<li><a href="<?= base_url('user/perfil');?>"><i class="zmdi zmdi-dot-circle"></i>Mi Cuenta</a></li>
											<li><a href="<?= base_url('user/carro_compra');?>"><i class="zmdi zmdi-dot-circle"></i>Mi Carrito</a></li>
										<?php else: ?>
										<li><a href="<?= base_url('home/registro');?>"><i class="zmdi zmdi-dot-circle"></i>Login</a></li>

										<?php endif ;?>
										<li><a href="#"><i class="zmdi zmdi-dot-circle"></i>Términos y Condiciones</a></li>
										<!--<li><a href="#"><i class="zmdi zmdi-dot-circle"></i>Check out</a></li>-->
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="single-footer">
									<h3 class="footer-title  title-border">Acerca de PUYA</h3>
									<ul class="footer-menu">
										<li><a href="<?= base_url('quienes_somos');?>"><i class="zmdi zmdi-dot-circle"></i>Quienes Somos</a></li>
										<li><a href="<?= base_url('mision_vision_valores');?>"><i class="zmdi zmdi-dot-circle"></i>Mision, Vision y Valores</a></li>
										<li><a href="<?= base_url('preguntas_frecuentes');?>"><i class="zmdi zmdi-dot-circle"></i>Preguntas Frecuentes</a></li>
										 <?php foreach ($lista_categorias as $cateItem): ?>
									        <li><a href="<?= ($cateItem['tipo_categoria']==0)?base_url('categoria/view/'.$cateItem['id'].'/'.$cateItem['nombre']):base_url('servicio/view/'.$cateItem['id'].'/'.$cateItem['nombre']) ;?> "><i class="zmdi zmdi-dot-circle"></i><?= $cateItem['nombre'];?></a></li>
									    <?php endforeach ?>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
								
							</div>
						</div>
					</div>
				</div>
				<!-- Footer-area end -->
				<!-- Copyright-area start -->
				<div class="copyright-area">
					<div class="container">
						<div class="row">
							<div class="col-sm-6 col-xs-12">
								<div class="copyright">
									<p class="mb-0">&copy; <a href="#" target="_blank"> PUYA  </a> <?= date('Y');?>. Todos los Derechos Reservados.</p>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12">
								<div class="payment  text-right">
									<a href="#"><img src="<?= base_url();?>assets/img/payment/1.png" alt="" /></a>
									<a href="#"><img src="<?= base_url();?>assets/img/payment/2.png" alt="" /></a>
									<a href="#"><img src="<?= base_url();?>assets/img/payment/3.png" alt="" /></a>
									<a href="#"><img src="<?= base_url();?>assets/img/payment/4.png" alt="" /></a>
									<a href="#"><img src="<?= base_url();?>assets/img/payment/5.png" alt="" /></a>
									<a href="#"><img src="<?= base_url();?>assets/img/payment/6.png" alt="" /></a>
									<a href="#"><img src="<?= base_url();?>assets/img/payment/7.png" alt="" /></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Copyright-area start -->
			</footer>
			<!-- FOOTER END -->
			<!-- QUICKVIEW PRODUCT -->
			<div id="quickview-wrapper">
			   <!-- Modal -->
			   <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<div class="modal-body">
								<div class="modal-product">
									<div class="product-images">
										<div class="main-image images">
											<img alt="#" src="<?= base_url();?>assets/img/product/quickview-photo.jpg"/>
										</div>
									</div><!-- .product-images -->

									<div class="product-info">
										<h1>Aenean eu tristique</h1>
										<div class="price-box-3">
											<hr />
											<div class="s-price-box">
												<span class="new-price">$160.00</span>
												<span class="old-price">$190.00</span>
											</div>
											<hr />
										</div>
										<a href="shop.html" class="see-all">See all features</a>
										<div class="quick-add-to-cart">
											<form method="post" class="cart">
												<div class="numbers-row">
													<input type="number" id="french-hens" value="3">
												</div>
												<button class="single_add_to_cart_button" type="submit">Add to cart</button>
											</form>
										</div>
										<div class="quick-desc">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero.
										</div>
										<div class="social-sharing">
											<div class="widget widget_socialsharing_widget">
												<h3 class="widget-title-modal">Share this product</h3>
												<ul class="social-icons">
													<li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="zmdi zmdi-google-plus"></i></a></li>
													<li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="zmdi zmdi-twitter"></i></a></li>
													<li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="zmdi zmdi-facebook"></i></a></li>
													<li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="zmdi zmdi-linkedin"></i></a></li>
												</ul>
											</div>
										</div>
									</div><!-- .product-info -->
								</div><!-- .modal-product -->
							</div><!-- .modal-body -->
						</div><!-- .modal-content -->
					</div><!-- .modal-dialog -->
			   </div>
			   <!-- END Modal -->
			</div>
			<!-- END QUICKVIEW PRODUCT -->
</div>
		<!-- WRAPPER END -->
		<!--Scripts de la pagina-->
<!--<script src="<?= base_url(); ?>fun/jquery-3.0.0.js"></script>
<script src="<?= base_url(); ?>fun/bootstrap.min.js"></script>-->
<script src="<?= base_url(); ?>fun/jquery.Rut.min.js"></script>
<script src="<?= base_url(); ?>fun/notify.min.js"></script>
<script src="<?= base_url(); ?>fun/notify-metro.js"></script>
<script src="<?= base_url(); ?>fun/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>fun/localization/methods_es_CL.min.js"></script>
<script src="<?= base_url(); ?>fun/noty.min.js"></script>
<script src="<?= base_url(); ?>fun/date.js"></script>
<!-- all js here -->
<!-- jquery.meanmenu js -->
<script src="<?= base_url(); ?>fun/jquery.meanmenu.js"></script>
<!-- slick.min js -->
<script src="<?= base_url(); ?>fun/slick.min.js"></script>
<!-- jquery.treeview js -->
<script src="<?= base_url(); ?>fun/jquery.treeview.js"></script>
<!-- lightbox.min js -->
<script src="<?= base_url(); ?>fun/lightbox.min.js"></script>
<!-- jquery-ui js -->
<script src="<?= base_url(); ?>fun/jquery-ui.min.js"></script>
<!-- jquery.nivo.slider js -->
<script src="<?= base_url(); ?>fun/jquery.nivo.slider.js"></script>
<script src="<?= base_url(); ?>fun/home.js"></script>
<!-- jquery.nicescroll.min js -->
<script src="<?= base_url(); ?>fun/jquery.nicescroll.min.js"></script>
<!-- countdon.min js -->
<script src="<?= base_url(); ?>fun/countdon.min.js"></script>
<!-- wow js -->
<script src="<?= base_url(); ?>fun/wow.min.js"></script>
<!-- plugins js -->
<script src="<?= base_url(); ?>fun/plugins.js"></script>
<!-- main js -->
<script src="<?= base_url(); ?>fun/main_tmp.js"></script>

<script src="<?= base_url();?>fun/main.js"></script>
<?php if (isset($_SESSION['cliente_data_puyasess']['logeado']) && $_SESSION['cliente_data_puyasess']['logeado']): ?>
	<script>estado_login = true;</script>
<?php endif; ?>
<?php
if ($this->uri->segment(1) === "cliente" || $this->uri->segment(1) === "home" &&
	isset($_SESSION['cliente_data_puyasess']['logeado'])||
	$this->uri->segment(2) === "index" ||
	$this->uri->segment(2) === "login" ||
	$this->uri->segment(2) === "registro" ||
	$this->uri->segment(2) === "carro_compra" ) {
?>
<script src="<?= base_url();?>fun/cliente.js"></script>
<script src="<?= base_url();?>fun/jquery.smartWizard.min.js"></script>
<?php } ?>
<?php
if ($this->uri->segment(1) === "categoria" &&
	$this->uri->segment(2) === "view") {
?>
<script>
$(document).ready(function(){
    mostrarCategoriasAll();
});
</script>
<?php } ?>
<?php
if ($this->uri->segment(1) === "sub_categoria" &&
	$this->uri->segment(2) === "view") {
?>
<script>
$(document).ready(function(){
    mostrarSubCategoriasAll();
});
</script>
<?php } ?>

