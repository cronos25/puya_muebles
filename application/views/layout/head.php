<head>    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('res/favicon.ico');?>" type="image/x-icon">
    <title>PUYA Muebles</title>
    <!-- Place favicon.ico in the root directory -->

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    
    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <?= link_tag("est/bootstrap.min.css");?>
    <!-- animate css -->
    <?= link_tag("est/animate.css");?>
    <!-- jquery-ui.min css -->
    <?= link_tag("est/jquery-ui.min.css");?>
    <!-- meanmenu css -->
    <?= link_tag("est/meanmenu.min.css");?>
    <!-- nivo-slider css -->
    <?= link_tag("est/nivo-slider.css");?>
    <?= link_tag("est/preview.css");?>
    <!-- slick css -->
    <?= link_tag("est/slick.css");?>
    <!-- lightbox css -->
    <?= link_tag("est/lightbox.min.css");?>
    <!-- material-design-iconic-font css -->
    <?= link_tag("est/material-design-iconic-font.css");?>
    <!-- All common css of theme -->
    <?= link_tag("est/default.css");?>
    <!-- style css -->
    <?= link_tag("est/style.css");?>
    <!-- shortcode css -->
    <?= link_tag("est/shortcode.css");?>
    <!-- responsive css -->
    <?= link_tag("est/responsive.css");?>
    <!-- modernizr css -->
    <?= link_tag("est/flipclock.css");?>
    <?= link_tag("est/noty.css");?>
    <?= link_tag("est/lightslider.css");?>
    <?= link_tag("est/flickity.css");?>

    <script src="<?= base_url(); ?>fun/jquery-3.0.0.js"></script>
    <script src="<?= base_url(); ?>fun/modernizr-2.8.3.min.js"></script>
    <script src="<?= base_url(); ?>fun/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>fun/flipclock.js"></script>
    <script src="<?= base_url(); ?>fun/lightslider.js"></script>
    <script src="<?= base_url(); ?>fun/flickity.pkgd.min.js"></script>
    <script src="<?= base_url(); ?>fun/jquery.countdown.js"></script>
</head>
  <body>
<!-- WRAPPER START -->
        <div class="wrapper">