<!-- MAIN-MENU START -->
        <div class="menu-toggle hamburger hamburger--emphatic hidden-xs">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
        <div class="main-menu  hidden-xs">
          <nav>
            <ul>
              <li><a href="<?= base_url();?>">home</a></li>
              <li><a href="#">productos</a>
                <div class="mega-menu menu-scroll">
                  <div class="table">
                    <div class="table-cell">
                      <?php foreach ($lista_categorias as $categoriaItem): ?>
                        <?php if ($categoriaItem['tipo_categoria']==0): ?>
                          <div class="half-width">
                            <ul>
                              <li class="menu-title"><?= $categoriaItem['nombre'];?></li>
                              <?php foreach ($lista_productos_ordenados as $itemProd): ?>
                                <?php if ($itemProd['categoria_id']==$categoriaItem['id']): ?>
                                  <li><a href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>"><?=$itemProd['nombre'];?></a></li>
                                <?php endif;?>
                              <?php endforeach;?>
                            </ul>
                          </div>
                        <?php endif;?>
                      <?php endforeach;?>

                      <div class="full-width">
                        <div class="mega-menu-img">
                          <?php foreach ($lista_productos_destacados as $prodDest): ?>
                             <a href="<?= base_url('productos/view/'.$prodDest['id'].'/'.str_replace("'","",str_replace('%','',$prodDest['nombre'])));?>" class="col-md-3"><img src="<?=$prodDest['foto_producto']?>" alt="" /></a>
                          <?php endforeach ?>
                         
                        </div>
                      </div>
                      <!--
                      <div class="half-width">
                        <ul>
                          <li class="menu-title">PUYA Diseño</li>
                          <li><a href="#">Servicio 1</a></li>
                          <li><a href="#">Servicio 2</a></li>
                          <li><a href="#">Servicio 3</a></li>
                          <li><a href="#">Servicio 4</a></li>
                          <li><a href="#">Servicio 5</a></li>
                          <li class="menu-title">PUYA Marker</li>
                          <li><a href="#">Servicio 1</a></li>
                          <li><a href="#">Servicio 2</a></li>
                        </ul>
                      </div>-->
                      <!--<div class="pb-80"></div>-->

                    </div>
                  </div>
                </div>
              </li>
              <li><a href="<?= base_url('quienes_somos'); ?>">Quienes Somos</a></li>
              <li><a href="<?= base_url('contacto'); ?>">Contáctanos</a></li>
              <li><a href="<?= base_url('catalogo'); ?>">Catálogo</a></li>
            </ul>
          </nav>
        </div>
        <!-- MAIN-MENU END -->