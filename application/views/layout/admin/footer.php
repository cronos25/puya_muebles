</div><!-- /row -->
            </div><!-- /container -->
        </div>
        <!-- END BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
        <div class="push-sticky-footer"></div>
    </div><!-- /wrapper -->
<!-- FOOTER -->
    <footer class="footer">
        &copy; <?= date('Y') ?> Asdev - &copy; <?= date('Y') ?> PuyaMubles.
    </footer>
    <!-- END FOOTER -->
<!--Scripts de la pagina-->
<!-- Javascript -->
<script src="<?= base_url(); ?>assets/js/jquery.Rut.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/modernizr.js"></script>  
<script src="<?= base_url(); ?>assets/admin/js/king-common.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/stat/jquery.easypiechart.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/raphael-2.1.0.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/stat/flot/jquery.flot.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/stat/flot/jquery.flot.resize.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/stat/flot/jquery.flot.time.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/stat/flot/jquery.flot.pie.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/stat/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/jquery.sparkline.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/jquery.masked-input.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/bootstrap-multiselect.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/datatable/jquery.dataTables.bootstrap.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/datatable/jquery.dataTables.bootstrap.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/jquery.mapael.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/maps/usa_states.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/king-chart-stat.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/bootstrap-tour.custom.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/daterangepicker.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/moment.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/king-table.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/bootstrap-tagsinput.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/king-components.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/king-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/interno/admin.js"></script>

