
	<!-- WRAPPER -->
	<div class="wrapper">
<!-- TOP GENERAL ALERT -->
		<!--<div class="alert alert-danger top-general-alert">
			<span>If you <strong>can't see the logo</strong> on the top left, please reset the style on right style switcher (for upgraded theme only).</span>
			<a type="button" class="close">&times;</a>
		</div>-->
		<!-- END TOP GENERAL ALERT -->

		<!-- TOP BAR -->
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<!-- logo -->
					<div class="col-md-2 logo logo_admin">
						<a href="<?= site_url('admin');?>">PuyaMuebles Admin</a>
						<h1 class="sr-only">PuyaMuebles Admin</h1>
					</div>
					<!-- end logo -->
					<div class="col-md-10">
						<div class="row">
							<div class="col-md-3">
								<!-- search box -->
								<div id="tour-searchbox" class="input-group searchbox">
									<input type="search" class="form-control" placeholder="enter keyword here...">
									<span class="input-group-btn">
										<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
									</span>
								</div>
								<!-- end search box -->
							</div>
							<div class="col-md-9">
								<div class="top-bar-right">
									<!-- responsive menu bar icon -->
									<a href="#" class="hidden-md hidden-lg main-nav-toggle"><i class="fa fa-bars"></i></a>
									<!-- end responsive menu bar icon -->

									<!-- logged user and the menu -->
									<div class="logged-user">
										<div class="btn-group">
											<a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
												<!--<img src="assets/img/user-avatar.png" />-->
												<span class="name">Bienvenido <?=$_SESSION['admin_ecom_data']['admin_email'];?></span> <span class="caret"></span>
											</a>
											<ul class="dropdown-menu" role="menu">
												<!--<li>
													<a href="#">
														<i class="fa fa-user"></i>
														<span class="text">Profile</span>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-cog"></i>
														<span class="text">Settings</span>
													</a>
												</li>-->
												<li>
													<a id="btn_end_session" href="#">
														<i class="fa fa-power-off"></i>
														<span class="text">Logout</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
									<!-- end logged user and the menu -->
								</div><!-- /top-bar-right -->
							</div>
						</div><!-- /row -->
					</div>
				</div><!-- /row -->
			</div><!-- /container -->
		</div><!-- /top -->
		