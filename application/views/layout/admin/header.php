<!-- BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
<div class="bottom">
	<div class="container">
		<div class="row">
			<!-- left sidebar -->
			<div class="col-md-2 left-sidebar">

				<!-- main-nav -->
				<nav class="main-nav">
					
					<ul class="main-menu">
						<li class="active"><a href="<?= site_url('admin');?>"><i class="fa fa-dashboard fa-fw"></i><span class="text">Home</span></a></li>
						<!--<li ><a href="#" class="js-sub-menu-toggle"><i class="fa fa-clipboard fa-fw"></i><span class="text">Pages</span>
							<i class="toggle-icon fa fa-angle-left"></i></a>
							<ul class="sub-menu ">
								<li ><a href="page-profile.html"><span class="text">Profile</span></a></li>
								<li ><a href="page-invoice.html"><span class="text">Invoice</span></a></li>
								<li ><a href="page-knowledgebase.html"><span class="text">Knowledge Base</span></a></li>
								<li ><a href="page-inbox.html"><span class="text">Inbox</span></a></li>
								<li ><a href="page-register.html"><span class="text">Register</span></a></li>
								<li ><a href="page-login.html"><span class="text">Login</span></a></li>
								<li ><a href="page-404.html"><span class="text">404</span></a></li>
								<li ><a href="page-blank.html"><span class="text">Blank Page</span></a></li>
							</ul>
						</li>--><li ><a href="<?= site_url('admin/banner_home');?>"><i class="fa fa-file-photo-o fa-fw"></i><span class="text">Imagenes Home</span></a></li>
						<li ><a href="<?= site_url('admin/user');?>"><i class="fa fa-group fa-fw"></i><span class="text">Usuarios</span></a></li>
						<li ><a href="<?= site_url('admin/region');?>"><i class="fa fa-map fa-fw"></i><span class="text">Regiones</span></a></li>
						<li ><a href="<?= site_url('admin/provincia');?>"><i class="fa fa-map fa-fw"></i><span class="text">Provincias</span></a></li>
						<li ><a href="<?= site_url('admin/comuna');?>"><i class="fa fa-map-marker fa-fw"></i><span class="text">Comunas</span></a></li>
						<li ><a href="<?= site_url('admin/precio_envio');?>"><i class="fa fa-truck fa-fw"></i><span class="text">Precio de envio</span></a></li>
						<li ><a href="<?= site_url('admin/cliente');?>"><i class="fa fa-group fa-fw"></i><span class="text">Clientes</span></a></li>
						<li ><a href="<?= site_url('admin/categoria');?>"><i class="fa fa-th fa-fw"></i><span class="text">Categorias</span></a></li>
						<li ><a href="<?= site_url('admin/sub_categoria');?>"><i class="fa fa-th fa-fw"></i><span class="text">Sub Categorias</span></a></li>
						<li ><a href="<?= site_url('admin/marca');?>"><i class="fa fa-tag fa-fw"></i><span class="text">Marcas</span></a></li>
						<li ><a href="<?= site_url('admin/color');?>"><i class="fa fa-cutlery fa-fw"></i><span class="text">Color</span></a></li>
						<li ><a href="<?= site_url('admin/productos');?>"><i class="fa fa-cube fa-fw"></i><span class="text">Productos</span></a></li>
						<li ><a href="<?= site_url('admin/stack');?>"><i class="fa fa-cubes fa-fw"></i><span class="text">Stacks</span></a></li>
						<li ><a href="<?= site_url('admin/newsletter');?>"><i class="fa fa-at fa-fw"></i><span class="text">Newsletter</span></a></li>
						<li ><a href="<?= site_url('admin/carro_compra');?>"><i class="fa fa-shopping-cart fa-fw"></i><span class="text">Carros de compra</span></a></li>
						<li ><a href="<?= site_url('admin/venta');?>"><i class="fa fa-shopping-cart fa-fw"></i><span class="text">Ventas</span></a></li>
						<li ><a href="<?= site_url('admin/codigo_promocion');?>"><i class="fa fa-ticket fa-fw"></i><span class="text">Codigos de Promocion</span></a></li>
						<li ><a href="<?= site_url('admin/promocion_temporal');?>"><i class="fa fa-clock-o fa-fw"></i><span class="text">Promocion Temporal</span></a></li>
						<li ><a href="<?= site_url('admin/oferta_especial');?>"><i class="fa fa-clock-o fa-fw"></i><span class="text">Oferta especial</span></a></li>
						<li ><a href="<?= site_url('admin/distribuidor');?>"><i class="fa fa-archive fa-fw"></i><span class="text">Distribuidores</span></a></li>
						<!--
						<li ><a href="<?= site_url('admin/especification');?>"><i class="fa fa-th-list fa-fw"></i><span class="text">Especificacion Categoria</span></a></li>
						<li ><a href="<?= site_url('admin/especification_product');?>"><i class="fa fa-list fa-fw"></i><span class="text">Especificacion Producto</span></a></li>
						
						<li ><a href="<?= site_url('admin/sales');?>"><i class="fa fa-area-chart fa-fw"></i><span class="text">Ventas</span></a></li>-->
					</ul>
				</nav><!-- /main-nav -->

				<div class="sidebar-minified js-toggle-minified">
					<i class="fa fa-angle-left"></i>
				</div>
			</div>
			<!-- end left sidebar -->

			<!-- content-wrapper -->