<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('res/favicon.ico');?>" type="image/x-icon">
    <title><?= $titulo; ?></title>

    <!-- Bootstrap Core CSS -->
    <?= link_tag("assets/css/bootstrap.min.css");?>
    <!-- Custom Fonts -->
    <?= link_tag("assets/css/font-awesome.min.css");?>
    <?= link_tag("assets/admin/css/main.css");?>
    <?= link_tag("assets/admin/css/fileinput.css");?>
    <?= link_tag("assets/admin/css/cropper.css");?>
    <?= link_tag("assets/admin/css/jquery-te-1.4.0.css");?>
    <?= link_tag("assets/admin/css/admin.css");?>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <script src="<?= base_url(); ?>assets/js/jquery-3.0.0.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/jquery.gritter.min.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/fileinput.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/exif.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/cropper.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/jquery-te-1.4.0.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/localization/methods_es_CL.min.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/main_admin.js"></script>
    <script src="<?= base_url(); ?>assets/admin/js/bootstrap-switch.min.js"></script>   
    
<script src="<?= base_url(); ?>assets/admin/js/select2.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/bootstrap-multiselect.js"></script>
</head>
