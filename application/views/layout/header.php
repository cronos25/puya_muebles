
<!--el header se cierra en el archivo nav.php-->
<!-- Mobile-header-top End -->
<!-- HEADER-AREA START -->
<header id="sticky-menu" class="header">
	<div class="header-area">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4 col-xs-7">
					<div class="logo text-center">
						<a href="<?=base_url();?>"><img src="<?=base_url();?>assets/img/logo/logo.png" alt="" /></a>
					</div>
				</div>
				<div class="col-sm-4 col-xs-5">
					<div class="mini-cart text-right">
						<ul>
							<li>
								<a class="cart-icon" href="<?= base_url('user/perfil');?>">
									<i class="zmdi zmdi-account"></i>
								</a>
								<?php if(isset($_SESSION['cliente_data_puyasess']['logeado'])){ ?>
								<div class="mini-cart-brief text-left">
									<div class="cart-items">
										<p class="mb-0">Bienvenido <span><?= $_SESSION['cliente_data_puyasess']['user_nombre'] ?></span> </p>
									</div>

									<div class="cart-totals">
										<h5 class="mb-0">Mis Datos </h5>
										<h5 class="mb-0">Mis Compras </h5>
									</div>
									<div class="cart-bottom  clearfix">
										<form action="<?= base_url("cliente/cerrarSession"); ?>" method="POST">
					                      <input type="hidden" name="cleardata" value="1">
					                      <button type="submit" class="button-one floatright" data-text="Cerrar Sesión">Cerrar Sesión</button>
					                    </form>
									</div>
								</div>
								<?php }else{ ?>
								<div class="mini-cart-brief text-left">
									<div class="cart-items">
										<p class="mb-0">Bienvenido a Puya</p>
									</div>
									<div class="cart-bottom  clearfix">
										<a href="<?= base_url('home/registro');?>" class="button-one floatright" data-text="Iniciar Sesión">Iniciar Sesión</a>
									</div>
								</div>
								<?php } ?>
							</li>
							<li>
								<?php if(isset($_SESSION['cliente_data_puyasess']['logeado'])){ ?>
								<a class="cart-icon" href="<?= base_url('user/carro_compra');?>">
									<i class="zmdi zmdi-shopping-cart"></i>
									<span><?= (isset($_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro']))?$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro']:0; ?></span>
								</a>
								<!--<div class="mini-cart-brief text-left">
									<div class="cart-items">
										<p class="mb-0">Usted tiene <span><?= (isset($_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro']))?$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro']:0; ?> items</span> en su carrito</p>
									</div>
									<div class="all-cart-product clearfix">
										<div class="single-cart clearfix">
											<div class="cart-photo">
												<a href="#"><img src="<?=base_url();?>assets/img/cart/1.jpg" alt="" /></a>
											</div>
											<div class="cart-info">
												<h5><a href="#">Silla Bonita</a></h5>
												<p class="mb-0">Precio : $ 100.000</p>
												
												<span class="cart-delete"><a href="#"><i class="zmdi zmdi-close"></i></a></span>
											</div>
										</div>
										<div class="single-cart clearfix">
											<div class="cart-photo">
												<a href="#"><img src="<?=base_url();?>assets/img/cart/2.jpg" alt="" /></a>
											</div>
											<div class="cart-info">
												<h5><a href="#">Nombre producto</a></h5>
												<p class="mb-0">Precio : $ 300.000</p>
												<span class="cart-delete"><a href="#"><i class="zmdi zmdi-close"></i></a></span>
											</div>
										</div>
									</div>
									<div class="cart-totals">
										<h5 class="mb-0">Total <span class="floatright">$400.000</span></h5>
									</div>
									<div class="cart-bottom  clearfix">
										<a href="<?= base_url('home/registro?next='.'user/carro_compra');?>" class="button-one floatleft text-uppercase" data-text="Ver Carrito">Ver Carrito</a>
										<a href="#" class="button-one floatright text-uppercase" data-text="Salir">Salir</a>
									</div>
								</div>-->
								<?php }else{ ?>
								<a class="cart-icon" href="<?= base_url('home/registro?next='.'user/carro_compra');?>">
									<i class="zmdi zmdi-shopping-cart"></i>
								</a>
								<?php } ?>
								
							</li>
							<li>
								<a class="cart-icon" href="<?= base_url('home/buscador');?>">
									<i class="zmdi zmdi-search"></i>
								</a>											
							</li>
										
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $vista_nav;?>
</header>
<!-- HEADER-AREA END -->