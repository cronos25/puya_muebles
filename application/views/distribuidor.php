



<div class="container">

  <div class="row container-distribuidores">

      <div class="col-md-12">

        <h2>Nuestros distribuidores</h2>

      </div>

      <!-- Box distribuidor -->
<?php foreach ($lista_distribuidores as $distribuidorItem): ?>
    <div class="col-md-6 distribuidor">

        <div class="row">

          <div class="col-md-2">

            <img src="<?= $distribuidorItem['imagen'] ?>" alt="" class="img img-responsive">

          </div>

          <div class="col-md-3">

            <a  class="name-distribuidor"  target="_blank" href="<?=makeClickableLinks($distribuidorItem['link'])?>" > <h4><?= $distribuidorItem['nombre'] ?></h4></a>

          </div>

          <div class="col-md-4">

            <div class="descripcion-distribuidor">

              <?= $distribuidorItem['descripcion'] ?>

            </div>
          </div>

          <div class="col-md-3 text-right">

            <div class="title-estado">

              Estado

            </div>
            <?php if ($distribuidorItem['autorizado'] == 0): ?>
              <div class="estado-distribuidor autorizado">

                Autorizado

              </div>
            <?php else: ?>
              <div class="estado-distribuidor no-autorizado">

                  No autorizado

              </div>
            <?php endif ?>
          </div>
        </div>

      </div><!-- fin box distribuidor  -->
<?php endforeach ?>
      
</div>



</div>

