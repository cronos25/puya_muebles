<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="banner-area">
  <div class="container-fluid-0">
    <div class="row container-fluid-0">
    <?php foreach ($lista_categorias as $cateItem): ?>
        <div class="col-lg-12 container-fluid-0">
          <!-- Single-banner start -->
          <div class="single-banner banner-1 banner-4">
            <?php  ?>
            <a class="banner-thumb" href="<?= ($cateItem['tipo_categoria']==0)?base_url('categoria/view/'.$cateItem['id'].'/'.$cateItem['nombre']):base_url('servicio/view/'.$cateItem['id'].'/'.$cateItem['nombre']) ;?> "><img src="<?= $cateItem['imagen'];?>" alt="" /></a>
            <!--<span class="pro-label new-label">Nuevos Productos</span>-->
          </div>
          <!-- Single-banner end -->          
        </div>
    <?php endforeach ?>
    
    </div>
  </div>
</div>
