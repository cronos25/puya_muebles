<!--"id":,
"nombre":,
"descripcion":,
"precio":,
"foto_etiqueta":"",
"foto_producto":"",
"foto_home":"",
"sku":"",
"descuento":"",
"envio_gratis":"",
"medida_s":"",
"medida_m":"",
"medida_l":"",
"medida_xl":"",
"categoria_id":"",
"color":,
"marca_id":"",
"stock":"",
"tipo_entrega":"",
"regalos":"",
"categoria":"",
"marca":""
"imagen_categoria":""
-->
<!-- HEADING-BANNER START -->
<div class="heading-banner-area-muebles overlay-bg" style="background: rgba(0, 0, 0, 0) url(<?= $arrayProducto['imagen_categoria'] ?>) no-repeat scroll center center / cover;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading-banner">
					<div class="heading-banner-title">
						<h2><?= $arrayProducto['categoria'];?></h2>
					</div>
					<div class="breadcumbs pb-15">
						<ul>
							<li><a href="<?= base_url();?>">Home</a></li>
							<li><a href="<?= base_url('categoria/view/'.$arrayProducto['categoria_id'].'/'.$arrayProducto['categoria']);?>"><?= $arrayProducto['categoria'];?></a></li>
							<li><?= $arrayProducto['nombre']; ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- HEADING-BANNER END -->
<div class="product-area pt-80">
	<div class="container-fluid">
		<div class="row">
		<div class="col-md-9 col-sm-12 col-xs-12">		
				<div class="row shop-list single-pro-info">
					<!-- Single-product start -->
					<div class="col-lg-12">
						<div class="single-product clearfix">
							<!-- Single-pro-slider Big-photo start -->
							<div class="single-pro-slider single-big-photo view-lightbox slider-for">
								<div>
									<img src="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_producto']); ?>" alt="" />
									<a class="view-full-screen" href="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_producto']); ?>"  data-lightbox="roadtrip" data-title="<?= $arrayProducto['nombre']; ?>">
										<i class="zmdi zmdi-zoom-in"></i>
									</a>
								</div>
								<?php if (!empty($arrayProducto['foto_etiqueta'])): ?>
								<div>
									<img src="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_etiqueta']); ?>" alt="" />
									<a class="view-full-screen" href="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_etiqueta']); ?>"  data-lightbox="roadtrip" data-title="<?= $arrayProducto['nombre']; ?>">
										<i class="zmdi zmdi-zoom-in"></i>
									</a>
								</div>
								<?php endif ?>
								<?php if (!empty($arrayProducto['foto_home'])): ?>
								<div>
									<img src="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_home']); ?>" alt="" />
									<a class="view-full-screen" href="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_home']); ?>"  data-lightbox="roadtrip" data-title="<?= $arrayProducto['nombre']; ?>">
										<i class="zmdi zmdi-zoom-in"></i>
									</a>
								</div>
								<?php endif ?>
								<!--
								<div>
									<img src="<?= base_url('assets/')?>img/p2.jpg" alt="" />
									<a class="view-full-screen" href="<?= base_url('assets/')?>img/p2.jpg"  data-lightbox="roadtrip" data-title="My caption">
										<i class="zmdi zmdi-zoom-in"></i>
									</a>
								</div>-->
								
							</div>	
							<!-- Single-pro-slider Big-photo end -->										
							<div class="product-info">
								<div class="fix">
									<h4 class="post-title floatleft"><?= $arrayProducto['nombre']; ?></h4>
									<!--<span class="pro-rating floatright">
										<a href="#"><i class="zmdi zmdi-star"></i></a>
										<a href="#"><i class="zmdi zmdi-star"></i></a>
										<a href="#"><i class="zmdi zmdi-star"></i></a>
										<a href="#"><i class="zmdi zmdi-star-half"></i></a>
										<a href="#"><i class="zmdi zmdi-star-half"></i></a>
										<span>( 27 Valoración )</span>
									</span>-->
								</div>
								<div class="fix">
									<h4 class="post-title floatleft"><?= $arrayProducto['sku']; ?></h4>
								</div>
								<div class="fix">
									<h4 class="post-title floatleft">Marca: <?= $arrayProducto['marca']; ?></h4>
								</div>
								<div class="fix">
									<h4 class="post-title floatleft">Stock: <?= $arrayProducto['stock']; ?></h4>
								</div>
								<div class="fix mb-20">
									<span class="pro-price"><?php 
					                      if ($arrayProducto['descuento']>0):
					                          
					                          echo "$".number_format(ceil($arrayProducto['precio']-(($arrayProducto['precio']*$arrayProducto['descuento'])/100)) ,0, "," ,".")."<s><small>$".number_format(ceil($arrayProducto['precio']) ,0, "," ,".")."</small></s>";
					                      else:
					                      	 echo "$".number_format(ceil($arrayProducto['precio']) ,0, "," ,".");
					                      endif; ?></span>
								</div>
								<div class="product-description">
									<?= $arrayProducto['descripcion']; ?>
								</div>
								
								<?php
									if(!isset($_SESSION['cliente_data_puyasess']['logeado'])){ ?>
										<?php if ($arrayProducto['is_stack']==0): ?>
											<!-- color start -->								
											<div class="color-filter single-pro-color mb-20 clearfix">
												<ul>
													<li><span class="color-title text-capitalize">colores disponibles</span></li>
													<?php foreach ($lista_colores as $colorItem): ?>
														
													<li><span class="color" style="background: <?= $colorItem['hexa'] ?>;" title="<?= $colorItem['color']?>"></span></li>

													<?php endforeach ?>
													<!--<li class="active"><a href="#"><span class="color color-1"></span></a></li>-->
												</ul>
											</div>
										<?php endif ?>
										<?php if ($arrayProducto['stock']>0): ?>
											<a href="<?= base_url('home/registro?next='.'productos/view/'.$arrayProducto['id'].'/'.str_replace("'","",str_replace('%','',$arrayProducto['nombre'])))?>" data-text="Agregar al Carrito" class="button-one submit-button mt-15 carritoboton">
												<!--<i class="fa fa-shopping-cart"></i>-->
												Agregar al carro
											</a>
										<?php else: ?>
											<button type="button" data-text="Agregar al Carrito" class="button-one submit-button mt-15 carritoboton">
												<!--<i class="fa fa-shopping-cart"></i>-->
												Producto Agotado
											</button>
										<?php endif ?>

									<?php }else{?>
										<?php if ($arrayProducto['stock']>0): ?>
											<form id="form_add_item_shoppingcart" role="form" class="form-inline">
											<?php if ($arrayProducto['is_stack']==0): ?>
											
											<!-- color start -->								
											<div class="color-filter single-pro-color mb-20 clearfix">
												<ul>
													<li><span class="color-title text-capitalize">color</span></li>
													<?php 
													$i = 0;
													foreach ($lista_colores as $colorItem): ?>

													<li <?=($i==0)?'class="active"':'';?>>
														<input type="radio" name="color_data" value="<?= $colorItem['id_color'] ?>" class="hide" checked="<?=($i==0)?'checked':'false';?>" >
														<span class="color selector_color" title="<?= $colorItem['color'] ?>" style="background: <?= $colorItem['hexa'] ?>;"></span></li>
												<?php
													$i++;
												 endforeach;?>
													
												</ul>
											</div>
											<!-- color end -->
											<?php endif ?>
											<!-- color end -->
											<div class="clearfix">
											<input type="hidden" name="producto_data" value="<?=$arrayProducto['id'];?>">

											<?php if ($arrayProducto['stock']>0): ?>
												<div class="cart-plus-minus">
			                                      <input type="text" value="1" data-max-value="<?=$arrayProducto['stock'];?>" readonly="read" name="cantidad_data" class="cart-plus-minus-box prod_spinner_cantidad">
			                                    </div>
											<?php endif ?>

											<?php if ($arrayProducto['stock']>0): ?>
												<button type="button" data-text="Agregar al Carrito" class="button-one submit-button mt-15 carritoboton agregaProdCarro" data-targe="8">
													<!--<i class="fa fa-shopping-cart"></i>-->
													Agregar al carro
												</button>
											<?php else: ?>
												<button type="button" class="btn  btn-add btn-no-disp" title="">
													<!--<i class="fa fa-shopping-cart"></i>-->
													Producto Agotado
												</button>
											<?php endif ?>
											</div>
										</form>
									<?php else: ?>
										<form role="form" class="form-inline">
											<?php if ($arrayProducto['is_stack']==0): ?>

											<div class="entry-input">
												<div class="title-input"><span class="title-select-view">Color</span>
											<!--<input class="input-info" name="" value="Chocolate" type="text">-->
												</div>
											</div>
											<?php endif ?>

											<div class="title-input"><span class="title-select-view"> Cantidad</span>
											
											</div>

											<div class="text-right">
												<button type="button" class="btn btn-no-disp" title="Agregar al carro">
													<!--<i class="fa fa-shopping-cart"></i>-->
													Producto Agotado
												</button>
											</div>
										</form>
									<?php endif ?>

								<?php } ?>

								<!-- Single-pro-slider Small-photo start -->
								<div class="single-pro-slider single-sml-photo slider-nav">
									<div>
										<img src="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_producto']); ?>" alt="" />
									</div>
									<?php if (!empty($arrayProducto['foto_etiqueta'])): ?>
									<div>
										<img src="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_etiqueta']); ?>" alt="" />
									</div>
									<?php endif ?>
									<?php if (!empty($arrayProducto['foto_home'])): ?>
									<div>
										<img src="<?= str_replace('/assets/uploads/productos','/show/img',$arrayProducto['foto_home']); ?>" alt="" />
									</div>
									<?php endif ?>
									<!--<div>
										<img src="<?= base_url('assets/')?>img/p2.jpg" alt="" />
									</div>-->
								</div>
								<!-- Single-pro-slider Small-photo end -->
								
							</div>
							<!-- descripcion-->
							<div class="col-xs-12 bg-dark-white pt-30 pb-30">
								
								<div class="single-pro-tab">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<div class="single-pro-tab-menu">
								<!-- Nav tabs -->
								<ul class="">
									<li class="active"><a href="#description" data-toggle="tab">Descripción</a></li>												
									<li><a href="#information" data-toggle="tab">Especificaciones</a></li>
									<!--<li><a href="#reviews"  data-toggle="tab">Calificaciones</a></li>-->
									
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="description">
									<div class="pro-tab-info pro-description">
										<h3 class="tab-title title-border mb-30">Descripcion del Producto</h3>
										<?= $arrayProducto['descripcion']; ?>
										<br>
										<?= $arrayProducto['regalos']; ?>
									</div>
								</div>
								
								<div class="tab-pane" id="information">
									<div class="pro-tab-info pro-information">
										<h3 class="tab-title title-border mb-30">Informacion Tecnica del Producto</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
									</div>											
								</div>
								<!--<div class="tab-pane" id="reviews">
									<div class="pro-tab-info pro-reviews">
									<div class="leave-review">
											<h3 class="tab-title title-border mb-30">Califica y Comenta</h3>
											<div class="your-rating mb-30">
												<p class="mb-10"><strong>Su calificación</strong></p>
												<span>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
												</span>
												<span class="separator">|</span>
												<span>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
												</span>
												<span class="separator">|</span>
												<span>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
												</span>
												<span class="separator">|</span>
												<span>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
												</span>
												<span class="separator">|</span>
												<span>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
													<a href="#"><i class="zmdi zmdi-star-outline"></i></a>
												</span>
											</div>
											<div class="reply-box">
												<form action="#">
													<div class="row">
														<div class="col-md-6">
															<input type="text" placeholder="Su nombre..." name="name" />
														</div>
														<div class="col-md-6">
															<input type="text" placeholder="Asunto..." name="name" />
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<textarea class="custom-textarea" name="message" placeholder="Su comentario..." ></textarea>
															<button type="submit" data-text="Subir Comentario" class="button-one submit-button mt-20">Subir cometario</button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="customer-review mb-60">
											<h3 class="tab-title title-border mb-30 pt-20">Comentarios</h3>
											<ul class="product-comments clearfix">
												<li>
													
													<div class="pro-reviewer-comment">
														<div class="fix">
															<div class="floatleft mbl-center">
																<h5 class="text-uppercase mb-0"><strong>Javier Machuca</strong></h5>
																<p class="reply-date">27 Jun, 2017</p>
															</div>
															
														</div>
														<p class="mb-0">Me salio weno el sillon, me sente y no se desarmo, aguanto bien mi peso. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
														<hr>
													</div>
												</li>
												<li>
													
													<div class="pro-reviewer-comment">
														<div class="fix">
															<div class="floatleft mbl-center">
																<h5 class="text-uppercase mb-0"><strong>Javier Machuca</strong></h5>
																<p class="reply-date">27 Jun, 2017</p>
															</div>
															
														</div>
														<p class="mb-0">Me salio weno el sillon, me sente y no se desarmo, aguanto bien mi peso. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
														<hr>
													</div>
												</li>
												
											</ul>
										</div>
										
									</div>		
								</div>-->
							</div>									
						</div>
					</div>
				</div>
							</div>
							<!--fin descripcion-->
						</div>
						
					</div>
					<!-- Single-product end -->
				</div>
				<!-- single-product-tab start -->
				
				<!-- single-product-tab end -->
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12 mt-tab-30">
				<!-- Widget-Categories start -->
				<aside class="widget widget-categories  mb-30">
					<div class="widget-title">
						<h4>Categorias</h4>
					</div>
					<div id="cat-treeview"  class="widget-info product-cat boxscrol2">
						<ul>
							<?php foreach ($lista_sub_categorias as $subItem): ?>
								<li><span><?= $subItem['nombre']; ?></span>
									<ul>
										<?php 
									 $f = 0; //contador para filas par o impar
									 $p = 0; //contador para productos
									 $r = 0; //contador para filas
									 //foreach ($lista_productos as $itemProd):
									 foreach ($lista_productos as $itemProd): 
										 if ($subItem['id'] == $itemProd['sub_categoria_id'] ):
									 	 ?>
											<li><a href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>"><?= $itemProd['nombre'] ?></a></li>
										<?php 
										$f++;
										endif;
									endforeach ;?>
									</ul>
								</li>
							<?php endforeach;?>
							<!--<li><span>Sillas</span>
								<ul>
									<li><a href="#">Silla 1</a></li>
									<li><a href="#">Silla 2</a></li>
									<li><a href="#">Silla 3</a></li>
									<li><a href="#">Silla 4</a></li>
									<li><a href="#">Silla 5</a></li>
								</ul>
							</li>  -->
						</ul>
					</div>
				</aside>
				<!-- Widget-categories end -->
				<!-- Widget-product start -->
				<aside class="widget widget-product mb-30">
					<div class="widget-title">
						<h4>Nuevos Productos</h4>
					</div>
					<div class="widget-info sidebar-product clearfix">
						<?php 
						 $f = 0; //contador para filas par o impar
						 $p = 0; //contador para productos
						 $r = 0; //contador para filas
						 //foreach ($lista_productos as $itemProd):
						 foreach ($lista_productos as $itemProd): 
						 	$date1 = new DateTime("now");
		                      $date2 = new DateTime($itemProd['fecha_creacion']);

		                      $diff = $date1->diff($date2);
		                      //un proucto creado hace menos de 3 semanas es considerado nuevo
							 if (($diff->d)<21 && $arrayProducto['id']!= $itemProd['id']):
						 	 ?>
								<!-- Single-product start -->
								<div class="single-product col-lg-12">
									<div class="product-img">
										<a href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>"><img src="<?=$itemProd['foto_producto']?>" alt="" /></a>
									</div>
									<div class="product-info">
										<h4 class="post-title"><a href="#"><?= $itemProd['nombre'] ?></a></h4>
										<span class="pro-price"><?php 
					                      if ($itemProd['descuento']>0):
					                          echo "$".number_format(ceil($itemProd['precio']-(($itemProd['precio']*$itemProd['descuento'])/100)) ,0, "," ,".");
					                      else:
					                      	 echo "$".number_format(ceil($itemProd['precio']) ,0, "," ,".");
					                      endif; ?>
						                </span>
									</div>
								</div>
								<!-- Single-product end -->
							<?php 
							$f++;
							endif;
						endforeach ;?>
															
					</div>
				</aside>
				<!-- Widget-product end -->
			</div>
		</div>
	</div>
</div>

