
<!-- HEADING-BANNER START -->
<div class="heading-banner-area-muebles overlay-bg" style="background: rgba(0, 0, 0, 0) url(<?=$arrayCategoria['imagen'];?>) no-repeat scroll center center / cover;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading-banner">
					<div class="heading-banner-title">
						<h2><?= $arrayCategoria['nombre']; ?></h2>
					</div>
					<div class="breadcumbs pb-15">
						<ul>
							<li><a href="<?= base_url();?>">Home</a></li>
							<li><a href="#">servicios</a></li>
							<li><?= $arrayCategoria['nombre']; ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- HEADING-BANNER END -->

<?php foreach ($ParrafosServicio as $parrafo):
	switch ($parrafo['tipo_parrafo']) {
		case 0:
			?>
			<!-- SERVICIOS AREA -->
			<div class="about-us-area  pt-80">
				<div class="container-fluid">	
					<div class="about-us bg-white">
					<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="servicio-foto-1">
									<img src="<?= $parrafo['imagen_parrafo']; ?>" alt="" />
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="servicios-1 bg-dark-white">
									<!--<h4 class="title-1 title-border text-uppercase mb-30">Titulo Detalle Servicio</h4>-->
									<?= $parrafo['mensaje_parrafo']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- SERVICIO-AREA FIN -->
			<?php
			break;
		case 1:
			?>
			<!-- SERVICIOS AREA -->
			<div class="about-us-area pt-80">
				<div class="container-fluid">	
					<div class="about-us bg-white">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="servicios-1 bg-dark-white">
									<!--<h4 class="title-1 title-border text-uppercase mb-30">Titulo Detalle Servicio</h4>-->
									<?= $parrafo['mensaje_parrafo']; ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="servicio-foto-2">
									<img src="<?= $parrafo['imagen_parrafo']; ?>" alt="" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- SERVICIO-AREA FIN -->
			<?php
			break;
		case 2:
			?>
			<!-- SERVICIOS AREA -->
			<div class="about-us-area  pt-80">
				<div class="container-fluid">	
					<div class="about-us bg-white">
					<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="servicios-1 bg-dark-white">
									<!--<h4 class="title-1 title-border text-uppercase mb-30">Titulo Detalle Servicio</h4>-->
									<?= $parrafo['mensaje_parrafo']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- SERVICIO-AREA FIN -->
			<?php
			break;
		case 3:
			?>
			
			<!-- SERVICIOS AREA -->
			<div class="about-us-area  pt-80">
				<div class="container-fluid">	
					<div class="about-us bg-white">
						<div class="row">		
							<img class="center-block" src="<?= $parrafo['imagen_parrafo']; ?>" alt="" />
						</div>
					</div>
				</div>
			</div>
			<!-- SERVICIO-AREA FIN -->
			<?php
			break;
		default:
			?>
			<?php
			break;
	}
 ?>
	
<?php endforeach ?>

<!-- contact-AREA -->
<div class="contact-us-area  pt-80 pb-80">
	<div class="container">	
		<div class="contact-us customer-login bg-white">
			<div class="row">
				<div class="col-xs-12">
					<div class="contact-details">
						<h4 class="title-1 title-border text-uppercase mb-30">Cuentanos sobre tu proyecto</h4>
						<ul>
							<?php if (!empty($datosServicio['telefono'])): ?>
								<li>
									<i class="zmdi zmdi-phone"></i>
									<span>+<?= $datosServicio['telefono']; ?></span>
								</li>
							<?php endif ?>
							
							<li>
								<i class="zmdi zmdi-email"></i>
								<span><?= $datosServicio['email']; ?></span>
								<span><?= $datosServicio['mail2']; ?></span>
								<span><?= $datosServicio['mail3']; ?></span>
							</li>
						</ul>
					</div>
					
					<div class="send-message mt-60">
						<?php if (!empty($datosServicio['email']) ): ?>
							<form id="form_contacto_servicio">
								<h4 class="title-1 title-border text-uppercase mb-30">Envia un Mensaje</h4>
								<input type="hidden" name="servicio" value="<?= $arrayCategoria['nombre'];?>" />
								<input type="text" name="nombre" placeholder="Su nombre aquí..." required="required" />
								<input type="text" name="email" placeholder="Su email aquí..." required="required" />
								<textarea class="custom-textarea" name="message" placeholder="Su mensaje aquí..." required="required"></textarea>
								<br>
								<button class="button-one submit-button mt-20" data-loading-text="Enviando Mensaje" id="submitContactoServicio" data-text="Enviar Mensaje" type="button">Enviar Mensaje</button>
							</form>
						<?php endif ?>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- contact-AREA -->		
			
			
			<!-- SERVICIOS AREA 
			<div class="about-us-area pt-80 pb-80">
				<div class="container-fluid">	
					<div class="about-us bg-white">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<div class="servicios-1 bg-dark-white">
									<h4 class="title-1 title-border text-uppercase mb-30">Titulo Detalle Servicio</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magn aliqua. Ut enim ad minim veniam, ommodo consequa.  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia is be deserunt mollit anim id est laborum.</p>
									<p>Sed ut Wperspiciatis unde omnis iste natus error sit. voluptatem accusantium doloremque laudantium, totam remes aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
									<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								
									<img class="center-block" src="img/bg/about.png" alt="" />
								
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<div class="servicios-1 bg-dark-white">
									<h4 class="title-1 title-border text-uppercase mb-30">Titulo Detalle Servicio</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magn aliqua. Ut enim ad minim veniam, ommodo consequa.  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia is be deserunt mollit anim id est laborum.</p>
									<p>Sed ut perspiciatis unde omnis iste natus error sit. voluptatem accusantium doloremque laudantium, totam remes aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
									<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> SERVICIO-AREA FIN -->