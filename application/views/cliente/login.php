<!-- HEADING-BANNER START -->
      <div class="heading-banner-area overlay-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="heading-banner">
                <div class="heading-banner-title">
                  <h2>Registro</h2>
                </div>
                <div class="breadcumbs pb-15">
                  <ul>
                    <li><a href="<?= base_url(); ?>">Home</a></li>
                    <li>Registro</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- HEADING-BANNER END -->
      <!-- SHOPPING-CART-AREA START -->
      <div class="login-area  pt-80 pb-80">
        <div class="container">
            <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
          <?= form_open(); ?>
              <?php if(isset($_GET["next"])): ?>
                  <input type="hidden" name="next" value="<?= $_GET["next"];?>">
             <?php endif; ?>
              <div class="customer-login text-left">
                <h4 class="title-1 title-border text-uppercase mb-30">Registro de Usuarios</h4>
                <p class="text-gray">Si usted posee una cuenta, por favor inicie sesión!</p>
                <input type="text" placeholder="Email aquí..." name="username">
                <input type="password" placeholder="Contraseña" name="password">
                <p><a href="<?= base_url('cliente/lost_password');?>" class="text-gray">¿Olvido su contraseña?</a></p>
                <button type="submit" data-text="Iniciar Sesión" class="button-one submit-button mt-15">Iniciar Sesión</button>
              </div>          
          <?= form_close(); ?>
            </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
          <form role="form" id="form_registro"> 
                <div class="customer-login text-left">
                  <h4 class="title-1 title-border text-uppercase mb-30">Nuevos Usuarios</h4>
                  <p class="text-gray"> Registrese completando los campos a continuación</p>
                  <input type="text" id="rut_cliente" name="rut_cliente" placeholder="Su rut aquí..." required="required">
                  <input type="text" name="nombre_cliente" id="nombre_cliente" placeholder="Su nombre aquí..." required="required">
                  <input type="text" name="apellido_cliente" id="apellido_cliente" placeholder="Su apellido aquí..." required="required">
                  <input type="text" name="telefono_cliente" id="telefono_cliente" placeholder="Su telefono aquí (solo numeros)..." required="required">
                  <input type="email" id="email_cliente" name="email_cliente" placeholder="Su email aquí..." required="required">
                  <input type="password" name="password_cliente" id="password_cliente" placeholder="Su contraseña aquí..." required="required">
                  <input type="text" id="direccion_cliente" name="direccion_cliente" placeholder="Dirección" required="required">

                  <select name="region_cliente" id="region_cliente" class="minimal" required="required">
                    <option value="">Seleccione Región</option>
                    <?php foreach ($lista_regiones as $regionItem):?>
                      <option value="<?= $regionItem['id'];?>"><?= $regionItem['nombre'];?></option>
                    <?php endforeach;?>
                  </select>
                  <select name="provincia_cliente" id="provincia_cliente" class="minimal" required="required">
                    <option value="">Seleccione Provincia</option>
                  </select>
                  <select name="comuna_cliente" id="comuna_cliente" class="minimal" required="required">
                    <option value="">Seleccione Comuna</option>
                  </select>
                  <input type="text"  id="edad_cliente" name="edad_cliente" placeholder="Edad" required="required">
                  <select name="sexo_cliente" id="sexo_cliente" class="minimal" required="required">
                    <option>Seleccione Sexo</option>
                    <option value="0">Masculino</option>
                    <option value="1">Femenino</option>
                  </select>
<!--                  <p class="mb-0">
                    <input type="checkbox" id="newsletter" name="newsletter" checked>
                    <label for="newsletter"><span>suscribirse a nuestro newsletter!</span></label>
                  </p>-->
                  <button type="button" id="btnregistrar" data-text="Regitarse" class="button-one submit-button mt-15">Regitrarse</button>
                </div>          
          </form>
              </div>
            </div>
        </div>
      </div>
      <!-- SHOPPING-CART-AREA END -->