<div class="container">
	<div class="content text-center">
		<div class="box-exito">
			<i class="fa fa-check"></i> 
			<h3 class="title-exito">Felicidades Su pago ha sido realizado con exito</h3>
			<p>La orden de pago numero <b><?= $data_flow['flororden']; ?></b> correspondiente al pago del carro de compra <b><?= $carro_pagado['codigo'];?></b> ha sido procesada con exito, los productos seran despachados a la dirección <b><?= $carro_pagado['direccion'];?></b> ingresada anteriormente </p>
			<p>con las instrucciones correspondientes </p>
			<p>se le comunicara al mail <b><?= $carro_pagado['email'];?></b> cuando los productos sean despachados.</p>
			<a class="btn-seguir" href="<?= site_url();?>">Seguir comprando</a>
		</div>
	</div>
</div>
