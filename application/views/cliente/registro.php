<div class="row">
  <div class="col-md-6 col-md-offset-3">
		  <form class="form-horizontal" role="form" id="form_registro">
        <div class="form-group">
            <label for="rut_cliente" class="col-sm-3 control-label">Rut</label>
            <div class="col-sm-9">
              <div class="input-group">
                <input type="text" class="form-control" id="rut_cliente" name="rut_cliente" placeholder="Rut">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
              </div>
            </div>
          </div>
        <div class="form-group">
          <label for="nombre_cliente" class="col-sm-3 control-label">Nombre</label>
          <div class="col-sm-9">
            <div class="input-group">
              <input type="text" name="nombre_cliente" class="form-control" id="nombre_cliente" placeholder="Nombre">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="apellido_cliente" class="col-sm-3 control-label">Apellido</label>
          <div class="col-sm-9">
            <div class="input-group">
              <input type="text" name="apellido_cliente" class="form-control" id="apellido_cliente" placeholder="Apellido">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="telefono_cliente" class="col-sm-3 control-label">Telefono</label>
          <div class="col-sm-9">
            <div class="input-group">
              <input type="text" class="form-control" name="telefono_cliente" id="telefono_cliente" placeholder="91234567">
              <span class="input-group-addon"><i class="fa fa-phone"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="email_cliente" class="col-sm-3 control-label">Email</label>
          <div class="col-sm-9">
            <div class="input-group">
              <input type="email" class="form-control" id="email_cliente" name="email_cliente" placeholder="Email">
              <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="password_cliente" class="col-sm-3 control-label">Password</label>
          <div class="col-sm-9">
            <div class="input-group">
              <input type="password" class="form-control" name="password_cliente" id="password_cliente" placeholder="Password">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="direccion_cliente" class="col-sm-3 control-label">Direccion</label>
          <div class="col-sm-9">
            <div class="input-group">
              <input type="text" class="form-control" id="direccion_cliente" name="direccion_cliente" placeholder="Direccion">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="region_cliente" class="col-sm-3 control-label">Region</label>
          <div class="col-sm-9">
            <div class="input-group">
              <select name="region_cliente" id="region_cliente" class="form-control">
                <option value="">Seleccione Region</option>
                <?php foreach ($lista_regiones as $regionItem):?>
                  <option value="<?= $regionItem['id'];?>"><?= $regionItem['nombre'];?></option>
                <?php endforeach;?>
              </select>
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="provincia_cliente" class="col-sm-3 control-label">Provincia</label>
          <div class="col-sm-9">
            <div class="input-group">
              <select name="provincia_cliente" id="provincia_cliente" class="form-control">
                <option value="">Seleccione Provincia</option>
              </select>
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="comuna_cliente" class="col-sm-3 control-label">Comuna</label>
          <div class="col-sm-9">
            <div class="input-group">
              <select name="comuna_cliente" id="comuna_cliente" class="form-control">
                <option value="">Seleccione Comuna</option>
              </select>
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="edad_cliente" class="col-sm-3 control-label">Edad</label>
          <div class="col-sm-9">
            <div class="input-group">
              <input type="text" class="form-control" id="edad_cliente" name="edad_cliente" placeholder="Edad">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="medio_pago_cliente" class="col-sm-3 control-label">Medio de Pago Preferido</label>
          <div class="col-sm-9">
            <div class="input-group">
              <select name="medio_pago_cliente" id="medio_pago_cliente" class="form-control">
                <option value="0">Efectivo</option>
                <option value="1">Transferencia</option>
                <option value="2">On-line (webpay)</option>
              </select>
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="button" id="btnregistrar" class="btn btn-primary"><i class="fa fa-check-circle"></i> Crear Cuenta</button>
            <a href="<?= base_url("cliente/login");?>" class="btn btn-warning">Volver</a>
          </div>
        </div>
      </form>
	</div>

</div>
