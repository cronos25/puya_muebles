<div class="container">
	<div class="row box-recuperar">
		<h1>¿Olvidaste tu contraseña ?</h1>
		<div class="info-recuperar">
			Por favor, ingrese su dirección de correo electrónico. Le enviaremos un link para cambiar su contraseña.
		</div>
		<?= form_open(); ?>
		<input type="email" name="email" id="email" placeholder="ingrese su correo" size="20" maxlength="50" value="">
		<div>
			<?= $this->recaptcha->render(); ?>
		</div>
		<input  class="enviar-contrasena" type="submit" id="" name="Submit" value="Enviar contraseña a tu Email">
		<?= form_close(); ?>
		<?php if (isset($mensaje)): ?>
			<?= $mensaje;?>
		<?php endif ?>
	</div>
</div>
