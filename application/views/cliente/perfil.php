<div class="login-area  pt-80 pb-80">
	<div class="container">
		<div class="tab-content row">
			<div class="tab-pane active" id="elements-preview-1">
	            <div class="elements-tab-3">
	                <!-- Nav tabs -->
	                <div class="col-sm-3">
	                    <ul class="elements-tab-menu clearfix lista">
	                        <li class="<?= (!isset($_GET['vista']))?'active':''; ?>"><a href="#resumen" data-toggle="tab">Mi Cuenta</a></li>
	                        <li class="<?= (isset($_GET['vista'])&&htmlspecialchars($_GET['vista'])=='pedidos')?'active':''; ?>"><a href="#compras" data-toggle="tab">Mis Compras</a></li>
	                        <li><a href="#facturacion"  data-toggle="tab">Datos de Facturación</a></li>
	                        <li><a href="#contrasena"  data-toggle="tab">Contraseña</a></li>
	                    </ul>
	                </div>
	                <!-- Tab panes -->
	                <div class="tab-content col-sm-9">
	                    <div class="tab-pane <?= (!isset($_GET['vista']))?'active':''; ?>" id="resumen">
							<div class="our-order payment-details pr-20">
								<h4 class="title-1 title-border text-uppercase mb-30">Resumen de Mi Cuenta</h4>
								<div class="">
									<form id="form_update_data_perfil" role="form">
									<input type="hidden" name="id_cliente" class="post-title" value="<?= $dataCliente['id'] ?>">
									<p class="text-uppercase text-dark-red mb-0">RUT:</p>
									<h4 class="post-title"><input type="text" id="rut_cliente" name="rut_cliente"  class="post-title" value="<?= $dataCliente['rut'] ?>" required="required"></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">Nombre:</p>
									<h4 class="post-title"><input type="text" name="nombre_cliente" class="post-title" value="<?= $dataCliente['nombre'] ?>" required="required"></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">Apellido:</p>
									<h4 class="post-title"><input type="text" name="apellido_cliente" class="post-title" value="<?= $dataCliente['apellido'] ?>" required="required"></h4>
									
									<hr>
									<p class="text-uppercase text-dark-red mb-0">Teléfono:</p>
									<h4 class="post-title"><input type="text" name="telefono_cliente" class="post-title" value="<?= $dataCliente['telefono'] ?>" required="required"></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">email:</p>
									<h4 class="post-title"><input type="text" name="email_cliente" class="post-title" value="<?= $dataCliente['email'] ?>" required="required"></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">direccion:</p>
									<h4 class="post-title"><input type="text" name="direccion_cliente" class="post-title" value="<?= $dataCliente['direccion'] ?>" required="required"></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">Región:</p>
									<h4 class="post-title"><select name="region_cliente" class=" post-title" id="region_cliente" required="required">
						            <option value="">Seleccione Región</option>
							            <?php foreach ($lista_regiones as $regionItem):?>
							              <option value="<?= $regionItem['id'];?>" <?php if($dataCliente['id_region']== $regionItem['id']) echo 'selected';?>><?= $regionItem['nombre'];?></option>
							            <?php endforeach;?>
							          </select></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">Provincia:</p>
									<h4 class="post-title"><select name="provincia_cliente" class="post-title" id="provincia_cliente" required="required">
							            <option value="">Seleccione Provincia</option>
							            <?php foreach ($lista_provincias as $provinciaItem):?>
							              <option value="<?= $provinciaItem['id'];?>" <?php if($dataCliente['id_provincia']== $provinciaItem['id']) echo 'selected';?>><?= $provinciaItem['nombre'];?></option>
							            <?php endforeach;?>
							          </select></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">Comuna:</p>
									<h4 class="post-title"><select name="comuna_cliente" class=" post-title" id="comuna_cliente" required="required">
							            <option value="">Seleccione Comuna</option>
							            <?php foreach ($lista_comunas as $comunaItem):?>
							              <option value="<?= $comunaItem['id'];?>" <?php if($dataCliente['id_comuna']== $comunaItem['id']) echo 'selected';?>><?= $comunaItem['nombre'];?></option>
							            <?php endforeach;?>
									</select></h4>
									<hr>
									<p class="text-uppercase text-dark-red mb-0">edad:</p>
									<h4 class="post-title"><input type="text" name="edad_cliente"  class="post-title" value="<?= $dataCliente['edad'] ?>" required="required"></h4>
									<hr><p class="text-uppercase text-dark-red mb-0">Sexo:</p>
									<h4 class="post-title">
										<select name="sexo_cliente" class="post-title" id="sexo_cliente">
							            <option value="0" <?php if($dataCliente['sexo']== 0) echo 'selected="selected"';?>>Masculino</option>
							            <option value="1" <?php if($dataCliente['sexo']== 1) echo 'selected="selected"';?>>Femenino</option>
							          </select></h4>
									<hr>
								<button id="btnUpdatePerfil" data-loading-text='Editando' class="button-one text-uppercase" data-text="Editar" class="btn-clear" type="button" name="button">Editar</button>	
								</form>
								</div>
							</div>
						</div>
	                    <div class="tab-pane <?= (isset($_GET['vista'])&&htmlspecialchars($_GET['vista'])=='pedidos')?'active':''; ?>" id="compras">
	                       <h4 class="title-1 title-border text-uppercase mb-30">Mis Compras</h4>
	                       <div class="">
	                       		<div class="our-order payment-details pr-20">
									<?php if ($ventas) :?>
										<table>
										<thead>
											<tr>
												<th><strong>N° Orden</strong></th>
												<th><strong>Fecha</strong></th>
												<th><strong>Estado</strong></th>
												<th class="text-right"><strong>Total</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($ventas as $ventaItem):?>
											
											<tr>
												<td><?= $ventaItem['numero_orden']; ?></td>
												<td><?= date('d-m-Y H:i:s',strtotime($ventaItem['fecha_proceso']));?></td>
												<td><?php
														switch ($ventaItem['estado']) {
															case 0:
																echo "esperando pago";
																break;
															case 1:
																echo "venta cancelada";
																break;
															case 2:
																echo "pagado";
																break;
															case 3:
																echo "enviado / listo para retiro";
																break;
															case 4:
																echo "finalizado";
																break;
															default:
																echo "esperando pago";
																break;
														}?></td>
												<td class="text-right">$<?= number_format(ceil($ventaItem['costo_envio']+$ventaItem['costo_productos']) ,0, "," ,"."); ?></td>
											</tr>
										<?php endforeach;?>
										</tbody>
									</table>

									<?php else:?>
										Aún no ha realizado ninguna compra
									<?php endif;?>
								</div>
							</div>
	                    </div>
	                    <div class="tab-pane" id="facturacion">
							<div class="our-order payment-details pr-20">
								<h4 class="title-1 title-border text-uppercase mb-30">Datos de Facturación</h4>
								<div class="">
									<form id="form_update_data_facturacion_perfil" role="form">
										<input type="hidden" name="id_cliente" class="post-title" value="<?= $dataCliente['id'] ?>">
										<p class="text-uppercase text-dark-red mb-0">rut empresa:</p>
										<h4 class="post-title"><input type="text" id="rut_facturacion" name="rut_facturacion" class="post-title" value="<?= $dataCliente['rut_fac'] ?>" required="required"></h4>
										<hr>
										<p class="text-uppercase text-dark-red mb-0">Nombre Empresa:</p>
										<h4 class="post-title"><input type="text" name="nombre_facturacion" class="post-title" value="<?= $dataCliente['nombre_fac'] ?>" required="required"></h4>
										<hr>
										<p class="text-uppercase text-dark-red mb-0">Giro Comercial:</p>
										<h4 class="post-title"><input type="text" name="giro_facturacion" class="post-title" value="<?= $dataCliente['giro_fac'] ?>" required="required"></h4>
										
										<hr>
										<p class="text-uppercase text-dark-red mb-0">Teléfono:</p>
										<h4 class="post-title"><input type="text" name="telefono_facturacion" class="post-title" value="<?= $dataCliente['telefono_fac'] ?>" required="required"></h4>
										<hr>
										<p class="text-uppercase text-dark-red mb-0">direccion:</p>
										<h4 class="post-title"><input type="text" name="direccion_facturacion" class="post-title" value="<?= $dataCliente['direccion_fac'] ?>" required="required"></h4>
										<hr>
										<p class="text-uppercase text-dark-red mb-0">Región:</p>
										<h4 class="post-title"><select name="region_facturacion" class=" post-title" id="region_facturacion" required="required">
							            <option value="">Seleccione Región</option>
								            <?php foreach ($lista_regiones as $regionItem):?>
								              <option value="<?= $regionItem['id'];?>" <?php if($dataCliente['region_id_fac']== $regionItem['id']) echo 'selected';?>><?= $regionItem['nombre'];?></option>
								            <?php endforeach;?>
								          </select></h4>
										<hr>
										<p class="text-uppercase text-dark-red mb-0">Provincia:</p>
										<h4 class="post-title"><select name="provincia_facturacion" class="post-title" id="provincia_facturacion" required="required">
								            <option value="">Seleccione Provincia</option>
								            <?php foreach ($lista_provincias as $provinciaItem):?>
								              <option value="<?= $provinciaItem['id'];?>" <?php if($dataCliente['provincia_id_fac']== $provinciaItem['id']) echo 'selected';?>><?= $provinciaItem['nombre'];?></option>
								            <?php endforeach;?>
								          </select></h4>
										<hr>
										<p class="text-uppercase text-dark-red mb-0">Comuna:</p>
										<h4 class="post-title"><select name="comuna_facturacion" class=" post-title" id="comuna_facturacion" required="required">
								            <option value="">Seleccione Comuna</option>
								            <?php foreach ($lista_comunas as $comunaItem):?>
								              <option value="<?= $comunaItem['id'];?>" <?php if($dataCliente['comuna_id_fac']== $comunaItem['id']) echo 'selected';?>><?= $comunaItem['nombre'];?></option>
								            <?php endforeach;?>
										</select></h4>
										<hr>
										<button id="btnUpdateFacturacion" data-loading-text='Editando' class="button-one text-uppercase" data-text="Editar" class="btn-clear" type="button" name="button">Editar</button>	
									</form>
								</div>
							</div>
	                    </div>
	                    <div class="tab-pane" id="contrasena">
	                       
	                       <div class="text-left">
								<h4 class="title-1 title-border text-uppercase mb-30">Cambiar Contraseña</h4>
								<form id="form_update_pass_perfil" role="form">
									<input type="hidden" name="id_cliente" value="<?= $dataCliente['id'] ?>">
									<input type="hidden" name="mail_cliente" class="info-cuenta" value="<?= $dataCliente['email'] ?>">
									<p class="text-uppercase text-dark-red mb-0">Contraseña actual:</p>
									<h4 class="post-title"><input type="password" maxlength="150" name="password" class="post-title" placeholder="ingrese contraseña actual" required="required"></h4>
									<p class="text-uppercase text-dark-red mb-0">Nueva contraseña:</p>
									<h4 class="post-title"><input type="password" maxlength="150" id="nueva_pass_cliente" name="nueva_pass_cliente" class="post-title" placeholder="Ingrese nueva contraseña" required="required"></h4>
									<p class="text-uppercase text-dark-red mb-0">Repita su nueva contraseña:</p>
									<h4 class="post-title"><input type="password" maxlength="150" id="nueva_pass_repeat_cliente" name="nueva_pass_repeat_cliente" class="post-title" placeholder="repita nueva contraseña"></h4>
									<button type="submit" id="btnUpdatePassPerfil" data-text="Guardar" data-loading-text='Guardando' class="button-one submit-button mt-15">Guardar</button>
								</form>	
							</div>
	                    </div>
	                </div>
	            </div>                                 
	        </div>
		</div>
	</div>
</div>