<table>
	<tr>
		<td><a href="<?= base_url(); ?>">Ir a la pgina</a></td>
	</tr>
	<tr>
		<td><a href="<?= base_url("cliente/"); ?>">perfil</a></td>
	</tr>
	<tr>
		<td><a href="<?= base_url("cliente/carro_compra"); ?>">carro de compra</a></td>
	</tr>
	<tr>
		<td><a href="<?= base_url("cliente/pedidos"); ?>">pedidos</a></td>
	</tr>
	<tr>
		<td><a href="">configuracion</a></td>
	</tr>
	<tr>
		<td>
			<form action="<?= base_url("cliente/cerrarSession"); ?>" method="POST">
				<input type="hidden" name="cleardata" value="1">
				<input type="submit" value="Cerrar Sesion">
			</form>
		</td>
	</tr>
</table>

<table class="text-center">
	<tr>
		<th>
			Codigo Venta	
		</th>
		<th>
			estado	
		</th>
		<th>
			fecha de creacion de venta
		</th>
	</tr>
	<?php 
		if ($ventas) {
			foreach ($ventas as $ventaItem) {
		?>
		<tr>
			<td>
				<?= $ventaItem['numero orden']; ?>
			</td>
			<td>
				<?php
				switch ($ventaItem['estado']) {
					case 0:
						echo "esperando pago";
						break;
					case 1:
						echo "venta cancelada";
						break;
					case 2:
						echo "pagado";
						break;
					case 3:
						echo "enviado / listo para retiro";
						break;
					case 4:
						echo "finalizado";
						break;
					default:
						echo "esperando pago";
						break;
				}?>
			</td>
			<td>
				<?= date('d-m-Y H:i:s',strtotime($ventaItem['fecha_proceso']));?>
			</td>
		</tr>
	<?php } 
	}?>
</table>