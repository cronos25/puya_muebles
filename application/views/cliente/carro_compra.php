
 <!--
<div class="container-fluid">

<div class="row">
    <div class=" container-carro">

      <div class="col-md-12">

        <a href="<?= base_url('cliente'); ?>">Volver al Perfil</a>

      </div>

      <div class="col-md-6">

        <div class="">

          <h1>Tu carro de compras</h1>

        </div>

        <?php

    		$totalVentaProd = 0;

        $descuentoPromo = 0;



    		if ($productos_carro) {

    			foreach ($productos_carro as $productoItem) {

    			$precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));

    			$precioProdDesc = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));

    			$precioVenta *= $productoItem['cantidad'];

          if(!empty($promoActiva)):

            if (($promoActiva['producto_id']>0 && $productoItem['id']==$promoActiva['producto_id'])||

              ($promoActiva['categoria_id']>0 && $productoItem['categoria_id']==$promoActiva['categoria_id'])||

              ($promoActiva['marca_id']>0 && $productoItem['marca_id']==$promoActiva['marca_id'])||

              ($promoActiva['sub_categoria_id']>0 && $productoItem['sub_categoria_id']==$promoActiva['sub_categoria_id'])) {

              $descuentoPromo += $precioProdDesc*($promoActiva['descuento']/100);

            }

          endif;



    			$totalVentaProd += $precioVenta;

    		?>

		

        <div class="row box-producto-carro">

            <div class="col-md-2">

              <img src="<?= $productoItem['imagen_prod']; ?>" class="img-responsive" alt="">

            </div>

            <div class="col-md-3">

              <div class="title-producto-carro"><b><?= $productoItem['marca']; ?> </b><?= $productoItem['nombre']; ?> </div>

              <div class="codigo-producto">Codigo producto:

              <span><?= $productoItem['codigo']; ?></span>

            </div>

            </div>

            <div class="col-md-4">

              <div class="title-input">

                 <form class="form_sabor_prod">

                  <input type="hidden" name="asoc_data" value="<?= $productoItem['asoc'];?>">

                    <span>Sabor</span>

                    <button class="btn-gys-default btn btn-update-flavour-cart" type="button" name="button" title="modificar sabor">

                              <svg class="icon" viewBox="0 0 32 32">

                            <path d="M27.802 5.197c-2.925-3.194-7.13-5.197-11.803-5.197-8.837 0-16 7.163-16 16h3c0-7.18 5.82-13 13-13 3.844 0 7.298 1.669 9.678 4.322l-4.678 4.678h11v-11l-4.198 4.197z"></path>

                            <path d="M29 16c0 7.18-5.82 13-13 13-3.844 0-7.298-1.669-9.678-4.322l4.678-4.678h-11v11l4.197-4.197c2.925 3.194 7.13 5.197 11.803 5.197 8.837 0 16-7.163 16-16h-3z"></path>

                          </svg>

                        </button>

                    <div class="form-group">
                        <div class="input-group">
                          <select class="minimal border" name="sabor_data" id="sabor_data">

                            <?php foreach ($productoItem['sabores'] as $saborItem): ?>

                              <option value="<?= $saborItem['id']?>" <?= ($saborItem['id']==$productoItem['asoc_sabor'])?'selected="selected"':''; ?> ><?= $saborItem['sabor']?></option>

                            <?php endforeach ?>

                          </select>
                        </div>
                      </div>



                    </form>

                  </div>

              <div class="title-input">

              	<form class="form_cantidad_prod">

        					<input type="hidden" name="asoc_data" value="<?= $productoItem['asoc'];?>">

        					<div class="input-group">

                    <div class="title-input"><span>Cantidad</span> <button class="btn-gys-default btn btn-update-quantity-cart" type="button" name="button" title="modificar cantidad">

                                <svg class="icon" viewBox="0 0 32 32">

                        <path d="M27.802 5.197c-2.925-3.194-7.13-5.197-11.803-5.197-8.837 0-16 7.163-16 16h3c0-7.18 5.82-13 13-13 3.844 0 7.298 1.669 9.678 4.322l-4.678 4.678h11v-11l-4.198 4.197z"></path>

                        <path d="M29 16c0 7.18-5.82 13-13 13-3.844 0-7.298-1.669-9.678-4.322l4.678-4.678h-11v11l4.197-4.197c2.925 3.194 7.13 5.197 11.803 5.197 8.837 0 16-7.163 16-16h-3z"></path>

                      </svg>

                    </button></div>

        						<ul class="list-inline prod_spinner_cantidad panel_background_gray">



        							<li><button type="button" class="btn btn-link prod_spinner_cantidad_down"><i class="fa fa-minus"></i></button></li>

        							<li><input type="text" data-max-value="<?=$productoItem['disponible'];?>" class="prod_spinner_cantidad_data input-info" name="cantidad_data" id="cantidad_data" value="<?= $productoItem['cantidad']; ?>"></li>

        							<li><button type="button" class="btn btn-link prod_spinner_cantidad_up"><i class="fa fa-plus"></i></button></li>

        							<li>



        							</li>

        						</ul>

        					</div>

        				</form>



              </div>

            </div>

            <div class="col-md-3">

              <button class="removeItemcart button-icon" data-asoc="<?= $productoItem['asoc'];?>" shopping-cart="<?= $productoItem['cart'];?>" data-val="<?= $productoItem['cantidad']; ?>"><span>

                  <svg class="icon icon-eliminar" viewBox="0 0 24 24">

                <path d="M18.984 6.422l-5.578 5.578 5.578 5.578-1.406 1.406-5.578-5.578-5.578 5.578-1.406-1.406 5.578-5.578-5.578-5.578 1.406-1.406 5.578 5.578 5.578-5.578z"></path>

                </svg>

                </span>

                Eliminar producto</button>

              <div class="title-precio">Precio unitario </div>

                <div class="precio-producto-carro">

                  <span>$<?= (($precioProdDesc)>=0)? number_format(ceil($precioProdDesc) ,0, "," ,"."):0; ?> <?php if ($precioProdDesc != $productoItem['asoc_val']): ?>

                    <span class="precio-des-c">$<?= number_format(ceil($productoItem['asoc_val']) ,0, "," ,"."); ?></span>

                  <?php endif ?></span>

                </div>

              <div class="title-precio">Precio</div>

                <div class="precio-producto-carro">

                  <span>$<?= (($precioVenta)>=0)? number_format(ceil($precioVenta) ,0, "," ,"."):0; ?></span>

                </div>

            </div>

            <?php if (!empty($productoItem['regalo'])): ?>

            <div class="col-md-12">

              <div class="box-regalo-carro">

                    <div class="title-producto-carro">

                      <span>

                        <svg class="icon" viewBox="0 0 28 32">

                    <path d="M14 30h10v-10h-10v10zM2 30h10v-10h-10v10zM13.992 11.756c0.983-0.109 2.090-0.231 2.73-0.304 2.621-0.292 4.98-2.652 5.271-5.272 0.291-2.617-1.594-4.504-4.209-4.21-2.045 0.229-3.915 1.718-4.785 3.604-0.87-1.887-2.74-3.376-4.784-3.606-2.616-0.292-4.504 1.595-4.21 4.212 0.292 2.62 2.652 4.979 5.271 5.272 0.642 0.072 1.748 0.196 2.731 0.304-0.016 0.132-0.008 0.244-0.008 0.244h2c0 0 0.008-0.112-0.008-0.244zM17.355 4.396c1.414-0.158 2.438 0.861 2.277 2.277-0.156 1.416-1.434 2.692-2.848 2.851-1.418 0.157-2.438-0.862-2.279-2.278s1.436-2.692 2.85-2.85zM9.216 9.524c-1.414-0.158-2.689-1.435-2.848-2.851s0.862-2.436 2.275-2.277c1.416 0.157 2.692 1.434 2.851 2.85s-0.862 2.436-2.278 2.278zM14 12v6h12v-6h-12zM0 18h12v-6h-12v6z"></path>

                    </svg>

                      </span>

                      Regalo por compra

                      <b><?= $productoItem['regalos']; ?></b>

                    </div>

              </div>

            </div>

            <?php endif ?>



            <i class="line-pro"></i>

        </div>

	<?php }

	}?>

      </div>

      <div class="col-md-3">
        <div class="col-md-12 panel-carro-envio">
          <div class="title-seccion">

          Codigo de descuento

          </div>
          <div class="row">

            <div class="col-md-12">

              <div class="form-group">

                <input type="text" class="input-codigo" id="cod_descuento_previa" placeholder="Codigo de descuento">

              </div>

            </div>

            <div class="col-md-12">

              <button class="btn-gys" id="consulta_desc_codigo_carro" type="button" name="button">Agregar codigo</button>

            </div>

          </div>

        </div>


        <div class=" col-md-12 direccion-de-envio">

          <div class="title-seccion">

            Direccion de envio

          </div>



  <span class="direccion-cliente">

    <?= $arrayCliente['direccion'].' , '.$arrayCliente['comuna'].' , '.$arrayCliente['provincia'].' , '.$arrayCliente['region'];?>

    <div class="no-direccion">

        <a  href="<?= base_url('cliente'); ?>">¿No es tu direccion ?</a>

    </div>

  </span>



        </div>



      </div>

      <div class="col-md-3">

        <div class=" col-md-12 panel-total-carro">

          <div class="title-seccion">

            Costo

          </div>

            <div class="row box-costo">

              <div class="col-md-6">

                Subtotal

              </div>

              <div class="col-md-6 text-right">

                <div class="precio-producto-carro">

                  <span id="subtotalcarro">$<?= number_format(ceil($totalVentaProd) ,0, "," ,"."); ?></span>

                </div>

              </div>

            </div>

            <?php

         if(!empty($promoActiva) && $descuentoPromo > 0):  ?>

            <div class="row box-costo">

              <div class="col-md-6">

                Descuento Promocional

              </div>

              <div class="col-md-6 text-right">

                <div class="precio-producto-carro">

                  <span id="descuentopromocarro">$<?= number_format(ceil(intval($descuentoPromo)) ,0, "," ,"."); ?></span>

                </div>

              </div>

            </div>

          <?php endif; ?>



            <div class="row  box-costo">

              <div class="col-md-6">

                Costo de envío:

              </div>

              <div class="col-md-6">

                <div class="precio-producto-carro text-right">

                  <span id="costoenviocarro">$<?=number_format(ceil(($costo_envio)?$costo_envio:5000) ,0, "," ,".");?></span>

                </div>

              </div>

            </div>

            <div class="row  box-costo">

              <div class="col-md-6">

                Cupon de descuento:

              </div>

              <div class="col-md-6">

                <div class="precio-producto-carro text-right">

                  <span id="valor_cupon">$0</span>

                </div>

              </div>

            </div>

            <div class="row">

              <div class="col-md-12 text-right">

                <?php if ($totalVentaProd <= 50000 && $costo_envio>0){

                 ?>

                 <div class="alert-enviogratis"> Te faltan $<?= number_format(ceil(50000-$totalVentaProd) ,0, "," ,".");?> para envío gratis</div>

                 <?php }else{

                  ?>

                  <div class="alert-enviogratis"> Felicidades, tu envío será gratis</div>

                  <?php } ?>



              </div>

            </div>

            <div class="row box-total">

              <i class="line-top"></i>

              <div class="col-md-6">

                Total

              </div>

              <div class="col-md-6 text-right">

                <div class="precio-producto-carro">

                  <span id="total_actual_carro">$<?= number_format(ceil($costo_envio+$totalVentaProd-$descuentoPromo) ,0, "," ,".");?></span>

                </div>

              </div>

            </div>

            <div class="col-md-12 text-right">

              <textarea  class="comentario-pedido" id="comentario-pedidos" name="name" placeholder="Ingrese un comentario a la venta"><?= $carro['mensaje'];?></textarea>

            </div>

            <div class="row">

              <div class="col-md-12 text-right">

                <button class="btn-gys" id="btn-muestra-panel-pago" data-loading-text="Generando botones de pago..." type="button" name="button">Selecciona Medio de pago</button>

              </div>

            </div>

        </div>

        <div id="panel_venta">

        </div>

        </div>

      </div>

    </div>

</div>-->

<!-- HEADING-BANNER START -->
      <div class="heading-banner-area overlay-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="heading-banner">
                <div class="heading-banner-title">
                  <h2>Orden</h2>
                </div>
                <div class="breadcumbs pb-15">
                  <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Orden</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- HEADING-BANNER END -->
      <!-- ORDER-AREA START -->
      <div class="shopping-cart-area  pt-80 pb-80">
        <div class="container"> 
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="shopping-cart" id="smartwizard">
                <!-- Nav tabs -->
                <ul class="cart-page-menu row clearfix mb-30 carro">
                  <li><a href="#carritodecompras" class="carro">Carrito de Compras</a></li>
                  <li><a href="#resumen" class="carro">Resumen de la Compra</a></li>
                  <li><a href="#medio-pago" class="carro" >Medio de Pago</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <!-- shopping-cart start -->
                  <div class="tab-pane active" id="carritodecompras">
                      <div class="shop-cart-table">
                        <div class="table-content table-responsive">
                          <table>
                            <thead>
                              <tr>
                                <th class="product-thumbnail">Producto</th>
                                <th class="product-price">Precio</th>
                                <th class="product-quantity">Cantidad</th>
                                <th class="product-subtotal">Total</th>
                                <th class="product-remove">Eliminar</th>
                              </tr>
                            </thead>
                            <tbody>

                          <?php

                            $totalVentaProd = 0;

                            $descuentoPromo = 0;

                            if ($productos_carro) {

                              foreach ($productos_carro as $productoItem) {

                              $precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));

                              $precioProdDesc = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));

                              $precioVenta *= $productoItem['cantidad'];

                              if(!empty($promoActiva)):

                                if (($promoActiva['producto_id']>0 && $productoItem['id']==$promoActiva['producto_id'])||
                                  ($promoActiva['categoria_id']>0 && $productoItem['categoria_id']==$promoActiva['categoria_id'])||
                                  ($promoActiva['marca_id']>0 && $productoItem['marca_id']==$promoActiva['marca_id'])||
                                  ($promoActiva['sub_categoria_id']>0 && $productoItem['sub_categoria_id']==$promoActiva['sub_categoria_id'])) {
                                  $descuentoPromo += $precioProdDesc*($promoActiva['descuento']/100);
                                }

                              endif;
                              
                              $totalVentaProd += $precioVenta;
                            ?>

                            <tr>
                                <td class="product-thumbnail  text-left">
                                  <!-- Single-product start -->
                                  <div class="single-product">
                                    <div class="product-img">
                                      <a href="single-product.html"><img src="<?= $productoItem['imagen_prod']?>" alt="" /></a>
                                    </div>
                                    <div class="product-info">
                                      <h4 class="post-title"><a class="text-light-black" href="<?= base_url('productos/view/'.$productoItem['id'].'/'.str_replace("'","",str_replace('%','',$productoItem['nombre'])));?>"><?= $productoItem['nombre']; ?></a></h4>
                                      <p class="mb-0">Color :  <?= $productoItem['asoc_color_nombre']; ?></p>
                                    </div>
                                  </div>
                                  <!-- Single-product end -->                       
                                </td>
                                <td class="product-price">$<?= (($precioProdDesc)>=0)? number_format(ceil($precioProdDesc) ,0, "," ,"."):0; ?> 
                                  <?php if ($precioProdDesc != $productoItem['asoc_val']): ?>
                                    <s>$<?= number_format(ceil($productoItem['asoc_val']) ,0, "," ,"."); ?></s>
                                  <?php endif ?></td>

                                <td class="product-quantity">

                                  <form class="form_cantidad_prod">
                                    <input type="hidden" name="asoc_data" value="<?= $productoItem['asoc'];?>">

                                    <div class="cart-plus-minus">
                                      <input type="text" value="<?= $productoItem['cantidad'];?>" data-valor-item="<?= (($precioProdDesc)>=0)?ceil($precioProdDesc):0;?> " data-max-value="<?=$productoItem['disponible']+$productoItem['cantidad'];?>" readonly="read" name="cantidad_data" class="cart-plus-minus-box input_cart_quantity">
                                    </div>
                                  </form>
                                </td>
                                <td class="product-subtotal">$<?= (($precioVenta)>=0)? number_format(ceil($precioVenta) ,0, "," ,"."):0; ?></td>
                                <td class="product-remove">
                                  <button class="removeItemcart button-icon" data-asoc="<?= $productoItem['asoc'];?>" shopping-cart="<?= $productoItem['cart'];?>" data-val="<?= $productoItem['cantidad']; ?>"><i class="zmdi zmdi-close"></i></button>
                                </td>
                              </tr>

                           <?php  }
                          }
                            ?>
                              <!--
                                <tr>
                                  <td class="product-thumbnail  text-left">
                                    <div class="single-product">
                                      <div class="product-img">
                                        <a href="single-product.html"><img src="<?= base_url('assets')?>/img/product/6.jpg" alt="" /></a>
                                      </div>
                                      <div class="product-info">
                                        <h4 class="post-title"><a class="text-light-black" href="#">Nombre del Porducto</a></h4>
                                        <p class="mb-0">Color :  Blanco</p>
                                        <p class="mb-0"><a href="#">Cambiar</a></p>
                                      </div>
                                    </div>                     
                                  </td>
                                  <td class="product-price">$56.000</td>
                                  <td class="product-quantity">
                                    <div class="cart-plus-minus">
                                      <input type="text" value="02" data-max-value="10" readonly="read" name="qtybutton" class="cart-plus-minus-box">
                                    </div>
                                  </td>
                                  <td class="product-subtotal">$112.000</td>
                                  <td class="product-remove">
                                    <a href="#"><i class="zmdi zmdi-close"></i></a>
                                  </td>
                                </tr>
                              -->
                            </tbody>
                          </table>
                          <!--<button type="submit" data-text="Continuar" class="button-one submit-button mt-15 pull-right">Continuar</button>-->
                        </div>
                        
                      </div>         
                      
                  </div>
                  <!-- shopping-cart end -->
                  <!-- wishlist start -->
                  <div class="tab-pane" id="resumen">
                      <div class="shop-cart-table check-out-wrap">
                        <div class="row">
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="billing-details mt-60  pr-20">
                              <form role="form" id="form_direccion_carro">
                                <input type="hidden" name="cart" value="<?= $carro['id'];?>">
                                <h4 class="title-1 title-border text-uppercase mb-30">Datos de Envio</h4>
                                <p class="mb-0">
                                  <label><input type="checkbox" id="carro_check_direccion_cliente" autocomplete="off"> Usar los datos de mi cuenta
                                  </label><br><br></p>
                                <input type="text" data-dir-user="<?= $arrayCliente['nombre'].' '.$arrayCliente['apellido'];?>" placeholder="Su Nombre" name="carro_nombre" value="<?= $carro['nombre_carro'];?>" required="required">
                                <input type="text" data-dir-user="<?= $arrayCliente['email'];?>" placeholder="Email" name="carro_email" value="<?= $carro['email_carro'];?>" required="required">
                                <input type="text" data-dir-user="<?= $arrayCliente['telefono'];?>" placeholder="Teléfono" name="carro_telefono" value="<?= $carro['telefono_carro'];?>" required="required">
                                
                                <?php if ($carro['region_carro']): ?>
                                  <select data-dir-user="<?= $arrayCliente['id_region'];?>" class="custom-select mb-15" name="carro_region" required="required">
                                    <option value="">Seleccione Región</option>
                                    <?php foreach ($lista_regiones as $regionItem):?>
                                      <option value="<?= $regionItem['id'];?>" <?php if($carro['region_carro']== $regionItem['id']) echo 'selected';?>><?= $regionItem['nombre'];?></option>
                                    <?php endforeach;?>
                                  </select>
                                <?php else: ?>
                                  <select data-dir-user="<?= $arrayCliente['id_region'];?>" class="custom-select mb-15" name="carro_region" required="required">
                                    <option value="">Seleccione Región</option>
                                    <?php foreach ($lista_regiones as $regionItem):?>
                                      <option value="<?= $regionItem['id'];?>"><?= $regionItem['nombre'];?></option>
                                    <?php endforeach;?>
                                  </select>
                                <?php endif ?>
                                
                                <?php if ($carro['provincia_carro']): ?>
                                  <select data-dir-user="<?= $arrayCliente['id_provincia'];?>" class="custom-select mb-15" name="carro_provincia" required="required">
                                    <<option value="">Seleccione Provincia</option>
                                        <?php foreach ($lista_provincias as $provinciaItem):?>
                                          <option value="<?= $provinciaItem['id'];?>" <?php if($carro['provincia_carro']== $provinciaItem['id']) echo 'selected';?>><?= $provinciaItem['nombre'];?></option>
                                        <?php endforeach;?>
                                  </select>
                                <?php else: ?>
                                  <select data-dir-user="<?= $arrayCliente['id_provincia'];?>" class="custom-select mb-15" name="carro_provincia" required="required">
                                  <option value="">Seleccione Provincia</option>
                                </select>
                                <?php endif ?>
                                
                                <?php if ($carro['comuna_carro']): ?>
                                  <select data-dir-user="<?= $arrayCliente['id_comuna'];?>" class="custom-select mb-15" name="carro_comuna" required="required">
                                    <option value="">Seleccione Comuna</option>
                                      <?php foreach ($lista_comunas as $comunaItem):?>
                                        <option value="<?= $comunaItem['id'];?>" <?php if($carro['comuna_carro']== $comunaItem['id']) echo 'selected';?>><?= $comunaItem['nombre'];?></option>
                                      <?php endforeach;?>
                                  </select>
                                <?php else: ?>
                                  <select data-dir-user="<?= $arrayCliente['id_comuna'];?>" class="custom-select mb-15" name="carro_comuna" required="required">
                                    <option value="">Seleccione Comuna</option>
                                  </select>
                                <?php endif ?>
                                
                                <textarea class="custom-textarea mb-15" data-dir-user="<?= $arrayCliente['direccion'];?>" placeholder="Dirección" maxlenght="240" name="carro_direccion" required="required"><?= $carro['direccion_carro'];?></textarea>
                                <textarea class="custom-textarea" placeholder="Datos referenciales" name="carro_mensaje"><?= $carro['mensaje'];?></textarea>
                                <button type="button" id="btn_guardar_direccion" data-text="Guardar" class="button-one floatright mt-15">Guardar</button>
                              </form>                 
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 mt-60  mt-xs-30">
                            <div class="our-order payment-details pr-20">
                              <h4 class="title-1 title-border text-uppercase mb-30">Retirar en el Local</h4>
                              <div class="contact-details">
                                <p class="mb-0"><input type="checkbox" <?= ($carro['retiro_tienda']==1)?'checked':'';?> data-val="<?= $carro['id'];?>" id="set_retiro_tienda"> Deseo retirar en el local PUYA<br><br></p>
                                <ul>
                                  <li>
                                    <i class="zmdi zmdi-truck"></i>
                                    <span>Puedes retirar a contar del día,</span>
                                    <span>Sábado 09 de Septiembre de 2017</span>
                                  </li>
                                  <!--<li>
                                    <i class="zmdi zmdi-time"></i>
                                    <span>Horario de atención en tienda:</span>
                                    <span>Lunes a Domingo 11:00 a 19:00 hrs</span>
                                  </li>-->
                                  <li>
                                    <i class="zmdi zmdi-email"></i>
                                    <span><strong>Notificación:</strong>Enviaremos un correo a <strong><?= $arrayCliente['email'];?></strong> cuando el producto este listo para ser retirado. Sólo deberás presentar tu boleta en la tienda.</span>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div class="our-order payment-details pr-20">
                              <h4 class="title-1 title-border text-uppercase mb-30">Resumen de su orden</h4>
                              <div id="content_resumen_envio">
                                <table>
                                  <thead>
                                    <tr>
                                      <th><strong>Producto</strong></th>
                                      <th class="text-right"><strong>Total</strong></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td> -- </td>
                                      <td class="text-right">$ --</td>
                                    </tr>
                                    
                                    <tr>
                                      <td>Orden Total</td>
                                      <td class="text-right">$ --</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          
                          <!-- payment-method -->
                        <!--<button type="submit" data-text="Continuar" class="button-one submit-button mt-15 pull-right">Continuar</button>-->
                        </div>
                        
                      </div>
                  </div>
                  <!-- wishlist end -->
                  <!-- check-out start -->
                  <div class="tab-pane" id="medio-pago">
                      <div class="shop-cart-table check-out-wrap">
                        <div class="row">
                          <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="our-order payment-details pr-20">
                              <h4 class="title-1 title-border text-uppercase mb-30">Resumen de su orden</h4>
                              <div id="content_resumen_pago">
                                <table>
                                  <thead>
                                    <tr>
                                      <th><strong>Producto</strong></th>
                                      <th class="text-right"><strong>Total</strong></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td> -- </td>
                                      <td class="text-right">$ -- </td>
                                    </tr>
                                    <tr>
                                      <td>Valor envio</td>
                                      <td class="text-right">$ --</td>
                                    </tr>
                                    <tr>
                                      <td>Orden Total</td>
                                      <td class="text-right">$ --</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <!-- payment-method -->
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <div id="contenedor_medios_pago">
                                <div class="payment-method pl-20">
                                  <h4 class="title-1 title-border text-uppercase mb-30">Medios de Pago</h4>
                                  <div class="payment-accordion">
                                    <!-- Accordion start  -->
                                    <h3 class="payment-accordion-toggle active">Webpay - Transbank</h3>
                                    <div class="row">
                                      <div class="col-xs-6"><a href="#"><img src="<?= base_url('assets')?>/img/webpay.png" class="img-responsive"></a></div>
                                      <!--<div class="col-xs-6"><a href="#"><img src="<?= base_url('assets')?>/img/khipu.jpg" class="img-responsive"></a></div>-->                                  
                                    </div> 
                                    <!-- Accordion end -->
                                    <!-- Accordion start -->
                                    <h3 class="payment-accordion-toggle">Transferencia Electronica</h3>
                                    <div class="payment-content">
                                      <p>Datos de transferencia</p>
                                    </div>
                                    <!-- Accordion end -->
                                  </div>                              
                                </div>
                            </div>
                            
                          </div>
                        </div>
                      </div>                  
                  </div>
                  <!-- check-out end -->                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- ORDER-AREA END -->