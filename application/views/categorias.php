<?php //json_encode($arrayCategoria); ?>
<!--
<br>
<span>id: <?= $arrayCategoria['id'] ?></span>
<br>
<span>nombre: <?= $arrayCategoria['nombre'] ?></span>
<br>
<span>descripcion: <?= $arrayCategoria['descripcion'] ?></span>
<br>
<span>imagen: <?= $arrayCategoria['imagen'] ?></span>
<br>
<span>tipo_categoria: <?= ($arrayCategoria['tipo_categoria']==0)?'Producto':'Servicio'; ?></span>
-->
<?php 
/*
data producto
{"id":,
"nombre":,
"descripcion":,
"precio":,
"foto_etiqueta":"",
"foto_producto":"",
"foto_home":"",
"sku":"",
"descuento":"",
"envio_gratis":"",
"medida_s":"",
"medida_m":"",
"medida_l":"",
"medida_xl":"",
"categoria_id":"",
"color":,
"marca_id":"",
"stock":"",
"tipo_entrega":"",
"regalos":"",
"categoria":"",
"marca":""}*/ ?>

<!-- HEADING-BANNER START -->
			<div class="heading-banner-area-muebles overlay-bg" style="background: rgba(0, 0, 0, 0) url(<?= $arrayCategoria['imagen'] ?>) no-repeat scroll center center / cover;">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="heading-banner">
								<div class="heading-banner-title">
									<h2><?= $arrayCategoria['nombre'] ?></h2>
								</div>
								<div class="breadcumbs pb-15">
									<ul>
										<li><a href="<?= base_url();?>">Home</a></li>
										<li><?= $arrayCategoria['nombre'] ?></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- HEADING-BANNER END -->
			<div class="banner-area pt-80">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-9 col-sm-12 col-xs-12 mt-tab-30">
							<?php 
							 $f = 0; //contador para filas par o impar
							 $p = 0; //contador para productos
							 $r = 0; //contador para filas
							 $listaprueba = array("p1","p2","p3","p4","p5","p6","p7");
							 //foreach ($lista_productos as $itemProd):
							 foreach ($lista_productos as $itemProd): 
							 	if ( ($f%2)==0):
							 		$r++;
							 		?>
								<div class="row pb-30">
							 	<?php endif;
							 	 ?>
							 	 <?php if (($r%2)!=0): ?>
							 	 	<div class="<?= ($p%2==0)?'col-lg-5 col-md-5 col-sm-5':'col-lg-7 col-md-7 col-sm-7';?> col-xs-12">
							 	 <?php else: ?>
							 	 	<div class="<?= ($p%2==0)?'col-lg-7 col-md-7 col-sm-7':'col-lg-5 col-md-5 col-sm-5';?> col-xs-12">
							 	 <?php endif ?>
									<!-- Single-banner start -->
									<div class="single-banner banner-1 banner-4">
										<a class="banner-thumb" href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>"><img src="<?=$itemProd['foto_producto']?>" alt="" /></a>
										<?php
											$date1 = new DateTime("now");
					                      $date2 = new DateTime($itemProd['fecha_creacion']);

					                      $diff = $date1->diff($date2);
					                      //un proucto creado hace menos de 3 semanas es considerado nuevo
										 if (($diff->d)<21 &&
										  	 ($diff->m)<1 &&
											 ($diff->y)<1 ): ?>
										<span class="pro-label new-label">Nuevo</span>
											
										<?php endif ?>
										<span class="price">
											<?php 
						                      if ($itemProd['descuento']>0):
						                          
						                          echo "$".number_format(ceil($itemProd['precio']-(($itemProd['precio']*$itemProd['descuento'])/100)) ,0, "," ,".")."<s><small>$".number_format(ceil($itemProd['precio']) ,0, "," ,".")."</small></s>";
						                      else:
						                      	 echo "$".number_format(ceil($itemProd['precio']) ,0, "," ,".");
						                      endif; ?>
										</span>
										<div class="banner-brief">
											<h2 class="banner-title"><a href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>"><?= $itemProd['nombre'] ?></a></h2>
											<p class="mb-0"><?= $itemProd['sub_categoria'] ?></p>
										</div>
										<?php if(isset($_SESSION['cliente_data_puyasess']['logeado'])): ?>
											<a href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>" class="button-one font-16px" data-text="Agregar al Carrito">Agregar al Carrito</a>
										<?php else: ?>
											<a href="<?= base_url('home/registro?next='.'productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])))?>" class="button-one font-16px" data-text="Agregar al Carrito">Agregar al Carrito</a>
										<?php endif; ?>										
									</div>
									<!-- Single-banner end -->					
								</div>
							<?php 
							$f++;
							$p++;
							echo ($f%2==0 )?'</div>':'';
							 endforeach ;
							echo (count($lista_productos)%2 != 0)?'</div>':''; ?>
							<!--<div class="row pb-30">
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
									<div class="single-banner banner-1 banner-4">
										<a class="banner-thumb" href="producto-int.html"><img src="<?= base_url('assets/')?>img/banner/3.jpg" alt="" /></a>
										<span class="pro-label new-label">Nuevo</span>
										<span class="price">$96.000</span>
										<div class="banner-brief">
											<h2 class="banner-title"><a href="producto-int.html">Nomre de Producto</a></h2>
											<p class="mb-0">Silla</p>
										</div>
										<a href="#" class="button-one font-16px" data-text="Agregar al Carrito">Agregar al Carrito</a>
									</div>					
								</div>
								<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
									<div class="single-banner  banner-1 banner-4">
										<a class="banner-thumb" href="producto-int.html"><img src="<?= base_url('assets/')?>img/banner/4.jpg" alt="" /></a>
										<span class="pro-label new-label">Nuevo</span>
										<span class="price">$121.000</span>
										<div class="banner-brief">
											<h2 class="banner-title"><a href="producto-int.html">Nomre de Producto</a></h2>
											<p class="mb-0">Silla</p>
										</div>
										<a href="#" class="button-one font-16px" data-text="Agregar al Carrito">Agregar al Carrito</a>
									</div>						
								</div>
							</div>
							-->
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12 mt-tab-30">
							<!-- Widget-Categories start -->
							<aside class="widget widget-categories  mb-30">
								<div class="widget-title">
									<h4>Descripción</h4>
								</div>
								<div class="widget-info">
									<?= $arrayCategoria['descripcion'];?>
								</div>
							</aside>
							<!-- Widget-categories end -->
							<!-- Widget-Categories start -->
							<aside class="widget widget-categories  mb-30">
								<div class="widget-title">
									<h4>Categorias</h4>
								</div>
								<div id="cat-treeview"  class="widget-info product-cat boxscrol2">
									<ul>
										<?php foreach ($lista_sub_categorias as $subItem): ?>
											<li><span><?= $subItem['nombre']; ?></span>
												<ul>
													<?php 
												 $f = 0; //contador para filas par o impar
												 $p = 0; //contador para productos
												 $r = 0; //contador para filas
												 //foreach ($lista_productos as $itemProd):
												 foreach ($lista_productos as $itemProd): 
													 if ($subItem['id'] == $itemProd['sub_categoria_id'] ):
												 	 ?>
														<li><a href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>"><?= $itemProd['nombre'] ?></a></li>
													<?php 
													$f++;
													endif;
												endforeach ;?>
												</ul>
											</li>
										<?php endforeach;?>
										<!--<li><span>Sillas</span>
											<ul>
												<li><a href="#">Silla 1</a></li>
												<li><a href="#">Silla 2</a></li>
												<li><a href="#">Silla 3</a></li>
												<li><a href="#">Silla 4</a></li>
												<li><a href="#">Silla 5</a></li>
											</ul>
										</li>  -->
									</ul>
								</div>
							</aside>
							<!-- Widget-categories end -->
							<!-- Widget-product start -->
							<aside class="widget widget-product mb-30">
								<div class="widget-title">
									<h4>Nuevos Productos</h4>
								</div>
								<div class="widget-info sidebar-product clearfix">
									<?php 
									 $f = 0; //contador para filas par o impar
									 $p = 0; //contador para productos
									 $r = 0; //contador para filas
									 //foreach ($lista_productos as $itemProd):
									 foreach ($lista_productos as $itemProd): 
									 	$date1 = new DateTime("now");
					                      $date2 = new DateTime($itemProd['fecha_creacion']);

					                      $diff = $date1->diff($date2);
					                      //un proucto creado hace menos de 3 semanas es considerado nuevo
										 if (($diff->d)<21 ):
									 	 ?>
											<!-- Single-product start -->
											<div class="single-product col-lg-12">
												<div class="product-img">
													<a href="<?= base_url('productos/view/'.$itemProd['id'].'/'.str_replace("'","",str_replace('%','',$itemProd['nombre'])));?>"><img src="<?=$itemProd['foto_producto']?>" alt="" /></a>
												</div>
												<div class="product-info">
													<h4 class="post-title"><a href="#"><?= $itemProd['nombre'] ?></a></h4>
													<span class="pro-price"><?php 
								                      if ($itemProd['descuento']>0):
								                          echo "$".number_format(ceil($itemProd['precio']-(($itemProd['precio']*$itemProd['descuento'])/100)) ,0, "," ,".");
								                      else:
								                      	 echo "$".number_format(ceil($itemProd['precio']) ,0, "," ,".");
								                      endif; ?>
									                </span>
												</div>
											</div>
											<!-- Single-product end -->
										<?php 
										$f++;
										endif;
									endforeach ;?>
																		
								</div>
							</aside>
							<!-- Widget-product end -->
						</div>
					</div>
				</div>
			</div>