
<style >
  .navbar-secundary {padding-bottom: 40px;}
</style>
<div class="row ">
  <div class="col-md-12 baner-category" style="background:url(<?=$arrayCategoria['imagen'];?>);background-size:cover;background-position:center;">

    <h1 class="title-category"><?=$arraySubCategoria['nombre'];?></h1>
  </div>
</div>

<div class="row">
<!--  Box producto destacado-->
<?php if (count($lista_productos)>0): ?>
<div class="col-md-3">
    <div class="box-filtros">
      <!-- box-filtro  -->
      <div class="row">
        <div class="col-md-6">
          <div class="title-filtro">Marca</div>
        </div>
        <div class="col-md-6">
          <div class="title-filtro text-right">Cantidad</div>
        </div>
      </div>
      <?php foreach($lista_marcas as $marcaItem):
        if($marcaItem['productos']>0):
      ?>
          <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
             <input id="checkbox<?= $marcaItem['id']; ?>" type="checkbox" class="check_filter_brand_subcate" name="chk_filtro_marcas_subcate" value="<?= $marcaItem['id']; ?>" aria-label="<?= $marcaItem['nombre']; ?>">
             <label class="nombre-opcion" for="checkbox<?= $marcaItem['id']; ?>">
                <?= $marcaItem['nombre']; ?>
              </label>
            <span class="cantidad"><?= $marcaItem['productos']; ?></span>
          </div>
      <?php
        endif;
      endforeach; ?>
      <!-- box-filtro  -->
      <!--<div class="row">
        <div class="col-md-6">
          <div class="title-filtro">Marca</div>
        </div>
        <div class="col-md-6">
          <div class="title-filtro text-right">Cantidad</div>
        </div>
      </div>
      <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
        <input id="checkbox5" type="checkbox" >
        <label class="nombre-opcion" for="checkbox5">
        Me too
        </label>
        <span class="cantidad">10</span>
      </div>
      <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
        <input id="checkbox6" type="checkbox" >
        <label class="nombre-opcion" for="checkbox6">
        Me too
        </label>
        <span class="cantidad">10</span>
      </div>
      <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
        <input id="checkbox7" type="checkbox" >
        <label class="nombre-opcion" for="checkbox7">
        Me too
        </label>
        <span class="cantidad">10</span>
      </div>
      <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
        <input id="checkbox8" type="checkbox" >
        <label class="nombre-opcion" for="checkbox8">
        Me too
        </label>
        <span class="cantidad">10</span>
      </div>-->

      <!-- box-filtro  -->
      <div class="row">
        <div class="col-md-6">
          <div class="title-filtro">Precio</div>
        </div>
        <div class="col-md-6">
          <div class="title-filtro text-right">Cantidad</div>
        </div>
      </div>

      <?php
      $precio0to10 = 0;
      $precio11to20 = 0;
      $precio21to35 = 0;
      $precio36to50 = 0;
      $precio50toPlus = 0;
      $valorData = 0;

      foreach($lista_productos as $productoItem):
        #if($productoItem['productos']>0):
        if ($productoItem['precio']<=10000) {
          $precio0to10+=1;
        }else if($productoItem['precio']>10000 && $productoItem['precio']<=20000 ) {
          $precio11to20+=1;
        }else if($productoItem['precio']>20000 && $productoItem['precio']<=35000) {
          $precio21to35+=1;
        }else if($productoItem['precio']>35000 && $productoItem['precio']<=50000) {
          $precio36to50+=1;
        }else if($productoItem['precio']>50000) {
          $precio50toPlus+=1;
        }

      endforeach;
        if ($precio0to10>0): ?>
          <div class="opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek1" class="check_filter_precio_subcate" name="chk_filtro_precio_sub_cate" value="1" aria-label="menos de $10.000">
            <label class="nombre-opcion" for="chek1">
            menos de $10.000
            </label>
            <span class="cantidad"><?= $precio0to10; ?></span>
          </div>
        <?php
        endif;
        if ($precio11to20>0): ?>
        <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek2" class="check_filter_precio_subcate" name="chk_filtro_precio_sub_cate" value="2" aria-label="$11.000 a $20.000">
            <label class="nombre-opcion" for="chek2">
             $11.000 a $20.000
            </label>
            <span class="cantidad"><?= $precio11to20; ?></span>
          </div>
        <?php
        endif;
        if ($precio21to35>0): ?>
        <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek3" class="check_filter_precio_subcate" name="chk_filtro_precio_sub_cate" value="3" aria-label="$21.000 a $35.000">
            <label class="nombre-opcion" for="chek3">
            $21.000 a $35.000
            </label>
            <span class="cantidad"><?= $precio21to35; ?></span>
          </div>
        <?php
        endif;
        if ($precio36to50>0): ?>
        <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek4" class="check_filter_precio_subcate" name="chk_filtro_precio_sub_cate" value="4" aria-label="$36.000 a $50.000">
            <label class="nombre-opcion" for="chek4">
            $36.000 a $50.000
            </label>
            <span class="cantidad"><?= $precio36to50; ?></span>
          </div>
        <?php
        endif;
        if ($precio50toPlus>0): ?>
          <div class=" opcion-filtro checkbox checkbox-info checkbox-circle">
            <input type="checkbox" id="chek5" class="check_filter_precio_subcate" name="chk_filtro_precio_sub_cate" value="5" aria-label="$50.000 o mas">
            <label class="nombre-opcion" for="chek5">
            $50.000 o más
            </label>
            <span class="cantidad"><?= $precio50toPlus; ?></span>
          </div>
        <?php
        endif;
        ?>
    </div>
</div>

<div class="col-md-9">
  <div class="row tab-content" id="content_main_sub_categorias"  data-view="<?=$arraySubCategoria["id"];?>">

        <!-- box-producto
      <div class="col-md-3">
        <div class="item-thumbnail">
          <img src="images/destacado1.png" alt="">
        </div>
        <div class="item-details">
          <h1 class="item-title">Platinum  <b>Hydrowhey</b></h1>
          <p class="item-info">PROTEINA ISOLATADA MAS PRE ENTRENO NITRAPROTEINA ISOLATADA MAS PRE ENTRENO NITRA</p>
          <div class="row">
            <div class="col-md-6">
              <div class="item-envio">
                <span>
                  <svg class="icon" viewBox="0 0 46 32">
					  <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.598 21.321v-19.371c0-1.077 0.873-1.95 1.95-1.95v0h19.362c1.077 0 1.95 0.873 1.95 1.95v19.373c0 0.353-0.287 0.64-0.64 0.64v0h-21.973c-0.003 0-0.006 0-0.009 0-0.353 0-0.64-0.287-0.64-0.64 0-0.001 0-0.002 0-0.002v0zM17.233 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c2.233 0 4.043 1.81 4.043 4.043v0zM15.211 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022 0.001 0 0.002 0 0.002 0h-0c1.116 0 2.020-0.904 2.020-2.020 0-0.001 0-0.002 0-0.002v0zM9.707 23.915h-9.067c-0.353 0-0.64 0.287-0.64 0.64v0 1.971c0 0.353 0.287 0.64 0.64 0.64h7.253c0.207-1.311 0.865-2.439 1.808-3.245l0.007-0.006zM38.383 27.957c0 2.233-1.81 4.043-4.043 4.043s-4.043-1.81-4.043-4.043c0-2.233 1.81-4.043 4.043-4.043v0c0.001 0 0.001 0 0.002 0 2.233 0 4.043 1.81 4.043 4.043v0zM36.363 27.957c0-1.117-0.905-2.022-2.022-2.022s-2.022 0.905-2.022 2.022c0 1.117 0.905 2.022 2.022 2.022v0c1.117 0 2.022-0.905 2.022-2.022v0zM45.536 24.565v1.971c0 0.353-0.287 0.64-0.64 0.64h-5.276c-0.382-2.604-2.6-4.581-5.28-4.581s-4.898 1.977-5.276 4.552l-0.003 0.029h-10.59c-0.198-1.316-0.851-2.452-1.792-3.265l-0.007-0.006h10.044v-18.575c0-0.718 0.582-1.299 1.299-1.299h6.133c0.001 0 0.002 0 0.003 0 1.783 0 3.357 0.898 4.293 2.267l0.012 0.018 3.955 5.856c0.56 0.816 0.895 1.825 0.896 2.912v8.83h1.581c0.001 0 0.003 0 0.004 0 0.353 0 0.64 0.287 0.64 0.64 0 0.004-0 0.008-0 0.011v-0.001zM38.944 12.329l-3.162-4.493c-0.117-0.167-0.309-0.275-0.526-0.275-0.002 0-0.004 0-0.006 0h-4.936c-0.353 0-0.64 0.287-0.64 0.64v0 4.493c0 0.353 0.287 0.64 0.64 0.64v0h8.085c0.005 0 0.010 0 0.016 0 0.353 0 0.64-0.287 0.64-0.64 0-0.137-0.043-0.265-0.117-0.369l0.001 0.002z"></path>
					  </svg>
				</span>
			    Envio gratis
				</div>

            </div>
            <div class="col-md-6">
              <div class="item-regalo">
				  <span><svg class="icon" viewBox="0 0 29 32">
				  <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M15.407 17.521h11.445v14.479h-11.445v-14.479z"></path>
				  <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M1.873 17.521h11.445v14.479h-11.445v-14.479z"></path>
				  <path fill="#00aff1" style="fill: var(--color1, #00aff1)" d="M23.285 5.903c0.306-0.551 0.486-1.209 0.486-1.909 0-2.206-1.788-3.994-3.994-3.994-0.001 0-0.001 0-0.002 0h0c-1.85 0.082-3.48 0.961-4.565 2.299l-0.009 0.012c-0.299 0.343-0.576 0.722-0.82 1.124l-0.020 0.036c-0.264-0.438-0.541-0.818-0.849-1.17l0.008 0.010c-1.094-1.349-2.722-2.228-4.558-2.31l-0.013-0c-0.001 0-0.001 0-0.002 0-2.206 0-3.994 1.788-3.994 3.994 0 0.7 0.18 1.358 0.496 1.93l-0.010-0.021h-5.44v9.532h13.318v-9.532h2.086v9.532h13.318v-9.532zM8.949 5.903c-1.053 0-1.907-0.854-1.907-1.907s0.854-1.907 1.907-1.907v0c2.065 0.306 3.709 1.814 4.21 3.779l0.008 0.035h-4.218zM19.778 5.903h-4.22c0.508-2.001 2.152-3.51 4.19-3.813l0.028-0.003c1.053 0 1.907 0.854 1.907 1.907s-0.854 1.907-1.907 1.907v0z"></path>
				  </svg></span>
				  Regalo
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="item-price">
                $35.000
              </div>
            </div>
            <div class="col-md-6">
              <button class="btn-item-car" type="button" name="button"> Comprar</button>
            </div>
          </div>
        </div>
      </div>
      fin-producto -->
  </div>
  <!--  pagination-->
  <div class="row">
    <div class="col-md-12 text-center grid-pagination">
      <nav aria-label="Page navigation ">
      <ul class="pagination" id="pagination_sub_categorias" role="tablist">
        <!--<li>
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>-->
      </ul>
    </nav>
    </div>
  </div><!-- fin pagination -->

</div>
<?php else: ?>
  <div class="container">
    <div class="sin-categoria">
      <div class="">
        <svg class="icon icon-triste" viewBox="0 0 32 32">
        <title>sad</title>
        <path d="M16 32c8.837 0 16-7.163 16-16s-7.163-16-16-16-16 7.163-16 16 7.163 16 16 16zM16 3c7.18 0 13 5.82 13 13s-5.82 13-13 13-13-5.82-13-13 5.82-13 13-13zM8 10c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM20 10c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM9.997 24.398l-2.573-1.544c1.749-2.908 4.935-4.855 8.576-4.855s6.827 1.946 8.576 4.855l-2.573 1.544c-1.224-2.036-3.454-3.398-6.003-3.398s-4.779 1.362-6.003 3.398z"></path>
        </svg>
      </div>
      <h2>No se encontrar productos en esta sub categoria</h2>
      <div class="bnt-volver">
        <a class="btn-gys" href="#">Volver al Inicio</a>
      </div>
    </div>


  </div>

    <!-- tentativo colocar productos relacionados o destacados -->
<?php endif ?>
  <!-- Fin producto destacado -->
</div>
