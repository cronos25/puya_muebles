<?php
/**
 * Clase para Configurar el cliente
 * @Filename: Config.class.php
 * @version: 2.0
 * @Author: flow.cl
 * @Email: csepulveda@tuxpan.com
 * @Date: 28-04-2017 11:32
 * @Last Modified by: Carlos Sepulveda
 * @Last Modified time: 28-04-2017 11:32
 */
 
 


 class ConfigClass {
 	
	static function get($name) {
		$COMMERCE_CONFIG = array(
			"APIKEY" => "3CBF2073-7607-4B0E-AE4E-79EF457L2606", // Registre aquí su apiKey
			"SECRETKEY" => "884f5945699e1469b2f0647cb23e42669f78935a", // Registre aquí su secretKey
			"APIURL" => "https://sandbox.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
			"BASEURL" => "https://www.micomercio.cl/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
		);
		
		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new Exception("The configuration element thas not exist", 1);
		}
		return $COMMERCE_CONFIG[$name];
	}

 }

$reflectionClass = new ReflectionClass('ConfigClass');

$method = new ReflectionMethod('ConfigClass', 'get');
$method->setAccessible(true);
