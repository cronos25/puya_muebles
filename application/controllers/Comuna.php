<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comuna extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//cargo el modelo de comunaes
            $this->load->model('Comuna_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		redirect('admin/index', 'refresh'); 
	}

	public function obtenerComunas(){
		if ($this->input->is_ajax_request()) {
			echo json_encode($this->Comuna_model->get_comunas($this->input->post('id_comuna')));
		}else{
			show_404();
		}
	}

	public function obtenerComunasProvincia(){
		if ($this->input->is_ajax_request()) {
			echo json_encode($this->Comuna_model->obtener_lista_comunas_provincia($this->input->post('id_provincia')));
		}else{
			show_404();
		}
	}

	public function guardarDataComuna(){
		if ($this->input->is_ajax_request()) {
			$arrayComuna = array('nombre'=>$this->input->post("nombre_comuna"),
							'id_provincia'=>$this->input->post("id_provincia"));
			$existeComuna = $this->Comuna_model->existe_comuna($arrayComuna['nombre'],$arrayComuna['id_provincia']);
			if(!$existeComuna){
				if($this->Comuna_model->inserta_comuna($arrayComuna)){
					echo "Registrado con éxito";
				}else{
					if (empty($arrayComuna['nombre'])&&
         				empty($arrayComuna['id_provincia'])) {
						echo "No puede tener datos vacios";
					}else if(empty($arrayComuna['nombre'])){
						echo "Falta el nombre de comuna";
					}else if(empty($arrayComuna['id_provincia'])){
						echo "Falta la provincia de la comuna";
					}
				}
			}else{
				echo "Error: La comuna ".$arrayComuna['nombre']." ya existe en esta provincia";
			}
		}else{
			show_404();
		}
		
	}

	public function editDataComuna($comuna = null){
		if ($this->input->is_ajax_request()) {
			$arrayComuna = array('id'=>$this->input->post("id_comuna"),
							'nombre'=>$this->input->post("nombre_comuna"),
							'id_provincia'=>$this->input->post("id_provincia"));

			if($this->Comuna_model->edita_comuna($arrayComuna)){
				echo "editado con exito";
			}else{
				echo "Error al editar Comuna, la comuna ".$arrayComuna['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataComuna($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Comuna_model->elimina_comuna($this->input->post("id_comuna"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Comuna";
			}
		}else{
			show_404();
		}
	}

}

/* End of file Comuna.php */
/* Location: ./application/controllers/Comuna.php */