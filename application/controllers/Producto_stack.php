<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto_stack extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Producto_stack_model');
            $this->load->model('Producto_model');
            $this->load->library('upload');
            $this->load->library('Comprime');
            
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	public function index(){
		show_404();
	}

	public function guardarDataProdStack(){

		if ($this->input->is_ajax_request()) {
			//upload file
			$actualProducto = $this->Producto_model->get_producto($this->input->post("id_producto"));
			$arrayStack = array('id_stack'=>$this->input->post("id_stack"),
							'id_producto'=>$this->input->post("id_producto"),
							'precio_producto'=>$actualProducto['precio']);

			$existeStack = $this->Producto_stack_model->existe_producto_stack($arrayStack['id_stack'],$arrayStack['id_producto']);
			if(!$existeStack){
				if($this->Producto_stack_model->inserta_producto_stack($arrayStack)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el stack ya contiene el producto ".$actualProducto['nombre'];
			}
		}else{
			show_404();
		}
	}

	public function editDataProdStack($user = null){
		if ($this->input->is_ajax_request()) {
			//upload file
			$arrayStack = array('id'=>$this->input->post("id_prod_stack"),
							'id_stack'=>$this->input->post("id_stack"),
							'id_producto'=>$this->input->post("id_producto"));

			if($this->Producto_stack_model->edita_producto_stack($arrayStack)){
				echo "Registrado con éxito";
			}else{
				echo "Error al registrar";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataProdStack($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Producto_stack_model->elimina_producto_stack($this->input->post("id_prod_asoc"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar el producto asociado";
			}
		}else{
			show_404();
		}
	}

	public function getDataListStacks(){
		if ($this->input->is_ajax_request()) {
			if($this->input->post("dfb") != null){
				if($this->input->post("marvalue")!= null){
					echo json_encode(array("estado"=>0,
						 "stacks"=>$this->Producto_stack_model->get_productos_filtrados_stacks($this->input->post("marvalue"),$this->input->post("dfb"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data filtro"));
				}
			}else{
				if($this->input->post("marvalue")!= null){
					echo json_encode(array("estado"=>0,
					 "stacks"=>$this->Producto_stack_model->get_productos_producto_stack($this->input->post("marvalue"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data normal"));
				}
			}
		}else{
			show_404();
		}
	}

}
