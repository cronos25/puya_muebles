<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vistas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('html','url_helper');
        $this->load->model('Producto_model');
        $this->load->model('Categoria_model');
        $this->load->model('Sub_categoria_model');
        $this->load->model('Banner_home_model');
        $this->load->model('Marca_model');
        $this->load->model('Promocion_temporal_model');
        $this->load->model('Oferta_especial_model');
	}

	public function index()
	{
		show_404();
	}

	public function preguntas_frecuentes(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		
	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		//creo el array con datos de configuración para la vista
		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('preguntas_frecuentes');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	public function contacto(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo

	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

		$dataPagina['respmail']="";
		if ($this->input->post('contacto_nombre') &&
			$this->input->post('contacto_mail') &&
			$this->input->post('contacto_asunto') && 
			$this->input->post('contacto_comentario')) {

			//$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';


			$this->email->initialize($config);

			$this->email->from('contacto@puyamuebles.cl', 'Contacto desde sitio Puya');
			$this->email->to('contacto@puyamuebles.cl');
			$this->email->subject('Contacto de desde sitio web motivo: '.$this->security->xss_clean($this->input->post('contacto_asunto')));
			$this->email->message('<!DOCTYPE html><html>
					 <head>
					  <title>Email Puya</title>
					</head>
					<body style="margin: 0; padding: 0;">
					   <table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
					      <tr style="color: #fff; font-family: Roboto,sans-serif">
					        <td align="center">
					         <img src="'.base_url('assets/img/logo.png').'" alt="" width="auto" height="150" style="display: block;padding:30px;" />
					        </td>
					      </tr>
					      
					      <tr>
					            <th>
					              <h1 style="font-family: roboto,sans-serif; color: #343434;">Contacto Sitio</h1>
					            </th>
					         </tr>
					         <tr>
					            <th style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 5px 30px;">
					               Nombre: '.$this->security->xss_clean($this->input->post('contacto_nombre')).'
					            </th>
					         </tr>
					         <tr>
					            <th style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 5px 30px;">
					               Email: <a href="mailto:'. $this->security->xss_clean($this->input->post('contacto_mail')) .'" target="_top">'. $this->security->xss_clean($this->input->post('contacto_mail')) .'</a>
					            </th>
					         </tr>
					         <tr>
					            <td style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 5px 30px;" >
					               Mensaje : 
					            </td>
					         </tr>
					         <tr>
					            <td style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 30px;">
					            '.$this->security->xss_clean($this->input->post('contacto_comentario')).'
					            </td>
					         </tr>
					   </table>
					</body>
					</html>');
			//$this->email->set_alt_message('This is the alternative message');
			if ($this->email->send()){
				$dataPagina['respmail']= "<script>
			$(document).ready(function(){
				new Noty({		
					type: 'info',    
					closeWith: ['click', 'button'],
					timeout: 1500,
					text: 'Se ha enviado el mensaje con exito, se le contactará a la brevedad'
				}).show();
			});
				</script>";
			}
			# code...
		} 

		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('contacto');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	public function envios(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		//cargo la vista pasando los datos de configuacion
		$this->load->view('layout/head',$dataPagina);
		$this->load->view('layout/nav',$dataPagina);

		//en esta parte va el contenido personalizado
		$this->load->view('envios');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	public function condiciones_uso(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

		//cargo la vista pasando los datos de configuacion
		$this->load->view('layout/head',$dataPagina);
		$this->load->view('layout/nav',$dataPagina);

	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		//en esta parte va el contenido personalizado
		$this->load->view('condiciones_uso');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	public function mision_vision_valores(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);
		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('mision_vision');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	public function quienes_somos(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		
	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('quienes_somos');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	public function catalogo(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		
	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('catalogo');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}
}

/* End of file Vistas.php */
/* Location: ./application/controllers/Vistas.php */ ?>