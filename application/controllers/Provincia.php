<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provincia extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//cargo el modelo de provincias
            $this->load->model('Provincia_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		redirect('admin/index', 'refresh'); 
	}

	public function obtenerProvincias(){
		if ($this->input->is_ajax_request()) {
			echo json_encode($this->Provincia_model->get_provincias($this->input->post('id_provincia')));
		}else{
			show_404();
		}
	}

	public function obtenerProvinciasRegion(){
		if ($this->input->is_ajax_request()){

			$arrayCR = $this->Provincia_model->obtener_lista_provincias_region(
				$this->input->post("id_region"));

			echo json_encode($arrayCR);
		}else{
			show_404();
		}
	}

	public function guardarDataProvincia(){
		if ($this->input->is_ajax_request()) {
			$arrayProvincia = array('nombre'=>$this->input->post("nombre_provincia"),
							'id_region'=>$this->input->post("id_region"));
			$existeProvincia = $this->Provincia_model->existe_provincia($arrayProvincia['nombre'],$arrayProvincia['id_region']);
			if(!$existeProvincia){
				if($this->Provincia_model->inserta_provincia($arrayProvincia)){
					echo "Registrado con éxito";
				}else{
					if (empty($arrayProvincia['nombre'])&&
         				empty($arrayProvincia['id_region'])) {
						echo "No puede tener datos vacios";
					}else if(empty($arrayProvincia['nombre'])){
						echo "Falta el nombre de provincia";
					}else if(empty($arrayProvincia['id_region'])){
						echo "Falta la region de la provincia";
					}
				}
			}else{
				echo "Error: La provincia ".$arrayProvincia['nombre']." ya existe en esta region";
			}
		}else{
			show_404();
		}
	}

	public function editDataProvincia($provincia = null){
		if ($this->input->is_ajax_request()) {
			$arrayProvincia = array('id'=>$this->input->post("id_provincia"),
							'nombre'=>$this->input->post("nombre_provincia"),
							'id_region'=>$this->input->post("id_region"));

			if($this->Provincia_model->edita_provincia($arrayProvincia)){
				echo "editado con exito";
			}else{
				echo "Error al editar Provincia,la provincia ".$arrayProvincia['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataProvincia($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Provincia_model->elimina_provincia($this->input->post("id_provincia"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Provincia";
			}
		}else{
			show_404();
		}
	}

}

/* End of file Provincia.php */
/* Location: ./application/controllers/Provincia.php */