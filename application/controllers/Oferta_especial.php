<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oferta_especial extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Oferta_especial_model');
        $this->load->helper('html','url_helper');
	}

	public function index()
	{
		show_404();
	}

	public function estadoOferta($id = null){
		if ($this->input->is_ajax_request()) {
			$arrayOferta = array('id' => $this->input->post("id_oferta"),
								'estado' => $this->input->post("oferta_estado"));			
			/*$this->Especificacion_model->elimina_especificacion_oferta($this->input->post("id_oferta"));*/
			$this->Oferta_especial_model->cambio_estado_oferta($arrayOferta);
			echo "estado actualizado";
		}else{
			show_404();
		}
	}

	public function guardarDataOfertaEspecial(){
		if ($this->input->is_ajax_request()) {
			$arrayOfertaEspecial = array('descuento'=>$this->input->post("descuento_promo_temporal"),
							'id_producto'=>$this->input->post("producto_promo_temporal"));

			if($this->Oferta_especial_model->inserta_oferta_especial($arrayOfertaEspecial)){
				echo "Registrado con éxito";
			}
		}else{
			show_404();
		}
	}

	public function editarDataOfertaEspecial(){
		if ($this->input->is_ajax_request()) {
			$arrayOfertaEspecial = array('id'=>$this->input->post("id_promo_temporal"),
							'descuento'=>$this->input->post("descuento_promo_temporal"),
							'id_producto'=>$this->input->post("producto_promo_temporal"));

			if($this->Oferta_especial_model->edita_oferta_especial($arrayOfertaEspecial)){
				echo "editado con éxito";
			}else{
				echo "no se realizaron cambios";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataOfertaEspecial($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Oferta_especial_model->elimina_oferta_especial($this->input->post("id_oferta_especial"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar el codigo";
			}
		}else{
			show_404();
		}
	}

}

/* End of file Oferta_especial.php */
/* Location: ./application/controllers/Oferta_especial.php */
?>