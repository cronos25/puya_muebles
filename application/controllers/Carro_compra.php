<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carro_compra extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de artículos
            $this->load->model('Carro_compra_model');
            $this->load->model('Categoria_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Marca_model');
            $this->load->model('Producto_model');
            $this->load->model('Precio_envio_model');
            $this->load->model('Promocion_temporal_model');
            $this->load->model('Region_model');
            $this->load->model('Provincia_model');
            $this->load->model('Comuna_model');
            $this->load->model('Venta_model');
            /*
            $this->load->model('pago_paypal/PPaypal_model');*/
            $this->load->model('PWebpay_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
			$this->load->model('Cliente_model');
            $this->load->model('Carro_compra_model');
            $this->load->model('Producto_model');
			$this->load->library('flowApiClass');
            $this->load->helper('html','url_helper');
    }
	public function index()
	{
		show_404();
	}

	public function callShoppingCartActive10Days(){
		if ($this->input->is_ajax_request()) {
			$datavuelta = array();
			$datavuelta = $this->Carro_compra_model->obtener_carros_vendidos();
			echo json_encode($datavuelta);
		}else{
			show_404();
		}
	}
	/*
		mensaje	
		nombre_carro	varchar(200)
		email_carro	varchar(200)
		telefono_carro	varchar(50)
		region_carro	int(11)	
		provincia_carro	int(11)	
		comuna_carro	int(11)		
		direccion_carro	varchar(240)

	*/

	public function direccion_valida(){
		if ($this->input->is_ajax_request()) {
			if($this->Carro_compra_model->existe_direccion($this->input->post("cart"))){
				echo 0;
			}else{
				echo 1;
			}
		}else{
			show_404();
		}
	}

	public function fijaRetiroTienda(){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayDirCarro = array("id"=>$this->security->xss_clean($this->input->post("cart")),
									"retiro_tienda"=>$this->security->xss_clean($this->input->post("retiro_tienda")));
			
			$this->Carro_compra_model->edita_carro_compra($arrayDirCarro);
			if($arrayDirCarro['retiro_tienda']==1){
				echo "se ha fijado el retiro en tienda";
			}else{
				echo "el producto sera enviado a la direccion ingresada";
			}
		}else{
			show_404();
		}
	}

	public function actualizarDireccionCarro(){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayDirCarro = array("id"=>$this->input->post("cart"),
									"nombre_carro"=>$this->security->xss_clean($this->input->post("carro_nombre")),
									"telefono_carro"=>$this->security->xss_clean($this->input->post("carro_telefono")),
									"email_carro"=>$this->security->xss_clean($this->input->post("carro_email")),
									"region_carro"=>$this->input->post("carro_region"),
									"provincia_carro"=>$this->input->post("carro_provincia"),
									"comuna_carro"=>$this->input->post("carro_comuna"),
									"direccion_carro"=>$this->security->xss_clean($this->input->post("carro_direccion")),
									"mensaje"=>$this->security->xss_clean($this->input->post("carro_mensaje")));
			if($this->Carro_compra_model->edita_carro_compra($arrayDirCarro) &&
				$this->Carro_compra_model->existe_direccion($arrayDirCarro['id'])){
				echo "dirección actualizada con éxito";
			}
		}else{
			show_404();
		}
	}

	public function addItemShoppingCart(){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			//tomando el precio del producto
			$userActual = $this->Cliente_model->get_cliente($this->session->userdata('cliente_data')['user_id']);
			$arrayCarro;
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}else{
				$arrayCarro = array('codigo'=>$_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id'],
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}
			
			$arrayAsoc = array("id_producto"=>$this->input->post("producto_data"),
								"id_carro"=>$arrayCarro['id'],	
								"fecha"=>date("Y-m-d"),						
								"cantidad"=>$this->input->post("cantidad_data"),
								"color"=>$this->input->post("color_data"));

			$arrayProducto = $this->Producto_model->get_producto($arrayAsoc['id_producto']);
			
			if($arrayProducto['stock']>=$arrayAsoc['cantidad']){
				/*$valorRealProd = $arrayProducto['precio']- ($arrayProducto['precio']*($arrayProducto['descuento']/100));

				if ($valorRealProd<0){
					$valorRealProd=0;
				}*/

				$arrayAsoc['precio'] = $arrayProducto['precio'];

				$arrayReduceProd = array('id'=>$arrayProducto['id'],
							'stock' => ($arrayProducto['stock']-$arrayAsoc['cantidad']));

				//$arrayAsoc['precio'] = $valorRealProd * $this->input->post("cantidad_data");
				$arrayAsoc['descuento'] = $arrayProducto['descuento'];
				//faltan las funciones de validacion de cantidad de producto para ser agregado al carro de compra
				//falta a agregar la cantidad de productos a asociar
				if(	$this->Carro_compra_model->agregar_producto_carro($arrayAsoc) &&
					$this->Producto_model->edita_cantidad_producto($arrayReduceProd)) {
					$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);

					$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);
					echo " Agregado con exito ";
				}else{
					echo "Error agregando el producto al carro";
				}
			}else{
				echo "La cantidad solicitada es mayor a la disponible, por favor intente nuevamente con una cantidad no mayor a ".$arrayProducto['stock'];
			}
		}else{
			show_404();
		}
	}

	private function devolverStockProducto($stock,$producto){
		$arrayProducto = $this->Producto_model->get_producto($producto);
		$arrayDevuelveStock= array('id'=>$arrayProducto['id'],
								'stock' => ($arrayProducto['stock']+$stock));
		return $this->Producto_model->edita_cantidad_producto($arrayDevuelveStock);
	}

	public function modificaCantidadCarro(){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {

			$arrayAsoc = array("id"=>$this->input->post("asoc_data"),
								"cantidad"=>$this->input->post("cantidad_data"));
			//faltan las funciones de validacion de cantidad de producto para ser agregado al carro de compra
			$asocCarroData = $this->Carro_compra_model->obtener_asoc_carro($this->input->post("asoc_data"));
			$arrayProducto = $this->Producto_model->get_producto($asocCarroData['id_producto']);
				 $cantidadReal = $asocCarroData['cantidad']-$arrayAsoc['cantidad'];
			if($arrayAsoc['cantidad']<=($arrayProducto['stock']+$arrayAsoc['cantidad']) && 
				$arrayAsoc['cantidad']>0){
				if($this->devolverStockProducto($cantidadReal,$asocCarroData['id_producto'])){
				/*$valorRealQuitar = $this->input->post("val_data");
				if($valorRealQuitar<0){
					$valorRealQuitar = 0;
				}*/
					if($this->Carro_compra_model->modifica_cantidad_productos_carro($arrayAsoc)){
						$arrayDataCarro = array('id-carro'=>$asocCarroData['id_carro']);

						$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);
						echo "Cantidad editada a ".$arrayAsoc['cantidad'];
					}else{
						echo "Error editando cantidad";
					}
				}else{
					echo "Error al modificar la cantidad del producto";
				}
			}else{
				echo "Las cantidades solicitadas y existentes no coinciden";
			}
			
		}else{
			show_404();
		}
	}

	public function modificaColorCarro(){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			
			$arrayAsoc = array("id"=>$this->input->post("asoc_data"),
								"color"=>$this->input->post("color_data"));
			//faltan las funciones de validacion de cantidad de producto para ser agregado al carro de compra
			
			if($this->Carro_compra_model->modifica_color_productos_carro($arrayAsoc)){
				echo "color Editado";
			}else{
				echo "Error Editando color";
			}			
		}else{
			show_404();
		}
	}

	public function removeItemShoppingCart($sc = null){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayAsoc = array("id"=>$this->input->post("asoc_data"),
								"id_carro"=>$this->input->post("cart"),
								"estado"=>2);
			//faltan las funciones de validacion de cantidad de producto para ser agregado al carro de compra

			if($this->Carro_compra_model->elimina_productos_carro($arrayAsoc)){
					
				$asocCarroData = $this->Carro_compra_model->obtener_asoc_carro($this->input->post("asoc_data"));

				/*$valorRealQuitar = $this->input->post("val_data");
				if($valorRealQuitar<0){
					$valorRealQuitar = 0;
				}*/

				if($this->devolverStockProducto($asocCarroData['cantidad'],$asocCarroData['id_producto'])){
					$arrayDataCarro = array('id-carro'=>$asocCarroData['id_carro']);

						$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);
					echo "Producto eliminado del carro";
				}else{
					echo "Error eliminando precio al carro";
				}
			}else{
				echo "Error al agregar el producto al carro";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataShoppingCart($id = null){
		if ($this->input->is_ajax_request()) {
			$dataCarro = $this->Carro_compra_model->get_carro_compra_codigo($this->input->post("carro"));
			if ($dataCarro['estado']<3) {
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';

				$html = '<!DOCTYPE html>
						<html>
						<head>
							<meta charset="utf-8">
							<meta http-equiv="X-UA-Compatible" content="IE=edge">
							<title>Mail Seleccion Pago Transferencia</title>
							<link rel="stylesheet" href="">
						</head>
						<body>
							<h2>Aviso de Expiracion forzosa de carro de compra</h2>
							<p>El carro de compra <b>'.$dataCarro['codigo'].'</b> ha expirado por parte del administrador, si usted no conoce el motivo, por favor comuniquese con el administrador de la pagina al mail <a href="mailto:ventas@puya.cl?subject=feedback eliminacion de carro" "email me">ventas@puya.cl</a> o en el chat de servicio de la pagina <a href="'.base_url().'" title="">'.base_url().'</a></p>
						</body>
						</html>';
				

				//hacer una funcion para enviar mail en base al cambio de estado del carro
				$arrayCarro = array('id'=>$dataCarro['id'],
									'estado' => 4);
				if($this->Carro_compra_model->edita_carro_compra($arrayCarro)){
					$arrayVentaCarro = array('id_carro'=>$dataCarro['id'],
							'estado_pago'=>2,
							'estado_venta'=>2);

					$this->Ventas_model->actualizacion_estado_venta_codigo($arrayVentaCarro);

					$this->email->initialize($config);
					$this->email->to($dataCarro['email']);
					$this->email->from('postventa@puya.cl','puya INFO');
					$this->email->subject('puya - Eliminacion de carro de compra');
					$this->email->message($html);
					$this->email->send();

				echo "Se ha notificado al dueño del carro via mail (".$dataCarro['email'].") de la eliminacion del carro de compra";
			}
			
		}
		}else{
			show_404();
		}
	}

	public function seleccionadoMedioPago($momentoPago){
		echo $fechaSeleccionPago = date("d/m/Y",$momentoPago);
		//configuracion 
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';

		$html = '<!DOCTYPE html>
				<html>
				<head>
					<meta charset="utf-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge">
					<title>Mail Seleccion Pago Transferencia</title>
					<link rel="stylesheet" href="">
				</head>
				<body>
					<h2>Aviso de Generacion de Orden '.$arrayVenta['numero_orden'].'</h2>
					<p>Con fecha '.$fechaSeleccionPago.' la venta con codigo '.$arrayVenta['numero_orden'].' Se ha generado la una venta para el usuario '.$arrayCliente['nombre'].' '.$arrayCliente['apellido'].' rut: ' .$arrayCliente['rut'].' <br> esta venta fue realizada mediante la opcion <b>Pago en efectivo / Transferencia</b>, El pago de los productos puede realizarse en efectivo en cualquiera de nuestras tiendas presentando el codigo de venta o mediante transferencia por el total de la venta $ '.number_format(ceil($arrayVenta['costo_envio'] + $arrayVenta['costo_productos']) ,0, "," ,".").'.
									La transferencia debe ser realizada a la cuenta con los siguientes datos.
									</p>
									<table border="1">
										<tr>
											<td>Banco</td>
											<td>Banco de Chile</td>
										</tr>
										<tr>
											<td>Tipo de Cuenta</td>
											<td>Cuenta Corriente</td>
										</tr>
										<tr>
											<td>Numero de cuenta</td>
											<td>2253921305</td>
										</tr>
										<tr>
											<td>Titular</td>
											<td>Comercializadora puya Limitada</td>
										</tr>
										<tr>
											<td>Rut</td>
											<td>76.551.488-6</td>
										</tr>
										<tr>
											<td>correo</td>
											<td>puya@puya.cl</td>
										</tr>
										<tr>
											<td>Asunto</td>
											<td>Codigo Venta '.$arrayVenta['numero_orden'].'</td>
										</tr>

										<tr>
											<td>monto</td>
											<td>$ '.number_format(ceil($arrayVenta['costo_envio'] + $arrayVenta['costo_productos']) ,0, "," ,".").'</td>
										</tr>
									</table>
									<br>
								De no hacerce efectiva la transferencia en un periodo de 24 Hrs. la venta se procesara como cancelada y los productos volveran al stock. 
								Los puntos por esta venta seran agregados una vez se valide el pago los productos.
								Gracias por Comprar con nosotros
								</body>
								</html>';
		

		//hacer una funcion para enviar mail en base al cambio de estado del carro
		$arrayCarro = array('id'=>$this->session->userdata('carro_data')['carro_id'],
							'estado' => 1);
		if($this->Carro_compra_model->edita_carro_compra($arrayCarro)){
			$arrayVentaCarro = array('id_carro'=>$this->session->userdata('carro_data')['carro_id'],
							'fecha' => date("Y-m-d",$momentoPago),
							'medio_pago'=>0,
							'estado_pago'=>0,
							'estado_venta'=>0);

			$this->Ventas_model->inserta_ventas_carro_compra($arrayVentaCarro);

			$carro_actual = array(
	                        'carro_id' =>$this->session->userdata('carro_data')['carro_id'],
	                        'carro_codigo' => $this->session->userdata('carro_data')['carro_codigo'],
	                        'carro_estado' => 1,
	                        'carro_total' => $valorRealQuitar,
						    'carro_nombre_comp' => $this->session->userdata('carro_data')['carro_nombre_comp'],
						    'carro_rut_comp' => $this->session->userdata('carro_data')['carro_rut_comp'],
						    'carro_direccion_comp' => $this->session->userdata('carro_data')['carro_direccion_comp'],
						    'carro_telefono_comp' => $this->session->userdata('carro_data')['carro_telefono_comp'],
						    'carro_observacion'=>$this->session->userdata('carro_data')['carro_observacion'],
						    'carro_email' => $this->session->userdata('carro_data')['carro_email']);
					$this->session->set_userdata(array("carro_data"=>$carro_actual));

			$this->email->initialize($config);
			$this->email->to($this->session->userdata('carro_data')['carro_email']);
			$this->email->from('alexis.asdev@gmail.com','puya INFO');
			$this->email->subject('puya - Pago de carro por transferencia');
			$this->email->message($html);
			$this->email->send();

			echo "Se ha enviado un mail con las instrucciones de pago por transferencia, el estado del carro a cambiado a pendiente de pago";
		}
	}

	public function convertir_precio($monto){
		$FromCurrency = 'CLP';
		$ToCurrency = 'USD';
		// get current exchange rates
		$exurl = 'http://api.exchangeratelab.com/api/single/'.$FromCurrency.'?apikey=DA9FDE620A82452E82B2B52978A3C4F4';

		$ch = curl_init($exurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$json_response_content = curl_exec($ch);
		curl_close($ch);

		$exchangeRatesResult = json_decode($json_response_content, true );
		$monto = $monto/$exchangeRatesResult['rate']['rate'];

		return $monto;
	}

/*vistas flow*/
	public function confirma(){
		
		try {
			// Lee los datos enviados por Flow
			$this->flowapi->read_confirm();		
		} catch (Exception $e) {
			// Si hay un error responde false
			echo $this->flowapi->build_response(false);
			return;
		}

		//Recupera Los valores de la Orden
		$FLOW_STATUS = $this->flowapi->getStatus();  //El resultado de la transacción (EXITO o FRACASO)
		$ORDEN_NUMERO = $this->flowapi->getOrderNumber(); // N° Orden del Comercio
		$MONTO = $this->flowapi->getAmount(); // Monto de la transacción
		$ORDEN_FLOW = $this->flowapi->getFlowNumber(); // Si $FLOW_STATUS = "EXITO" el N° de Orden de Flow
		$PAGADOR = $this->flowapi->getPayer(); // El email del pagador
		
		if($FLOW_STATUS == "EXITO") {
			
			//SELECT `id`, `orden_comercio`, `orden_flow`, `fecha`, `estado_pago`, `mail_pagador`, `monto` FROM `pagos_webpay` WHERE 1

			$registrarPagoWebPay = array(
									"orden_comercio"=>$ORDEN_NUMERO,
									"orden_flow"=>$ORDEN_FLOW,
									"monto"=>$MONTO,
									"fecha"=>date("Y-m-d"),
									"mail_pagador"=>$PAGADOR,
									"estado_pago"=>0);

			$this->PWebpay_model->inserta_pagos_webpay($registrarPagoWebPay);
			
			$arrayCarro = array('id'=>$this->session->userdata('carro_data')['carro_id'],
										'estado' => 2);
			$this->Carro_compra_model->edita_carro_compra($arrayCarro);

			$arrayVentaCarro = array('id_carro'=>$this->session->userdata('carro_data')['carro_id'],
							'fecha' => date("Y-m-d"),
							'medio_pago'=>1,
							'estado_pago'=>1,
							'estado_venta'=>1);

			$this->Ventas_model->inserta_ventas_carro_compra($arrayVentaCarro);

			// La transacción fue aceptada por Flow
			$arrayCarroRegistro;
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}else{
				$arrayCarroRegistro= array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}
			
			$arrayDataCarro = array('id-carro'=>$arrayCarroRegistro['id']);

			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
			$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayUsuario['id_comuna'])['precio'];
			if($costoEnvio < 0 ||$costoEnvio  == null ){
				$costoEnvio = 0;
			}
			$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);
			$totalVentaProd = 0;
			if ($productos_carro) {
				foreach ($productos_carro as $productoItem) {
					$precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));
					$precioVenta *= $productoItem['cantidad'];
					$totalVentaProd += $precioVenta;
				}
			}
	        // Monto de la transacción (venta de productos + envio - codigo de descuentos)
	        //falta agregar el descuento realizado por el codigo de promocion
			
            $arrayCarro = array('id'=>$arrayCarroRegistro['id'],
								'estado'=>2);
			$arrayVenta = array('id_carro'=>$arrayCarroRegistro['id'],
								'estado'=>2,
								'costo_envio'=>$costoEnvio,
								'comuna_id'=>$arrayUsuario['id_comuna'],
								'costo_productos'=>$totalVentaProd,
								'metodo_pago'=>1,
								'codigo'=>'',
								'fecha_proceso'=>date('Y-m-d H:m:s'),
								'fecha_pago'=>$result->transactionDate,
								'numero_orden'=>$result->buyOrder,
								'codigo_documento'=>strtotime(date('Y-m-d H:i:s')));
			
			if ($this->session->tempdata('codigo_descuento_activo')!== null) {
				$arrayVenta['codigo'] = $this->session->tempdata('codigo_descuento_activo');
			}

			if($this->Carro_compra_model->edita_carro_compra($arrayCarro) && 
			   $this->Carro_compra_model->procesa_productos_carro($arrayCarro)){
				$arrayCarro = array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				if($this->Venta_model->inserta_venta($arrayVenta) &&
				   $this->Carro_compra_model->inserta_carro_compra($arrayCarro)){
					$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);
					echo "La venta se ha procesado con exito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el registrando la venta ya existe";
			}

            //echo $message;

            $dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
			$dataPagina['promoActiva']=$dataPromo;
			//pido los ultimos artículos al modelo
		  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_categorias'] = $dataCategoria;
			//pido los ultimos artículos al modelo
			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
			$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
			$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
			$dataPagina['resultado'] = $result;
			//cargo la vista pasando los datos de configuacion
			$this->load->view('layout/head',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('layout/nav', $dataPagina);
			$this->load->view('exito_pago',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('layout/footer', $dataPagina);
			// Aquí puede actualizar su información con los datos recibidos por Flow
			echo $this->flowapi->build_response(true); // Comercio envía recepción de la confirmación
		} else {
			// La transacción fue rechazada por Flow

			$registrarPagoWebPay = array(
									"orden_comercio"=>$ORDEN_NUMERO,
									"orden_flow"=>$ORDEN_FLOW,
									"monto"=>$MONTO,
									"fecha"=>date("Y-m-d"),
									"mail_pagador"=>$PAGADOR,
									"estado_pago"=>1);

			$this->PWebpay_model->inserta_pagos_webpay($registrarPagoWebPay);
			
			$arrayCarro = array('id'=>$this->session->userdata('carro_data')['carro_id'],
										'estado' => 2);
			$this->Carro_compra_model->edita_carro_compra($arrayCarro);

			$arrayVentaCarro = array('id_carro'=>$this->session->userdata('carro_data')['carro_id'],
							'fecha' => date("Y-m-d"),
							'medio_pago'=>1,
							'estado_pago'=>1,
							'estado_venta'=>1);

			$this->Ventas_model->inserta_ventas_carro_compra($arrayVentaCarro);

			// Aquí puede actualizar su información con los datos recibidos por Flow
			echo $this->flowapi->build_response(true); // Comercio envía recepción de la confirmación
		}
	
	}

	public function exitof(){
		$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"puya CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
	  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_marcas'] = $dataMarca;
		//cargo la vista pasando los datos de configuacion
		//$dataPagina["ultimoSubido"] = $this->Producto_model->get_productos_nuevos()[0];

		try {
			// Lee los datos enviados por Flow
			$this->flowapi->read_result();
		} catch (Exception $e) {
			error_log($e->getMessage());
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Ha ocurrido un error interno', true, 500);
			return;
		}

		//Recupera Los valores de la Orden
		$FLOW_STATUS = $this->flowapi->getStatus();  //El resultado de la transacción (EXITO o FRACASO)
		$ORDEN_NUMERO = $this->flowapi->getOrderNumber(); // N° Orden del Comercio
		$MONTO = $this->flowapi->getAmount(); // Monto de la transacción
		$ORDEN_FLOW = $this->flowapi->getFlowNumber(); // Si $FLOW_STATUS = "EXITO" el N° de Orden de Flow
		$PAGADOR = $this->flowapi->getPayer(); // El email del pagador

		if($FLOW_STATUS == "EXITO") {
			
			//SELECT `id`, `orden_comercio`, `orden_flow`, `fecha`, `estado_pago`, `mail_pagador`, `monto` FROM `pagos_webpay` WHERE 1
			$registrarPagoWebPay = array(
									"orden_comercio"=>$ORDEN_NUMERO,
									"orden_flow"=>$ORDEN_FLOW,
									"monto"=>$MONTO,
									"fecha"=>date("Y-m-d"),
									"mail_pagador"=>$PAGADOR,
									"estado_pago"=>0);

			$this->PWebpay_model->inserta_pagos_webpay($registrarPagoWebPay);
			
			$arrayCarro = array('id'=>$this->session->userdata('carro_data')['carro_id'],
										'estado' => 2);
			$this->Carro_compra_model->edita_carro_compra($arrayCarro);

			$arrayVentaCarro = array('id_carro'=>$this->session->userdata('carro_data')['carro_id'],
							'fecha' => date("Y-m-d"),
							'medio_pago'=>1,
							'estado_pago'=>1,
							'estado_venta'=>1);

			
			$arrayCarroRegistro;
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}else{
				$arrayCarroRegistro= array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}
			
			$arrayDataCarro = array('id-carro'=>$arrayCarroRegistro['id']);

			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
			$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayUsuario['id_comuna'])['precio'];
			if($costoEnvio < 0 ||$costoEnvio  == null ){
				$costoEnvio = 0;
			}
			$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);
			$totalVentaProd = 0;
			if ($productos_carro) {
				foreach ($productos_carro as $productoItem) {
					$precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));
					$precioVenta *= $productoItem['cantidad'];
					$totalVentaProd += $precioVenta;
				}
			}
	        // Monto de la transacción (venta de productos + envio - codigo de descuentos)
	        //falta agregar el descuento realizado por el codigo de promocion
			if ($arrayCarroRegistro['retiro_tienda']==1) {
				$costoEnvio = 0;
			}

            $arrayCarro = array('id'=>$arrayCarroRegistro['id'],
								'estado'=>2);
			$arrayVenta = array('id_carro'=>$arrayCarroRegistro['id'],
								'estado'=>2,
								'costo_envio'=>$costoEnvio,
								'comuna_id'=>$arrayUsuario['id_comuna'],
								'costo_productos'=>$totalVentaProd,
								'metodo_pago'=>1,
								'codigo'=>'',
								'fecha_proceso'=>date('Y-m-d H:m:s'),
								'fecha_pago'=>strtotime(date('Y-m-d H:i:s')),
								'numero_orden'=>$ORDEN_NUMERO,
								'codigo_documento'=>strtotime(date('Y-m-d H:i:s')));
			
			if ($this->session->tempdata('codigo_descuento_activo')!== null) {
				$arrayVenta['codigo'] = $this->session->tempdata('codigo_descuento_activo');
			}

			if($this->Carro_compra_model->edita_carro_compra($arrayCarro) && 
			   $this->Carro_compra_model->procesa_productos_carro($arrayCarro)){
				$arrayCarro = array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				if($this->Venta_model->inserta_venta($arrayVenta) &&
				   $this->Carro_compra_model->inserta_carro_compra($arrayCarro) &&
					$this->PWebpay_model->inserta_pagos_webpay($registrarPagoWebPay)){
					$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);
					//echo "La venta se ha procesado con exito";
				}else{
					//echo "Error al registrar";
				}
			}else{
				echo "Error: el registrando la venta ya existe";
			}
		} else {
			// La transacción fue rechazada por Flow

			$registrarPagoWebPay = array(
									"orden_comercio"=>$ORDEN_NUMERO,
									"orden_flow"=>$ORDEN_FLOW,
									"monto"=>$MONTO,
									"fecha"=>date("Y-m-d"),
									"mail_pagador"=>$PAGADOR,
									"estado_pago"=>1);
			
			$arrayCarroRegistro;
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}else{
				$arrayCarroRegistro= array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarroRegistro);

				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}
			
			$arrayDataCarro = array('id-carro'=>$arrayCarroRegistro['id']);

			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
			$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayUsuario['id_comuna'])['precio'];
			if($costoEnvio < 0 ||$costoEnvio  == null ){
				$costoEnvio = 0;
			}
			$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);
			$totalVentaProd = 0;
			if ($productos_carro) {
				foreach ($productos_carro as $productoItem) {
					$precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));
					$precioVenta *= $productoItem['cantidad'];
					$totalVentaProd += $precioVenta;
				}
			}
	        // Monto de la transacción (venta de productos + envio - codigo de descuentos)
	        //falta agregar el descuento realizado por el codigo de promocion
			if ($arrayCarroRegistro['retiro_tienda']==1) {
				$costoEnvio = 0;
			}

            $arrayCarro = array('id'=>$arrayCarroRegistro['id'],
								'estado'=>2);
			$arrayVenta = array('id_carro'=>$arrayCarroRegistro['id'],
								'estado'=>2,
								'costo_envio'=>$costoEnvio,
								'comuna_id'=>$arrayUsuario['id_comuna'],
								'costo_productos'=>$totalVentaProd,
								'metodo_pago'=>1,
								'codigo'=>'',
								'fecha_proceso'=>date('Y-m-d H:m:s'),
								'fecha_pago'=>strtotime(date('Y-m-d H:i:s')),
								'numero_orden'=>$ORDEN_NUMERO,
								'codigo_documento'=>strtotime(date('Y-m-d H:i:s')));
			
			if ($this->session->tempdata('codigo_descuento_activo')!== null) {
				$arrayVenta['codigo'] = $this->session->tempdata('codigo_descuento_activo');
			}

			if($this->Carro_compra_model->edita_carro_compra($arrayCarro) && 
			   $this->Carro_compra_model->procesa_productos_carro($arrayCarro)){
				$arrayCarro = array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				if($this->Venta_model->inserta_venta($arrayVenta) &&
				   $this->Carro_compra_model->inserta_carro_compra($arrayCarro) &&
					$this->PWebpay_model->inserta_pagos_webpay($registrarPagoWebPay)){
					$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);
					//echo "La venta se ha procesado con exito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error registrando, la venta ya existe";
			}
		}

		//Recupera los datos enviados por Flow
		$concepto = $this->flowapi->getConcept();
		$pagador = $this->flowapi->getPayer();
		
		$arrayPagoFlow = array(
						"orden_compra"=>$ORDEN_NUMERO,
						"monto"=>$MONTO,
						"concepto"=>$concepto,
						"pagador"=>$PAGADOR,
						"flororden"=>$ORDEN_FLOW
							);
		if ($arrayCarroRegistro['retiro_tienda']==0) {
			$dataRegion = $this->Region_model->get_region($arrayCarroRegistro['region_carro']);
			$dataProvincia = $this->Provincia_model->get_provincia($arrayCarroRegistro['provincia_carro']);
			$dataComunas = $this->Comuna_model->get_comuna($arrayCarroRegistro['comuna_carro']);
			$dataPagina['region_envio'] = $dataRegion;
			$dataPagina['provincia_envio'] = $dataProvincia;
			$dataPagina['comuna_envio'] = $dataComunas;
		}
		
		$dataPagina['data_flow'] = $arrayPagoFlow;
		$dataPagina['data_carro'] = $arrayCarroRegistro;

		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataPagina['promoActiva']=$dataPromo;
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		
		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);
		$this->load->view('exito_pago', $dataPagina);	
		$this->load->view('layout/footer', $dataPagina);
			
	}

	public function fracaso(){
		$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"puya CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
		$dataCarro = array("id-carro"=>$this->session->userdata('carro_data')['carro_id']);
		$datavuelta = array();
		$datavuelta = $this->Carro_compra_model->llamar_productos_carro($dataCarro);
		$dataPagina['lista_agregados'] = $datavuelta;
		//tomando los datos de post de paypal
		$dataPagina['data_paypal'] = $_POST;
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
	  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_marcas'] = $dataMarca;
		//$dataPagina["ultimoSubido"] = $this->Producto_model->get_productos_nuevos()[0];
		//cargo la vista pasando los datos de configuacion
		$this->load->view('layout/head',$dataPagina);
		$this->load->view('layout/header', $dataPagina);
		$this->load->view('layout/nav', $dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('fracaso_pago', $dataPagina);		
		$this->load->view('layout/footer', $dataPagina);
	}
/*fin vistas flow*/
}
