<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stack extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Stack_model');
            $this->load->model('Categoria_model');
            /*$this->load->model('productos/Producto_model');*/
            $this->load->library('upload');
            $this->load->library('Comprime');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	public function index(){
		show_404();
	}

	public function guardarDataStack(){

		if ($this->input->is_ajax_request()) {
			//upload file

			//upload file
			$url_final = "";
			$destination_thumbs = "assets/uploads/stacks/";
	        $config['upload_path'] = './assets/uploads/stacks/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB

	        if(!file_exists("./assets/uploads/stacks/")){
	          if(!mkdir("./assets/uploads/stacks/", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }

	        if (isset($_FILES['img_cortada']['name'])) {
		        	//
		            if (0 < $_FILES['img_cortada']['error']) {
		                echo 'Error during file upload' . $_FILES['img_cortada']['error'];
		            } else {
		                if (file_exists("./assets/uploads/stacks/" . $_FILES['img_cortada']['name'])) {
		                    echo 'Ya existe el archivo: ./assets/uploads/stacks/' . $_FILES['img_cortada']['name'];          
		                } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('img_cortada')){
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 70);
								$url_final = base_url().'./assets/uploads/stacks/' .$file_data['file_name'].'.jpg';
		                    }

		                }
		            }
		        }

			$arrayStack = array('nombre'=>$this->input->post("nombre_stack"),
								'descripcion'=>$this->input->post("descripcion_stack"),
								'sku'=>$this->input->post("codigo_stack"),
								'precio'=>$this->input->post("precio_stack"),
								'descuento'=>$this->input->post("descuento_stack"),
								//se debe calcular el stock en base a los productos
								'stock'=>$this->input->post("stock_stack"),
								'categoria_id'=>$this->input->post("categoria_stack"),
								'sub_categoria_id'=>$this->input->post("sub_categoria_stack"),
								'marca_id'=>$this->input->post("marca_stack"),
								'envio_gratis'=>$this->input->post("envio_gratis_stack"),
								'tipo_entrega'=>$this->input->post("tipo_entrega_stack"),
								'foto_producto'=>$url_final,
								'foto_home'=>$url_final);

			$existeStack = $this->Stack_model->existe_stack($arrayStack['nombre']);
			if(!$existeStack){
				if($this->Stack_model->inserta_stack($arrayStack)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el stack ".$arrayStack['nombre']." ya existe";
			}

		}else{
			show_404();
		}
	}

	public function editDataStack($user = null){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
			$destination_thumbs = "assets/uploads/stacks/";
	        $config['upload_path'] = './assets/uploads/stacks/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB

	        if(!file_exists("./assets/uploads/stacks/")){
	          if(!mkdir("./assets/uploads/stacks", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }

	        if (isset($_FILES['img_cortada']['name'])) {

	        	//
	            if (0 < $_FILES['img_stack']['error']) {
	                echo 'Error during file upload' . $_FILES['img_stack']['error'];
	            } else {
	                if (file_exists('./assets/uploads/stacks/' . $_FILES['img_stack']['name'])) {
	                    echo 'File already exists : assets/uploads/stacks/' . $_FILES['img_stack']['name'];          
	                } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('img_cortada')){
	                    //if (!$this->upload->do_upload('img_stack')){
	                        echo $this->upload->display_errors();
	                    } else {
	                    	echo "imagen subida con exito";
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/stacks/'.$file_data['file_name'].'.jpg';
	                    }

	                }
	            }
	        }

			//upload file
			$arrayStack = array('id'=>$this->input->post("id_stack"),
							'nombre'=>$this->input->post("nombre_stack"),
							'descripcion'=>$this->input->post("descripcion_stack"),
							'sku'=>$this->input->post("codigo_stack"),
							'precio'=>$this->input->post("precio_stack"),
							'descuento'=>$this->input->post("descuento_stack"),
							//se debe calcular el stock en base a los productos
							'stock'=>$this->input->post("stock_stack"),
							'categoria_id'=>$this->input->post("categoria_stack"),
							'sub_categoria_id'=>$this->input->post("sub_categoria_stack"),
							'marca_id'=>$this->input->post("marca_stack"),
							'envio_gratis'=>$this->input->post("envio_gratis_stack"),
							'tipo_entrega'=>$this->input->post("tipo_entrega_stack"));
			
			if ($url_final != ""){
				$actual_imagen = str_replace(base_url(), './', $this->Stack_model->actual_imagen($arrayStack['id']));
				if(!file_exists($actual_imagen['foto_producto'])){
					try {
						unlink($actual_imagen['foto_producto']);
					} catch (Exception $e) {
					}
		        }
				$arrayStack['foto_producto']=$url_final;
				$arrayStack['foto_home']=$url_final;
			}

			if($this->Stack_model->edita_stack($arrayStack)){
				echo "Editado con éxito";
			}else{
				echo "Error al editar";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataStack($id = null){
		if ($this->input->is_ajax_request()) {
			$actual_imagen = str_replace(
					base_url(), 
					'./', 
					$this->Stack_model->actual_imagen($this->input->post("id_stack")));
			if(file_exists($actual_imagen['foto_producto'])){
				try {
					unlink($actual_imagen['foto_producto']);
				} catch (Exception $e) {
				}
	        }
			if($this->Stack_model->elimina_stack($this->input->post("id_stack"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Usuario";
			}
		}else{
			show_404();
		}
	}

	public function getDataListStacks(){
		if ($this->input->is_ajax_request()) {
			if($this->input->post("dfb") != null){
				if($this->input->post("marvalue")!= null){
					echo json_encode(array("estado"=>0,
						 "stacks"=>$this->Stack_model->get_productos_filtrados_stacks($this->input->post("marvalue"),$this->input->post("dfb"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data filtro"));
				}
			}else{
				if($this->input->post("marvalue")!= null){
					echo json_encode(array("estado"=>0,
					 "stacks"=>$this->Stack_model->get_productos_stack($this->input->post("marvalue"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data normal"));
				}
			}
		}else{
			show_404();
		}
	}

	public function view($dataView=null,$nombre=null){
		if(!is_null($dataView)){

				$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"Prueba CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
				//pido al modelo el artículo que se desea ver
				$arrayStack = $this->Stack_model->get_stack($dataView);

				//compruebo si he recibido un artículo
				if (!$arrayStack){
				 //no he recibido ningún artículo
				 //voy a lanzar un error 404 de página no encontrada
					echo "no existe";
				 show_404();
				}else{
					 //he encontrado el artículo
					 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
					
					//pido los ultimos artículos al modelo
				  	$dataCategoria = $this->Categoria_model->get_total_prods_categorias_stacks($dataView);
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_categorias'] = $dataCategoria;
					//pido los ultimos artículos al modelo
				  	$dataStack = $this->Stack_model->obtener_lista_stacks();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_stacks'] = $dataStack;

	      			$ultimosArticulos = $this->Stack_model->get_productos_stack($dataView);

					$dataPagina['arrayStack']=$arrayStack;//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/head',$dataPagina);
					$this->load->view('layout/header', $dataPagina);
					$this->load->view('layout/nav', $dataPagina);
					//en esta parte va el contenido personalizado
					$this->load->view('home/stacks', $dataPagina);		
					$this->load->view('layout/footer', $dataPagina);
				}
		}else{
			show_404();
		}	
	}

}
