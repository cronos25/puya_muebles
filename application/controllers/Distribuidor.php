<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distribuidor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Distribuidor_model');
            $this->load->model('Producto_model');
            $this->load->model('Promocion_temporal_model');
            $this->load->model('Categoria_model');
            $this->load->model('Sub_categoria_model');
            /*$this->load->model('especificacion/Especificacion_model');
            */
            $this->load->library('upload');
            $this->load->library('Comprime');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
            $this->load->helper('ayuda_helper');
    }
	public function index(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

		//pido los ultimos artículos al modelo
	  	$dataDistribuidores = $this->Distribuidor_model->obtener_lista_distribuidores();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_distribuidores'] = $dataDistribuidores;
		//cargo la vista pasando los datos de configuacion
		$this->load->view('layout/head',$dataPagina);
		$this->load->view('layout/nav',$dataPagina);

		//en esta parte va el contenido personalizado
		$this->load->view('distribuidor');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	//
	public function estadoDistribuidor($id = null){
		if ($this->input->is_ajax_request()) {
			$arrayDistribuidor = array('id' => $this->input->post("id_distribuidor"),
								'autorizado' => $this->input->post("autoriza_estado"));			
			/*$this->Especificacion_model->elimina_especificacion_distribuidor($this->input->post("id_distribuidor"));*/
			$this->Distribuidor_model->cambio_autorizado_distribuidor($arrayDistribuidor);
			echo "estado actualizado";
		}else{
			show_404();
		}
	}

	public function guardarDataDistribuidor(){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/distribuidor/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 		
	 		if(!file_exists("./assets/uploads/distribuidor/")){
	          if(!mkdir("./assets/uploads/distribuidor", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }

	        if (isset($_FILES['imagen_distribuidor']['name'])) {
	            if (0 < $_FILES['imagen_distribuidor']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_distribuidor']['error'];
	            } else {
	                if (file_exists('./assets/uploads/distribuidor/' . $_FILES['imagen_distribuidor']['name'])) {
	                    echo 'File already exists : assets/uploads/' . $_FILES['imagen_distribuidor']['name'];           } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_distribuidor')){
	                        echo "error subiendo la imagen ";
	                        echo $this->upload->display_errors();
	                    } else {
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/distribuidor/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        } else {
	            echo 'Please choose a file';
	        }


			$arrayDistribuidor = array('autorizado'=>$this->input->post("autorizado_distribuidor"),
							'nombre'=>$this->input->post("nombre_distribuidor"),
							'link'=>$this->input->post("link_distribuidor"),
							'descripcion'=>$this->input->post("descripcion_distribuidor"),
							'imagen'=>$url_final);
			
			if($this->Distribuidor_model->inserta_distribuidor($arrayDistribuidor)){
				echo "Registrado con éxito";
			}else{
				echo "Error al registrar";
			}
		}else{
			show_404();
		}
	}

	public function editDataDistribuidor($user = null){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/distribuidor/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 
	        if (isset($_FILES['imagen_distribuidor']['name'])) {
	            if (0 < $_FILES['imagen_distribuidor']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_distribuidor']['error'];
	            } else {
	                if (file_exists('./assets/uploads/distribuidor/' . $_FILES['imagen_distribuidor']['name'])) {
	                    echo 'File already exists : assets/uploads/distribuidor/' . $_FILES['imagen_distribuidor']['name'];           } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_distribuidor')){
	                        echo $this->upload->display_errors();
	                    } else {
	                    	echo "imagen subida con exito";
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
	                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
                    		$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/distribuidor/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        }

				
			$arrayDistribuidor = array('id'=>$this->input->post("id_distribuidor"),
							'autorizado'=>$this->input->post("autorizado_distribuidor"),
							'nombre'=>$this->input->post("nombre_distribuidor"),
							'link'=>$this->input->post("link_distribuidor"),
							'descripcion'=>$this->input->post("descripcion_distribuidor"));
			if ($url_final != ""){
				$actual_imagen = str_replace(base_url(), './', $this->Distribuidor_model->actual_imagen($arrayDistribuidor['id']));
				if(!file_exists($actual_imagen['imagen'])){
					try {
						unlink($actual_imagen['imagen']);
					} catch (Exception $e) {
					}
		        }
				$arrayDistribuidor['imagen']=$url_final;
			}

			if($this->Distribuidor_model->edita_distribuidor($arrayDistribuidor)){
				echo "Editado con exito";
			}else{
				echo "no se realizaron cambios";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataDistribuidor($id = null){
		if ($this->input->is_ajax_request()) {
			$actual_imagen = str_replace(
				base_url(), 
				'./', 
				$this->Distribuidor_model->actual_imagen($this->input->post("id_distribuidor"))['imagen']
				);
			if(file_exists($actual_imagen)){
				try {
					unlink($actual_imagen);
				} catch (Exception $e) {
				}
	        }
			/*$this->Especificacion_model->elimina_especificacion_distribuidor($this->input->post("id_distribuidor"));*/
			if($this->Distribuidor_model->elimina_distribuidor($this->input->post("id_distribuidor"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Usuario";
			}
		}else{
			show_404();
		}
	}
}
