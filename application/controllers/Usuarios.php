<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de artículos
            $this->load->model('Usuario_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('url_helper');
    }
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function guardarDataUser(){
		if ($this->input->is_ajax_request()) {
			$arrayUser = array('nombre'=>$this->input->post("nombre_usuario"),
							'apellido'=>$this->input->post("apellido_usuario"),
							'telefono'=>$this->input->post("telefono_usuario"),
							'email'=>$this->input->post("email_usuario"),
							'password'=>$this->input->post("password_usuario"));
			$existeMail = $this->Usuario_model->existe_email($arrayUser['email']);
			if(!$existeMail){
				if($this->Usuario_model->inserta_usuario($arrayUser)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el mail ".$arrayUser['email']." ya existe";
			}
		}else{
			show_404();
		}
		
	}

	public function editDataUser($user = null){
		if ($this->input->is_ajax_request()) {
			$arrayUser = array('id'=>$this->input->post("id_usuario"),
							'nombre'=>$this->input->post("nombre_usuario"),
							'apellido'=>$this->input->post("apellido_usuario"),
							'telefono'=>$this->input->post("telefono_usuario"),
							'email'=>$this->input->post("email_usuario"),
							'password'=>$this->input->post("password_usuario"));

			if($this->Usuario_model->edita_usuario($arrayUser)){
				echo "editado con exito";
			}else{
				echo "Error al editar Usuario,el mail ".$arrayUser['email']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataUser($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Usuario_model->elimina_usuario($this->input->post("id_usuario"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Usuario";
			}
		}else{
			show_404();
		}
	}
}
