<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Region extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//cargo el modelo de regiones
            $this->load->model('Region_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		redirect('admin/index', 'refresh'); 
	}

	public function obtenerRegiones(){
		if ($this->input->is_ajax_request()) {
			//return $this->Region_model->get_region($this->input->post("id_region"));
			echo json_encode($this->Region_model->get_region());
		}else{
			show_404();
		}
	}

	public function guardarDataRegion(){
		if ($this->input->is_ajax_request()) {
			$arrayRegion = array('nombre'=>$this->input->post("nombre_region"));
			$existeRegion = $this->Region_model->existe_region($arrayRegion['nombre']);
			if(!$existeRegion){
				if($this->Region_model->inserta_region($arrayRegion)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el mail ".$arrayRegion['nombre']." ya existe";
			}
		}else{
			show_404();
		}
		
	}

	public function editDataRegion($region = null){
		if ($this->input->is_ajax_request()) {
			$arrayRegion = array('id'=>$this->input->post("id_region"),
							'nombre'=>$this->input->post("nombre_region"));

			if($this->Region_model->edita_region($arrayRegion)){
				echo "editado con exito";
			}else{
				echo "Error al editar Region,la region ".$arrayRegion['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataRegion($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Region_model->elimina_region($this->input->post("id_region"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Region";
			}
		}else{
			show_404();
		}
	}

}

/* End of file Region.php */
/* Location: ./application/controllers/Region.php */