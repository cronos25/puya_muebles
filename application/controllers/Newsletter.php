<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//cargo el modelo de regiones
            $this->load->model('Newsletter_model');
            $this->load->model('Promocion_temporal_model');
            $this->load->model('Categoria_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Producto_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		redirect('admin/index', 'refresh'); 
	}

	public function obtenerNewsletteres(){
		if ($this->input->is_ajax_request()) {
			//return $this->Newsletter_model->get_region($this->input->post("id_region"));
			echo json_encode($this->Newsletter_model->obtener_lista_newsletteres());
		}else{
			show_404();
		}
	}

	public function suscribirNewsletter(){
		if ($this->input->is_ajax_request()) {
			$arrayNewsletter = array('email'=>$this->input->post("email"),
				'fecha_inscripcion'=>date("Y-m-d"));
			$existeNewsletter = $this->Newsletter_model->existe_suscrito($arrayNewsletter['email']);
			if(!$existeNewsletter){
				if($this->Newsletter_model->inserta_newsletter($arrayNewsletter)){

				//$this->load->library('email');
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				
				//configuracion 

				$this->email->initialize($config);

				$this->email->from('no-replay@gymness.cl', 'Info Gymness');
				$this->email->to($arrayNewsletter['email']);

				$this->email->subject('Suscrito a Newsletter Gymness');
				$this->email->message('se ha suscrito al newsletter de GYMNESS, agradecemos su preferencia y prepate para recibir nuestras mejores ofertas. <br>

					si deseas desuscribirte por favor haz click en el siguiente link <a href="'.base_url("newsletter/desuscribir?email=".$arrayNewsletter['email']).'" title="">desuscribir</a>');
				$this->email->set_header('Header1', 'Value1');
				//$this->email->set_alt_message('This is the alternative message');
				if ($this->email->send())
				{
					echo "Suscrito al newsletter";
				}
				}else{
					echo "Error al suscribir";
				}

			}else{
				echo "El mail ".$arrayNewsletter['email']." ya esta suscrito, gracias ";
			}
		}else{
			show_404();
		}
		
	}

	public function editDataNewsletter($region = null){
		if ($this->input->is_ajax_request()) {
			$arrayNewsletter = array('id'=>$this->input->post("id_newsletter"),
							'email'=>$this->input->post("email"));

			if($this->Newsletter_model->edita_newsletter($arrayNewsletter)){
				echo "editado con exito";
			}else{
				echo "Error al editar Newsletter ".$arrayNewsletter['email']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function desuscribir(){
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataPagina['promoActiva']=$dataPromo;
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

		if($this->Newsletter_model->desuscribe($this->security->xss_clean($this->input->get("email")))){
			$dataPagina['mensaje'] = "se ha desuscrito de nuestro newsletter";
		}else{
			$dataPagina['mensaje'] = "Error al desuscribir del newsletter";
		}

		//cargo la vista pasando los datos de configuacion
		$this->load->view('layout/head',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('layout/nav', $dataPagina);
		$this->load->view('desuscribe',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);

	}

	public function eliminaDataNewsletter($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Newsletter_model->elimina_newsletter($this->input->post("email"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Newsletter";
			}
		}else{
			show_404();
		}
	}
}

/* End of file Newsletter.php */
/* Location: ./application/controllers/Newsletter.php */