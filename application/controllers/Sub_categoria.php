<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_categoria extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//cargo el modelo de subcategorias
            $this->load->model('Categoria_model');
            $this->load->model('Producto_model');
            $this->load->model('Marca_model');

            $this->load->model('Promocion_temporal_model');
            $this->load->model('Sub_categoria_model');
            $this->load->library('Comprime');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		redirect('admin/index', 'refresh'); 
	}

	public function obtenerSubCategorias(){
		if ($this->input->is_ajax_request()) {
			echo json_encode($this->Sub_categoria_model->get_sub_categoria($this->input->post('id_sub_cate')));
		}else{
			show_404();
		}
	}

	public function obtenerSubCategoriasCategorias(){
		if ($this->input->is_ajax_request()) {
			echo json_encode($this->Sub_categoria_model->obtener_lista_sub_categorias_categoria($this->input->post('id_categoria')));
		}else{
			show_404();
		}
	}

	public function guardarDataSubCategoria(){
		if ($this->input->is_ajax_request()) {
			$arraySub_categoria = array('nombre'=>$this->input->post("nombre_sub_categoria"),
							'id_categoria'=>$this->input->post("id_categoria"),
							'id_marca'=>$this->input->post("id_marca"));
			$existeSub_categoria = $this->Sub_categoria_model->existe_sub_categoria($arraySub_categoria['nombre'],$arraySub_categoria['id_categoria']);
			if(!$existeSub_categoria){
				if($this->Sub_categoria_model->inserta_sub_categoria($arraySub_categoria)){
					echo "Registrado con éxito";
				}else{
					echo "No puede tener datos vacios";
				}
			}else{
				echo "Error: La sub categoria ".$arraySub_categoria['nombre']." ya existe en esta ciudad";
			}
		}else{
			show_404();
		}
		
	}

	public function editDataSubCategoria($subcategoria = null){
		if ($this->input->is_ajax_request()) {
			$arraySub_categoria = array('id'=>$this->input->post("id_sub_categoria"),
							'nombre'=>$this->input->post("nombre_sub_categoria"),
							'id_categoria'=>$this->input->post("id_categoria"),
							'id_marca'=>$this->input->post("id_marca"));

			if($this->Sub_categoria_model->edita_sub_categoria($arraySub_categoria)){
				echo "editado con exito";
			}else{
				echo "No se realizaron cambios";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataSubCategoria($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Sub_categoria_model->elimina_sub_categoria($this->input->post("id_sub_categoria"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Sub_categoria";
			}
		}else{
			show_404();
		}
	}

	public function getDataListSubCategorias(){
		if ($this->input->is_ajax_request()) {
			if($this->input->post("dfb") != null){
				if($this->input->post("catevalue")!= null){
					echo json_encode(array("estado"=>0,
						 "sub_categorias"=>$this->Sub_categoria_model->get_productos_filtrados_sub_categoria($this->input->post("catevalue"),$this->input->post("dfb"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data filtro"));
				}
			}else{
				if($this->input->post("catevalue")!= null){
					echo json_encode(array("estado"=>0,
					 "sub_categorias"=>$this->Sub_categoria_model->get_productos_sub_categoria($this->input->post("catevalue"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data normal"));
				}
			}
		}else{
			show_404();
		}
	}

	public function view($dataView=null,$nombre=null){
		if(!is_null($dataView)){

				$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"Prueba CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
				//pido al modelo el artículo que se desea ver
				$arraySubCategoria = $this->Sub_categoria_model->get_sub_categoria($dataView);

				//compruebo si he recibido un artículo
				if (!$arraySubCategoria){
				 //no he recibido ningún artículo
				 //voy a lanzar un error 404 de página no encontrada
					echo "no existe";
				 show_404();
				}else{
					 //he encontrado el artículo
					 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
					$arrayCategoria = $this->Categoria_model->get_categoria($arraySubCategoria['id_categoria']);
					//pido los ultimos artículos al modelo
				  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_categorias'] = $dataCategoria;
					//pido los ultimos artículos al modelo
				  	$dataMarca = $this->Marca_model->get_total_prods_marcas_sub_categoria($dataView);
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_marcas'] = $dataMarca;
					$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
					$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
					$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
					$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
	  				
	      			$ultimosArticulos = $this->Sub_categoria_model->get_productos_sub_categoria($dataView);
	      			$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
					$dataPagina['promoActiva']=$dataPromo;
					$dataPagina['arraySubCategoria']=$arraySubCategoria;
					$dataPagina['arrayCategoria']=$arrayCategoria;
					$dataPagina['lista_productos']=$ultimosArticulos;
					//$dataPagina["ultimoSubido"] = $this->Producto_model->get_productos_nuevos()[0];
					 //cargo la vista pasando los datos de configuacion
					$this->load->view('layout/head',$dataPagina);
					//$this->load->view('layout/header', $dataPagina);
					$this->load->view('layout/nav', $dataPagina);
					//en esta parte va el contenido personalizado
					$this->load->view('sub_categorias', $dataPagina);		
					$this->load->view('layout/footer', $dataPagina);
				}
		}else{
			show_404();
		}	
	}


}

/* End of file Sub_categoria.php */
/* Location: ./application/controllers/Sub_categoria.php */