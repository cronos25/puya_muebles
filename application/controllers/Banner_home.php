<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Banner_home_model');
            $this->load->model('Producto_model');
            $this->load->library('Comprime');
            /*$this->load->model('especificacion/Especificacion_model');
            */
            $this->load->library('upload');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	public function index(){
		show_404();
	}

	//
	public function estadoBanner($id = null){
		if ($this->input->is_ajax_request()) {
			$arrayBanner = array('id' => $this->input->post("id_banner"),
								'estado' => $this->input->post("banner_estado"));			
			/*$this->Especificacion_model->elimina_especificacion_banner($this->input->post("id_banner"));*/
			$this->Banner_home_model->cambio_estado_banner($arrayBanner);
			echo "estado actualizado";
		}else{
			show_404();
		}
	}

	public function guardarDataBannerHome(){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/banner/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 		
	 		if(!file_exists("./assets/uploads/banner/")){
	          if(!mkdir("./assets/uploads/banner", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }

	        if (isset($_FILES['imagen_banner_home']['name'])) {
	        	
	            if (0 < $_FILES['imagen_banner_home']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_banner_home']['error'];
	            } else {
	                if (file_exists('./assets/uploads/banner/' . $_FILES['imagen_banner_home']['name'])) {
	                    echo 'File already exists : assets/uploads/' . $_FILES['imagen_banner_home']['name'];           } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_banner_home')){
	                        echo "error subiendo la imagen ";
	                        echo $this->upload->display_errors();
	                    } else {
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
	                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
                    		$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/banner/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        } else {
	            echo 'Please choose a file';
	        }

			$arrayBanner = array('estado'=>$this->input->post("estado_banner"),
							'tipo_banner'=>$this->input->post("tipo_banner"),
							'imagen'=>$url_final);
			if ($arrayBanner['tipo_banner']==0) {
				$arrayBanner['producto_id']=$this->input->post("producto_banner");
				$arrayBanner['texto_btn_banner']=$this->input->post("texto_boton_banner_home");
			}
			
			if($this->Banner_home_model->inserta_banner($arrayBanner)){
				echo "Registrado con éxito";
			}else{
				echo "Error al registrar";
			}
		}else{
			show_404();
		}
	}

	public function editDataBannerHome($user = null){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/banner/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 
	        if (isset($_FILES['imagen_banner_home']['name'])) {
	            if (0 < $_FILES['imagen_banner_home']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_banner_home']['error'];
	            } else {
	                if (file_exists('./assets/uploads/banner/' . $_FILES['imagen_banner_home']['name'])) {
	                    echo 'File already exists : assets/uploads/banner/' . $_FILES['imagen_banner_home']['name'];           } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_banner_home')){
	                        echo $this->upload->display_errors();
	                    } else {
	                    	echo "imagen subida con exito";
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
	                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
                    		$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/banner/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        }

			$arrayBanner = array('id'=>$this->input->post("id_banner"),
							'estado'=>$this->input->post("estado_banner"),
							'tipo_banner'=>$this->input->post("tipo_banner"));
			if ($arrayBanner['tipo_banner']==0) {
				$arrayBanner['producto_id']=$this->input->post("producto_banner");
				$arrayBanner['texto_btn_banner']=$this->input->post("texto_boton_banner_home");
			}
			if ($url_final != ""){
				$actual_imagen = str_replace(base_url(), './', $this->Banner_home_model->actual_imagen($arrayBanner['id']));
				if(file_exists($actual_imagen['imagen'])){
					try {
						unlink($actual_imagen['imagen']);
					} catch (Exception $e) {
					}
		        }
				$arrayBanner['imagen']=$url_final;
			}

			if($this->Banner_home_model->edita_banner($arrayBanner)){
				echo "Editado con exito";
			}else{
				echo "no se realizaron cambios";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataBannerHome($id = null){
		if ($this->input->is_ajax_request()) {
			$actual_imagen = str_replace(
				base_url(), 
				'./', 
				$this->Banner_home_model->actual_imagen($this->input->post("id_banner"))['imagen']
				);
			if(file_exists($actual_imagen)){
				try {
					unlink($actual_imagen);
				} catch (Exception $e) {
				}
	        }
			
			/*$this->Especificacion_model->elimina_especificacion_banner($this->input->post("id_banner"));*/
			if($this->Banner_home_model->elimina_banner($this->input->post("id_banner"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Usuario";
			}
		}else{
			show_404();
		}
	}
}
