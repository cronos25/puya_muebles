<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Codigo_promocion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
            $this->load->model('Codigo_promocion_model');
            $this->load->model('Carro_compra_model');
            $this->load->model('Cliente_model');
            $this->load->model('Precio_envio_model');
            $this->load->model('Promocion_temporal_model');

            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		show_404();
	}

	public function guardarDataCodigoPromo(){
		if ($this->input->is_ajax_request()) {
			$arrayCodigo = array('codigo'=>$this->input->post("codigo_codigo_promo"),
							'descuento'=>$this->input->post("descuento_codigo_promo"),
							'fecha_inicio'=>$this->input->post("fecha_inicio"),
							'fecha_fin'=>$this->input->post("fecha_fin"));
			$existeCodigo = $this->Codigo_promocion_model->existe_codigo($arrayCodigo);
			if(!$existeCodigo){
				if($this->Codigo_promocion_model->inserta_codigo_promocion($arrayCodigo)){
					echo "Registrado con éxito";
				}
			}else{
				echo "Error: El codigo ".$arrayCodigo['codigo']." ya existe para la fecha seleccionada";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataCodigoPromo($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Codigo_promocion_model->elimina_codigo_promocion($this->input->post("id_codigo_promo"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar el codigo";
			}
		}else{
			show_404();
		}
	}

	public function previaCodigoDescuento(){
		if ($this->input->is_ajax_request()&&
			isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$codigo_descuento = $this->Codigo_promocion_model->obtener_codigo_promocion_previa($this->security->xss_clean($this->input->post("codigo_previo")));
			$codigoValido = $this->Codigo_promocion_model->codigo_valido($this->security->xss_clean($codigo_descuento['id']),$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_id']));
			
			if (!empty($codigo_descuento) && $codigoValido) {
				$arrayCarro;
				if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
					$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
				}else{
					$arrayCarro = array('codigo'=>$_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id'],
						'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
					$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

					$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
				}
				
				$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);

				/*codigo de descuento a usar*/
				
				$valorDescuentoCodigo = 0;
				/*fin*/

				$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
				$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayUsuario['id_comuna'])['precio'];
				$promoActiva = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
				
				$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);
				$totalVentaProd = 0;
				$descuentoPromo = 0;
				if ($productos_carro) {
					foreach ($productos_carro as $productoItem) {
						$precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));

						if(!empty($promoActiva)):
							if (($promoActiva['producto_id']>0 && $productoItem['id']==$promoActiva['producto_id'])||
							  ($promoActiva['categoria_id']>0 && $productoItem['categoria_id']==$promoActiva['categoria_id'])||
							  ($promoActiva['marca_id']>0 && $productoItem['marca_id']==$promoActiva['marca_id'])||
							  ($promoActiva['sub_categoria_id']>0 && $productoItem['sub_categoria_id']==$promoActiva['sub_categoria_id'])) {
							  $descuentoPromo += $precioVenta*($promoActiva['descuento']/100);
							}
						endif;
						if (!empty($codigo_descuento)) {
							if (($codigo_descuento['producto_id']>0 && 
								$productoItem['id']==$codigo_descuento['producto_id'])||
							  ($codigo_descuento['categoria_id']>0 && 
							  	$productoItem['categoria_id']==$codigo_descuento['categoria_id'])||
							  ($codigo_descuento['marca_id']>0 && 
							  	$productoItem['marca_id']==$codigo_descuento['marca_id'])||
							  ($codigo_descuento['sub_categoria_id']>0 && 
							  	$productoItem['sub_categoria_id']==$codigo_descuento['sub_categoria_id'])) {
							  $valorDescuentoCodigo += $productoItem['asoc_val']*($codigo_descuento['descuento']/100);
							}
						}
						

						$precioVenta *= $productoItem['cantidad'];
						
						$totalVentaProd += $precioVenta;
					}
				}


		        // Monto de la transacción (venta de productos + envio - codigo de descuentos)
		        //falta agregar el descuento realizado por el codigo de promocion
				if($costoEnvio < 0 ||$costoEnvio  == null ){
					$costoEnvio = 0;
				}

		        $amount = ($totalVentaProd+$costoEnvio)-$descuentoPromo;
		        if ($valorDescuentoCodigo>0) {
				$descuento =number_format(ceil(intVal($valorDescuentoCodigo)) ,0, "," ,".");
		        $total = number_format(ceil(intVal($amount)),0, "," ,".");
		        $promo =number_format(ceil(intVal($descuentoPromo)),0, "," ,".");
		        $envio = number_format(ceil(intVal($costoEnvio)),0, "," ,".");
		        	$valoresActuales = array('estado'=>0,
		        							'mensaje'=>'se a aplicado el descuento en base a los productos de su carro de compra',
		        							'total' =>$total,
		        							'descuento'=> $descuento,
		        							'promo'=> $promo,
		        							'envio'=> $envio);
		        	echo json_encode($valoresActuales);
		        }else{
		        	$descuento =number_format(ceil(intVal($amount*($codigo_descuento['descuento']/100))) ,0, "," ,".");
		        $total =number_format(intVal($amount-($amount*($codigo_descuento['descuento']/100))) ,0, "," ,".");
		        $promo =number_format(ceil(intVal($descuentoPromo)),0, "," ,".");
		        $envio = number_format(ceil(intVal($costoEnvio)),0, "," ,".");
	        	$valoresActuales = array('estado'=>0,
	        							'mensaje'=>'se a aplicado el descuento al total de su compra',
	        							'total' =>$total,
	        							'descuento'=> $descuento,
	        							'promo'=> $promo,
	        							'envio'=> $envio);
	        	echo json_encode($valoresActuales);
		        }
			}else{
				$valoresActuales = array('estado'=>1,
	        							'mensaje'=>'el codigo no existe o ya no es valido',
	        							'descuento'=> 0);
				if (!$codigoValido) {
					$valoresActuales = array('estado'=>1,
        							'mensaje'=>'el codigo ya fue usado con anterioridad',
        							'descuento'=> 0);
				}
				
		        echo json_encode($valoresActuales);
			}
		}else{
			show_404();
		}
	}


}

/* End of file Codigo_promocion.php */
/* Location: ./application/controllers/Codigo_promocion.php */