<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocion_temporal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
            $this->load->model('Promocion_temporal_model');
            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		show_404();
	}

	public function guardarDataPromoTemporal(){
		if ($this->input->is_ajax_request()) {
			$arrayPromoTemporal = array('descuento'=>$this->input->post("descuento_promo_temporal"),
							'mensaje'=>$this->input->post("mensaje_promo_temporal"),
							'fecha_inicio'=>$this->input->post("fecha_inicio"),
							'fecha_fin'=>$this->input->post("fecha_fin"));

			$existeCodigo = $this->Promocion_temporal_model->existe_promo($arrayPromoTemporal);
			if(!$existeCodigo){
				/*if ($this->input->post("categoria_promo_temporal")!== false) {
					$arrayPromoTemporal['categoria_id'] = $this->input->post("categoria_promo_temporal");
				}*/
				if ($this->input->post("producto_promo_temporal")!== false) {
					$arrayPromoTemporal['producto_id'] = $this->input->post("producto_promo_temporal");
				}/*
				if ($this->input->post("marca_promo_temporal")!==false) {
					$arrayPromoTemporal['marca_id'] = $this->input->post("marca_promo_temporal");
				}
				if ($this->input->post("sub_categoria_promo_temporal")!==false) {
					$arrayPromoTemporal['sub_categoria_id'] = $this->input->post("sub_categoria_promo_temporal");
				}*/
				if($this->Promocion_temporal_model->inserta_promocion_temporal($arrayPromoTemporal)){
					echo "Registrado con éxito";
				}
			}else{
				echo "Ya existe una promocion en este periodo de tiempo ";
			}
		}else{
			show_404();
		}
	}

	public function editarDataPromoTemporal(){
		if ($this->input->is_ajax_request()) {
			$arrayPromoTemporal = array('id'=>$this->input->post("id_promo_temporal"),
							'descuento'=>$this->input->post("descuento_promo_temporal"),
							'mensaje'=>$this->input->post("mensaje_promo_temporal"),
							'fecha_inicio'=>$this->input->post("fecha_inicio"),
							'fecha_fin'=>$this->input->post("fecha_fin"));
			$existeCodigoEdit = false;
			$existeCodigoEdit = $this->Promocion_temporal_model->existe_promo_edit($arrayPromoTemporal);
			if(!$existeCodigoEdit){
				/*if ($this->input->post("categoria_promo_temporal")!== false) {
					$arrayPromoTemporal['categoria_id'] = $this->input->post("categoria_promo_temporal");
				}*/
				if ($this->input->post("producto_promo_temporal")!= false) {
					$arrayPromoTemporal['producto_id'] = $this->input->post("producto_promo_temporal");
				}
				/*if ($this->input->post("marca_promo_temporal")!==false) {
					$arrayPromoTemporal['marca_id'] = $this->input->post("marca_promo_temporal");
				}
				if ($this->input->post("sub_categoria_promo_temporal")!==false) {
					$arrayPromoTemporal['sub_categoria_id'] = $this->input->post("sub_categoria_promo_temporal");
				}*/
				if($this->Promocion_temporal_model->edita_promocion_temporal($arrayPromoTemporal)){
					echo "editado con éxito";
				}else{
					echo "no se realizaron cambios";
				}
			}else{
				echo "Ya existe una promocion en este periodo de tiempo ";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataPromoTemporal($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Promocion_temporal_model->elimina_promocion_temporal($this->input->post("id_promo_temporal"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar el codigo";
			}
		}else{
			show_404();
		}
	}


}

/* End of file Codigo_promocion.php */
/* Location: ./application/controllers/Codigo_promocion.php */