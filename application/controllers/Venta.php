<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venta extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//cargo el modelo de ventas
            $this->load->model('Venta_model');
            $this->load->model('Carro_compra_model');
            $this->load->model('Producto_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Categoria_model');
            $this->load->model('Promocion_temporal_model');
            //$this->load->model('Webpay_model');
            $this->load->model('PWebpay_model');
            $this->load->model('Cliente_model');
            $this->load->model('Precio_envio_model');
            
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
            //$this->load->library('Webpay','Configuration');
	}

	public function index()
	{
		redirect('admin/index', 'refresh'); 
	}

	public function callSellsActive10Days(){
		if ($this->input->is_ajax_request()) {
			$datavuelta = array();
			$datavuelta = $this->Venta_model->obtener_ventas_pagadas_10dias();
			echo json_encode($datavuelta);
		}else{
			show_404();
		}
	}

	public function obtenerVentas(){
		if ($this->input->is_ajax_request()) {
			//return $this->Venta_model->get_venta($this->input->post("id_venta"));
			echo json_encode($this->Venta_model->get_venta());
		}else{
			show_404();
		}
	}

	public function resumen_prod_venta(){
		if ($this->input->is_ajax_request()&&isset($_SESSION['admin_ecom_data']['logeado'])) {

			$arrayCarro;
			$arrayVenta = $this->Venta_model->get_venta($this->security->xss_clean($this->input->post("id_ven")));
			$arrayCarro = $this->Carro_compra_model->get_carro_compra($arrayVenta['id_carro']);
			
			$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);
			$arrayUsuario = $this->Cliente_model->get_cliente_full($arrayCarro['id_cliente']);
			$productos_carro;
			$productos_carro = $this->Carro_compra_model->llamar_productos_carro_resumen($arrayDataCarro);

			$infoResumen = array('productos_data'=>$productos_carro,
								'cliente_data'=>$arrayUsuario,
								'carro_data'=>$arrayCarro,
								'venta_data' => $arrayVenta);

			echo json_encode($infoResumen);
		}else{
			show_404();
		}
	}

//falta tomar los datos de la respuesta para procesar la venta
	public function realizaVenta(){
		if ($this->input->is_ajax_request() &&
			isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			//falta agregar los siguientes campos, por el momento en la db esta como null
			//region_id	
			//ciudad_id		

			$arrayCarro = array('id'=>$this->input->post("id_carro"),
								'mensaje'=>$this->input->post("mensaje_venta"),
								'estado'=>2);
			$arrayVenta = array('id_carro'=>$this->input->post("id_carro"),
								'estado'=>0,
								'costo_envio'=>$this->input->post("costo_envio"),
								'comuna_id'=>$this->input->post("comuna_venta"),
								'costo_productos'=>$this->input->post("costo_productos"),
								'metodo_pago'=>$this->input->post("metodo_pago"),
								'fecha_proceso'=>date('Y-m-d H:m:s'),
								'fecha_pago'=>$this->input->post("id_carro"),
								'codigo'=>$this->input->post("codigo_descuento"),
								'numero_orden'=>$this->input->post("numero_orden"),
								'codigo_documento'=>strtotime(date('Y-m-d H:i:s')));
			
			if($this->Carro_compra_model->edita_carro_compra($arrayCarro) && 
			   $this->Carro_compra_model->procesa_productos_carro($arrayCarro)){
				$arrayCarro = array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				if($this->Venta_model->inserta_venta($arrayVenta) &&
				   $this->Carro_compra_model->inserta_carro_compra($arrayCarro)){
				   	$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
					$arrayCliente = $this->Cliente_model->get_cliente($arrayCarro['id_cliente']); 
					$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);
					$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);

					$html = '<!DOCTYPE html>
						<html>
						<head>
							<meta charset="utf-8">
							<meta http-equiv="X-UA-Compatible" content="IE=edge">
							<title>Mail Seleccion Pago Transferencia</title>
							<link rel="stylesheet" href="">
						</head>
						<body>';
					 $fechaSeleccionPago = date("d/m/Y");

					$html .= '<h2>Aviso de Generacion de Orden '.$arrayVenta['numero_orden'].'</h2>
					<p>Con fecha '.$fechaSeleccionPago.' la venta con codigo '.$arrayVenta['numero_orden'].' Se ha generado la una venta para el usuario '.$arrayCliente['nombre'].' '.$arrayCliente['apellido'].' rut: ' .$arrayCliente['rut'].' <br> esta venta fue realizada mediante la opcion <b>Pago en efectivo / Transferencia</b>, El pago de los productos puede realizarse en efectivo en cualquiera de nuestras tiendas presentando el codigo de venta o mediante transferencia por el total de la venta $ '.number_format(ceil($arrayVenta['costo_envio'] + $arrayVenta['costo_productos']) ,0, "," ,".").'.
									La transferencia debe ser realizada a la cuenta con los siguientes datos.
									</p>
									<table border="1">
										<tr>
											<td>Banco</td>
											<td>Nombre banco</td>
										</tr>
										<tr>
											<td>Tipo de Cuenta</td>
											<td>Cuenta</td>
										</tr>
										<tr>
											<td>Numero de cuenta</td>
											<td>000000000000</td>
										</tr>
										<tr>
											<td>Titular</td>
											<td>Comercializadora Puya</td>
										</tr>
										<tr>
											<td>Rut</td>
											<td>11.111.111-1</td>
										</tr>
										<tr>
											<td>correo</td>
											<td>puya@puyamuebles.cl</td>
										</tr>
										<tr>
											<td>Asunto</td>
											<td>Orden compra '.$arrayVenta['numero_orden'].'</td>
										</tr>

										<tr>
											<td>monto</td>
											<td>$ '.number_format(ceil($arrayVenta['costo_envio'] + $arrayVenta['costo_productos']) ,0, "," ,".").'</td>
										</tr>
									</table>
									<br>
								De no hacerce efectiva la transferencia en un periodo de 24 Hrs. la venta se procesara como cancelada y los productos volveran al stock. 
								Gracias por Comprar con nosotros
								</body>
								</html>';
						//$this->load->library('email');
						$config['protocol'] = 'sendmail';
						$config['mailpath'] = '/usr/sbin/sendmail';
						$config['charset'] = 'iso-8859-1';
						$config['wordwrap'] = TRUE;
						$config['mailtype'] = 'html';
						$this->email->initialize($config);
						$this->email->to($arrayCliente['apellido']);
						$this->email->from('post_venta@puyamuebles.cl','Puya INFO');
						$this->email->subject('Puya - Venta Procesada');
						$this->email->message($html);
						$this->email->attach($this->generaPdf($arrayVenta['numero_orden'],$html));
						$this->email->send();


					echo "La venta se ha procesado con exito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el ".json_encode($arrayVenta)." ya existe";
			}
		}else{
			show_404();
		}
	}

	private function generaPdf($codigoVenta,$contenido){
		$data = [];

		$hoy = date("dmyhis");

        //load the view and saved it into $html variable
        $html =$contenido;
        // $html = $this->load->view('v_dpdf',$date,true);
 		
 		//$html="asdf";
        //this the the PDF filename that user will get to download
        if(!file_exists("./assets/uploads/ventas/")){
	          if(!mkdir("./assets/uploads/ventas", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }
        $pdfFilePath = "./assets/uploads/ventas/".$codigoVenta."_".$hoy.".pdf";
 
        //load mPDF library
        $this->load->library('M_pdf');
        $mpdf = new mPDF('c', 'A4'); 
 		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "F");
		return $pdfFilePath;
       // //generate the PDF from the given html
       //  $this->m_pdf->pdf->WriteHTML($html);
 
       //  //download it.
       //  $this->m_pdf->pdf->Output($pdfFilePath, "D"); 
	}

	public function setSaleState($user = null){
		if ($this->input->is_ajax_request()) {
			$arrayVenta = array('id'=>$this->input->post("ven"),
							'estado'=>$this->input->post("estado_v"));
			$arrayVentaActual = $this->Venta_model->get_venta($arrayVenta['id']);
			$arrayVenta['id_carro'] = $arrayVentaActual['id_carro'];
			$arrayVenta['costo_productos'] = $arrayVentaActual['costo_productos'];
			$arrayVenta['costo_envio'] = $arrayVentaActual['costo_envio'];
			$html = '<!DOCTYPE html>
				<html>
				<head>
					<meta charset="utf-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge">
					<title>Mail Seleccion Pago Transferencia</title>
					<link rel="stylesheet" href="">
				</head>
				<body>';
			 $fechaSeleccionPago = date("d/m/Y");			
			 switch ($arrayVenta['estado']) {
			 	case 0:
			//estado 1= venta cancelada, se cancela el carro y los productos de este tambien
			 		break;
			 	case 1:
			//estado 1= venta cancelada, se cancela el carro y los productos de este tambien
			 		$arrayCarro = array('id'=>$arrayVentaActual['id_carro'],
									'estado' => 1);
					$this->Carro_compra_model->edita_carro_compra($arrayCarro);
					$this->Carro_compra_model->procesa_productos_carro_eliminado($arrayCarro);
					
					$html .= '<h2>Aviso de cambio de estado de venta codigo '.$arrayVentaActual['numero_orden'].'</h2>
					<p>Con fecha '.$fechaSeleccionPago.' la venta con codigo '.$arrayVentaActual['numero_orden'].' a cambiado su estado a <b>CANCELADO</b>, los productos se devolveran al stock';
			 		break;
			 	case 2:
				//estado 2= venta pagada
					$arrayCarro = array('id'=>$arrayVentaActual['id_carro'],
									'estado' => 2);

					$this->Carro_compra_model->edita_carro_compra($arrayCarro);
					$html .= '<h2>Aviso de cambio de estado de venta codigo '.$arrayVentaActual['numero_orden'].'</h2>
					<p>Con fecha '.$fechaSeleccionPago.' la venta con codigo '.$arrayVentaActual['numero_orden'].' a cambiado su estado a <b>PAGADO</b>, se estan preparando para ser enviados';
			 		break;
			 	case 3:
				//estado 3= venta enviada o lista para retiro en caso de pago en efectivo
					$html .= '<h2>Aviso de cambio de estado de venta codigo '.$arrayVentaActual['numero_orden'].'</h2>
					<p>Con fecha '.$fechaSeleccionPago.' la venta con codigo '.$arrayVentaActual['numero_orden'].' a cambiado su estado a <b>';
						if ($arrayVentaActual['metodo_pago']==0) {
							$html .='Listo para retiro</b>, los productos estan disponible para su retiro en tienda';
						}else{
							$html .='Enviado</b>, los productos fueron enviados, su codigo de seguimiento es '.$this->input->post("estado_p");
						}
					$arrayVenta['codigo_seguimiento'] = $this->input->post("estado_p");
			 		break;
			 	case 4:
				//estado 4= venta finalizada,el carro queda como finalizado, asi como sus productos
			 		$arrayCarro = array('id'=>$arrayVentaActual['id_carro'],
									'estado' => 3);

					$this->Carro_compra_model->edita_carro_compra($arrayCarro);
					$carroactual = $this->Carro_compra_model->get_carro_compra($arrayCarro['id']);
					$this->Carro_compra_model->procesa_productos_carro_eliminado($arrayCarro);
					$html .= '<h2>Aviso de cambio de estado de venta codigo '.$arrayVentaActual['numero_orden'].'</h2>
					<p>Con fecha '.$fechaSeleccionPago.' la venta con codigo '.$arrayVentaActual['numero_orden'].' a cambiado su estado a <b>FINALIZADO</b>';
			 		break;
			 	default:
			 		# code...
			 		break;
			 }
				$html .= '</body></html>';

			if($this->Venta_model->edita_venta($arrayVenta)){
				echo "estado actualizado con exito";
				$carroactual = $this->Carro_compra_model->get_carro_compra($arrayVentaActual['id_carro']);
				$cliente =$this->Cliente_model->get_cliente($carroactual['id_cliente']);

				//$this->load->library('email');
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->to($cliente['email']);
				$this->email->from('postventa@puyamuebles.cl','PUYAMUEBLES INFO');
				$this->email->subject('puyamuebles - Cambio de estado');
				$this->email->message($html);
				$this->email->attach($this->generaPdf($arrayVentaActual['numero_orden'],$html));
				$this->email->send();

			}else{
				echo "error actualizando el estado de la venta";
			}
		}else{
			show_404();
		}
	}

	public function guardarDataVenta(){
		if ($this->input->is_ajax_request()) {
			$arrayVenta = array('nombre'=>$this->input->post("nombre_venta"));
			if(!$existeVenta){
				if($this->Venta_model->inserta_venta($arrayVenta)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el mail ".$arrayVenta['nombre']." ya existe";
			}
		}else{
			show_404();
		}	
	}

	public function editDataVenta($ventas = null){
		if ($this->input->is_ajax_request()) {
			$arrayVenta = array('id'=>$this->input->post("id_venta"),
							'nombre'=>$this->input->post("nombre_venta"));

			if($this->Venta_model->edita_venta($arrayVenta)){
				echo "editado con exito";
			}else{
				echo "Error al editar Venta,la ventas ".$arrayVenta['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataVenta($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Venta_model->elimina_venta($this->input->post("id_venta"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Venta";
			}
		}else{
			show_404();
		}
	}

	public function getResult(){
		/*
		{"accountingDate":"0725",
		"buyOrder":"25938",
		"cardDetail":{
			"cardNumber":"5678",
			"cardExpirationDate":null
		},
		"detailOutput":{
			"authorizationCode":"135871",
			"paymentTypeCode":"VD",
			"responseCode":0,
			"sharesNumber":0,
			"amount":"1900",
			"commerceCode":"597020000541",
			"buyOrder":"25938"
			},
		"sessionId":"5977733fdd321",
		"transactionDate":"2017-07-25T12:35:12.024-04:00",
		"urlRedirection":"https:\/\/webpay3gint.transbank.cl\/filtroUnificado\/voucher.cgi",
		"VCI":"TSY"
		} 
		*/
		/**
		 * TRANSACCIÓN DE AUTORIZACIÓN NORMAL:
		 * Una transacción de autorización normal (o transacción normal), corresponde a una solicitud de 
		 * autorización financiera de un pago con tarjetas de crédito o débito, en donde quién realiza el pago
		 * ingresa al sitio del comercio, selecciona productos o servicio, y el ingreso asociado a los datos de la
		 * tarjeta de crédito o débito lo realiza en forma segura en Webpay.
		 * 
		 *  Respuestas WebPay: 
		 * 
		 *  TSY: Autenticación exitosa
		 *  TSN: autenticación fallida.
		 *  TO : Tiempo máximo excedido para autenticación.
		 *  ABO: Autenticación abortada por tarjetahabiente.
		 *  U3 : Error interno en la autenticación.
		 *  Puede ser vacío si la transacción no se autentico.
		 *
		 *  Códigos Resultado
		 * 
		 *  0  Transacción aprobada.
		 *  -1 Rechazo de transacción.
		 *  -2 Transacción debe reintentarse.
		 *  -3 Error en transacción.
		 *  -4 Rechazo de transacción.
		 *  -5 Rechazo por error de tasa.
		 *  -6 Excede cupo máximo mensual.
		 *  -7 Excede límite diario por transacción.
		 *  -8 Rubro no autorizado.
		 */

		$tx_step = "Get Result";

        if (!isset($_POST["token_ws"])) redirect('cliente/?venta=perfil', 'refresh');;
            

        /** Token de la transacción */
        $token = filter_input(INPUT_POST, 'token_ws');
        
        $request = array(
            "token" => filter_input(INPUT_POST, 'token_ws')
        );

        /** Rescatamos resultado y datos de la transaccion */
        $result = $this->webpay->getNormalTransaction()->getTransactionResult($token);
        
        /** propiedad de HTML5 (web storage), que permite almacenar datos en nuestro navegador web */
        $resultadoAceptado = array('accountingDate'=>$result->accountingDate, 
        	'buyOrder'=>$result->buyOrder, 
        	'cardDetail_number'=>$result->cardDetail->cardNumber, 
        	'cardDetail_expiration'=>$result->cardDetail->cardExpirationDate, 
        	'do_authorizationCode'=>$result->detailOutput->authorizationCode, 
        	'do_paymentTypeCode'=>$result->detailOutput->paymentTypeCode, 
        	'do_responseCode'=>$result->detailOutput->responseCode, 
        	'do_sharesNumber'=>$result->detailOutput->sharesNumber, 
        	'do_amount'=>$result->detailOutput->amount, 
        	'do_commerceCode'=>$result->detailOutput->commerceCode, 
        	'do_buyOrder'=>$result->detailOutput->buyOrder, 
        	'session_id'=>$result->sessionId, 
        	'transactionDate'=>$result->transactionDate, 
        	'VCI'=>$result->VCI);
        $this->Webpay_model->inserta_webpay_trans($resultadoAceptado);

        /** Verificamos resultado  de transacción */
        if ($result->detailOutput->responseCode === 0) {
			
			$arrayCarroRegistro;
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}else{
				$arrayCarroRegistro= array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

				$arrayCarroRegistro= $this->Carro_compra_model->get_carro_compra_codigo($this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']));
			}
			
			$arrayDataCarro = array('id-carro'=>$arrayCarroRegistro['id']);

			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
			$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayUsuario['id_comuna'])['precio'];
			if($costoEnvio < 0 ||$costoEnvio  == null ){
				$costoEnvio = 0;
			}
			$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);
			$totalVentaProd = 0;
			if ($productos_carro) {
				foreach ($productos_carro as $productoItem) {
					$precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));
					$precioVenta *= $productoItem['cantidad'];
					$totalVentaProd += $precioVenta;
				}
			}
	        // Monto de la transacción (venta de productos + envio - codigo de descuentos)
	        //falta agregar el descuento realizado por el codigo de promocion
			
            $arrayCarro = array('id'=>$arrayCarroRegistro['id'],
								'estado'=>2);
			$arrayVenta = array('id_carro'=>$arrayCarroRegistro['id'],
								'estado'=>2,
								'costo_envio'=>$costoEnvio,
								'comuna_id'=>$arrayUsuario['id_comuna'],
								'costo_productos'=>$totalVentaProd,
								'metodo_pago'=>1,
								'codigo'=>'',
								'fecha_proceso'=>date('Y-m-d H:m:s'),
								'fecha_pago'=>$result->transactionDate,
								'numero_orden'=>$result->buyOrder,
								'codigo_documento'=>strtotime(date('Y-m-d H:i:s')));
			
			if ($this->session->tempdata('codigo_descuento_activo')!== null) {
				$arrayVenta['codigo'] = $this->session->tempdata('codigo_descuento_activo');
			}

			if($this->Carro_compra_model->edita_carro_compra($arrayCarro) && 
			   $this->Carro_compra_model->procesa_productos_carro($arrayCarro)){
				$arrayCarro = array('codigo'=>$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']),
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				if($this->Venta_model->inserta_venta($arrayVenta) &&
				   $this->Carro_compra_model->inserta_carro_compra($arrayCarro)){
					$_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);
					echo "La venta se ha procesado con exito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el registrando la venta ya existe";
			}

            echo '<script>window.localStorage.clear();</script>';
            echo '<script>localStorage.setItem("authorizationCode", '.$result->detailOutput->authorizationCode.')</script>';
            echo '<script>localStorage.setItem("amount", '.$result->detailOutput->amount.')</script>';
            echo '<script>localStorage.setItem("buyOrder", '.$result->buyOrder.')</script>';
            $next_page = $result->urlRedirection;

            $message = '
            <br>Pago ACEPTADO por webpay (se deben guardatos para mostrar voucher) <br>
            <!--<form action="'.$next_page.'" id="form_result_wp" method="post">
			    <input type="hidden" name="token_ws" value="'.$token.'">
			    <label>Pago aceptado</label>
			    <input type="submit" value="Continuar">
			</form>-->
			<script>
				/*setTimeout(function () {
					document.getElementById("form_result_wp").submit();
				},400);*/
			</script>
			';
            //echo $message;

            $dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
			$dataPagina['promoActiva']=$dataPromo;
			//pido los ultimos artículos al modelo
		  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_categorias'] = $dataCategoria;
			//pido los ultimos artículos al modelo
			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
			$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
			$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
			$dataPagina['resultado'] = $result;
			//cargo la vista pasando los datos de configuacion
			$this->load->view('layout/head',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('layout/nav', $dataPagina);
			$this->load->view('exito_pago',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('layout/footer', $dataPagina);
	        /*$message = "Transacion Finalizada";
	        $next_page = base_url()."?action=nullify";
	        $button_name = "Anular Transacci&oacute;n &raquo;";*/
        } else {
        	$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
			$dataPagina['promoActiva']=$dataPromo;
			//pido los ultimos artículos al modelo
		  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_categorias'] = $dataCategoria;
			//pido los ultimos artículos al modelo
			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
			$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
			$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
			$message = "Pago <b>RECHAZADO</b> por webpay - " . ($result->detailOutput->responseDescription);
            $dataPagina['mensaje'] = $message;
			//cargo la vista pasando los datos de configuacion
			$this->load->view('layout/head',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('layout/nav', $dataPagina);
			$this->load->view('fracaso_pago',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('layout/footer', $dataPagina);
        }
	}

	public function end(){
		$post_array = true;
        $result = $_POST;
        $resultadoAceptado = array('accountingDate'=>'', 
        	'buyOrder'=>$result['TBK_ORDEN_COMPRA'], 
        	'cardDetail_number'=>'', 
        	'cardDetail_expiration'=>'', 
        	'do_authorizationCode'=>'', 
        	'do_paymentTypeCode'=>'', 
        	'do_responseCode'=>'', 
        	'do_sharesNumber'=>0, 
        	'do_amount'=>0, 
        	'do_commerceCode'=>'', 
        	'do_buyOrder'=>$result['TBK_ORDEN_COMPRA'], 
        	'session_id'=>$result['TBK_ID_SESION'], 
        	'transactionDate'=>date('Y-m-d H:m:s'), 
        	'VCI'=>'');
        $this->Webpay_model->inserta_webpay_trans($resultadoAceptado);
        redirect('cliente/carro_compra', 'refresh');

        $tx_step = "End";
        $request = "";
        
	}

}

/* End of file Venta.php */
/* Location: ./application/controllers/Venta.php */