<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marca extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Marca_model');
            $this->load->model('Categoria_model');
            
            $this->load->model('Producto_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Promocion_temporal_model');
            $this->load->library('upload');
            $this->load->library('Comprime');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	public function index(){
		show_404();
	}

	public function guardarDataBrand(){

		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/brands/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 		
	 		if(!file_exists("./assets/uploads/brands/")){
	          if(!mkdir("./assets/uploads/brands", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }

	        if (isset($_FILES['imagen_marca']['name'])) {
	            if (0 < $_FILES['imagen_marca']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_marca']['error'];
	            } else {
	                if (file_exists('./assets/uploads/brands/' . $_FILES['imagen_marca']['name'])) {
	                    echo 'File already exists : assets/uploads/' . $_FILES['imagen_marca']['name'];           
	                } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_marca')){
	                        echo json_encode($this->upload->display_errors());
	                    } else {
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
	                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    	$this->comprime->comprime_image($archivo, 70);

			    			$url_final = base_url().'assets/uploads/brands/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        } else {
	            echo 'Please choose a file';
	        }

			$arrayBrand = array('nombre'=>$this->input->post("nombre_marca"),
							'descripcion'=>$this->input->post("descripcion_marca"),
							'logo'=>$url_final);
			$existeMarca = $this->Marca_model->existe_marca($arrayBrand['nombre']);
			if(!$existeMarca){
				if($this->Marca_model->inserta_marca($arrayBrand)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: la marca ".$arrayBrand['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function editDataBrand($user = null){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/brands/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 
	        if (isset($_FILES['imagen_marca']['name'])) {
	            if (0 < $_FILES['imagen_marca']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_marca']['error'];
	            } else {
	                if (file_exists('./assets/uploads/brands/' . $_FILES['imagen_marca']['name'])) {
	                    echo 'File already exists : assets/uploads/' . $_FILES['imagen_marca']['name'];           } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_marca')){
	                        echo $this->upload->display_errors();
	                    } else {
	                    	echo "imagen subida con exito";
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
	                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    	$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/brands/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        }

			$arrayBrand = array('id'=>$this->input->post("id_marca"),
							'nombre'=>$this->input->post("nombre_marca"),
							'descripcion'=>$this->input->post("descripcion_marca"));
			if ($url_final != ""){
				$actual_imagen = str_replace(base_url(), './', $this->Marca_model->actual_logo($arrayBrand['id']));
				if(file_exists($actual_imagen['logo'])){
					try {
						unlink($actual_imagen['logo']);
					} catch (Exception $e) {
					}
		        }
				$arrayBrand['logo']=$url_final;
			}

			if($this->Marca_model->edita_marca($arrayBrand)){
				echo "Registrado con éxito";
			}else{
				echo "Error al registrar";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataBrand($id = null){
		if ($this->input->is_ajax_request()) {
			$actual_imagen = str_replace(
					base_url(), 
					'./', 
					$this->Marca_model->actual_logo($this->input->post("id_marca"))
					);
			if(file_exists($actual_imagen['logo'])){
				try {
					unlink($actual_imagen['logo']);
				} catch (Exception $e) {
				}
	        }
			if($this->Marca_model->elimina_marca($this->input->post("id_marca"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Usuario";
			}
		}else{
			show_404();
		}
	}

	public function getDataListMarcas(){
		if ($this->input->is_ajax_request()) {
			if($this->input->post("dfb") != null){
				if($this->input->post("marvalue")!= null){
					echo json_encode(array("estado"=>0,
						 "marcas"=>$this->Marca_model->get_productos_filtrados_marcas($this->input->post("marvalue"),$this->input->post("dfb"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data filtro"));
				}
			}else{
				if($this->input->post("marvalue")!= null){
					echo json_encode(array("estado"=>0,
					 "marcas"=>$this->Marca_model->get_productos_marca($this->input->post("marvalue"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data normal"));
				}
			}
		}else{
			show_404();
		}
	}

	public function view($dataView=null,$nombre=null){
		if(!is_null($dataView)){

				$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"Prueba CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
				//pido al modelo el artículo que se desea ver
				$arrayMarca = $this->Marca_model->get_marca($dataView);

				//compruebo si he recibido un artículo
				if (!$arrayMarca){
				 //no he recibido ningún artículo
				 //voy a lanzar un error 404 de página no encontrada
					echo "no existe";
				 show_404();
				}else{
					 //he encontrado el artículo
					 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
					
					//pido los ultimos artículos al modelo
				  	$dataCategoria = $this->Categoria_model->get_total_prods_categorias_marcas($dataView);
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_categorias'] = $dataCategoria;
					//pido los ultimos artículos al modelo
				  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_marcas'] = $dataMarca;

	      			$ultimosArticulos = $this->Marca_model->get_productos_marca($dataView);

					$dataPagina['arrayMarca']=$arrayMarca;
					$dataPagina['lista_productos']=$ultimosArticulos;
					$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
					$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
					$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
					$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
	  				$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
					$dataPagina['promoActiva']=$dataPromo;
					//$dataPagina["ultimoSubido"] = $this->Producto_model->get_productos_nuevos()[0];
					 //cargo la vista pasando los datos de configuacion
					$this->load->view('layout/head',$dataPagina);
					$this->load->view('layout/header', $dataPagina);
					$this->load->view('layout/nav', $dataPagina);
					//en esta parte va el contenido personalizado
					$this->load->view('marcas', $dataPagina);		
					$this->load->view('layout/footer', $dataPagina);
				}
		}else{
			show_404();
		}	
	}

}
