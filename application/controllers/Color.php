<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Color extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de artículos
            $this->load->model('Color_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function guardarDataColor(){
		if ($this->input->is_ajax_request()) {
			$arrayColor = array('nombre'=>$this->input->post("nombre_color"),
								'descripcion'=>$this->input->post("descripcion_color"),
								'hexa'=>$this->input->post("hexa_color"));
			$existeColor = $this->Color_model->existe_color($arrayColor['nombre']);
			if(!$existeColor){
				if($this->Color_model->inserta_color($arrayColor)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el color ".$arrayColor['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function editDataColor($user = null){
		if ($this->input->is_ajax_request()) {
			$arrayColor = array('id'=>$this->input->post("id_color"),
							'nombre'=>$this->input->post("nombre_color"),
							'descripcion'=>$this->input->post("descripcion_color"),
							'hexa'=>$this->input->post("hexa_color"));

			if($this->Color_model->edita_color($arrayColor)){
				echo "editado con exito";
			}else{
				echo "Error al editar Color,el color ".$arrayColor['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataColor($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Color_model->elimina_color($this->input->post("id_color"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar el color";
			}
		}else{
			show_404();
		}
	}
}
