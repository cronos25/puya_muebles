<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicio extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Categoria_model');
            $this->load->model('Servicio_model');
            $this->load->model('Producto_model');
            $this->load->model('Marca_model');
            /*$this->load->model('especificacion/Especificacion_model');
            */
            $this->load->library('upload');
            $this->load->library('Comprime');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	public function index(){
		show_404();
	}

	public function view($dataView=null,$nombre=null){
		if(!is_null($dataView)){
				$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"Prueba CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
				//pido al modelo el artículo que se desea ver
				$arrayCategoria = $this->Categoria_model->get_categoria($dataView);
				
				//compruebo si he recibido un artículo
				if (!$arrayCategoria){
				 //no he recibido ningún artículo
				 //voy a lanzar un error 404 de página no encontrada
					echo "no existe";
				 show_404();
				}else{
					 //he encontrado el artículo
					 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
					//pido los ultimos artículos al modelo
				  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_categorias'] = $dataCategoria;
					//pido los ultimos artículos al modelo
				  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_marcas'] = $dataMarca;

					//creo el array con datos de configuración para la vista
					$datosServicio = $this->Servicio_model->get_datos_servicio_categoria($arrayCategoria['id']);
					$ParrafosServicio = $this->Servicio_model->obtener_lista_parrafos_categoria($arrayCategoria['id']);

					 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
					$dataPagina['arrayCategoria']=$arrayCategoria;	
					$dataPagina['datosServicio']=$datosServicio;
					$dataPagina['ParrafosServicio']=$ParrafosServicio;
					

				  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
					$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;

					$ultimosArticulos = $this->Categoria_model->get_productos_categoria($arrayCategoria['id']);

	      			$dataPagina['lista_productos']=$ultimosArticulos;

	      			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
					$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
					$dataPagina['lista_productos_relacionados']=$ultimosArticulos;
					/*$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
					$dataPagina['promoActiva']=$dataPromo;*/
					 //cargo la vista pasando los datos de configuacion
					$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

					$this->load->view('layout/head',$dataPagina);
					//el menu de navegacion se encuetra dentro del header
					$this->load->view('layout/header',$dataPagina);
					//en esta parte va el contenido personalizado
					$this->load->view('producto/view_servicio', $dataPagina);		
					$this->load->view('layout/footer', $dataPagina);
				}
		}else{
			show_404();
		}	
	}


	public function editDataServicio($user = null){
		if ($this->input->is_ajax_request()) {
			//upload file

			$arrayDatosServicio = array("id_categoria"=>$this->input->post("id_categoria"),
							"email"=>$this->input->post("email_servicio_categoria"),
							"mail2"=>$this->input->post("mail2_servicio_categoria"),
							"mail3"=>$this->input->post("mail3_servicio_categoria"),
							"telefono"=>$this->input->post("telefono_servicio_categoria"));
			if($this->Servicio_model->edita_datos_servicio($arrayDatosServicio)){
				echo "Registrado con éxito";
			}else{
				echo "no se realizaron cambios";
			}
		}else{
			show_404();
		}
	}


	public function guardarParrafoServicio(){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/parrafos/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 		
	 		if(!file_exists("./assets/uploads/parrafos/")){
	          if(!mkdir("./assets/uploads/parrafos", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }
        	
        	if (isset($_FILES['imagen_parrafo'])) {
        		if (isset($_FILES['imagen_parrafo']['name'])) {
		            if (0 < $_FILES['imagen_parrafo']['error']) {
		                echo 'Error during file upload' . $_FILES['imagen_parrafo']['error'];
		            } else {
		                if (file_exists('./assets/uploads/parrafos/' . $_FILES['imagen_parrafo']['name'])) {
		                    echo 'File already exists : assets/uploads/' . $_FILES['imagen_parrafo']['name'];           } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('imagen_parrafo')){
		                        echo "error subiendo la imagen ";
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 70);
				    			$url_final = base_url().'assets/uploads/parrafos/'.$file_data['file_name'].'.jpg';
		                    }
		                }
		            }
		        } else {
		            echo 'Please choose a file';
		        }
        	}
        	
        

			$arrayParagraph = array('id_categoria'=>$this->input->post("categoria_parrafo"),
							'mensaje_parrafo'=>$this->input->post("mensaje_parrafo_servicio"),
							"tipo_parrafo"=>$this->input->post("tipo_parrafo_servicio"),
							"numero_parrafo"=>$this->input->post("numero_parrafo_servicio"),
							'imagen_parrafo'=>$url_final);

			if($this->Servicio_model->inserta_parrafo_servicio($arrayParagraph)){
				echo "Registrado con éxito";
			}else{
				echo "Error al registrar";
			}
		}else{
			show_404();
		}
	}

	public function contactarServicio(){
		if ($this->input->is_ajax_request()) {
			$servicio = $this->security->xss_clean($this->input->post("servicio"));
			$nombre = $this->security->xss_clean($this->input->post("nombre"));
			$email = $this->security->xss_clean($this->input->post("email"));
			$message = $this->security->xss_clean($this->input->post("message"));
			if (!empty($servicio) &&
				!empty($nombre) &&
				!empty($email) &&
				!empty($message) ) {
				$arrayCategoria = $this->Categoria_model->get_categoria($this->security->xss_clean($this->input->post("servicio")));
				$datosServicio = $this->Servicio_model->get_datos_servicio_categoria($arrayCategoria['id']);

				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				
				//configuracion 

				$this->email->initialize($config);

				$this->email->from('contacto@puyamuebles.cl', 'Contacto Servicio '.$arrayCategoria['nombre']);
				if (!empty($datosServicio['email'])) {
					$this->email->to($datosServicio['email']);
				}
				if (!empty($datosServicio['mail2'])) {
					$this->email->to($datosServicio['mail2']);
				}
				if (!empty($datosServicio['mail3'])) {
					$this->email->to($datosServicio['mail3']);
				}

				$this->email->subject('Contacto de Servicio '.$arrayCategoria['nombre']);
				$this->email->message('<!DOCTYPE html><html>
				 <head>
				  <title>Contacto Servicio Puya Muebles</title>
				</head>
				<body style="margin: 0; padding: 0;">
				   <table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
				      <tr style="color: #fff; font-family: Roboto,sans-serif">
				        <td align="center">
				         <img src="http://www.puyamuebles.cl/img/logo.png" alt="" width="auto" height="150" style="display: block;padding:30px;" />
				        </td>
				      </tr>
				      
				      <tr>
				            <th>
				              <h1 style="font-family: roboto,sans-serif; color: #000;">Contacto Clientes</h1>
				            </th>
				         </tr>
				         <tr>
				            <th style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 5px 30px;">
				               Nombre: '.$this->security->xss_clean($this->input->post("nombre")).'
				            </th>
				         </tr>
				         <tr>
				            <th style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 5px 30px;">
				               Email: <a href="mailto:'. $this->security->xss_clean($this->input->get("email")) .'" target="_top">'. $this->security->xss_clean($this->input->get("email")) .'</a>
				            </th>
				         </tr>
				         <tr>
				            <th style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 5px 30px;">
				               Servicio:  '.$arrayCategoria['nombre'].'
				            </th>
				         </tr>
				         <tr>
				            <td style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 5px 30px;" >
				               Mensaje : 
				            </td>
				         </tr>
				         <tr>
				            <td style="text-align: left; font-family: noto,sans-serif; color: rgb(71, 71, 71); padding: 30px;">
				            '.$this->security->xss_clean($this->input->get("message")).'
				            </td>
				         </tr>
				   </table>
				</body>
				</html>');
				$this->email->set_alt_message('Se ha contactado por el servicio '.$arrayCategoria['nombre'].' desde el mail: '. $this->security->xss_clean($this->input->get("email")) .', con el siguiente mensaje: '.$this->security->xss_clean($this->input->get("message")).'');
				if ($this->email->send())
				{
					echo "Mensaje enviado";
				}

			}else{
					echo "Existen errores en el formulario";
			}
			
		}
		
	}

	public function eliminaParrafoServicio($id = null){
		if ($this->input->is_ajax_request()) {
			$actual_imagen = str_replace(
					base_url(), 
					'./', 
					$this->Servicio_model->actual_imagen($this->input->post("id_parrafo"))['imagen_parrafo']
					);
			if(file_exists($actual_imagen)){
				try {
					unlink($actual_imagen);
				} catch (Exception $e) {
				}
	        }
			
			/*$this->Especificacion_model->elimina_especificacion_categoria($this->input->post("id_categoria"));*/
			if($this->Servicio_model->elimina_parrafo_servicio($this->input->post("id_parrafo"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar el Parrafo";
			}
		}else{
			show_404();
		}
	}
}