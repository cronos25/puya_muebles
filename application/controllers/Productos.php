<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Categoria_model');
            $this->load->model('Marca_model');
            $this->load->model('Producto_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Producto_stack_model');
            $this->load->model('Promocion_temporal_model');
            $this->load->model('Color_model');
            $this->load->library('upload');
            $this->load->library('Comprime');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
            $this->load->helper('ayuda_helper');
    }
	public function index(){
		show_404();
	}

	public function guardarDataProduct(){
		if ($this->input->is_ajax_request()) {
			//echo count($_FILES['imagenes_productos']['name']);
			//upload file
			if(!file_exists("./assets/uploads/productos/")){
	          if(!mkdir("./assets/uploads/productos/", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }
			$existeProducto = $this->Producto_model->existe_producto($this->input->post("nombre_producto"));
			if(!$existeProducto){
				
				if(!file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/")){
		          if(!mkdir("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/", 0777, true)) {
		            die('Fallo al crear las carpetas...');
		          }
		        }

				$url_final_productos = "";
				$url_final_etiqueta = "";
				$url_final_home = "";
				$destination_thumbs = "assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/";
		        $config['upload_path'] = './assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/";
		        $config['allowed_types'] = '*';
		        $config['max_filename'] = '200';
		        $config['encrypt_name'] = TRUE;
		        $config['max_size'] = '2048'; //2 MB

		        if (isset($_FILES['img_productos']['name'])) {
		        	//
		            if (0 < $_FILES['img_productos']['error']) {
		                echo 'Error during file upload' . $_FILES['img_productos']['error'];
		            } else {
		                if (file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/" . $_FILES['img_productos']['name'])) {
		                    echo 'Ya existe el archivo: productos/'.$this->input->post('nombre_producto').'/' . $_FILES['img_productos']['name'];          
		                } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('img_productos')){
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 80);
								$url_final_productos = base_url().'assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto"))).'/' .$file_data['file_name'].'.jpg';
		                    }
		                }
		            }
		        }
		        if (isset($_FILES['img_etiquetas']['name'])) {
		        	//
		            if (0 < $_FILES['img_etiquetas']['error']) {
		                echo 'Error during file upload' . $_FILES['img_etiquetas']['error'];
		            } else {
		                if (file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/" . $_FILES['img_etiquetas']['name'])) {
		                    echo 'Ya existe el archivo: productos/'.$this->input->post('nombre_producto').'/' . $_FILES['img_etiquetas']['name'];          
		                } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('img_etiquetas')){
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 80);
				    			$url_final_etiqueta = base_url().'assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto"))).'/' .$file_data['file_name'].'.jpg';
		                    }

		                }
		            }
		        }
		        if (isset($_FILES['img_home']['name'])) {
		        	//
		            if (0 < $_FILES['img_home']['error']) {
		                echo 'Error during file upload' . $_FILES['img_home']['error'];
		            } else {
		                if (file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/" . $_FILES['img_home']['name'])) {
		                    echo 'Ya existe el archivo: productos/'.$this->input->post('nombre_producto').'/' . $_FILES['img_home']['name'];          
		                } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('img_home')){
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 80);
				    			$url_final_home = base_url().'assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto"))).'/' .$file_data['file_name'].'.jpg';
		                    }

		                }
		            }
		        }
		        $altoProducto = 0;
		        $anchoProducto = 0;
		        $largoProducto = 0;
		        if ($this->input->post("medida_s_alto_producto") == null || $this->input->post("medida_s_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_s_alto_producto");

		        }
		        if ($this->input->post("medida_s_ancho_producto") == null || $this->input->post("medida_s_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_s_ancho_producto");

		        }
		        if ($this->input->post("medida_s_largo_producto") == null || $this->input->post("medida_s_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_s_largo_producto");

		        }
		        
		        $medidas_s = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

		        if ($this->input->post("medida_m_alto_producto") == null || $this->input->post("medida_m_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_m_alto_producto");

		        }
		        if ($this->input->post("medida_m_ancho_producto") == null || $this->input->post("medida_m_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_m_ancho_producto");

		        }
		        if ($this->input->post("medida_m_largo_producto") == null || $this->input->post("medida_m_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_m_largo_producto");

		        }
		        
		        $medidas_m = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

		         if ($this->input->post("medida_l_alto_producto") == null || $this->input->post("medida_l_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_l_alto_producto");

		        }
		        if ($this->input->post("medida_l_ancho_producto") == null || $this->input->post("medida_l_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_l_ancho_producto");

		        }
		        if ($this->input->post("medida_l_largo_producto") == null || $this->input->post("medida_l_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_l_largo_producto");

		        }
		        
		        $medidas_l = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

		        if ($this->input->post("medida_xl_alto_producto") == null || $this->input->post("medida_xl_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_xl_alto_producto");

		        }
		        if ($this->input->post("medida_xl_ancho_producto") == null || $this->input->post("medida_xl_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_xl_ancho_producto");

		        }
		        if ($this->input->post("medida_xl_largo_producto") == null || $this->input->post("medida_xl_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_xl_largo_producto");

		        }
		        
		        $medidas_xl = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

				$arrayProduct = array('nombre'=>$this->input->post("nombre_producto"),
								'descripcion'=>$this->input->post("descripcion_producto"),
								'sku'=>$this->input->post("codigo_producto"),
								'precio'=>$this->input->post("precio_producto"),
								'descuento'=>$this->input->post("descuento_producto"),
								'stock'=>$this->input->post("stock_producto"),
								'categoria_id'=>$this->input->post("categoria_producto"),
								'sub_categoria_id'=>$this->input->post("sub_categoria_producto"),
								'marca_id'=>$this->input->post("marca_producto"),
								'regalos'=>$this->input->post("regalo_producto"),
								'envio_gratis'=>$this->input->post("envio_gratis_producto"),
								'tipo_entrega'=>$this->input->post("tipo_entrega_producto"),
								'precio_m'=>$this->input->post("preciom_producto"),
								'precio_l'=>$this->input->post("preciol_producto"),
								'precio_xl'=>$this->input->post("precioxl_producto"),
								'medida_s'=>$medidas_s,
								'medida_m'=>$medidas_m,
								'medida_l'=>$medidas_l,
								'medida_xl'=>$medidas_xl,
								'foto_producto'=>$url_final_productos,
								'foto_etiqueta'=>$url_final_etiqueta,
								'foto_home'=>$url_final_home);

				$idregistrado = $this->Producto_model->inserta_producto($arrayProduct);
				$arraySku = array('id'=>$idregistrado,
									'sku' => $arrayProduct['sku'].$idregistrado);
				if (count($this->input->post("color_producto"))>0) {
					foreach ($this->input->post("color_producto") as $key) {
		              	$this->Producto_model->asigna_color($key,$idregistrado); 
		            }
				}
		            
				$this->Producto_model->creaskupostregistro($arraySku);

				echo "Producto Registrado con exito";
			}else{
				echo "Error: el producto ".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function setProductoDestacado(){
		if ($this->input->is_ajax_request()) {

			if($this->Producto_model->asigna_destacado($this->input->post("id_producto_dest"))){
				echo "asignado con exito";
			}else{
				echo "Error al asignar el producto";
			}
		}else{
			show_404();
		}
	}

	public function editDataProduct(){
		if ($this->input->is_ajax_request()) {
			//echo count($_FILES['imagenes_productos']['name']);
			$actualProducto = $this->Producto_model->get_producto($this->input->post("id_producto"));
			

				if(!file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/")){
		          if(!mkdir("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/", 0777, true)) {
		            die('Fallo al crear las carpetas...');
		          }
		        }

				$url_final_productos = "";
				$url_final_etiqueta = "";
				$url_final_home = "";
				$destination_thumbs = "assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/";
		        $config['upload_path'] = './assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/";
		        $config['allowed_types'] = '*';
		        $config['max_filename'] = '200';
		        $config['encrypt_name'] = TRUE;
		        $config['max_size'] = '2048'; //2 MB

		        if (isset($_FILES['img_productos']['name'])) {
		        	//
		        	if (0 < $_FILES['img_productos']['error']) {
		                echo 'Error during file upload' . $_FILES['img_productos']['error'];
		            } else {
		                if (file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/" . $_FILES['img_productos']['name'])) {
		                    echo 'Ya existe el archivo: productos/'.$this->input->post('nombre_producto').'/' . $_FILES['img_productos']['name'];          
		                } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('img_productos')){
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 80);
				    			$url_final_productos = base_url().'assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto"))).'/' .$file_data['file_name'].'.jpg';
		                    }

		                }
		            }
		        }
		        if (isset($_FILES['img_etiquetas']['name'])) {
		        	//
		            if (0 < $_FILES['img_etiquetas']['error']) {
		                echo 'Error during file upload' . $_FILES['img_etiquetas']['error'];
		            } else {
		                if (file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/" . $_FILES['img_etiquetas']['name'])) {
		                    echo 'Ya existe el archivo: productos/'.$this->input->post('nombre_producto').'/' . $_FILES['img_etiquetas']['name'];          
		                } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('img_etiquetas')){
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 80);
				    			$url_final_etiqueta = base_url().'assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto"))).'/' .$file_data['file_name'].'.jpg';
		                    }

		                }
		            }
		        }
		        if (isset($_FILES['img_home']['name'])) {
		        	//
		            if (0 < $_FILES['img_home']['error']) {
		                echo 'Error during file upload' . $_FILES['img_home']['error'];
		            } else {
		                if (file_exists("./assets/uploads/productos/".str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto")))."/" . $_FILES['img_home']['name'])) {
		                    echo 'Ya existe el archivo: productos/'.$this->input->post('nombre_producto').'/' . $_FILES['img_home']['name'];          
		                } else {
		                    $this->upload->initialize($config);
		                    if (!$this->upload->do_upload('img_home')){
		                        echo $this->upload->display_errors();
		                    } else {
		                    	$file_data = $this->upload->data();
		                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
		                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
	                    		$this->comprime->comprime_image($archivo, 80);
				    			$url_final_home = base_url().'assets/uploads/productos/'.str_replace('%','',str_replace(' ', '_',$this->input->post("nombre_producto"))).'/' .$file_data['file_name'].'.jpg';
		                    }

		                }
		            }
		        }
				$altoProducto = 0;
		        $anchoProducto = 0;
		        $largoProducto = 0;
		        if ($this->input->post("medida_s_alto_producto") == null || $this->input->post("medida_s_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_s_alto_producto");

		        }
		        if ($this->input->post("medida_s_ancho_producto") == null || $this->input->post("medida_s_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_s_ancho_producto");

		        }
		        if ($this->input->post("medida_s_largo_producto") == null || $this->input->post("medida_s_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_s_largo_producto");

		        }
		        
		        $medidas_s = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

		        if ($this->input->post("medida_m_alto_producto") == null || $this->input->post("medida_m_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_m_alto_producto");

		        }
		        if ($this->input->post("medida_m_ancho_producto") == null || $this->input->post("medida_m_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_m_ancho_producto");

		        }
		        if ($this->input->post("medida_m_largo_producto") == null || $this->input->post("medida_m_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_m_largo_producto");

		        }
		        
		        $medidas_m = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

		         if ($this->input->post("medida_l_alto_producto") == null || $this->input->post("medida_l_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_l_alto_producto");

		        }
		        if ($this->input->post("medida_l_ancho_producto") == null || $this->input->post("medida_l_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_l_ancho_producto");

		        }
		        if ($this->input->post("medida_l_largo_producto") == null || $this->input->post("medida_l_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_l_largo_producto");

		        }
		        
		        $medidas_l = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

		        if ($this->input->post("medida_xl_alto_producto") == null || $this->input->post("medida_xl_alto_producto") < 0) {
		        	$altoProducto = 0;
		        }else{
		        	$altoProducto = $this->input->post("medida_xl_alto_producto");

		        }
		        if ($this->input->post("medida_xl_ancho_producto") == null || $this->input->post("medida_xl_ancho_producto") < 0) {
		        	$anchoProducto = 0;
		        }else{
		        	$anchoProducto = $this->input->post("medida_xl_ancho_producto");

		        }
		        if ($this->input->post("medida_xl_largo_producto") == null || $this->input->post("medida_xl_largo_producto") < 0) {
		        	$largoProducto = 0;
		        }else{
		        	$largoProducto = $this->input->post("medida_xl_largo_producto");

		        }
		        
		        $medidas_xl = $altoProducto.'_'.$anchoProducto.'_'.$largoProducto; //alto_ancho_largo

			$arrayProduct = array(
							'id'=>$this->input->post("id_producto"),
							'nombre'=>$this->input->post("nombre_producto"),
							'descripcion'=>$this->input->post("descripcion_producto"),
							'sku'=>$this->input->post("codigo_producto"),
							'precio'=>$this->input->post("precio_producto"),
							'descuento'=>$this->input->post("descuento_producto"),
							'stock'=>$this->input->post("stock_producto"),
							'categoria_id'=>$this->input->post("categoria_producto"),
							'sub_categoria_id'=>$this->input->post("sub_categoria_producto"),
							'marca_id'=>$this->input->post("marca_producto"),
							'regalos'=>$this->input->post("regalo_producto"),
							'envio_gratis'=>$this->input->post("envio_gratis_producto"),
							'precio_m'=>$this->input->post("preciom_producto"),
							'precio_l'=>$this->input->post("preciol_producto"),
							'precio_xl'=>$this->input->post("precioxl_producto"),
							'medida_s'=>$medidas_s,
							'medida_m'=>$medidas_m,
							'medida_l'=>$medidas_l,
							'medida_xl'=>$medidas_xl,
							'tipo_entrega'=>$this->input->post("tipo_entrega_producto"));
			if ($url_final_productos != ""){
				if(file_exists(str_replace(base_url(), './',$actualProducto['foto_producto']))){
					try {
						unlink(str_replace(base_url(), './',$actualProducto['foto_producto']));
					} catch (Exception $e) {
					}
		        }
				$arrayProduct['foto_producto']=$url_final_productos;
			}

			if ($url_final_etiqueta != ""){
				if(file_exists(str_replace(base_url(), './',$actualProducto['foto_etiqueta']))){
					try {
						unlink(str_replace(base_url(), './',$actualProducto['foto_etiqueta']));
					} catch (Exception $e) {
					}
		        }
				$arrayProduct['foto_etiqueta']=$url_final_etiqueta;
			}

			if ($url_final_home != ""){
				if(file_exists(str_replace(base_url(), './',$actualProducto['foto_home']))){
					try {
						unlink(str_replace(base_url(), './',$actualProducto['foto_home']));
					} catch (Exception $e) {
					}
		        }
				$arrayProduct['foto_home']=$url_final_home;
			}

			if($this->Producto_model->edita_producto($arrayProduct)){
	          	$this->Producto_model->elimina_colores($this->input->post("id_producto"));
	            if (count($this->input->post("color_producto"))>0) {
					foreach ($this->input->post("color_producto") as $key) {
	              		$this->Producto_model->asigna_color($key,$this->input->post("id_producto")); 
	            	}
				}
				echo "Producto Editado con exito";
			}else{
				$this->Producto_model->elimina_colores($this->input->post("id_producto"));
	            foreach ($this->input->post("color_producto") as $key) {
	              	$this->Producto_model->asigna_color($key,$this->input->post("id_producto")); 
	            }
				echo "Fin de la edicion de producto";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataProduct($id = null){
		if ($this->input->is_ajax_request()) {
			$actual_imagen = str_replace(
					base_url(), 
					'./', 
					$this->Producto_model->actual_imagen($this->input->post("id_producto"))['foto_producto']
					);
			if(file_exists($actual_imagen)){
				try {
					unlink($actual_imagen);
				} catch (Exception $e) {
				}
	        }
	        $actual_imagen = str_replace(
					base_url(), 
					'./', 
					$this->Producto_model->actual_imagen($this->input->post("id_producto"))['foto_etiqueta']
					);
			if(file_exists($actual_imagen)){
				try {
					unlink($actual_imagen);
				} catch (Exception $e) {
				}
	        }
	        $actual_imagen = str_replace(
					base_url(), 
					'./', 
					$this->Producto_model->actual_imagen($this->input->post("id_producto"))['foto_home']
					);
			if(file_exists($actual_imagen)){
				try {
					unlink($actual_imagen);
				} catch (Exception $e) {
				}
	        }

			if($this->Producto_model->elimina_producto($this->input->post("id_producto"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar el producto";
			}
		}else{
			show_404();
		}
	}

	public function view($dataView=null,$nombre=null){
		if(!is_null($dataView)){
				$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"Prueba CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
				//pido al modelo el artículo que se desea ver
				$arrayProducto = $this->Producto_model->get_producto($dataView);

				//compruebo si he recibido un artículo
				if (!$arrayProducto){
				 //no he recibido ningún artículo
				 //voy a lanzar un error 404 de página no encontrada
					echo "no existe";
				 show_404();
				}else{
					 //he encontrado el artículo
					 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
					//pido los ultimos artículos al modelo
				  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_categorias'] = $dataCategoria;
					//pido los ultimos artículos al modelo
				  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_marcas'] = $dataMarca;

	      			$ultimosArticulos = $this->Producto_model->get_productos_relacionado($arrayProducto['id'],$arrayProducto['marca_id'],$arrayProducto['categoria_id']);
	      			$dataColores = $this->Color_model->get_total_colores_producto_detalle($dataView);
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_colores'] = $dataColores;
	      			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
					$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
					$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
					$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
	  				
				  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
					$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
					$dataPagina['arrayProducto']=$arrayProducto;

					$ultimosArticulos = $this->Categoria_model->get_productos_categoria($arrayProducto['categoria_id']);
	      			$dataPagina['lista_productos']=$ultimosArticulos;

					if ($arrayProducto['is_stack']) {
						$dataStack = $this->Producto_stack_model->get_productos_stack($arrayProducto['id']);
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_productos_stack'] = $dataStack;
					}
					

					$dataPagina['lista_productos_relacionados']=$ultimosArticulos;
					/*$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
					$dataPagina['promoActiva']=$dataPromo;*/
					 //cargo la vista pasando los datos de configuacion
					$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

					$this->load->view('layout/head',$dataPagina);
					//el menu de navegacion se encuetra dentro del header
					$this->load->view('layout/header',$dataPagina);
					//en esta parte va el contenido personalizado
					$this->load->view('producto/view', $dataPagina);		
					$this->load->view('layout/footer', $dataPagina);
				}
		}else{
			show_404();
		}	
	}

	public function getDataListFiltrados(){
		if ($this->input->is_ajax_request()) {
			/*
				tdf ----> tipo de filtro
				d --->destacado
				p --->producto
				b --->buscador
			*/
			if($this->input->post("tdf") != null){
				if ($this->input->post("tdf")=='d') {
						echo json_encode(array("estado"=>0,		
					 			"productos"=>$this->Producto_model->get_productos_destacados($this->input->post("fm"),$this->input->post("fc"))));
				}
				if ($this->input->post("tdf")=='b') {
					//$productosSinfiltrar = $this->Producto_model->get_productos_filtrados($this->input->post("fm"),$this->input->post("fc"));
					
					$productosFiltrados = $this->Producto_model->get_productos_filtrados($this->input->post("fm"),$this->input->post("fc"),$this->input->post("fco"),$this->input->post("buscar"));
					
					//$filtroString = $this->input->post("buscarText");
					//$productosFiltrados = array();
						/*foreach ($productosSinfiltrar as $productoItem) {
						if( strpos( 
							strtolower($productoItem['descripcion']),strtolower($filtroString)  ) !== false ||
							strpos( 
							strtolower($productoItem['nombre']),strtolower($filtroString)  ) !== false ||
							strpos( 
							strtolower($productoItem['categoria']),strtolower($filtroString)  ) !== false ||
							strpos( 
							strtolower($productoItem['codigo']),strtolower($filtroString)  ) !== false ||
							strpos( 
							strtolower($productoItem['link_producto']),strtolower($filtroString)  ) !== false ||
							strpos( 
							strtolower($productoItem['marca']),strtolower($filtroString)  ) !== false ||
							strpos(
							strtolower($productoItem['marca_descripcion']),strtolower($filtroString) ) !== false || 
							strpos(
							strtolower($productoItem['categoria_descripcion']),strtolower($filtroString) ) !== false ) {
						   array_push($productosFiltrados,$productoItem);
						}
							
						}*/
						echo json_encode(array("estado"=>0,		
					 			"productos"=>$productosFiltrados));
				}
			}
		}else{
			show_404();
		}
	}

	public function destacados(){
			$dataPagina = array('titulo'=>"Puya Muebles",
				'nombre_app'=>"Prueba CODEIGNITER",
				'descripcion'=>"Layout hecho en codeigniter");
			//pido al modelo el artículo que se desea ver

			//compruebo si he recibido un artículo
			 //he encontrado el artículo
			 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
			
			//pido los ultimos artículos al modelo
		  	$dataCategoria = $this->Categoria_model->get_total_prods_categorias();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_categorias'] = $dataCategoria;
			//pido los ultimos artículos al modelo
		  	$dataMarca = $this->Marca_model->get_total_prods_marcas();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_marcas'] = $dataMarca;

			$ultimosArticulos = $this->Producto_model->get_productos_destacados();

			//$dataPagina['arrayCategoria']=$arrayCategoria;
			$dataPagina['lista_productos']=$ultimosArticulos;
			$dataPagina["ultimoSubido"] = $this->Producto_model->get_productos_nuevos()[0];
			 //cargo la vista pasando los datos de configuacion
			$this->load->view('layout/head',$dataPagina);
			//$this->load->view('layout/header', $dataPagina);
			$this->load->view('layout/nav', $dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('home/productos', $dataPagina);		
			$this->load->view('layout/footer', $dataPagina);	
	}

	public function buscar(){
		$dataPagina = array('titulo'=>"Puya Muebles",
			'nombre_app'=>"Prueba CODEIGNITER",
			'descripcion'=>"Layout hecho en codeigniter");
		//pido al modelo el artículo que se desea ver

		//compruebo si he recibido un artículo
		 //he encontrado el artículo
		 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
		//$filtro = $this->security->xss_clean($this->input->get("buscar"));
		$filtro = "osho";
		//pido los ultimos artículos al modelo

		$dataPagina['busqueda']= $filtro;
		
		//$ultimosArticulos = $this->Producto_model->get_productos_buscador($filtro);
		$ultimosArticulos = $this->Producto_model->obtener_lista_productos_todo();
		$productosMarcas = array();
		foreach ($ultimosArticulos as $productoItem):
			array_push($productosMarcas,$productoItem['id']);
		endforeach;

		$dataColores = $this->Color_model->get_total_prods_colores_detalle();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_colores'] = $dataColores;

		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->get_total_prods_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
	  	$dataCategoriaFiltros = $this->Categoria_model->get_total_prods_categorias_buscar($productosMarcas);
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias_filtros'] = $dataCategoriaFiltros;
		$dataMarca = $this->Marca_model->get_total_prods_marcas_buscar($productosMarcas);
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_marcas'] = $dataMarca;
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		//$dataPagina['arrayCategoria']=$arrayCategoria;
		$dataPagina['lista_productos']=$ultimosArticulos;
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataPagina['promoActiva']=$dataPromo;
		 //cargo la vista pasando los datos de configuacion
		$this->load->view('layout/head',$dataPagina);
		$this->load->view('layout/header', $dataPagina);
		$this->load->view('layout/nav', $dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('buscador', $dataPagina);		
		$this->load->view('layout/footer', $dataPagina);
	}
}
