<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Precio_envio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//cargo el modelo de comunaes
            $this->load->model('Precio_envio_model');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
	}

	public function index()
	{
		redirect('admin/index', 'refresh'); 
	}

	public function guardarDataPrecioEnvio(){
		if ($this->input->is_ajax_request()) {
			$arrayPrecioEnvio = array('precio'=>$this->input->post("precio_precio_envio"),
							'region_id'=>$this->input->post("id_region"),
							'provincia_id'=>$this->input->post("id_provincia"),
							'comuna_id'=>$this->input->post("id_comuna"),
							'desde'=>$this->input->post("desde"),
							'hasta'=>$this->input->post("hasta"),
							'tipo_medida'=>$this->input->post("tipo_medida"));
			$existePrecioEnvio = $this->Precio_envio_model->existe_precio_envio($arrayPrecioEnvio);
			if(!$existePrecioEnvio){
				if($this->Precio_envio_model->inserta_precio_envio($arrayPrecioEnvio)){
					echo "Registrado con éxito";
				}else{
					if (empty($arrayPrecioEnvio['precio'])&&
         				empty($arrayPrecioEnvio['provincia_id'])) {
						echo "No puede tener datos vacios";
					}else if(empty($arrayPrecioEnvio['precio'])){
						echo "Falta el nombre de Ciudad";
					}else if(empty($arrayPrecioEnvio['provincia_id'])){
						echo "Falta la provincia de la ruta";
					}
				}
			}else{
				echo "Error: Ya Existe un Precio de envio para esta ruta ";
			}
		}else{
			show_404();
		}
		
	}

	public function editDataPrecioEnvio($comuna = null){
		if ($this->input->is_ajax_request()) {
			$arrayPrecioEnvio = array('id'=>$this->input->post("id_precio_envio"),
							'precio'=>$this->input->post("precio_precio_envio"),
							'region_id'=>$this->input->post("id_region"),
							'provincia_id'=>$this->input->post("id_provincia"),
							'comuna_id'=>$this->input->post("id_comuna"),
							'desde'=>$this->input->post("desde"),
							'hasta'=>$this->input->post("hasta"),
							'tipo_medida'=>$this->input->post("tipo_medida"));

			if($this->Precio_envio_model->edita_precio_envio($arrayPrecioEnvio)){
				echo "editado con exito";
			}else{
				echo "Error al editar Precio de envio";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataPrecioEnvio($id = null){
		if ($this->input->is_ajax_request()) {
			if($this->Precio_envio_model->elimina_precio_envio($this->input->post("id_precio_envio"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Precio de envio";
			}
		}else{
			show_404();
		}
	}

}

/* End of file PrecioEnvio.php */
/* Location: ./application/controllers/PrecioEnvio.php */