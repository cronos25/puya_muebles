<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de artículos
            $this->load->model('Cliente_model');
            $this->load->model('Region_model');
            $this->load->model('Provincia_model');
            $this->load->model('Comuna_model');
            $this->load->model('Carro_compra_model');
            $this->load->model('Categoria_model');
            $this->load->model('Producto_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Venta_model');
            $this->load->model('Precio_envio_model');
            $this->load->model('Promocion_temporal_model');
            $this->load->model('Codigo_promocion_model');
            $this->load->library('Recaptcha');
			$this->load->library('flowApiClass');
            //
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	
	public function index(){
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter");
		//Go to private area

		$siguiente = $this->security->xss_clean($this->input->get("next"));

		if(isset($_SESSION['cliente_data_puyasess']['logeado'])){
			$arrayCliente = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
			$dataRegion = $this->Region_model->obtener_lista_regiones();
			$dataProvincia = $this->Provincia_model->obtener_lista_provincias_region($arrayCliente['id_region']);
			$dataComunas = $this->Comuna_model->obtener_lista_comunas_provincia($arrayCliente['id_provincia']);
			$dataPagina["dataCliente"] = $arrayCliente;
			$dataPagina["lista_regiones"] = $dataRegion;
			$dataPagina["lista_provincias"] = $dataProvincia;
			$dataPagina["lista_comunas"] = $dataComunas;

		  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
			$dataPagina['lista_categorias'] = $dataCategoria;
			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
			$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
			$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
			$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			$listaCarros = $this->Carro_compra_model->get_carros_compra_procesado_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			
			$dataPagina['carro'] = $arrayCarro;
			$dataPagina['ventas'] = $this->Venta_model->get_venta_usuario($listaCarros);
		  	$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
			$dataPagina['promoActiva']=$dataPromo;

			$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

			$this->load->view('layout/head',$dataPagina);
			//el menu de navegacion se encuetra dentro del header
			$this->load->view('layout/header',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('cliente/perfil');

			//en esta parte va el contenido personalizado

			$this->load->view('layout/footer', $dataPagina);
		}else{
			if (!empty($siguiente)) {
				redirect('home/registro?next='.$siguiente, 'refresh'); 
			}else{
				redirect('home/registro', 'refresh'); 
			}
		}
	}

	public function login(){
		if(!isset($_SESSION['cliente_data_puyasess']['logeado'])){
			$siguiente = $this->security->xss_clean($this->input->post("next"));

			if(null !== $this->input->post('password') && null !== $this->input->post('username')){
				if ($this->abrirIniciarSesion($this->input->post('username'),$this->input->post('password'))) {
					if (!empty($siguiente)) {
						redirect($siguiente, 'refresh'); 
					}else{
						redirect('user/perfil', 'refresh');
					}
				}else{
						echo "<script src='".base_url()."assets/js/jquery-3.0.0.js'></script>
						<script>$(document).ready(function() {
							new Noty({		
								type: 'error',    
								closeWith: ['click', 'button'],
								timeout: 1500,
								text: 'Contraseña Erronea'
							}).show();
						});
						</script>";
			
					}
				}

			$dataCategoria = $this->Categoria_model->obtener_lista_categorias();

	     	$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'lista_categorias'=>$dataCategoria,
      						'descripcion'=>"Layout hecho en codeigniter");
	     	$dataRegion = $this->Region_model->obtener_lista_regiones();
			$dataCiudad = $this->Provincia_model->obtener_lista_provincias();
			$dataComunas = $this->Comuna_model->obtener_lista_comunas();
			$dataPagina["lista_regiones"] = $dataRegion;
			$dataPagina["lista_provincias"] = $dataCiudad;
			$dataPagina["lista_comunas"] = $dataComunas;
			$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
			$dataPagina['promoActiva']=$dataPromo;
			//pido los ultimos artículos al modelo
		  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_categorias'] = $dataCategoria;


	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
			//pido los ultimos artículos al modelo
			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
			$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
			$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

			$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

			$this->load->view('layout/head',$dataPagina);
			//el menu de navegacion se encuetra dentro del header
			$this->load->view('layout/header',$dataPagina);
			$this->load->view('cliente/login');
			//en esta parte va el contenido personalizado
			$this->load->view('layout/footer', $dataPagina);

		}else{
			redirect('user/perfil', 'refresh'); 
		}	
	}

	public function registro(){
     	$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"Prueba CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
     	$dataRegion = $this->Region_model->obtener_lista_regiones();
		$dataCiudad = $this->Provincia_model->obtener_lista_provincias();
		$dataComunas = $this->Comuna_model->obtener_lista_comunas();
		$dataPagina["lista_regiones"] = $dataRegion;
		$dataPagina["lista_provincias"] = $dataCiudad;
		$dataPagina["lista_comunas"] = $dataComunas;

		//cargo la vista pasando los datos de configuacion
		$this->load->view('layout/head',$dataPagina);
		//en esta parte va el contenido personalizado
		$this->load->view('cliente/registro');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}

	private function abrirIniciarSesion($user,$pass){
		/*if ($this->input->is_ajax_request()) {
			$user= $this->input->post("username");
			$pass = $this->input->post("password");*/
			$dataSession = $this->Cliente_model->login($user, $pass);

			if($dataSession){
				if(isset($_SESSION['cliente_data_puyasess'])){
					$this->session->unset_userdata('cliente_data_puyasess');
				}
				if(isset($_SESSION['cliente_data_puyasess'])){
					unset($_SESSION['cliente_data_puyasess']);
				}
				if(isset($_SESSION['cliente_data_puyasess'])){
					session_unset($_SESSION['cliente_data_puyasess']);
				}
				
				$sesionadmin = false;
				if(isset($_SESSION['admin_ecom_data']) &&
			    	$_SESSION['admin_ecom_data']['logeado']){
			    	$adminActual = array(
			         'admin_id' => $_SESSION['admin_ecom_data']['admin_id'],
			         'admin_email' => $_SESSION['admin_ecom_data']['admin_email'],
			         'logeado' => true
			       );
					$sesionadmin = true;

				}

				session_destroy();
				session_start();
				session_regenerate_id(true);

				if($sesionadmin){
			    	$this->session->set_userdata(array("admin_ecom_data"=>$adminActual));
				}

				$clienteActual = array();
				foreach($dataSession as $row)
				{
					$clienteActual = array(
					 'user_id' => $row->id,
					 'user_email' => $row->email,
					 'user_nombre' => $row->nombre.' '.$row->apellido,
					 'user_r' => $row->rut,
					 'logeado' => true
					);
				}

				if ($this->Carro_compra_model->existe_carro_compra($clienteActual['user_id'])) {
					$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($clienteActual['user_r'].$clienteActual['user_id']);
				}else{
					$arrayCarro = array('codigo'=>$clienteActual['user_r'].$clienteActual['user_id'],
						'id_cliente'=>$clienteActual['user_id']);
					$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

					$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($clienteActual['user_r'].$clienteActual['user_id']);
				}

				$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);

				/*				$clienteActual['user_vista_prod_carro'] = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);
				*/
				$clienteActual['user_cantidad_prod_carro'] = $this->Carro_compra_model->obtener_cantidad_productos_carro($arrayDataCarro);

				$this->session->set_userdata(array("cliente_data_puyasess"=>$clienteActual));
				//redirect('index', 'refresh'); 
				//echo json_encode(array("link"=>base_url().'index',"mensaje"=>"Bienvenido"));
				return true;
			}else{
				return false;
			}	
				
			/*
		}else{
			show_404();
		}	*/
	}

	public function cerrarSession(){
		if(isset($_SESSION['cliente_data_puyasess']['logeado'])){
			$cerrar = $this->input->post("cleardata");
			if($cerrar == 1){
				if(isset($_SESSION['cliente_data_puyasess'])){
					$this->session->unset_userdata('cliente_data_puyasess');
				}
				if(isset($_SESSION['cliente_data_puyasess'])){
					unset($_SESSION['cliente_data_puyasess']);
				}
				if(isset($_SESSION['cliente_data_puyasess'])){
					session_unset($_SESSION['cliente_data_puyasess']);
				}
				
				$sesionadmin = false;
				if(isset($_SESSION['admin_ecom_data']) &&
			    	$_SESSION['admin_ecom_data']['logeado']){
			    	$adminActual = array(
			         'admin_id' => $_SESSION['admin_ecom_data']['admin_id'],
			         'admin_email' => $_SESSION['admin_ecom_data']['admin_email'],
			         'logeado' => true
			       );
					$sesionadmin = true;

				}

				session_destroy();
				session_start();
				session_regenerate_id(true);

				if($sesionadmin){
			    	$this->session->set_userdata(array("admin_ecom_data"=>$adminActual));
				}
				redirect('', 'refresh'); 
				//redirect('login', 'refresh');
			}
		}else{
			redirect('home/registro', 'refresh');
		}	
	}

	public function guardarDataCliente(){
		if ($this->input->is_ajax_request()&&isset($_SESSION['admin_ecom_data']['logeado'])) {
			$arrayCliente = array('nombre'=>$this->input->post("nombre_cliente"),
							'apellido'=>$this->input->post("apellido_cliente"),
							'rut'=>$this->input->post("rut_cliente"),
							'telefono'=>$this->input->post("telefono_cliente"),
							'email'=>$this->input->post("email_cliente"),
							'password'=>$this->input->post("password_cliente"),
							'direccion'=>$this->input->post("direccion_cliente"),
							'id_region'=>$this->input->post("region_cliente"),
							'id_provincia'=>$this->input->post("provincia_cliente"),
							'id_comuna'=>$this->input->post("comuna_cliente"),
							'edad'=>$this->input->post("edad_cliente"),
							'puntos'=>$this->input->post("puntos_cliente"),
							'sexo'=>$this->input->post("sexo_cliente"));			
			$existeMail = $this->Cliente_model->existe_email($arrayCliente['email']);
			if(!$existeMail){
				$respuesta=$this->Cliente_model->inserta_cliente($arrayCliente);
				if($respuesta['estado']){
					$arrayCarro = array('codigo'=>$arrayCliente['rut'].$respuesta['user'],
							'id_cliente'=>$respuesta['user']);
					$this->Carro_compra_model->inserta_carro_compra($arrayCarro);
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el mail ".$arrayCliente['email']." ya existe";
			}
		}else{
			show_404();
		}		
	}

	public function guardarDataRegistro(){
		if ($this->input->is_ajax_request()) {
			$arrayCliente = array('nombre'=>$this->input->post("nombre_cliente"),
							'apellido'=>$this->input->post("apellido_cliente"),
							'rut'=>$this->input->post("rut_cliente"),
							'telefono'=>$this->input->post("telefono_cliente"),
							'email'=>$this->input->post("email_cliente"),
							'password'=>$this->input->post("password_cliente"),
							'direccion'=>$this->input->post("direccion_cliente"),
							'id_region'=>$this->input->post("region_cliente"),
							'id_provincia'=>$this->input->post("provincia_cliente"),
							'id_comuna'=>$this->input->post("comuna_cliente"),
							'edad'=>$this->input->post("edad_cliente"),
							'puntos'=>0,
							'sexo'=>$this->input->post("sexo_cliente"));			
			$existeMail = $this->Cliente_model->existe_email($arrayCliente['email']);
			if(!$existeMail){
				$respuesta=$this->Cliente_model->inserta_cliente($arrayCliente);
				if($respuesta['estado']){
					$arrayCarro = array('codigo'=>$arrayCliente['rut'].$respuesta['user'],
							'id_cliente'=>$respuesta['user']);
					$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

					//$this->load->library('email');
					$config['protocol'] = 'sendmail';
					$config['mailpath'] = '/usr/sbin/sendmail';
					$config['charset'] = 'iso-8859-1';
					$config['wordwrap'] = TRUE;
					$config['mailtype'] = 'html';
					
					$this->email->initialize($config);

					$this->email->from('no-replay@puyamuebles.cl', 'Info puyamuebles');
					$this->email->to($this->input->post("email_cliente"));

					$this->email->subject('Bienvenido a Puya');
					$this->email->message('gracias por registrarte en Puya bienvenido a un nuevo estilo en diseño e innovacion de muebles, visita nuestro sitio y sorprendete con los bellos productos que tenemos para ofrecerte. <a href="https://www.puyamuebles.cl" title="">www.puyamuebles.cl</a>');
					$this->email->set_header('Header1', 'Value1');
					//$this->email->set_alt_message('This is the alternative message');
					$this->email->send();
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: el mail ".$arrayCliente['email']." ya existe";
			}
		}else{
			show_404();
		}		
	}

	public function editDataCliente($user = null){
		if ($this->input->is_ajax_request()&&isset($_SESSION['admin_ecom_data']['logeado'])) {
			$arrayCliente = array('id'=>$this->input->post("id_cliente"),
							'nombre'=>$this->input->post("nombre_cliente"),
							'apellido'=>$this->input->post("apellido_cliente"),
							'rut'=>$this->input->post("rut_cliente"),
							'telefono'=>$this->input->post("telefono_cliente"),
							'email'=>$this->input->post("email_cliente"),
							'direccion'=>$this->input->post("direccion_cliente"),
							'id_region'=>$this->input->post("region_cliente"),
							'id_provincia'=>$this->input->post("provincia_cliente"),
							'id_comuna'=>$this->input->post("comuna_cliente"),
							'edad'=>$this->input->post("edad_cliente"),
							'puntos'=>$this->input->post("puntos_cliente"),
							'sexo'=>$this->input->post("sexo_cliente"));

			if($this->Cliente_model->edita_cliente($arrayCliente)){
				echo "editado con exito";
			}else{
				echo "Error al editar Cliente,el mail ".$arrayCliente['email']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function editDataPerfil($user = null){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCliente = array('id'=>$this->input->post("id_cliente"),
							'nombre'=>$this->input->post("nombre_cliente"),
							'apellido'=>$this->input->post("apellido_cliente"),
							'rut'=>$this->input->post("rut_cliente"),
							'telefono'=>$this->input->post("telefono_cliente"),
							'email'=>$this->input->post("email_cliente"),
							'edad'=>$this->input->post("edad_cliente"),
							'sexo'=>$this->input->post("sexo_cliente"),
							'direccion'=>$this->input->post("direccion_cliente"),
							'id_region'=>$this->input->post("region_cliente"),
							'id_provincia'=>$this->input->post("provincia_cliente"),
							'id_comuna'=>$this->input->post("comuna_cliente"));

			if($this->Cliente_model->edita_cliente_perfil($arrayCliente)){
				$respuesta = array('estado' => true,'respuesta'=>'editado con exito');
				echo json_encode($respuesta);
			}else{
				$respuesta = array('estado' => false,'respuesta'=>'No se realizaron cambios');
				echo json_encode($respuesta);
			}
		}else{
			show_404();
		}
	}

	public function editDataPerfilDireccion($user = null){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCliente = array('id'=>$this->input->post("id_cliente"),
							'direccion'=>$this->input->post("direccion_cliente"),
							'id_region'=>$this->input->post("region_cliente"),
							'id_provincia'=>$this->input->post("provincia_cliente"),
							'id_comuna'=>$this->input->post("comuna_cliente"));

			if($this->Cliente_model->edita_cliente_perfil_direccion($arrayCliente)){
				$respuesta = array('estado' => true,'respuesta'=>'editado con exito');
				echo json_encode($respuesta);
			}else{
				$respuesta = array('estado' => false,'respuesta'=>'No se realizaron cambios');
				echo json_encode($respuesta);
			}
		}else{
			show_404();
		}
	}

	public function editDataPerfilFacturacion($user = null){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCliente = array('id'=>$this->input->post("id_cliente"),
							'rut_fac'=>$this->input->post("rut_facturacion"),
							'nombre_fac'=>$this->input->post("nombre_facturacion"),
							'giro_fac'=>$this->input->post("giro_facturacion"),
							'telefono_fac'=>$this->input->post("telefono_facturacion"),
							'direccion_fac'=>$this->input->post("direccion_facturacion"),
							'region_id_fac'=>$this->input->post("region_facturacion"),
							'provincia_id_fac'=>$this->input->post("provincia_facturacion"),
							'comuna_id_fac'=>$this->input->post("comuna_facturacion"));

			if($this->Cliente_model->edita_cliente_perfil_facturacion($arrayCliente)){
				$respuesta = array('estado' => true,'respuesta'=>'editado con exito');
				echo json_encode($respuesta);
			}else{
				$respuesta = array('estado' => false,'respuesta'=>'No se realizaron cambios');
				echo json_encode($respuesta);
			}
		}else{
			show_404();
		}
	}

	public function editDataPerfilPassword($user = null){
		if ($this->input->is_ajax_request() && isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCliente = array('id'=>$this->input->post("id_cliente"),
							'email'=>$this->input->post("mail_cliente"),
							'password'=>$this->input->post("nueva_pass_cliente"));

			if($this->Cliente_model->edita_cliente_perfil_pass($arrayCliente)){
				$respuesta = array('estado' => true,'respuesta'=>'contraseña actualizada con exito');
				echo json_encode($respuesta);
			}else{
				$respuesta = array('estado' => false,'respuesta'=>'No se cambió la contraseña');
				echo json_encode($respuesta);
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataCliente($id = null){
		if ($this->input->is_ajax_request()&&isset($_SESSION['admin_ecom_data']['logeado'])) {
			if($this->Cliente_model->elimina_cliente($this->input->post("id_cliente"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar al Cliente";
			}
		}else{
			show_404();
		}
	}

	public function getMetodosPago(){
		if ($this->input->is_ajax_request()&&
			isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCarro;
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}else{
				$arrayCarro = array('codigo'=>$_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id'],
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}
			
			$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);

			$arrayCarroMensaje = array('id'=>$arrayCarro['id'],
								'mensaje'=>$this->security->xss_clean($this->input->post("mensaje_venta")));
			
			$this->Carro_compra_model->edita_carro_compra($arrayCarroMensaje);

			/*codigo de descuento a usar*/
			$codigo_descuento = $this->Codigo_promocion_model->obtener_codigo_promocion_previa($this->security->xss_clean($this->input->post("cod_previo")));
			$codigoValido = $this->Codigo_promocion_model->codigo_valido($this->security->xss_clean($codigo_descuento['id']),$this->security->xss_clean($_SESSION['cliente_data_puyasess']['user_id']));
			$valorDescuentoCodigo = 0;

			/*fin*/

			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
			$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayUsuario['id_comuna'])['precio'];
			$promoActiva = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
			
			$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);
			$totalVentaProd = 0;
			$descuentoPromo = 0;
			if ($productos_carro) {
				foreach ($productos_carro as $productoItem) {
					$precioVenta = $productoItem['asoc_val']-( $productoItem['asoc_val']*($productoItem['asoc_val_desc']/100));

					if(!empty($promoActiva)):
						if (($promoActiva['producto_id']>0 && $productoItem['id']==$promoActiva['producto_id'])||
						  ($promoActiva['categoria_id']>0 && $productoItem['categoria_id']==$promoActiva['categoria_id'])||
						  ($promoActiva['marca_id']>0 && $productoItem['marca_id']==$promoActiva['marca_id'])||
						  ($promoActiva['sub_categoria_id']>0 && $productoItem['sub_categoria_id']==$promoActiva['sub_categoria_id'])) {
						  $descuentoPromo += $precioVenta*($promoActiva['descuento']/100);
						}
					endif;
					if (!empty($codigo_descuento)) {
						if (($codigo_descuento['producto_id']>0 && 
							$productoItem['id']==$codigo_descuento['producto_id'])||
						  ($codigo_descuento['categoria_id']>0 && 
						  	$productoItem['categoria_id']==$codigo_descuento['categoria_id'])||
						  ($codigo_descuento['marca_id']>0 && 
						  	$productoItem['marca_id']==$codigo_descuento['marca_id'])||
						  ($codigo_descuento['sub_categoria_id']>0 && 
						  	$productoItem['sub_categoria_id']==$codigo_descuento['sub_categoria_id'])) {
						  $valorDescuentoCodigo += $productoItem['asoc_val']*($codigo_descuento['descuento']/100);
						}
					}
					$precioVenta *= $productoItem['cantidad'];
					
					$totalVentaProd += $precioVenta;
				}
			}

			//$sample_baseurl = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

			/** Creacion Objeto Webpay */

			$tx_step = "Init";

	        // Monto de la transacción (venta de productos + envio - codigo de descuentos)
	        //falta agregar el descuento realizado por el codigo de promocion
			if ($costoEnvio == null || $costoEnvio < 0 || $arrayCarro['retiro_tienda'] == 1) {
				$costoEnvio = 0;
			}

			if (!empty($codigo_descuento) && $codigoValido) {
				$this->session->set_tempdata('codigo_descuento_activo', $codigo_descuento['codigo'], 300);
			 	if ($valorDescuentoCodigo>0) {
			 		$valorDescuentoCodigo = intVal($valorDescuentoCodigo);
		     	}else{
		        	$valorDescuentoCodigo = intVal((($totalVentaProd+$costoEnvio)-$descuentoPromo)*($codigo_descuento['descuento']/100));
		        }
			}else{
				$this->session->unset_tempdata('codigo_descuento_activo');
				$valorDescuentoCodigo = 0;
			}
	        //$this->load->library('Webpay','Configuration');

	        $amount = ($totalVentaProd+$costoEnvio)-$descuentoPromo-$valorDescuentoCodigo;

	        // Orden de compra de la tienda
	        $buyOrder = strtotime(date('Y-m-d H:i:s'));

	        //Código comercio de la tienda entregado por Transbank
	        /*$sessionId = $this->configuration->getCommerceCode();
	        
	        // URL de retorno
	        $urlReturn = $sample_baseurl."/venta/getResult";
	        
	        // URL Final
			$urlFinal  = $sample_baseurl."/venta/end";

	        $request = array(
	            "amount"    => $amount,
	            "buyOrder"  => $buyOrder,
	            "sessionId" => $sessionId,
	            "urlReturn" => $urlReturn,
	            "urlFinal"  => $urlFinal,
	        );
	        // Iniciamos Transaccion
	        $result = $this->webpay->getNormalTransaction()->initTransaction($amount, $buyOrder, $sessionId, $urlReturn, $urlFinal);
	        
	        // Verificamos respuesta de inicio en webpay 
	        if (!empty($result->token) && isset($result->token)) {
	            $message = "Sesion iniciada con exito en Webpay";
	            $token = $result->token;
	            $next_page = $result->url;
	        } else {
	            $message = "webpay no disponible";
	        }
	        */
	        $orden_compra = strtotime(date('Y-m-d H:i:s'));

			$monto = $amount;
			$concepto = "Pago de Orden ".$buyOrder;
			$email_pagador = $_SESSION['cliente_data_puyasess']['user_email'];

			try {
				// Genera una nueva Orden de Pago, Flow la firma y retorna un paquete de datos firmados
				$flow_pack = $this->flowapi->new_order($orden_compra, $monto, $concepto, $email_pagador);
				// Si desea enviar el medio de pago usar la siguiente línea
				//$flow_pack = $flowAPI->new_order($orden_compra, $monto, $concepto, $email_pagador, $medioPago);
				
			} catch (Exception $e) {
				header('location: errorf.php');
			}

			$panelPago='<div class="payment-method  pl-20">
	              <h4 class="title-1 title-border text-uppercase mb-30">Medios de Pago</h4>
	              <div class="payment-accordion">
	                <!-- Accordion start  -->
	                <h3 class="payment-accordion-toggle active">Webpay - Transbank</h3>
	                <div class="row">
	                  <div class="col-xs-6">
						<!--Formulario HTML que envía la Nueva Orden -->
							<form method="post" action="'.$this->flowapi->flow_url_pago.'">
							  <input type="hidden" name="parameters" value="'.$flow_pack.'" />
							  <input type="image" class="img-responsive" src="'.base_url('assets/img/webpay.png').'" alt="Submit Form" />
							</form>
	                  </div>                               
	                </div> 
	                <!-- Accordion end -->
	                <!-- Accordion start -->
	                <h3 class="payment-accordion-toggle">Transferencia Electronica</h3>
	                <div class="payment-content">
	                  <p>Datos de transferencia</p>
	                  <form id="form_venta_efectivo">
	            <br>
	            orden de compra:'.$buyOrder.'<input type="hidden" value="'.$arrayCarro['id'].'" name="id_carro">
	            <br>
	            precio del total de productos= $ '.number_format(ceil($totalVentaProd) ,0, "," ,".").'
	            <input type="hidden" value="'.$totalVentaProd.'" name="costo_productos">
	            <input type="hidden" value="'.$buyOrder.'" name="numero_orden">
	            <input type="hidden" value="'.$arrayUsuario['id_comuna'].'" name="comuna_venta">';
	            if (!empty($codigo_descuento) && $codigoValido) {
					$panelPago.='<input type="hidden" value="'.$codigo_descuento['codigo'].'" name="codigo_descuento">';
				}else{
					$panelPago.='<input type="hidden" value="" name="codigo_descuento">';
				}
	           $panelPago.='<input type="hidden" value="'.$costoEnvio.'" name="costo_envio">
			            <br>
			            precio envio: $'.number_format(ceil($costoEnvio) ,0, "," ,".").'
			            <br>
			            <input type="hidden" value="0" name="metodo_pago">
			            <br>
			            total de esta venta $'.number_format(ceil($amount) ,0, "," ,".").'
			            <br>
			            <table border="1">
							<tr>
								<td>Banco</td>
								<td>nombre banco</td>
							</tr>
							<tr>
								<td>Tipo de Cuenta</td>
								<td>colocar tipo cuenta</td>
							</tr>
							<tr>
								<td>Numero de cuenta</td>
								<td>000000000</td>
							</tr>
							<tr>
								<td>Titular</td>
								<td>Puya Muebles</td>
							</tr>
							<tr>
								<td>Rut</td>
								<td>11.111.111-1</td>
							</tr>
							<tr>
								<td>correo</td>
								<td>contacto@puyamuebles.cl</td>
							</tr>
							<tr>
								<td>Asunto</td>
								<td>Codigo Venta '.$buyOrder.'</td>
							</tr>

							<tr>
								<td>monto</td>
								<td>$ '.number_format(ceil($amount) ,0, "," ,".").'</td>
							</tr>
						</table>
						<br>
			            <button class="btn btn-success realiza_venta" type="button" id="realiza_venta">Comprar mediante transferencia</button>
			          </form>
	                </div>
	                <!-- Accordion end -->
	              </div>
	              <script>/*-------------------------
	accordion toggle function
--------------------------*/
	$(".payment-accordion").find(".payment-accordion-toggle").on("click", function(){
		//Expand or collapse this panel
		$(this).next().slideToggle(500);
		//Hide the other panels
		$(".payment-content").not($(this).next()).slideUp(500);
	});
/* -------------------------------------------------------
	accordion active class for style
----------------------------------------------------------*/
	$(".payment-accordion-toggle").on("click", function(event) {
		$(this).siblings(".active").removeClass("active");
		$(this).addClass("active");
		event.preventDefault();
	}); 
	</script>';

			echo json_encode($panelPago);

		}else{
			show_404();
		}
	}

	public function usaDireccionCliente(){
		if (isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
		}else{
			redirect('home/registro', 'refresh'); 
		}
	}

	public function llamar_productos_resumen(){
		if ($this->input->is_ajax_request() && 
			isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCarro;
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}else{
				$arrayCarro = array('codigo'=>$_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id'],
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}
			
			$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);
			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
					
			$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayCarro['comuna_carro'])['precio'];
			if ($costoEnvio == null || $costoEnvio < 0 || $arrayCarro['retiro_tienda'] == 1) {
				$costoEnvio = 0;
			}
					
			$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);

			$arrayCarro['costo_envio'] = $costoEnvio;

			$resumenData = array('carro_data'=>$arrayCarro,
								 'productos_data'=>$productos_carro);

			echo json_encode($resumenData);
		}else{
			show_404();
		}
		;
	}

	public function carro_compra(){
		if (isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCarro;


	  	
			if ($this->Carro_compra_model->existe_carro_compra($_SESSION['cliente_data_puyasess']['user_id'])) {
				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}else{
				$arrayCarro = array('codigo'=>$_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id'],
					'id_cliente'=>$_SESSION['cliente_data_puyasess']['user_id']);
				$this->Carro_compra_model->inserta_carro_compra($arrayCarro);

				$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			}
			
			$arrayDataCarro = array('id-carro'=>$arrayCarro['id']);
			$arrayUsuario = $this->Cliente_model->get_cliente_full($_SESSION['cliente_data_puyasess']['user_id']);
			
			$costoEnvio = $this->Precio_envio_model->get_precio_envio_comuna($arrayUsuario['id_comuna'])['precio'];

			$dataRegion = $this->Region_model->obtener_lista_regiones();
			$dataProvincia = $this->Provincia_model->obtener_lista_provincias_region($arrayCarro['region_carro']);
			$dataComunas = $this->Comuna_model->obtener_lista_comunas_provincia($arrayCarro['provincia_carro']);
			

			$productos_carro = $this->Carro_compra_model->llamar_productos_carro($arrayDataCarro);

	        //validacion que se pudo iniciar la conexion con webpay

		  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
			
			$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'productos_carro'=>$productos_carro,
      						'lista_regiones'=>$dataRegion,
      						'lista_provincias'=>$dataProvincia,
      						'lista_comunas'=>$dataComunas,
      						'costo_envio'=>$costoEnvio,
      						'arrayCliente'=>$arrayUsuario,
      						'lista_categorias'=>$dataCategoria,
      						"carro"=>$arrayCarro);
			$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
			$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
			$dataPagina['promoActiva']=$dataPromo;
			$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
			$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
			$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
			$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
			
			$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

			$this->load->view('layout/head',$dataPagina);
			//el menu de navegacion se encuetra dentro del header
			$this->load->view('layout/header',$dataPagina);

			//en esta parte va el contenido personalizado
			$this->load->view('cliente/carro_compra');
			//en esta parte va el contenido personalizado
			$this->load->view('layout/footer', $dataPagina);


		}else{
			redirect('home/registro', 'refresh'); 
		}
	}

	public function pedidos(){
		if (isset($_SESSION['cliente_data_puyasess']['logeado'])) {
			$arrayCarro = $this->Carro_compra_model->get_carro_compra_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			$listaCarros = $this->Carro_compra_model->get_carros_compra_procesado_codigo($_SESSION['cliente_data_puyasess']['user_r'].$_SESSION['cliente_data_puyasess']['user_id']);
			
			$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						"carro"=>$arrayCarro,
      						"ventas"=>$this->Venta_model->get_venta_usuario($listaCarros));
		//cargo la vista pasando los datos de configuacion
			$this->load->view('layout/head',$dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('cliente/pedidos');
			//en esta parte va el contenido personalizado
			$this->load->view('layout/footer', $dataPagina);
		}else{
			redirect('home/registro', 'refresh'); 
		}
	}

	public function lost_password(){
		$this->load->library('Recaptcha');
		$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter",
      						'promoActiva'=>$dataPromo,
      						'lista_categorias'=>$dataCategoria);
		//pido los ultimos artículos al modelo
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_categorias'] = $dataCategoria;
		//pido los ultimos artículos al modelo
		$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		// Catch the user's answer
		$captcha_answer = $this->input->post('g-recaptcha-response');

		// Verify user's answer
		$response = $this->recaptcha->verifyResponse($captcha_answer);

		// Processing ...
		if ($response['success']) {
			$mailPerdido = $this->input->post('email');
			
			$existeMail = $this->Cliente_model->existe_email($mailPerdido);
			if($existeMail){

				//$this->load->library('email');
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				
				$this->email->initialize($config);

				$this->email->from('no-replay@puyamuebles.cl', 'Info puyamuebles');
				$this->email->to($mailPerdido);

				$this->email->subject('Recuperacion de contrase&#241;a puyamuebles.cl');
				$this->email->message('se a solicitado un cambio de contrase&#241;a para su cuenta, se le ha asignado una contrase&#241;a temporal para su nuevo ingreso <br>'.$this->Cliente_model->olvida_contrasenia(array("email"=>$mailPerdido))['timepass'].'<br>por favor utilice esta contrase&#241;a para su siguiente ingreso');
				$this->email->set_header('Header1', 'Value1');
				//$this->email->set_alt_message('This is the alternative message');
				if ($this->email->send())
				{
					$dataPagina['mensaje'] = "se ha enviado una contraseña temporal a su mail";
				}
			}else{
				$dataPagina['mensaje'] = "el mail ingresado no se encuetra registrado en nuestro sitio";
			}
		}

		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);

		//en esta parte va el contenido personalizado
		$this->load->view('cliente/recupera_password');
		//en esta parte va el contenido personalizado
		$this->load->view('layout/footer', $dataPagina);
	}
}
