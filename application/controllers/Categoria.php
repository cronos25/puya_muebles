<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      	//cargo el modelo de usuarios
            $this->load->model('Categoria_model');
            $this->load->model('Marca_model');
            
            $this->load->model('Producto_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Promocion_temporal_model');
            /*$this->load->model('especificacion/Especificacion_model');
            */
            $this->load->library('upload');
            $this->load->library('Comprime');
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }
	public function index(){
		show_404();
	}

	public function getDataListCategorias(){
		if ($this->input->is_ajax_request()) {
			if($this->input->post("dfb") != null){
				if($this->input->post("catevalue")!= null){
					echo json_encode(array("estado"=>0,
						 "categorias"=>$this->Categoria_model->get_productos_filtrados_categoria($this->input->post("catevalue"),$this->input->post("dfb"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data filtro"));
				}
			}else{
				if($this->input->post("catevalue")!= null){
					echo json_encode(array("estado"=>0,
					 "categorias"=>$this->Categoria_model->get_productos_categoria($this->input->post("catevalue"))));
				}else{
					echo json_encode(array("estado"=>1,
						"mensaje"=>"error en el llamado de la data normal"));
				}
			}
		}else{
			show_404();
		}
	}

	public function view($dataView=null,$nombre=null){
		if(!is_null($dataView)){

				$dataPagina = array('titulo'=>"Puya Muebles",
  						'nombre_app'=>"Prueba CODEIGNITER",
  						'descripcion'=>"Layout hecho en codeigniter");
				//pido al modelo el artículo que se desea ver
				$arrayCategoria = $this->Categoria_model->get_categoria($dataView);

				//compruebo si he recibido un artículo
				if (!$arrayCategoria){
				 //no he recibido ningún artículo
				 //voy a lanzar un error 404 de página no encontrada
					echo "no existe";
				 show_404();
				}else{
					 //he encontrado el artículo
					 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
					
					//pido los ultimos artículos al modelo
				  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_categorias'] = $dataCategoria;
					//pido los ultimos artículos al modelo
				  	$dataMarca = $this->Marca_model->get_total_prods_marcas_categoria($dataView);
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_marcas'] = $dataMarca;
					$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
					$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;

	  				$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
					$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
					$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
					$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
	  				
	      			$ultimosArticulos = $this->Categoria_model->get_productos_categoria($dataView);
	      			$dataPagina['lista_productos']=$ultimosArticulos;
	      			/*$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
					$dataPagina['promoActiva']=$dataPromo;
					*/
					$dataPagina['arrayCategoria']=$arrayCategoria;
					//$dataPagina["ultimoSubido"] = $this->Producto_model->get_productos_nuevos()[0];
					 //cargo la vista pasando los datos de configuacion
					$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

					$this->load->view('layout/head',$dataPagina);
					//el menu de navegacion se encuetra dentro del header
					$this->load->view('layout/header',$dataPagina);
					//en esta parte va el contenido personalizado
					$this->load->view('categorias', $dataPagina);		
					$this->load->view('layout/footer', $dataPagina);
				}
		}else{
			show_404();
		}	
	}

	private function obtenerproductosfiltrados($categoria = null ,$marcas = array()){
		if(!is_null($categoria)){
			if(!empty($marcas)){

			}else{
				return $this->Categoria_model->get_productos_categoria($categoria);
			}
		}
	}

	public function guardarDataCategory(){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/category/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	 		
	 		if(!file_exists("./assets/uploads/category/")){
	          if(!mkdir("./assets/uploads/category", 0777, true)) {
	            die('Fallo al crear las carpetas...');
	          }
	        }

	        if (isset($_FILES['imagen_categoria']['name'])) {
	            if (0 < $_FILES['imagen_categoria']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_categoria']['error'];
	            } else {
	                if (file_exists('./assets/uploads/category/' . $_FILES['imagen_categoria']['name'])) {
	                    echo 'File already exists : assets/uploads/' . $_FILES['imagen_categoria']['name'];           } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_categoria')){
	                        echo "error subiendo la imagen ";
	                        echo $this->upload->display_errors();
	                    } else {
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
	                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
                    		$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/category/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        } else {
	            echo 'Please choose a file';
	        }

			$arrayCategory = array('nombre'=>$this->input->post("nombre_categoria"),
							'descripcion'=>$this->input->post("descripcion_categoria"),
							"tipo_categoria"=>$this->input->post("tipo_categoria"),
							'imagen'=>$url_final);
			$existeCategoria = $this->Categoria_model->existe_categoria($arrayCategory['nombre']);
			if(!$existeCategoria){
				if($this->Categoria_model->inserta_categoria($arrayCategory)){
					echo "Registrado con éxito";
				}else{
					echo "Error al registrar";
				}
			}else{
				echo "Error: la categoria ".$arrayCategory['nombre']." ya existe";
			}
		}else{
			show_404();
		}
	}

	public function editDataCategory($user = null){
		if ($this->input->is_ajax_request()) {
			//upload file
			$url_final = "";
	        $config['upload_path'] = './assets/uploads/category/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '200';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '2048'; //2 MB
	        if (isset($_FILES['imagen_categoria']['name'])) {
	            if (0 < $_FILES['imagen_categoria']['error']) {
	                echo 'Error during file upload' . $_FILES['imagen_categoria']['error'];
	            } else {
	                if (file_exists('./assets/uploads/category/' . $_FILES['imagen_categoria']['name'])) {
	                    echo 'File already exists : assets/uploads/category/' . $_FILES['imagen_categoria']['name'];           } else {
	                    $this->upload->initialize($config);
	                    if (!$this->upload->do_upload('imagen_categoria')){
	                        echo $this->upload->display_errors();
	                    } else {
	                    	$file_data = $this->upload->data();
	                    	rename($file_data['full_path'],$file_data['file_path'].$file_data['file_name'].'.jpg');
	                    	$archivo = str_replace(base_url(), './', $file_data['full_path'].'.jpg');
                    		$this->comprime->comprime_image($archivo, 70);
			    			$url_final = base_url().'assets/uploads/category/'.$file_data['file_name'].'.jpg';
	                    }
	                }
	            }
	        }

			$arrayCategory = array("id"=>$this->input->post("id_categoria"),
							"nombre"=>$this->input->post("nombre_categoria"),
							"tipo_categoria"=>$this->input->post("tipo_categoria"),
							"descripcion"=>$this->input->post("descripcion_categoria"));
			if ($url_final != ""){
				$actual_imagen = str_replace(base_url(), './', $this->Categoria_model->actual_imagen($arrayCategory['id']));
				if(file_exists($actual_imagen['imagen'])){
					try {
						unlink($actual_imagen['imagen']);
					} catch (Exception $e) {
					}
		        }
				$arrayCategory["imagen"]=$url_final;
			}

			if($this->Categoria_model->edita_categoria($arrayCategory)){
				echo "Registrado con éxito";
			}else{
				echo "no se realizaron cambios";
			}
		}else{
			show_404();
		}
	}

	public function eliminaDataCategory($id = null){
		if ($this->input->is_ajax_request()) {
			$actual_imagen = str_replace(
					base_url(), 
					'./', 
					$this->Categoria_model->actual_imagen($this->input->post("id_categoria"))['imagen']
					);
			if(file_exists($actual_imagen)){
				try {
					unlink($actual_imagen);
				} catch (Exception $e) {
				}
	        }
			
			/*$this->Especificacion_model->elimina_especificacion_categoria($this->input->post("id_categoria"));*/
			if($this->Categoria_model->elimina_categoria($this->input->post("id_categoria"))){
				echo "eliminado con exito";
			}else{
				echo "Error al eliminar la CAtegoria";
			}
		}else{
			show_404();
		}
	}
}
