<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
		//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
            $this->load->model('Producto_model');
            $this->load->model('Categoria_model');
            $this->load->model('Marca_model');
            $this->load->model('Sub_categoria_model');
            $this->load->model('Banner_home_model');
            $this->load->model('Promocion_temporal_model');
            $this->load->model('Oferta_especial_model');
    }

	public function index()
	{
		//pido los ultimos artículos al modelo
	  	$dataProducto = $this->Producto_model->obtener_lista_productos_todo();
	  	$dataProductoDestacado = $this->Producto_model->get_productos_destacados_home();
	  	$dataProductosOrdenados = $this->Producto_model->get_productos_nuevos_orden();
	  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
	  	//pido los ultimos artículos al modelo
	  	$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
		
	  	$dataMarca = $this->Marca_model->obtener_lista_marcas();

	  	$dataBanners = $this->Banner_home_model->obtener_lista_banners();
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_banners'] = $dataBanners;
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_productos'] = $dataProducto;
		$dataPagina['lista_productos_destacados'] = $dataProductoDestacado;
		$dataPagina['lista_productos_ordenados'] = $dataProductosOrdenados;
		//creo el array con datos de configuración para la vista
		$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
		$dataPagina['lista_categorias'] = $dataCategoria;
		$dataPagina['lista_marcas'] = $dataMarca;
	  	$dataPromo = $this->Promocion_temporal_model->obtener_lista_promocion_activo();
		$dataPagina['promoActiva']=$dataPromo;
		$dataOferta_especial = $this->Oferta_especial_model->oferta_especial_actual();
		$dataPagina['ofertaEspecial']=$dataOferta_especial;
		$dataPagina['vista_nav']=$this->load->view('layout/nav',$dataPagina);

		$this->load->view('layout/head',$dataPagina);
		//el menu de navegacion se encuetra dentro del header
		$this->load->view('layout/header',$dataPagina);
		$this->load->view('home',$dataPagina);
		$this->load->view('layout/footer');
	}
}
