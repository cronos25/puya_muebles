<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller intdiv(dividend, divisor)
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
            parent::__construct();
      		//cargo el modelo de usuarios
	            $this->load->model('Usuario_model');
	            $this->load->model('Cliente_model');
	            $this->load->model('Region_model');
	            $this->load->model('Provincia_model');
	            $this->load->model('Comuna_model');
	            $this->load->model('Precio_envio_model');
	            $this->load->model('Categoria_model');
	            $this->load->model('Servicio_model');
	            $this->load->model('Sub_categoria_model');
	            $this->load->model('Marca_model');
	            $this->load->model('Color_model');
	            $this->load->model('Producto_model');
	            $this->load->model('Stack_model');
	            $this->load->model('Producto_stack_model');
	            $this->load->model('Newsletter_model');
	            $this->load->model('Carro_compra_model');
	            $this->load->model('Venta_model');
	            $this->load->model('Codigo_promocion_model');
	            $this->load->model('Promocion_temporal_model');
	            $this->load->model('Banner_home_model');
	            $this->load->model('Distribuidor_model');
	            $this->load->model('Oferta_especial_model');
			//cargo el helper de url, con funciones para trabajo con URL del sitio
            $this->load->helper('html','url_helper');
    }

	public function index(){
		$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter");
		//Go to private area
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
			//cargo la vista pasando los datos de configuacion
			//pido los ultimos artículos al modelo
			$dataVentas = $this->Venta_model->obtener_lista_ventas();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_ventas'] = $dataVentas;
			$ListaDestacados = $this->Producto_model->get_productos_destacados_home();
			$dataPagina['lista_destacados'] = $ListaDestacados;
			$dataCliente = $this->Cliente_model->obtener_clientes_nuevo_a_viejo();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_clientes'] = $dataCliente;

			$dataProdutosPopulares = $this->Producto_model->get_productos_populares();
			//creo el array con datos de configuración para la vista
			$dataPagina['lista_populares'] = $dataProdutosPopulares;
			

			$this->load->view('layout/admin/head',$dataPagina);
			$this->load->view('layout/admin/nav', $dataPagina);
			$this->load->view('layout/admin/header', $dataPagina);
			//contiene la barra de la izquierda
			$this->load->view('layout/admin/content', $dataPagina);
			
			//en esta parte va el contenido personalizado
			$this->load->view('admin/index',$dataPagina);

			//en esta parte va el contenido personalizado

			$this->load->view('layout/admin/footer', $dataPagina);
		}else{
			redirect(base_url('admin/login'), 'refresh'); 
		}
	}

	public function login(){
		if(!isset($_SESSION['admin_ecom_data']['logeado'])){

			if(null !== $this->input->post('password') && null !== $this->input->post('username')){
				$this->abrirIniciarSesion($this->input->post('username'),$this->input->post('password'));
			}

	     	$dataPagina = array('titulo'=>"Puya Muebles",
      						'nombre_app'=>"Prueba CODEIGNITER",
      						'descripcion'=>"Layout hecho en codeigniter");
		//cargo la vista pasando los datos de configuacion
			$this->load->view('layout/admin/head',$dataPagina);
			$this->load->view('layout/admin/nav_login', $dataPagina);
			//en esta parte va el contenido personalizado
			$this->load->view('admin/login');
			//en esta parte va el contenido personalizado
			$this->load->view('layout/admin/footer', $dataPagina);

		}else{
			redirect('admin/index', 'refresh'); 
		}	
	}

	public function abrirIniciarSesion($user, $pass){
		/*if ($this->input->is_ajax_request()) {
			$user= $this->input->post("username");
			$pass = $this->input->post("password");*/
			$dataSession = $this->Usuario_model->login($user, $pass);

			if($dataSession){
				if(isset($_SESSION['admin_ecom_data'])){
					$this->session->unset_userdata('admin_ecom_data');
				}
				if(isset($_SESSION['admin_ecom_data'])){
					unset($_SESSION['admin_ecom_data']);
				}
				if(isset($_SESSION['admin_ecom_data'])){
					session_unset($_SESSION['admin_ecom_data']);
				}
				
				$sesionusuario = false;
				if(isset($_SESSION['cliente_data_puyasess']) &&
			    	$_SESSION['cliente_data_puyasess']['logeado']){
			    	$clienteActual = array(
					 'user_id' => $_SESSION['cliente_data_puyasess']['user_id'],
					 'user_email' => $_SESSION['cliente_data_puyasess']['user_email'],
					 'user_nombre' => $_SESSION['cliente_data_puyasess']['user_nombre'],
					 'user_r' => $_SESSION['cliente_data_puyasess']['user_r'],
					 'user_cantidad_prod_carro' => $_SESSION['cliente_data_puyasess']['user_cantidad_prod_carro'],
					 'logeado' => true
					);
					$sesionusuario = true;

				}

				session_destroy();
				session_start();
				session_regenerate_id(true);

				if($sesionusuario){
			    	$this->session->set_userdata(array("cliente_data_puyasess"=>$clienteActual));
				}

				$adminActual = array();
				foreach($dataSession as $row)
			     {
			       $adminActual = array(
			         'admin_id' => $row->id,
			         'admin_email' => $row->email,
			         'logeado' => true
			       );
			     }

				$this->session->set_userdata(array("admin_ecom_data"=>$adminActual));
				//redirect('admin/index', 'refresh'); 
				//echo json_encode(array("link"=>base_url().'admin/index',"mensaje"=>"Bienvenido"));
				redirect('admin/index', 'refresh'); 
			}else{
				redirect('admin/login#badPassword');
			}	
		/*}else{
			show_404();
		}	*/
	}

	public function cerrarSession(){
		/*if ($this->input->is_ajax_request()) {*/
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
			$cerrar = $this->input->post("cleardata");
			if($cerrar == 1){
				if(isset($_SESSION['admin_ecom_data'])){
					$this->session->unset_userdata('admin_ecom_data');
				}
				if(isset($_SESSION['admin_ecom_data'])){
					unset($_SESSION['admin_ecom_data']);
				}
				if(isset($_SESSION['admin_ecom_data'])){
					session_unset($_SESSION['admin_ecom_data']);
				}
				
				$sesionusuario = false;
				if(isset($_SESSION['cliente_data_puyasess']) &&
			    	$_SESSION['cliente_data_puyasess']['logeado']){
			    	$clienteActual = array(
					 'user_id' => $_SESSION['cliente_data_puyasess']['user_id'],
					 'user_email' => $_SESSION['cliente_data_puyasess']['user_email'],
					 'user_nombre' => $_SESSION['cliente_data_puyasess']['user_nombre'],
					 'user_r' => $_SESSION['cliente_data_puyasess']['user_r'],
					 'logeado' => true
					);
					$sesionusuario = true;

				}
				session_destroy();
				session_start();
				session_regenerate_id(true);

				if($sesionusuario){
			    	$this->session->set_userdata(array("cliente_data"=>$clienteActual));
				}
				redirect('', 'refresh'); 
				//redirect('login', 'refresh');
			}
		}
		/*}else{
			show_404();
		}	*/
	}

	public function user($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/user/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayUsuario = $this->Usuario_model->get_usuario($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayUsuario){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayUsuario'=>$arrayUsuario);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/user/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataUsuario = $this->Usuario_model->obtener_lista_usuarios();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_usuarios'=>$dataUsuario);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/user/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function cliente($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataRegion = $this->Region_model->obtener_lista_regiones();
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'lista_regiones'=>$dataRegion);
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/cliente/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCliente = $this->Cliente_model->get_cliente($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCliente){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							$dataRegion = $this->Region_model->obtener_lista_regiones();
							$dataProvincia = $this->Provincia_model->obtener_lista_provincias();
							$dataComunas = $this->Comuna_model->obtener_lista_comunas();
							$dataRegion = $this->Region_model->obtener_lista_regiones();
							$dataProvincia = $this->Provincia_model->obtener_lista_provincias_region($arrayCliente['id_region']);
							$dataComunas = $this->Comuna_model->obtener_lista_comunas_provincia($arrayCliente['id_provincia']);
							 $dataPagina = array('titulo'=> "Ecommerce",
		      						'nombre_app'=> "Prueba CODEIGNITER",
		      						'descripcion'=> "Layout hecho en codeigniter",
		      						'arrayCliente'=> $arrayCliente,
		      						'lista_regiones'=> $dataRegion,
	      							'lista_provincias'=> $dataProvincia,
	      							'lista_comunas'=> $dataComunas);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/cliente/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataCliente = $this->Cliente_model->obtener_lista_clientes();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_clientes'=>$dataCliente);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/cliente/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function region($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/region/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayRegion = $this->Region_model->get_region($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayRegion){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayRegion'=>$arrayRegion);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/region/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataRegion = $this->Region_model->obtener_lista_regiones();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_regiones'=>$dataRegion);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/region/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function provincia($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataRegion = $this->Region_model->obtener_lista_regiones();
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'lista_regiones'=>$dataRegion);
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/provincia/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayProvincia = $this->Provincia_model->get_provincia($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayProvincia){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataRegion = $this->Region_model->obtener_lista_regiones();
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayProvincia'=>$arrayProvincia,
		      						'lista_regiones'=>$dataRegion);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/provincia/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataProvincia = $this->Provincia_model->obtener_lista_provincias();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_provincias'=>$dataProvincia);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/provincia/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function comuna($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataProvincia = $this->Provincia_model->obtener_lista_provincias();
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'lista_provincias'=>$dataProvincia);
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/comuna/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayComuna = $this->Comuna_model->get_comuna($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayComuna){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataProvincia = $this->Provincia_model->obtener_lista_provincias();
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayComuna'=>$arrayComuna,
		      						'lista_provincias'=>$dataProvincia);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/comuna/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataComunas = $this->Comuna_model->obtener_lista_comunas();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_comunas'=>$dataComunas);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/comuna/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function categoria($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     				$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/categoria/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCategoria = $this->Categoria_model->get_categoria($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCategoria){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayCategoria']=$arrayCategoria;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/categoria/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				case 'servicio':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCategoria = $this->Categoria_model->get_categoria($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCategoria){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								$datosServicio = $this->Servicio_model->get_datos_servicio_categoria($arrayCategoria['id']);
								$ParrafosServicio = $this->Servicio_model->obtener_lista_parrafos_categoria($arrayCategoria['id']);

								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayCategoria']=$arrayCategoria;
								$dataPagina['datosServicio']=$datosServicio;
								$dataPagina['ParrafosServicio']=$ParrafosServicio;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/categoria/servicio', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_categorias'] = $dataCategoria;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/categoria/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function sub_categoria($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     				$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
	     				//pido los ultimos artículos al modelo
					  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_categorias'] = $dataCategoria;

						//pido los ultimos artículos al modelo
					  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_marcas'] = $dataMarca;
			switch ($vista) {
				case 'add':
						
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/sub_categoria/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arraySubCategoria = $this->Sub_categoria_model->get_sub_categoria($dataView);

							//compruebo si he recibido un artículo
							if (!$arraySubCategoria){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arraySubCategoria']=$arraySubCategoria;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/sub_categoria/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/sub_categoria/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function marca($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/marca/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayMarca = $this->Marca_model->get_marca($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayMarca){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayMarca']=$arrayMarca;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/marca/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_marcas'] = $dataMarca;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/marca/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}			
	}

	public function precio_envio($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");

			switch ($vista) {
				case 'add':
						$dataProvincia = $this->Provincia_model->obtener_lista_provincias();
				  		$dataComunas = $this->Comuna_model->obtener_lista_comunas();
						$dataRegion = $this->Region_model->obtener_lista_regiones();
						
						$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_provincias'=>$dataProvincia,
		      					'lista_regiones'=>$dataRegion,
	      						'lista_comunas'=>$dataComunas);
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/precio_envio/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayPrecioEnvio = $this->Precio_envio_model->get_precio_envio($dataView);
						$dataProvincia = $this->Provincia_model->obtener_lista_provincias();
				  		$dataComunas = $this->Comuna_model->obtener_lista_comunas();
						$dataRegion = $this->Region_model->obtener_lista_regiones();
							//compruebo si he recibido un artículo
							if (!$arrayPrecioEnvio){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_provincias'=>$dataProvincia,
		      					'lista_regiones'=>$dataRegion,
	      						'lista_comunas'=>$dataComunas,
	      						'arrayPrecioEnvio'=> $arrayPrecioEnvio);
								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/precio_envio/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataMarca = $this->Precio_envio_model->obtener_lista_precio_envios();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_precio_envios'] = $dataMarca;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/precio_envio/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}			
	}

	public function color($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/color/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayColor = $this->Color_model->get_color($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayColor){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayColor'=>$arrayColor);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/color/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataColor = $this->Color_model->obtener_lista_colores();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_colores'=>$dataColor);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/color/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function productos($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						//pido los ultimos artículos al modelo
					  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_categorias'] = $dataCategoria;

						//pido los ultimos artículos al modelo
					  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_marcas'] = $dataMarca;

						$dataColor = $this->Color_model->obtener_lista_colores();
						$dataPagina['lista_colores'] = $dataColor;
						//pido los ultimos artículos al modelo
					  	$ultimoId = $this->Producto_model->get_last_id();
						//creo el array con datos de configuración para la vista
						$dataPagina['ultimo_producto'] = $ultimoId+1;

						//cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/producto/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayProducto = $this->Producto_model->get_producto($dataView);

							//pido los ultimos artículos al modelo
						  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
							//creo el array con datos de configuración para la vista
							$dataPagina['lista_categorias'] = $dataCategoria;

							//pido los ultimos artículos al modelo
						  	$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias_categoria($arrayProducto['categoria_id']);
							//creo el array con datos de configuración para la vista
							$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

							//pido los ultimos artículos al modelo
						  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
							//creo el array con datos de configuración para la vista
							$dataPagina['lista_marcas'] = $dataMarca;

													
							//obtengo las imagenes del producto
							/*$imagenes_actuales = $this->Imagen_model->get_imagen_producto($arrayProducto['id']);
							
							$dataPagina['lista_imagenes'] = $imagenes_actuales;
							
							$arrayEspecificacion = $this->Especificacion_model->get_espec_producto_asignado($dataView);*/
							//compruebo si he recibido un artículo
							if (!$arrayProducto){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayProducto']=$arrayProducto;
								
							$dataColor = $this->Color_model->obtener_lista_colores();
							$dataPagina['lista_colores'] = $dataColor;

							$dataColorEle = $this->Color_model->get_total_prods_colores_producto($dataView);
							$dataPagina['lista_colores_elegidos'] = $dataColorEle;
								//$dataPagina['arrayEspecificacion'] = $arrayEspecificacion;
								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/producto/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataProducto = $this->Producto_model->obtener_lista_productos_todo();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_productos'] = $dataProducto;
					$ListaDestacados = $this->Producto_model->get_productos_destacados_home();
					$dataPagina['lista_destacados'] = $ListaDestacados;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/producto/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}	
	}

	public function stack($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						//pido los ultimos artículos al modelo
					  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_categorias'] = $dataCategoria;

						//pido los ultimos artículos al modelo
					  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_marcas'] = $dataMarca;

						$dataColor = $this->Color_model->obtener_lista_colores();
						$dataPagina['lista_colores'] = $dataColor;
						
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/stack/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayStack = $this->Stack_model->get_stack($dataView);
							
							//compruebo si he recibido un artículo
							if (!$arrayStack){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayStack']=$arrayStack;
								//pido los ultimos artículos al modelo
							  	$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
								//creo el array con datos de configuración para la vista
								$dataPagina['lista_categorias'] = $dataCategoria;

								//pido los ultimos artículos al modelo
							  	$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias_categoria($arrayStack['categoria_id']);
								//creo el array con datos de configuración para la vista
								$dataPagina['lista_sub_categorias'] = $dataSubCategoria;

								//pido los ultimos artículos al modelo
							  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
								//creo el array con datos de configuración para la vista
								$dataPagina['lista_marcas'] = $dataMarca;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/stack/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				case 'edit_prod':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayStack = $this->Stack_model->get_stack($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayStack){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayStack']=$arrayStack;
								$dataProducto = $this->Producto_model->obtener_lista_productos_todo();
								//creo el array con datos de configuración para la vista
								$dataPagina['lista_productos'] = $dataProducto;
								$dataStack = $this->Producto_stack_model->get_productos_stack($dataView);
								//creo el array con datos de configuración para la vista
								$dataPagina['lista_productos_stack'] = $dataStack;
								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/stack/edit_prod', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataStack = $this->Stack_model->obtener_lista_stacks();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_stacks'] = $dataStack;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/stack/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}			
	}

	public function producto_stack($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/producto_stack/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayStack = $this->Producto_stack_model->get_producto_stack($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayStack){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayStack']=$arrayStack;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/producto_stack/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataStack = $this->Producto_stack_model->get_total_prods_stacks();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_producto_stacks'] = $dataStack;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/producto_stack/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}			
	}

	public function newsletter($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/newsletter/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayNewsletter = $this->Newsletter_model->get_newsletter($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayNewsletter){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayNewsletter'=>$arrayNewsletter);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/newsletter/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataNewsletter = $this->Newsletter_model->obtener_lista_newsletteres();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'lista_newsletteres'=>$dataNewsletter);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/newsletter/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function carro_compra($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/carro_compra/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCarroCompra = $this->Carro_compra_model->get_carro_compra($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCarroCompra){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los 
								//datos del array del artículo
								$arrayClienteCarro = $this->Cliente_model->get_cliente($arrayCarroCompra['id_cliente']);

								$dataPagina['arrayCarroCompra']=$arrayCarroCompra;
								$dataPagina['arrayUser'] = $arrayClienteCarro;
								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/carro_compra/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				case 'pending':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCarroCompra = $this->Carro_compra_model->get_carro_compra($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCarroCompra){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayCarroCompra']=$arrayCarroCompra;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/carro_compra/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataCarroCompra = $this->Carro_compra_model->obtener_lista_carro_compras_todo();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_carro_compra'] = $dataCarroCompra;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/carro_compra/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}	
	}

	public function venta($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/venta/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCarroCompra = $this->Venta_model->get_carro_compra($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCarroCompra){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los 
								//datos del array del artículo
								$arrayClienteCarro = $this->Cliente_model->get_cliente($arrayCarroCompra['id_cliente']);

								$dataPagina['arrayCarroCompra']=$arrayCarroCompra;
								$dataPagina['arrayUser'] = $arrayClienteCarro;
								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/ventas/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				case 'pending':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCarroCompra = $this->Venta_model->get_carro_compra($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCarroCompra){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayCarroCompra']=$arrayCarroCompra;

								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/ventas/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataVentas = $this->Venta_model->obtener_lista_ventas();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_ventas'] = $dataVentas;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/ventas/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}	
	}

	public function codigo_promocion($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/codigo_promocion/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayCodigoPromo = $this->Codigo_promocion_model->obtener_codigo_promocion($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayCodigoPromo){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayCodigoPromo'=>$arrayCodigoPromo);
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/codigo_promocion/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataCodigos = $this->Codigo_promocion_model->obtener_lista_codigos();
					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'dataCodigos'=>$dataCodigos);
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/codigo_promocion/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function banner_home($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     				$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						$dataProducto = $this->Producto_model->obtener_lista_productos_todo();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_productos'] = $dataProducto;
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/banner_home/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayBanner = $this->Banner_home_model->get_banner($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayBanner){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayBanner']=$arrayBanner;
								$dataProducto = $this->Producto_model->obtener_lista_productos_todo();
								//creo el array con datos de configuración para la vista
								$dataPagina['lista_productos'] = $dataProducto;
								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/banner_home/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataBanners = $this->Banner_home_model->obtener_lista_banners();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_banners'] = $dataBanners;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/banner_home/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function distribuidor($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     				$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
			switch ($vista) {
				case 'add':
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/distribuidor/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayDistribuidor = $this->Distribuidor_model->get_distribuidor($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayDistribuidor){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
								 //he encontrado el artículo
								 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
								$dataPagina['arrayDistribuidor']=$arrayDistribuidor;
								 //cargo la vista pasando los datos de configuacion
								$this->load->view('layout/admin/head',$dataPagina);
								$this->load->view('layout/admin/nav', $dataPagina);
								$this->load->view('layout/admin/header', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('admin/distribuidor/edit', $dataPagina);
								//en esta parte va el contenido personalizado
								$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataDistribuidores = $this->Distribuidor_model->obtener_lista_distribuidores();
					//creo el array con datos de configuración para la vista
					$dataPagina['lista_distribuidores'] = $dataDistribuidores;
					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/distribuidor/index',$dataPagina);

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function promocion_temporal($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
						$dataProducto = $this->Producto_model->obtener_lista_productos_todo();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_productos'] = $dataProducto;
						$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_categorias'] = $dataCategoria;
						//pido los ultimos artículos al modelo
					  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_marcas'] = $dataMarca;

						//pido los ultimos artículos al modelo
					  	$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_sub_categorias'] = $dataSubCategoria;
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/promocion_temporal/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayPromoTemp = $this->Promocion_temporal_model->obtener_promocion_temporal($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayPromoTemp){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayPromoTemp'=>$arrayPromoTemp);
							 $dataProducto = $this->Producto_model->obtener_lista_productos_todo();
							//creo el array con datos de configuración para la vista
							$dataPagina['lista_productos'] = $dataProducto;
							$dataCategoria = $this->Categoria_model->obtener_lista_categorias();
							//creo el array con datos de configuración para la vista
							$dataPagina['lista_categorias'] = $dataCategoria;
							//pido los ultimos artículos al modelo
						  	$dataMarca = $this->Marca_model->obtener_lista_marcas();
							//creo el array con datos de configuración para la vista
							$dataPagina['lista_marcas'] = $dataMarca;

							//pido los ultimos artículos al modelo
						  	$dataSubCategoria = $this->Sub_categoria_model->obtener_lista_sub_categorias();
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/promocion_temporal/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataPromos = $this->Promocion_temporal_model->obtener_lista_promocion_temporal();

					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'dataPromos'=>$dataPromos);

					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/promocion_temporal/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}

	public function oferta_especial($vista = null,$dataView=null){
		if(isset($_SESSION['admin_ecom_data']['logeado'])){
	     	switch ($vista) {
				case 'add':
						$dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter");
						$dataProducto = $this->Producto_model->obtener_lista_productos_todo();
						//creo el array con datos de configuración para la vista
						$dataPagina['lista_productos'] = $dataProducto;
						 //cargo la vista pasando los datos de configuacion
						$this->load->view('layout/admin/head',$dataPagina);
						$this->load->view('layout/admin/nav', $dataPagina);
						$this->load->view('layout/admin/header', $dataPagina);
						//en esta parte va el contenido personalizado
						$this->load->view('admin/oferta_especial/add', $dataPagina);

						//en esta parte va el contenido personalizado
						$this->load->view('layout/admin/footer', $dataPagina);

					break;
				case 'edit':
						if(!is_null($dataView)){
							//pido al modelo el artículo que se desea ver
							$arrayPromoTemp = $this->Oferta_especial_model->obtener_oferta_especial($dataView);

							//compruebo si he recibido un artículo
							if (!$arrayPromoTemp){
							 //no he recibido ningún artículo
							 //voy a lanzar un error 404 de página no encontrada
								echo "no existe";
							 show_404();
							}else{
							 //he encontrado el artículo
							 //muestro la vista de la página de mostrar un artículo pasando los datos del array del artículo
							 $dataPagina = array('titulo'=>"Puya Muebles",
		      						'nombre_app'=>"Prueba CODEIGNITER",
		      						'descripcion'=>"Layout hecho en codeigniter",
		      						'arrayOfertaEspecial'=>$arrayPromoTemp);
							 $dataProducto = $this->Producto_model->obtener_lista_productos_todo();
							//creo el array con datos de configuración para la vista
							$dataPagina['lista_productos'] = $dataProducto;
							 //cargo la vista pasando los datos de configuacion
							$this->load->view('layout/admin/head',$dataPagina);
							$this->load->view('layout/admin/nav', $dataPagina);
							$this->load->view('layout/admin/header', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('admin/oferta_especial/edit', $dataPagina);
							//en esta parte va el contenido personalizado
							$this->load->view('layout/admin/footer', $dataPagina);
							}
						}else{
							show_404();
						}
					break;
				default:
					//pido los ultimos artículos al modelo
				  	$dataOfertas = $this->Oferta_especial_model->obtener_lista_oferta_especial();

					//creo el array con datos de configuración para la vista
					$dataPagina = array('titulo'=>"Puya Muebles",
	      						'nombre_app'=>"Prueba CODEIGNITER",
	      						'descripcion'=>"Layout hecho en codeigniter",
	      						'dataOfertas'=>$dataOfertas);

					//cargo la vista pasando los datos de configuacion
					$this->load->view('layout/admin/head',$dataPagina);
					$this->load->view('layout/admin/nav', $dataPagina);
					$this->load->view('layout/admin/header', $dataPagina);
					//contiene la barra de la izquierda
					$this->load->view('layout/admin/content', $dataPagina);
					
					//en esta parte va el contenido personalizado
					$this->load->view('admin/oferta_especial/index');

					//en esta parte va el contenido personalizado

					$this->load->view('layout/admin/footer', $dataPagina);
					break;
			}
		}else{
			redirect('admin/index', 'refresh'); 
		}
	}
}
