<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//si no existe la función makeClickableLinks la creamos
if(!function_exists('makeClickableLinks'))
{   
	function makeClickableLinks($text)
	{
	        $text = html_entity_decode($text);
	        $text = preg_replace('/^\/\/|^(?!https?:)/','http://', $text);
	        return $text;
	}  


}

//si no existe la función makeClickableLinks la creamos
if(!function_exists('closetags'))
{   
	function closetags($html) {
	    preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
	    $openedtags = $result[1];
	    preg_match_all('#</([a-z]+)>#iU', $html, $result);
	    $closedtags = $result[1];
	    $len_opened = count($openedtags);
	    if (count($closedtags) == $len_opened) {
	        return $html;
	    }
	    $openedtags = array_reverse($openedtags);
	    for ($i=0; $i < $len_opened; $i++) {
	        if (!in_array($openedtags[$i], $closedtags)) {
	            $html .= '</'.$openedtags[$i].'>';
	        } else {
	            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
	        }
	    }
	    return $html;
	}
}
//end application/helpers/ayuda_helper.php
